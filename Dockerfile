#FROM jacobwgillespie/nodejs-ffmpeg
#FROM node:10.0.0
#FROM mhart/alpine-node:10.16
FROM 581411254354.dkr.ecr.us-east-1.amazonaws.com/node10.19.0:latest

#RUN apt-get update \
#      && apt install nodejs -y \
#      && apt install npm -y \
#      && apt install ffmpeg -y

RUN apt-get update \
    && apt-get install -y wget gnupg \
    && wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - \
    && sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list' \
    && apt-get update \
    && apt-get install -y google-chrome-unstable fonts-ipafont-gothic fonts-wqy-zenhei fonts-thai-tlwg fonts-kacst fonts-freefont-ttf \
      --no-install-recommends \
    && rm -rf /var/lib/apt/lists/*

RUN mkdir /home/app

COPY package.json /home/app/package.json

WORKDIR /home/app

RUN npm install

#RUN npm install ffmpeg

#RUN ffmpeg -v
#RUN npm i ffmpeg-nodejs

ADD . /home/app

EXPOSE  4000 3001

CMD npm run dev
