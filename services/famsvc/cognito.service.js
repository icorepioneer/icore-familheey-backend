/* eslint-disable indent */

"use strict";
//aws
const AmazonCognitoIdentity = require('amazon-cognito-identity-js');
global.fetch = require('node-fetch');
global.navigator = () => null;
var moment = require('moment');
var _async = require('async');
var bcrypt = require('bcrypt');
let saltRounds = 10;
/**
 * Aws cognito UserPool
 */
const poolData = {
  UserPoolId: process.env.UserPoolId, //user_pool_id
  CLIENT_SECRET: process.env.CLIENT_SECRET,
  ClientId: process.env.ClientId, //client_id
};
const userPool = new AmazonCognitoIdentity.CognitoUserPool(poolData)

//functions for generating password
const passwordGen = (length) => {
  let result = 'F-';
  let characters = 'ABCDEFGHIJKL!@#$%^&*(';
  let charactersLength = characters.length;
  for (let i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}
const AWS = require('aws-sdk');

module.exports = {
  name: "cognito",
  settings: {},
  actions: {
    addUser: {
      params: {
        country: 'string',
        phone: 'string'
      },
      async handler(ctx) {
        let { country, phone, password, from } = ctx.params;
        //Trim spaces start
        country = country.trim();

        //Temporarily fix start
        if(country == '+595') {
          country = '+592'; 
        }
        //Temporarily fix end

        phone = phone.trim();
        //Trim spaces end
        //Check & insert + to start of country code start
        let countryCheck = country.startsWith('+');
        if(!countryCheck) {
          country = '+' + country;
        }
        //Check & insert + to start of country code end
        let phone_number_fetched = country + phone;
        if (!password) { password = passwordGen(6) }
        password = bcrypt.hashSync(password, saltRounds);
        let userAttribute = [];
        userAttribute.push(new AmazonCognitoIdentity.CognitoUserAttribute({
          Name: 'phone_number',
          Value: phone_number_fetched
        }));
        return new Promise((resolve, reject) => {
          ctx.call('db.users.find', { query: { phone: phone_number_fetched } })
            .then((userDetails) => {
              if (userDetails && userDetails.length > 0) {
                delete userDetails[0].password;
                this.otherUsers(ctx, userDetails[0]);
              }

              if (userDetails.length > 0 && userDetails[0].is_verified == true) {
                if (from && from == 'web' && userDetails[0].auto_password == 0) {
                  resolve(userDetails[0]);
                  return;
                }
                this.checkOtp(ctx, userDetails, phone_number_fetched, function (err, res) {
                  if (res) {
                    resolve(res);
                  }
                  if (err) {
                    reject(err);
                  }
                });
              }
              if (userDetails.length > 0 && userDetails[0].is_verified == false) {
                ctx.call('cognito.resendOTP', { phone: phone_number_fetched }).then((res) => {
                  resolve(res)
                }).catch((err) => reject(err));
              }
              userDetails.length <= 0 && userPool.signUp(phone_number_fetched, password, userAttribute, null, (error, result) => {
                if (error) {
                  console.log("cognito error === > ", error);
                  reject(error.code)
                }
                let save_data = { ...ctx.params, phone: phone_number_fetched, password: password,family_notification_off:[] } //family_notification_off:[] added by anand to solve post comment issue for selected families 
                ctx.call('db.users.create', save_data)
                  .then((res) => {
                    let { id } = res;
                    ctx.call('db.subscriptions.create',{ user_id:id,subscribe_expiry:moment().add(30,'days') })
                    this.otherUsers(ctx, res);
                    resolve(res);
                    //Track user to user history start
                    let historyObj = {
                      user_id : parseInt(id),
                      login_ip : ctx.params.login_ip ? ctx.params.login_ip : '',
                      login_location : ctx.params.login_location ? ctx.params.login_location : '',
                      login_browser : ctx.params.login_browser ? ctx.params.login_browser : '',
                      login_country : ctx.params.login_country ? ctx.params.login_country : '',
                      login_type : ctx.params.login_type ? ctx.params.login_type : '',
                      login_device : ctx.params.login_device ? ctx.params.login_device : '',
                      app_version : ctx.params.app_version ? ctx.params.app_version : ''
                    }
                    ctx.call('db.user_history.create',historyObj)
                    .then(res2=>{
                        console.log("Successfully tracked user history");
                    }).catch(err=>{
                        console.log("Tracked user history has some error");
                    });
                    //Track user to user history end
                  })
                  .catch((err) => {
                    reject(err);
                  })
              })
            }).catch(err => reject(err))
        })
      }
    },
    getAccessToken: {
        params: {
          refresh_token: 'string',
          phone:'string'
        },
        handler(ctx) {
            let {refresh_token,phone} = ctx.params;
            return new Promise(async (resolve, reject) => {
                let userData = {
                  Username: phone,
                  Pool: userPool
                };
                let token = new AmazonCognitoIdentity.CognitoRefreshToken({ RefreshToken: refresh_token })
                let cognitoUser = new AmazonCognitoIdentity.CognitoUser(userData); 
                cognitoUser.refreshSession(token, (err, session) => { 
                    if (err) {
                        console.log(err);
                        reject(err);
                    } else {
                        let accessToken = session.getAccessToken().getJwtToken();
                        let idToken = session.getIdToken().getJwtToken();
                        let resultObj = {
                            accessToken : accessToken,
                            idToken : idToken 
                        }
                        resolve(resultObj);
                    }
                }); 
            })
        }
    },

    /**
    * login a user
    */
    loginUser: {
      /*add this => "global.navigator = () => null;" to top for working */
      params: {
        phone: 'string'
      },
      metrics: {
        params: true,
        meta: true,
      },
      handler(ctx) {
        let { phone, password, from } = ctx.params;
        return new Promise(async (resolve, reject) => {
          let _user = await ctx.broker.call('db.users.find', { query: { phone: phone }, fields: ['password'] })

          var validpwd = bcrypt.compareSync(password, _user[0].password);

          if (from && from == 'internal') {
            validpwd = (password == _user[0].password);
          }

          if (!validpwd) reject("username or password is incorrect..!")
          else ctx.params.password = _user[0].password;

          let authenticationDetails = new AmazonCognitoIdentity.AuthenticationDetails({
            Username: ctx.params.email ? ctx.params.email : ctx.params.phone,
            Password: ctx.params.password
          });
          let userData = {
            Username: ctx.params.email ? ctx.params.email : ctx.params.phone,
            Pool: userPool
          };
          let cognitoUser = new AmazonCognitoIdentity.CognitoUser(userData);

          cognitoUser.authenticateUser(authenticationDetails, {
            onSuccess: function (result) {
              let accessToken = result.getAccessToken().getJwtToken();
              let refreshToken = result.getRefreshToken().getToken();
              let search = {}
              if (ctx.params.phone) { search.phone = ctx.params.phone }
              else { search.email = ctx.params.email }
              ctx.call('db.users.find', { query: search })
                .then((res) => {
                  if (res && res.length > 0) {
                    delete res[0].password;
                    res[0].accessToken = accessToken;
                    res[0].refreshToken = refreshToken;
                    resolve(res[0]);
                  } else {
                    reject({ "error": "usernot found" })
                  }
                })
                .catch((err) => {
                  console.log(err)
                  reject(err);
                })
            },
            onFailure: function (err) {
              reject(err)
              console.log(err);
            },
          });
        })
      }
    },
    verifyOTP: {
      params: {
        phone: 'string',
        otp: "string"
      },
      handler(ctx){
        let { phone, otp } = ctx.params;
        return new Promise((resolve, reject) => {
          // This code set is for app store review process
          if (phone == '+919496838835' && otp == '050409') {
            ctx.call('db.users.find', { query: { phone: phone } })
              .then((res) => {
                ctx.call('cognito.loginUser', { phone: res[0].phone, password: res[0].password, from: 'internal' })
                  .then((loginres) => {
                    var data = loginres;
                    data['reLogin'] = true;
                    resolve(data);
                  }).catch((err) => {
                    console.log('errror', err);
                    reject(err);
                  });
              });
            return;
          }
          // This code set is for app store review process
          ctx.call('db.users.find', { query: { phone: phone, otp: otp } })
          .then((res) => {
            if (res && res.length > 0 && res[0].is_verified) {
              var otpCompareTime = moment.utc();
              var otpdbtime = res[0] ? moment(res[0].otp_time) : '';
              var difference = otpCompareTime.diff(otpdbtime, 'minutes');
              if (difference > 335) {
                console.log('OTP is timed out');
              } else {
                ctx.call('cognito.loginUser', { phone: res[0].phone, password: res[0].password, from: 'internal' })
                  .then((cognitores) => {
                    var data = cognitores;
                    data['reLogin'] = true;
                    resolve(data);
                  }).catch((err) => {
                    reject(err);
                  });
              }
            } else {
              let userData = {
                Username: phone,
                Pool: userPool
              };
              let cognitoUser = new AmazonCognitoIdentity.CognitoUser(userData);
              // return new Promise((resolve, reject) => {
              cognitoUser.confirmRegistration(otp, true, async (err, resp) => {
                if (err) reject(err.message);
                let userDetails = await ctx.call('db.users.find', { query: { phone: phone } })
                let timeStamp = new Date().toISOString();
                if (userDetails) {
                  resp && ctx.call('db.users.update', {
                    id: userDetails[0].id,
                    otp: otp,
                    //is_active: "true",
                    is_verified: "true",
                    otp_time: timeStamp
                  }).then(async(user_res) => {
                    //Login function start
                    await ctx.call('cognito.loginUser', { phone: user_res.phone, password: user_res.password, from: 'internal' })
                    .then((res2) => {
                      resolve(res2);
                    }).catch((err2) => {
                      reject(err2);
                    });
                    //Login function end
                    //resolve(res);
                  })
                  .catch((err3) => {
                    reject(err3);
                  });
                }
              });
            }
          })
        })
      }
    },
    resendOTP: {
      params: {
        phone: 'string',
      },
      metrics: {
        params: true,
        meta: true,
      },
      handler(ctx) {
        let { phone } = ctx.params;
        let userData = {
          Username: phone,
          Pool: userPool,
        };
        let cognitoUser = new AmazonCognitoIdentity.CognitoUser(userData);
        return new Promise((resolve, reject) => {
          ctx.call("db.users.find", { query: { phone: phone, is_verified: true } })
          .then(async (res) => {
            res.length > 0 ? (await ctx.broker.call("cognito.loginWithOtp", { phone: phone })
              .then(res2 => resolve(res2))
              .catch(err => reject(err))) : (
                cognitoUser.resendConfirmationCode(function (err, result) {
                  if (err) {
                  console.log('OTP Error resendOTP:'+err);
                    reject(err);
                  } else {
                    resolve(result);
                  }
                }))
          })
        })
      },
    },
    loginWithOtp: {
      params: {
        phone: 'string'
      },
      async handler(ctx) {
        let { phone } = ctx.params;
        let userDetails = await ctx.call('db.users.find', { query: { phone: phone } });
        return new Promise((resolve, reject) => {
          if (userDetails.length > 0) {
            this.checkOtp(ctx, userDetails, phone, function (err, res) {
              if (res) {
                resolve({ data: res, message: 'otp send' });
              }
              if (err) {
               console.log('OTP Error loginWithOtp:'+err);
                reject({ data: err, message: 'otp not send' });
              }
            })
          } else {
            reject({ data: '', message: 'user is not exist' });
          }
        })
      }
    },

    otherUsersList: {
      async handler(ctx) {
        this.otherUsers(ctx, ctx.params)
      }
    },

    /**
     * send the code to reset password
     * aws send an otp to registered phone number
     */
    forgotPassword: {
      params: {
        phone: "string"
      },

      handler(ctx) {
        let { phone } = ctx.params;
        return new Promise((resolve, reject) => {
          let userData = {
            Username: phone,
            Pool: userPool
          };
          let cognitoUser = new AmazonCognitoIdentity.CognitoUser(userData);
          ctx.call('db.users.find', { query: { phone: phone } })
            .then(user => {
              if (user && user.length > 0) {
                cognitoUser.forgotPassword({
                  onSuccess: function (result) {
                    resolve({ code: 200, message: "sms send" })
                  },
                  onFailure: function (err) {
                    reject(err)
                  }
                })
              }
              else {
                reject("User not exist")
              }
            })
        });
      }
    },

    /**
     * change the password of a user
     * new password with otp
     */
    changePassword: {
      params: {
        otp: "string",
        new_pwd: "string",
        confirm_pwd: "string",
        phone: "string"
      },
      handler(ctx) {
        let { phone, new_pwd, confirm_pwd, otp } = ctx.params;
        let userData = {
          Username: phone,
          Pool: userPool
        };
        let cognitoUser = new AmazonCognitoIdentity.CognitoUser(userData);
        return new Promise((resolve, reject) => {

          // if (new_pwd.length < 8) reject("Password length minimum 8")
          if (new_pwd != confirm_pwd) reject("Password Mismatch!")

          new_pwd = bcrypt.hashSync(new_pwd, saltRounds);

          cognitoUser.confirmPassword(otp, new_pwd, {
            onFailure(err) {
              reject(err);
            },
            onSuccess() {
              ctx.call('db.users.find', { query: { phone: phone }, fields: ["id"] })
                .then(user => {
                  return ctx.call('db.users.update', { id: user[0].id, password: new_pwd, auto_password:0 })
                })
                .then((doc) => {
                  resolve({ code: 200, message: "Password updated" })
                })
                .catch((err) => {
                })
            },
          })
        })
      }
    },

    /**
     * reset a user's password 
     * who is logined IN
     */
    resetPassword: {
      params: {
        old_pwd: 'string',
        new_pwd: 'string',
        phone: 'string'
      },
      handler(ctx) {
        let { new_pwd, phone } = ctx.params;
        return new Promise(async (resolve, reject) => {
          if (new_pwd.length < 8) reject("Password length minimum 8");

          var _user = await ctx.broker.call("db.users.find", { query: { phone: phone }, fields: ['id', 'password'] })
          let { id, password } = _user[0];
          let _new_pwd = bcrypt.hashSync(new_pwd, saltRounds);

          let authenticationDetails = new AmazonCognitoIdentity.AuthenticationDetails({
            Username: phone,
            Password: password
          });

          let userData = {
            Username: phone,
            Pool: userPool
          }

          let cognitoUser = new AmazonCognitoIdentity.CognitoUser(userData);

          cognitoUser.authenticateUser(authenticationDetails, {
            onSuccess: function (result) {
              let accessToken = result.getAccessToken().getJwtToken();
              let refreshToken = result.getRefreshToken().getToken();
              let data = { accessToken: accessToken, refreshToken: refreshToken,id:id }
              cognitoUser.changePassword(password, _new_pwd, async function (err, doc) {
                console.log('================== new passw qor', _new_pwd);
                await ctx.broker.call("db.users.update", { id: id, password: _new_pwd, auto_password: 0 })
                  .then((res) => {
                    console.log(res)
                  })
                  .catch(err4 => {
                    console.log(err4)
                  })
              })
              let response = { data: data, code: 200 }
              resolve(response)
            },
            onFailure: function (err) {
              reject(err)
              console.log("======== error ====")
              console.log(err);
              reject(err)
            },


            newPasswordRequired: function (userAttributes, requiredAttributes) {
              // User was signed up by an admin and must provide new
              // password and required attributes, if any, to complete
              // authentication.

              // the api doesn't accept this field back
              delete userAttributes.email_verified;
              delete userAttributes.phone_number_verified;

              // Get these details and call
              cognitoUser.completeNewPasswordChallenge(_new_pwd, userAttributes, this);
              ctx.broker.call("db.users.update", { id: id, password: _new_pwd, auto_password: 0 })
                .then((res) => {
                  console.log(res)
                })
                .catch(err => {
                  console.log(err)
                })
            }


          });
        })
      }
    },

    verifyAuthToken:{
      params:{
        token:"string"
      },
      handler(ctx){
        var cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();
        let params = {
          AccessToken: ctx.params.token /* required */
        };
        return new Promise((resolve, reject) => {
          cognitoidentityserviceprovider.getUser(params, function(err, data) {
            if (err) reject(err); // an error occurred
            else     resolve(data);           // successful response
          });
        })
      }
    },

    /**
     * this functionality only use when aws account cognito migration
     * while cognito migrate phone no & password isuue will occur
     * so from DB aws admin create user and set password from db
     */
    adminAddUserManually: {
      params: {},
      handler(ctx) {
        return new Promise((resolve, reject) => {
          var COGNITO_CLIENT = new AWS.CognitoIdentityServiceProvider();
          let tmp_pwd = passwordGen(6); //temporary password while create a user
          let { limit, offset } = ctx.params;
          ctx.call("db.users.find", { fields: ["id", "email", "password", "phone"], limit: limit, offset: offset, sort: "id" })
            .then((users) => {
              console.log("-== = users= = = = ", users, users.length);
              if(users.length == 0){ //all users complete
                resolve(" all success "); return;
              }
              var i=0;
              users.forEach(elem => {
                i++;
                let getuser = {
                  UserPoolId: process.env.UserPoolId,
                  Username: elem.phone
                }

                //here get the details of current phone from aws cognito
                COGNITO_CLIENT.adminGetUser(getuser, (err, dataz) => {
                  console.log(elem.phone,"== err ===", err)
                  if (err) { //user not found then create
                    var user_arr = [
                      { Name: "phone_number", Value: elem.phone },
                      { Name: "phone_number_verified", Value: "true" }
                    ]
                    if (elem.email) {
                      user_arr.push({ Name: "email", Value: elem.email })
                      user_arr.push({ Name: "email_verified", Value: "true" })
                    }
                    var poolData2 = {
                      UserPoolId: process.env.UserPoolId,
                      Username: elem.phone,
                      TemporaryPassword: tmp_pwd,
                      MessageAction: "SUPPRESS", //this is important, will not send sms/email if not given sms&email send to user
                      UserAttributes: user_arr
                    };
                    //here create a user with temporary password
                    COGNITO_CLIENT.adminCreateUser(poolData2, (error, data) => {
                      console.log(elem.phone,"== errrorr = = = = ", error)
                      if (data) {
                        let newParams = {
                          Password: elem.password,
                          Permanent: true,
                          Username: elem.phone,
                          UserPoolId: process.env.UserPoolId,
                        }
                        //after create update the password from db to cognito
                        COGNITO_CLIENT.adminSetUserPassword(newParams, function (err5, data3) {
                          if (err5) console.log(elem.phone,err5, err5.stack); // an error occurred
                          else {
                            console.log("@@@@@@#@#@#@#@#@#@##@#@#@@#@")
                            if(i== users.length ) resolve(` success next offset ${ limit + offset } `)
                          }
                        });
                      }
                    });
                    
                  }else{
                    console.log(" = = =  user already exist = = = = ")
                    reject( err )
                  }
                });
              });
            }).catch((err) => {
              console.log(" -== err ===  ", err)
            })
        });
      }
    }


  },
  events: {},
  methods: {

    createOtp() {
      var string = '0123456789';
      let OTP = '';
      // Find the length of string 
      var len = string.length;
      for (let i = 0; i < 6; i++) {
        OTP += string[Math.floor(Math.random() * len)];
      }
      return (OTP);
    },

    checkOtp(ctx, userDetails, phone, callback) {
      let otpDb = this.createOtp();
      let updateid = userDetails[0].id;
      ctx.call('db.users.update', { id: updateid, otp: otpDb, otp_time: new Date() })
        .then((res) => {
          console.log("update", res);
          ctx.call('notification.sendSms', {
            message: `Your otp to login is ${otpDb}`,
            PhoneNumber: phone,
            subject: "OTP TO LOGIN"
          })
          callback('', res)
        })
        .catch((err) => {
          callback(err)
        });
    },

    otherUsers(ctx, user_data) {
      let { phone, id } = user_data;
      _async.parallel({
        User: (callback) => {
          ctx.call("db.users.find", { query: { phone: phone } })
            .then(res => {
              callback(null, res[0]);
            }).catch(err => {
              console.log("error happened -------- familyMembers", err);
            })
        },
        events: (callback) => {
          ctx.call("db.other_users.find", { query: { phone: phone, is_active: true, type: 'events' } })
            .then((_user) => {
              return ctx.call("db.other_users.find", { query: { phone: phone, is_active: true, type: 'events' } })
            })
            .then(res => {
              callback(null, res);
            }).catch(err => {
              console.log("error happened -------- familyMembers", err);
            })
        },
        groups: (callback) => {
          ctx.call("db.other_users.find", { query: { phone: phone, is_active: true, type: 'group' } })
            .then(res => {
              callback(null, res);
            }).catch(err => {
              console.log("error happened -------- familyMembers", err);
            })
        },
        contribute_others: (callback) => {
          ctx.call("db.post_requests_item_contributions.find", { query: { phone_no: phone, contribute_user_id: 2 } })
            .then(res => {
              callback(null, res);
            }).catch(err => {
              console.log("error happened -------- familyMembers", err);
            })
        }
      }, function (err, result) {
        let { events, groups, User, contribute_others } = result;

        if (events && events.length > 0) {

          //check for other users db for which the loined user based data found
          events.forEach(elem => {
            let ev_invite = {
              event_id: elem.event_id.toString(),
              from_user: elem.from_user.toString(),
              type: "invitation",
              user_id: [id],
              view:'template1'
            }
            if (elem.rsvp) {
              ctx.call("events.respondToEvents", { event_id: elem.user_id, resp: rsvp })
            }
            ctx.call("events.eventShareOrinvite", ev_invite)
            ctx.broker.call("db.other_users.update", { id: elem.id, is_active: false })
          });
        }

        if (groups && groups.length > 0) {
          groups.forEach(async elem => {
            let _fromUser = await ctx.call("db.users.find", { query:{ id : elem.from_user }, fields:["id","propic","full_name"] })
            let _group   = await ctx.call("db.groups.find", { query:{ id : elem.group_id }, fields:["id","group_name","logo"] })
            if (elem.type_value == 'invite') {
              let _type = 'create';
              let _params = { user_id: User.id, group_id: elem.group_id, following: 'true' }
              //Add user to group
              let isUserExist = await ctx.call("db.group_map.find", { query: { user_id: User.id, group_id: elem.group_id }, fields:["id"] })
              if(isUserExist && isUserExist.length>0){
                _type = 'update';
                _params = { ..._params, id: isUserExist[0].id  }
              }
              ctx.call(`db.group_map.${_type}`,_params)
              .then(res => {
                console.log('User successfully added to family');

                //send notification top others
                ctx.broker.call("db.group_map.find",{query:{group_id:elem.group_id, is_blocked:false, is_removed:false}, fields:['user_id']})
                .then(gp_m => {
                    let newUsername = User.full_name ? User.full_name : 'a new member';
                    gp_m.forEach(async elmUser => {
                      let sendObj = {
                          from_id: _fromUser.id,
                          type: 'family',
                          propic: _fromUser.length > 0 ? _fromUser[0].propic : '',
                          message: `${_fromUser[0].full_name} has added ${newUsername} to ${_group[0].group_name}`,
                          category: 'add_to_group',
                          message_title: 'familheey',
                          type_id: parseInt(elem.group_id),
                          link_to:parseInt(elem.group_id),
                          to_id: parseInt(elmUser.user_id),
                          sub_type:''
                      }                    
                      if(elmUser.user_id != parseInt(User.id)){
                        ctx.broker.call("notification.sendAppNotification", sendObj);
                        // let _user_notify = await ctx.call("db.users.find",{ query:{id:_sendObj_n.to_id}, fields:["notification"] })
                        // if(_user_notify.length>0 && _user_notify[0].notification) _this.pushNotify(ctx,_sendObj_n)
                      }
                    });  
                }).catch(err6 => {
                  console.log("getting users from group has error", err6);
                })

              }).catch(err7 => {
                console.log("error happened to add family", err7);
              })

              // ctx.call("db.requests.create", { user_id: User.id, group_id: elem.group_id, type: 'request', status: 'pending', from_id: elem.from_user })
              //   .then(res => {
              //     let sendObj = {
              //       from_id: elem.from_user,
              //       type: 'user',
              //       propic: _fromUser.length > 0 ? _fromUser[0].propic : '',
              //       message: `<b>${_fromUser[0].full_name}</b>  has invited you to join  <b> ${_group[0].group_name} </b> `,
              //       category: 'invitation',
              //       message_title: 'familheey',
              //       type_id: parseInt(user_data.id),
              //       link_to:parseInt(user_data.id),
              //       to_id: parseInt(user_data.id),
              //       sub_type:'request'
              //   }
              //   ctx.broker.call("notification.sendAppNotification", sendObj)
              //     /*return ctx.call('groups.invitationStatusbyUser', {
              //       id: res.id.toString(),
              //       user_id: User.id.toString(),
              //       group_id: elem.group_id.toString(),
              //       status: "accepted",
              //       from_id:elem.from_user.toString()
              //     })*/
              //   })
              //   .then(res => console.log(res)).catch(err => console.log(err))


            }
            ctx.broker.call("db.other_users.update", { id: elem.id, is_active: false })
          });
        }

        if(contribute_others && contribute_others.length > 0) {
          let ids = contribute_others.map(item=>item.id);
          let params = {
            ids:ids,
            contribute_user_id:id
          }
          ctx.call('db.post_requests_item_contributions.bulkidUpdate', params)
        }
        return (result);
      })
    }

  },
  created() { },
  started() { },
  stopped() { }
}