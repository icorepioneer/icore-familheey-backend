"use strict";

const path = require("path");
const mkdir = require("mkdirp").sync;
let AWS = require("aws-sdk");
var FCM = require('fcm-node');
let firebase = require('firebase');
const moment = require('moment');
AWS.config.region = "us-east-1";
AWS.config.update({
    accessKeyId: process.env.AWS_ACCESSKEY,
    secretAccessKey: process.env.AWS_SECRETKEY,
});

//Temp config with artora ceditentials
// let AWS2 = require("aws-sdk");
// AWS2.config.region = "ap-southeast-1";
// AWS2.config.update({
//     accessKeyId: "AKIAR5H66HZQ4CS6MB77",
//     secretAccessKey: "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",
// });
//Temp config with artora ceditentials

let sns = new AWS.SNS();
let ses = new AWS.SES();
mkdir(path.join(__dirname, "../../public/uploads"));

const firebaseConfig = {
    apiKey: "AIzaSyBMcgkL2w85PWnbMnTKghiC2bR1kIETz8c",
    authDomain: "familheey-255407.firebaseapp.com",
    databaseURL: "https://familheey-255407.firebaseio.com",
    projectId: "familheey-255407",
    storageBucket: "familheey-255407.appspot.com",
    messagingSenderId: "964830520116",
    appId: "1:964830520116:web:ed1dcd08310550614fbb61",
    measurementId: "G-MRRLVZY8JS"
};

const accountSid = 'AC96282a973b156967bcd80a4246b98f3b';
const authToken = '5c5d8afbd93e7be817b4c9e7b923d5a3';
const client = require('twilio')(accountSid, authToken);
var Keys=[];  //array Keys is initialized by Sarath on 22/02/2021 for storing the key getting from firebase after sending app notification
var tempKeys=[]; //array tempKeys is initialized by Sarath on 02/03/2021 for storing the values in array Keys

var admin = require("firebase-admin");

var serviceAccount = require("./familheey-255407-firebase-adminsdk.json");

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://familheey-255407.firebaseio.com"
});

module.exports = {
    name: "notification",
    settings: {},
    actions: {
        /**
         * function for inapp notification
         */
        sendAppNotification: {
            params: {},
            handler(ctx) {
                tempKeys=[];  //array is cleared by Sarath on 02/03/2021
                return new Promise((resolve, reject) => {
                    ctx.params = { ...ctx.params, notify_type: 'app_notify' }
                    //for both notification somee condition need to check
                    //which are same condition check
                    if(ctx.params.type && ctx.params.type != null && ctx.params.type != '') {
                        this.commonNotificationMethod(ctx);
                    }
                })
            }
        },

        /**
         * function for push notification
         */
        sendPushNotification: {
            params:{},
            handler(ctx) {
                ctx.params = { ...ctx.params, notify_type: 'push_notify' }
                let {message} = ctx.params
                if(message && message != '') {
                    message = message.toString();
                    ctx.params.message = message.replace( /(<([^>]+)>)/ig, '');
                }
                //for both notification somee condition need to check
                //which are same condition check
                if(ctx.params.type && ctx.params.type != null && ctx.params.type != '') {
                    this.commonNotificationMethod(ctx);
                }
            }
        },
        sendSms: {
            params: {},
            handler(ctx) {
                return new Promise((resolve, reject) => {
                    let { message, PhoneNumber, subject } = ctx.params
                    let param = {
                        Message: message,
                        MessageStructure: "string",
                        PhoneNumber: PhoneNumber,
                        Subject: subject
                    };

                    sns.publish(param, function (err, data) {
                        if (err) {
                            reject(err);
                        }
                        else {
                            resolve(data);
                        }       // successful response
                    });

                });
            }
        },
        sendEmail: {
            params: {},
            handler(ctx) {
                let { ccAddress, toAddresses, message, subject } = ctx.params;
                return new Promise((resolve, reject) => {
                    let params = {
                        Destination: {
                            CcAddresses: ccAddress,
                            ToAddresses: toAddresses
                        },
                        Message: {
                            Body: {
                                Html: {
                                    Charset: "UTF-8",
                                    Data: message
                                },
                                Text: {
                                    Charset: "UTF-8",
                                    Data: message
                                }
                            },
                            Subject: {
                                Charset: "UTF-8",
                                Data: subject
                            }
                        },
                        Source: "noreply@familheey.com", /* required */
                        ReplyToAddresses: ["noreply@familheey.com"],
                    };
                    let sendPromise = ses.sendEmail(params).promise();
                    sendPromise.then(function (data) {
                        resolve(data);
                        return "Success";
                    }).catch(function (err) {
                        console.error("Email NOT Send --- :: ", err, err.stack);
                        reject(err);
                        return "Error";
                    });
                });
            }
        },
        
        /*createFirebaseChannel: {
            params: {
                user_id: "string"
            },
            handler: (ctx) => {

                try {
                    if (!firebase.apps.length) {
                        firebase.initializeApp(firebaseConfig);
                    }
                    firebase.database().ref(user_id + '_notification')
                } catch (e) {

                }

            }
        },*/
        sendTwilioSms: {
            params: {},
            handler(ctx) {
                return new Promise((resolve, reject) => {
                    let { message } = ctx.params
                    let param = {
                        body: message,
                        from: '+12565703343',
                        to: '+917034084766'
                    };
                    try {
                        client.messages
                            .create(param)
                            .then(message2 => resolve(message2.sid))
                            .done();
                    } catch (err) {
                        console.log('Send twilio sms Error', err);
                        reject(err);
                    }

                });
            }
        },
        clearNotification: {
            params: {},
            handler(ctx) {
                return new Promise((resolve, reject) => {
                    try { 
                        if (!firebase.apps.length) {
                            firebase.initializeApp(firebaseConfig);
                        }
                        let {user_id} = ctx.params;
                        let cancel_id = (process.env.ENV || 'dev') + '_' + user_id + '_notification';
                        firebase.database().ref(cancel_id).remove()
                        .then(message => resolve('deleted notifications from ' + cancel_id,message))

                        //Update user table with notification clear status start
                        let userUpdateData = {
                            id:user_id,
                            notification_auto_delete:false
                        }
                        ctx.call("db.users.update",userUpdateData);
                        //Update user table with notification clear status end
                    } catch (err) {
                        console.log('Clear Notification Error', err);
                        reject(err);
                    }   
                });                    
            }
        },
        /**
         * delete only one message from notifiaction
         */
        DeleteOneNotification:{
            params:{
                user_id:'string',
                notification_id:'string'
            },
            handler(ctx) {
                return new Promise((resolve, reject) => {
                    try { 
                        if (!firebase.apps.length) {
                            firebase.initializeApp(firebaseConfig);
                        }
                        let { user_id, notification_id } = ctx.params;
                        let cancel_id = (process.env.ENV || 'dev') + '_' + parseInt(user_id) + '_notification' + '/' + notification_id;
                        firebase.database().ref(cancel_id).remove()
                        .then(message => resolve('deleted notifications from ' + cancel_id,message))

                        //Update user table with notification clear status start
                        let userUpdateData = {
                            id:user_id,
                            notification_auto_delete:false
                        }
                        ctx.call("db.users.update",userUpdateData);
                        //Update user table with notification clear status end
                    } catch (err) {
                        console.log('Clear Notification Error', err);
                        reject(err);
                    }   
                });                    
            }
        },

        clearNotificationCroneJob:{
            params: {
                limit:'string',
                offset:'string'
            },
            handler(ctx) {
                return new Promise(async(resolve, reject) => {
                    let { limit, offset } = ctx.params;
                    limit  = limit  ? limit  : 30;
                    offset = offset ? offset : 0;
                    try { 
                        if (!firebase.apps.length) {
                            firebase.initializeApp(firebaseConfig);
                        }
                        //Delete all staging datas start
                        // ctx.broker.call('db.users.find', {fields:["id"]})
                        // .then(async(res) => {
                        //     let removedIds = res;
                        //     let cancel_id = '';
                        //     for(let i=0 ; i < removedIds.length ; i++){
                        //         cancel_id = 'staging_' + removedIds[i].id + '_notification';
                        //         firebase.database().ref(cancel_id).remove()
                        //         .then(message => console.log('deleted notifications from ' + cancel_id,message))
                        //     }
                        //     resolve('Success');
                        // })
                        //Delete all staging datas end

                        let users_are = await ctx.call("db.users.find",{ fields:['id'], sort:'id' , offset:offset, limit: limit })
                        let i=0;
                        users_are.forEach(u => {
                            let { id } = u;
                            let ref = firebase.database().ref(`${process.env.ENV}_${id}_notification`);
                            // let now = Date.now();
                            //     now = new Date(now);
                            let cutoff = moment().subtract(30, "days").utc().format();
                            let old = ref.orderByChild('create_time').endAt(cutoff);
                            old.once('value', function(snapshot) {
                                let updates = {}; 
                                snapshot.forEach(index => {
                                    updates[index.key] = null;
                                });
                                ref.update(updates).then((msg)=>{}).catch((err)=>console.log("== err ==",err))
                            });
                            i++;
                            if(i == users_are.length) resolve(` success next offset ${ limit + offset } `)
                        });
                        
                    } catch (err) {
                        console.log('Clear Notification Error', err);
                        reject(err);
                    }   
                });                    
            }   
        },

        /**
         * mark all notifications as read
         */
        readNotifications: {
            params: {
                user_id:'string'
            },
            handler(ctx) {
                return new Promise(async (resolve, reject) => {
                    try {
                        let {user_id} = ctx.params;
                        if (!firebase.apps.length) {
                            firebase.initializeApp(firebaseConfig);
                        }
                        let ref = firebase.database().ref(`${process.env.ENV}_${user_id}_notification`);
                        let old = ref.orderByChild('visible_status').equalTo("unread");
                        old.once('value', function(snapshot) {
                            let updates = {}; 
                            snapshot.forEach(index => {
                                updates[`${index.key}/visible_status`] = 'read';
                            });
                            ref.update(updates)
                            .then((msg)=>{
                                resolve("Successfully updated notifications");
                            })
                            .catch((err)=>console.log("== err ==",err))
                        });
                    } catch (err) {
                        console.log('Read Notification Error', err);
                        reject(err);
                    }
                });
            }   
        },

        /**
         * clear all notification absed on env
         * not based ona single user or a single child 
         */
        clearFullNotificationEnv: {
            params: {},
            handler(ctx) {
                return new Promise(async (resolve, reject) => {
                    try {
                        if (!firebase.apps.length) {
                            firebase.initializeApp(firebaseConfig);
                        }
                        //Delete all staging datas start
                        ctx.broker.call('db.users.find', { fields: ["id"],  sort: 'id' })
                        .then(async (res) => {
                            let removedIds = res;
                            for (let i = 0; i < removedIds.length; i++) {
                                firebase.database().ref(`${process.env.ENV}_${removedIds[i].id}_notification`).remove()
                                .then(message => {
                                    console.log(i, removedIds.length, 'deleted notifications from ' + `${process.env.ENV}_${removedIds[i].id}_notification`, message)
                                    if (i == removedIds.length - 1) resolve(`Success`);
                                }).catch((err) => {
                                    reject(err)
                                })
                            }
                        });
                    } catch (err) {
                        console.log('Clear Notification Error', err);
                        reject(err);
                    }
                });
            }
        },

        /**
         * test the push notification working
         */
        testPushNotification: {
            params: {
                type:"string",
                to_id:"string"
            },
            handler(ctx) {
                return this.pushNotify(ctx)
            }
        },
    },
    events: {},
    methods: {
        /**
         * common notification method for conditional check
         * and function call to send notifiactions
         */
        commonNotificationMethod(ctx){
            return new Promise((resolve, reject) => {
                let { to_id,type,type_id, message, sub_type, group_id } = ctx.params;
                let _this = this;
                //Check user notification setting star
                ctx.broker.call('db.users.find', { query:{ id:to_id }, fields:["notification","public_notification","conversation_notification","announcement_notification","family_notification_off","event_notification"]})
                .then(async(res) => {
                    ctx.params = { ...ctx.params, title:message }
                    if(res && res.length > 0) {
                        if(res[0].notification == true) {
                            //Notification previlage check start
                            switch (type) {
                                case 'announcement':
                                    ctx.broker.call('posts.get_notification_post_data', { post_id: type_id.toString() })
                                    .then((resType) => {
                                        let { post_attachment,post_type, ...posts_rem } = resType[0];
                                        ctx.params = { ...ctx.params, cover_image:'', ...posts_rem }
                                        post_attachment.map(item => {
                                            let ext = item.type.split("/")[0];
                                            if(ext == 'image') ctx.params = { ...ctx.params, cover_image:item.filename }
                                        });

                                        if(res[0].announcement_notification == true) {
                                            ctx.params = { ...ctx.params, title:resType[0].description, privacy_type:'private' }
                                            _this.notificationSend(ctx);
                                        } else {
                                            console.log("No previlage for send notification")   
                                        }
                                    })
                                    .catch((err)=>{
                                        console.log("== err= = == ", err)
                                        reject(err)
                                    });
                                break;

                                case 'add_users_to_topic':
                                case 'create_topic':
                                case 'topic':
                                if(res[0].conversation_notification == true) {
                                    let uid = ctx.params.from_id.toString();
                                    let userIs = await ctx.call("userops.viewProfile",{ user_id: uid,profile_id: uid });
                                    let { profile } = userIs;
                                    let new_not_data = {
                                        created_by_user:profile.full_name,
                                        created_by_propic: profile.propic,
                                        cover_image: '',
                                        description:''
                                    }
                                    ctx.params = { ...ctx.params, ...new_not_data }
                                    _this.notificationSend(ctx);
                                } else {
                                    console.log("No previlage for send notification")   
                                }
                                break;

                                case 'post':
                                //Check post is public
                                ctx.broker.call('posts.get_notification_post_data', { post_id: type_id })
                                .then((resType) => {
                                    if(resType && resType.length > 0) {
                                        let { post_attachment,post_type, ...posts_rem } = resType[0];
                                        ctx.params = { ...ctx.params, cover_image:'', ...posts_rem }
                                        post_attachment.map(item => {
                                            let ext = item.type.split("/")[0];
                                            if(ext == 'image') ctx.params = { ...ctx.params, cover_image:item.filename }
                                        });
                                        if(resType[0].post_type == "public") {
                                            if(res[0].public_notification == true) {
                                                ctx.params = { ...ctx.params, title:resType[0].description, privacy_type:post_type }
                                                _this.notificationSend(ctx);
                                            } else {
                                                console.log("No previlage for send notification")   
                                            }
                                        } else if(resType[0].post_type == "only_groups") { //if only group family cond
                                            let offGroupIds = res[0].family_notification_off;
                                            let checkArray = Array.isArray(offGroupIds);
                                            ctx.params = { ...ctx.params, privacy_type:'private' }
                                            if (checkArray) {
                                                if (offGroupIds.includes(parseInt(group_id))) {
                                                    console.log("No previlage for send notification")
                                                } else {
                                                    _this.notificationSend(ctx);
                                                }
                                            } 
                                        }
                                    } else {
                                        console.log("Getting post setting has some error")
                                    }
                                })
                                break;

                                case 'home': 
                                //Check post is public
                                (sub_type =='familyfeed') && ctx.broker.call('posts.get_notification_post_data', { post_id: type_id.toString() })
                                .then((resType) => {
                                    if(resType && resType.length > 0) {
                                        ctx.params = { ...ctx.params, cover_image:'' }
                                        let { post_attachment, post_type, ...posts_rem } = resType[0];
                                        ctx.params = { ...ctx.params, ...posts_rem }
                                        post_attachment.map(item => {
                                            let ext = item.type.split("/")[0];
                                            if(ext == 'image') ctx.params = { ...ctx.params, cover_image:item.filename }
                                        });
                                        if(resType[0].post_type == "public") {
                                            if(res[0].public_notification == true) {
                                                let pType = (ctx.params.category=='post_share') ? 'private' : post_type;
                                                ctx.params = { ...ctx.params, title:resType[0].description, privacy_type:pType, post_type:post_type }
                                                _this.notificationSend(ctx);
                                            } else {
                                                console.log("No previlage for send notification")   
                                            }
                                        } else if(resType[0].post_type == "only_groups") { //if only group family cond
                                            let offGroupIds2 = res[0].family_notification_off;
                                            let checkArray2 = Array.isArray(offGroupIds2);
                                            ctx.params = { ...ctx.params, privacy_type:'private', post_type:post_type }
                                            if (checkArray2) {
                                                if (offGroupIds2.includes(parseInt(group_id))) {
                                                    console.log("No previlage for send notification")
                                                } else {
                                                    _this.notificationSend(ctx); 
                                                }
                                            } 
                                        }
                                    } else {
                                        console.log("Getting post setting has some error")
                                    }
                                });

                                if(sub_type == 'requestFeed') {
                                    let { category } = ctx.params;
                                    let paramsis = { request_id: type_id.toString() }
                                    
                                    ctx.call("post_needs.get_notification_needs_data",paramsis)
                                    .then(async(doc)=>{
                                        let { type:T1, ...reqrem } = doc.length>0 && doc[0];
                                        ctx.params = { ...ctx.params, ...reqrem, privacy_type:T1, cover_image:'', description:'' }
                                        if( category == 'contribution_create'){
                                            let contributedUser = await ctx.call("db.users.find",{ query:{ id: ctx.params.from_id }, fields:["full_name", "propic", "location"] });
                                            if(contributedUser && contributedUser.length>0){
                                                ctx.params = { ...ctx.params,
                                                    created_by_user: contributedUser[0].full_name,
                                                    created_by_propic: contributedUser[0].propic, 
                                                    location:contributedUser[0].location 
                                                }
                                            }
                                        }
                                        console.log("=== ctx.params == ", ctx.params)
                                        _this.notificationSend(ctx);
                                    }).catch((err)=>{
                                        console.log("== err === ", err)
                                    });
                                }
                                break;

                                case 'family':
                                let grp_noti_data = await ctx.call("groups.get_notification_group_data",{ group_id: type_id.toString() });
                                ctx.params = { ...ctx.params, ...grp_noti_data[0] }
                                //Check family_notification_off families
                                let offGroupIds3 = res[0].family_notification_off;
                                let checkArray3 = Array.isArray(offGroupIds3);
                                if(checkArray3) {
                                    if (offGroupIds3.includes(type_id)) {
                                        console.log("No previlage for send notification") 
                                    } else {
                                        _this.notificationSend(ctx);   
                                    }
                                } else {
                                    _this.notificationSend(ctx);
                                }
                                break;

                                case 'event':
                                    if(res[0].event_notification == true) {
                                        let evnt_noti_data = await ctx.call("events.get_notification_event_data",{ event_id: type_id.toString() });
                                        ctx.params = { ...ctx.params, ...evnt_noti_data[0] }
                                        _this.notificationSend(ctx);
                                    } else {
                                        console.log("No previlage for send notification")   
                                    }
                                    break;
                                
                                case 'user':
                                    let usr_grp_noti_data = await ctx.call("groups.get_notification_group_data",{ group_id: ctx.params.group_id.toString() });
                                    ctx.params = { ...ctx.params, ...usr_grp_noti_data[0] }
                                    _this.notificationSend(ctx);
                                    break;

                                default:
                                    _this.notificationSend(ctx);
                                break;
                            }
                            //Notification previlage check end
                        } else {
                            console.log("No previlage for send notification")
                        }
                    }
                })
                .catch((err) => {
                    console.log("get user has Error", err);
                })
                //Check user notification setting end
            });
        },

        /**
         * send notification 
         *  INAPP/ PUSH based on variable `notify_type`
         */
        notificationSend(ctx) {
            return new Promise((resolve, reject) => { 
                let {
                    from_id,
                    type,
                    propic,
                    message,
                    category,
                    type_id,
                    link_to,
                    to_id,
                    sub_type,
                    r_id,
                    notify_type,
                    group_id
                } = ctx.params;
                switch (notify_type) {
                    case 'app_notify':
                        
                        if (!firebase.apps.length) { firebase.initializeApp(firebaseConfig); }
                        let sendObj = {
                            from_id: from_id ? parseInt(from_id) : '',
                            type: type ? type : '',
                            propic: propic ? propic : '',
                            message: message ? message.toString().replace(/(<([^>]+)>)/ig, '') : '',
                            category: category ? category : '',
                            message_title: 'familheey',
                            type_id: type_id ? parseInt(type_id) : '',
                            link_to: link_to ? parseInt(link_to) : '',
                            sub_type: sub_type ? sub_type : ''
                        }
                        if(group_id) {
                            sendObj = { 
                                ...sendObj, 
                                group_id: parseInt(group_id) 
                            }  
                        }
                        sendObj.create_time = moment.utc().format();
                        sendObj.visible_status = 'unread';

                        sendObj = { ...ctx.params, ...sendObj }
                        if (to_id) {
                            // firebase.database().ref(to_id + '_notification').push({ ...sendObj });
                            var fireBasePost = firebase.database().ref((process.env.ENV || 'dev') + '_' + to_id + '_notification').push({ ...sendObj });  //the result is stored in the variable fireBasePost by Sarath on 17/02/2021
                            Keys.push({notificationKey:fireBasePost.key,toId:to_id}) //sarath
                        } else {
                            resolve('User Id & Group Id is missing');
                            return;
                        }
                        resolve('Successfully send notification');
                        console.log(to_id);
                        console.log(Keys); //sarath
                        break;

                    case 'push_notify':
                        var serverKey = process.env.FCM_SERVER_KEY; //put your server key here
                        var fcm = new FCM(serverKey);
                        var _message = {};
                        //values in Keys is stored in tempKeys and Keys is cleared by Sarath on 02/03/2021
                        if(Keys.length>0){
                            tempKeys=Keys;
                            Keys=[]
                            }
                        //filter for get corresponding notification id using to_id by Sarath on 22/02/2021
                        let noti_key= tempKeys.filter(function(e){
                            return e.toId==to_id
                            });
                        // collapseid is the id which may be event_id,family_id or post_id also.
                        var collapseKey = (ctx.params.user_id && ctx.params.collapseid) ? ctx.params.user_id + ctx.params.collapseid : '';
                        if (ctx.params.device_type == "android") {
                            _message = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
                                to: ctx.params.token,
                                collapse_key: collapseKey,
                                tag: to_id,
                                renotify: false,
                                silent: true,
                                data: {
                                    type: ctx.params.type ? ctx.params.type : '',
                                    type_id: ctx.params.type_id ? parseInt(ctx.params.type_id) : '',
                                    sub_type: ctx.params.sub_type ? ctx.params.sub_type : '',
                                    title: ctx.params.title,
                                    body: ctx.params.body,
                                    notification_key: noti_key[0].notificationKey   //notificationKey is added in data as notification_key by Sarath on 22/02/2021 which is the key getting after sending app notification
                                }
                            };
                        } else {
                            _message = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
                                to: ctx.params.token,
                                collapse_key: collapseKey,
                                notification: {
                                    title: ctx.params.title,
                                    body: ctx.params.body,
                                    "thread-id": to_id,
                                    tag: to_id
                                    //sound: "default"
                                },
                                data: {
                                    type: ctx.params.type ? ctx.params.type : '',
                                    type_id: ctx.params.type_id ? parseInt(ctx.params.type_id) : '',
                                    sub_type: ctx.params.sub_type ? ctx.params.sub_type : '',
                                    notification_key: noti_key[0].notificationKey //notificationKey is added in data as notification_key by Sarath on 22/02/2021 which is the key getting after sending app notification
                                }
                            };
                        }
                        fcm.send(_message, function (err, response) {
                            if (err) {
                                ctx.call("db.device_token.remove", { id: r_id }) 
                                console.log("Toid:"+to_id+"-------------------Something went wrong in push notification!---------------------", err); 
                                 if (ctx.params.device_type == "android") {
                                console.log("Toid:"+to_id+":title:",_message.data.title );
                                console.log("Toid:"+to_id+":body:",_message.data.body); 
                                console.log("Toid:"+to_id+":type:",_message.data.type );
                                console.log("Toid:"+to_id+":type_id:",_message.data.type_id);
                                console.log("Toid:"+to_id+":sub_type:",_message.data.sub_type);
                                console.log("Toid:"+to_id+":notification_key:",_message.data.notification_key);                                       
                                }
                                else{
                                     console.log("Toid:"+to_id+":title:",_message.notification.title );
                                     console.log("Toid:"+to_id+":body:",_message.notification.body);   
                                     console.log("Toid:"+to_id+":type:",_message.data.type );
                                     console.log("Toid:"+to_id+":type_id:",_message.data.type_id);
                                     console.log("Toid:"+to_id+":sub_type:",_message.data.sub_type);
                                     console.log("Toid:"+to_id+":notification_key:",_message.data.notification_key);
                                }
                                reject(err);
                            } else {
                                console.log(to_id); //sarath
                                console.log("Successfully sent push notification with response: ", response);
                                resolve(response);
                            }
                        });
                        break
                    default:
                    break;
                }
            })
        },
        pushNotify(ctx) {
            let { to_id } = ctx.params;
            ctx.broker.call("userops.getregisterToken", { user_id: to_id, is_active: true })
            .then((_devices) => {
                let { data } = _devices;
                data && data.forEach(elem => {
                    ctx.params = {
                        ...ctx.params,
                        token: elem.device_token,
                        device_type: elem.device_type.toLowerCase(),
                        title: ctx.params.message_title ? ctx.params.message_title : '',
                        body: ctx.params.message ? this.removeTags(ctx.params.message) : '',
                        sub_type: ctx.params.sub_type ? ctx.params.sub_type : ''
                    }
                    return ctx.call("notification.sendPushNotification", ctx.params);
                })
            })
            .catch(e => e)
        }
    },
    created() { },
    started() { },
    stopped() { }
};