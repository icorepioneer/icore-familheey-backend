"use strict";
const moment = require('moment')
const _async = require('async')

module.exports = {
    name: "userops",
    settings: {},
    actions: {
        updateProfile: {
            params: {
                "id": "string"
            },
            handler(ctx) {
                return new Promise((resolve, reject) => {
                    if (ctx.meta && ctx.meta.file) {
                        ctx.meta.file.forEach(element => {
                            ctx.params[element.fieldname] = element.filename;
                            this.calls3(ctx, element)
                        });
                    }
                    if(ctx.params.phone){ delete ctx.params.phone }
                    ctx.call('db.users.update', ctx.params)
                        .then((res) => {
                            delete res.password;
                            // let redisArr = ['userops.viewProfile:user_id|' + ctx.params.id + '|*',
                            // 'userops.userProfileDetails:login_id|' + ctx.params.id + '*',
                            // 'db.users.find:*|id|' + ctx.params.id];
                            // ctx.broker.call('redis.deleteKeys', redisArr);

                            resolve(res);
                            // Track user activities start
                            let trackerData = {
                                user_id: parseInt(res.id),
                                type: 'User',
                                type_id: parseInt(res.id),
                                activity: 'updated user profile'
                            };
                            ctx.broker.call('user_activities.trackUserActivities', trackerData)
                            .then((trackres) => {
                                console.log("Successfully track data");
                            })
                            .catch((err) => {
                                console.log(err);
                            })
                            // Track user activities end
                        })
                        .catch((err) => {
                            reject(err);
                        })
                })
            }
        },
        /**
         * get the profile detals by userid
         */
        viewProfile: {
            params: {
                user_id: "string",
                profile_id: "string",
            },
            async handler(ctx) {
                //let cache_key = `MOL-familheey-`;
                return new Promise((resolve, reject) => {
                    let { profile_id, user_id } = ctx.params;
                    _async.parallel({
                        profile: function (callback) { //profile details of a user
                            ctx.call("db.users.getByid", { id: profile_id })
                                .then((res) => {
                                    var _profile = {};
                                    if (res && res.length > 0) {
                                        _profile = res[0];
                                        delete _profile.password;
                                    }
                                    callback(null, _profile);
                                })
                                .catch((err) => {
                                    console.log("profile Search Error", err);
                                })
                        },
                        /*groups: function (callback) { //groups details of a user
                            ctx.call("db.group_map.findGroupOfaUser", { user_id: profile_id })
                                .then((_groups) => {
                                    callback(null, _groups);
                                })
                                .catch((err) => {
                                    console.log("group Search Error", err);
                                })
                        },*/
                        count: function (callback) {
                            ctx.broker.call("userops.userProfileDetails", { login_id: user_id, profile_id: profile_id }).then(res => {
                                callback(null, res.data)
                            }).catch(err => reject(err))
                        }
                    }, function (err, results) {
                        if (err) {
                            console.log(' failed ==> ', err);
                            reject(err);
                        } else {
                            resolve(results)
                        }
                    });
                })
            }
        },
        /**
         * staus creation to a group by a user
         */
        createStatus: {
            params: {},
            handler(ctx) {
                ctx.params.expire_time = moment().add(24, 'hours').unix(); //set to 24hours from now
                if (ctx.meta && ctx.meta.file) {
                    ctx.meta.file.forEach(element => {
                        ctx.params.file_type = element.mimetype; //file mimetype
                        ctx.params[element.fieldname] = element.filename;
                        this.calls3(ctx, element);
                    });
                }
                return new Promise((resolve, reject) => {
                    var response = {}
                    ctx.call('db.status.create', ctx.params)
                        .then((doc) => {
                            response.data = [doc];
                            resolve(response);
                        })
                        .catch((err) => {
                            response.err = err;
                            reject(response);
                        })
                })
            }
        },
        /**
         * fetch status based on user/group
         */
        fetchStatus: {
            params: {},
            handler(ctx) {
                return new Promise((resolve, reject) => {
                    var response = {}
                    ctx.broker.call('db.status.fetchStatus', ctx.params)
                        .then((doc) => {
                            response.data = doc;
                            resolve(response);
                        })
                        .catch((err) => {
                            response.err = err;
                            reject(response);
                        })
                })
            }
        },

        updateStatus: {
            params: {},
            handler(ctx) {
                return new Promise((resolve, reject) => {
                    var response = {}
                    let { viewed_by } = ctx.params;
                    if (viewed_by) { delete ctx.params.viewed_by }

                    ctx.call('db.status.update', ctx.params)
                        .then(async (doc) => {
                            if (viewed_by) {
                                ctx.params.viewed_by = viewed_by;
                                let returndata = await ctx.broker.call('db.status.updateStatus', ctx.params)
                                return returndata;
                            }
                        })
                        .then((doc) => {
                            response.data = doc;
                            resolve(response);
                        })
                        .catch((err) => {
                            response.err = err;
                            reject(response);
                        })
                })
            }
        },
        userProfileDetails: {
            params: {
                login_id: "string",
                profile_id: "string"
            },
            handler: (ctx) => {
                let response = {};
                let { login_id, profile_id } = ctx.params;
                let flag = profile_id == login_id;
                return new Promise((resolve, reject) => {
                    flag ? (
                        _async.parallel({
                            familyCount: (callback) => {
                                ctx.call("db.group_map.getFamilyCount", { user_id: login_id }).then((res) => {
                                    callback(null, res);
                                }).catch(err => console.log("error happened ------ familyCount", err))
                            },
                            connections: (callback) => {
                                ctx.call("db.group_map.getConnection", { user_id: login_id }).then(res => {
                                    callback(null, res);
                                }).catch(err => {
                                    console.log("error happened ------ connections", err)
                                })
                            }
                        }, function (err, res) {
                            if(err) {
                                console.log("error happened"); 
                                reject(err);    
                            } else {
                                console.log(res);
                                response.data = { connections: res.connections.length, familyCount: parseInt(res.familyCount[0].count) };
                                resolve(response);
                            }
                        })
                    ) : (
                            _async.parallel({
                                familyCount: (callback) => {
                                    ctx.call("db.group_map.getFamilyCount", { user_id: profile_id }).then((res) => {
                                        callback(null, res);
                                    }).catch(err => console.log("error happened ------ familyCount", err))
                                },
                                connections: (callback) => {
                                    ctx.call("db.group_map.getConnection", { user_id: profile_id }).then(res => {
                                        callback(null, res);
                                    }).catch(err => {
                                        console.log("error happened ------ connections", err)
                                    })
                                },
                                mutual_connections: (callback) => {
                                    ctx.call("db.group_map.getMutualConnections", { user_one_id: login_id, user_two_id: profile_id }).then(res => {
                                        callback(null, res);
                                    }).catch(err => {
                                        console.log("error happened ------ mutual_connections", res)
                                    })
                                },
                                mutual_families: (callback) => {
                                    ctx.call("db.group_map.getMutualFamilies", { user_id_one: login_id, user_id_two: profile_id }).then(res => {
                                        callback(null, res)
                                    }).catch(err => console.log("error happened ----- mutual_families", res))
                                }
                            }, function (err, res) {
                                if(err) {
                                    console.log("error happened");
                                    reject(err);   
                                } else {
                                    response.data = {
                                        connections: res.connections.length,
                                        familyCount: parseInt(res.familyCount[0].count),
                                        mutual_families: parseInt(res.mutual_families.length),
                                        mutual_connections: parseInt(res.mutual_connections.length)
                                    };
                                    resolve(response)   
                                }
                            })
                        )
                })
            }
        },
        getConnections: {
            params: {
                user_id: "string"
            },
            handler: (ctx) => {
                let { user_id } = ctx.params;
                let response = {};
                return new Promise((resolve, reject) => {
                    ctx.call("db.group_map.getConnection", { user_id: user_id }).then(res => {
                        response.data = res;
                        resolve(res);
                    }).catch(err => {
                        response.err = err;
                        reject(response);
                    })
                })
                //return ctx.params
            }
        },
        /**
         * get list of blocked users events/groups/post
         */
        blockedUsers: {
            params: {
                user_id: "string",
                type: "string"
            },
            handler(ctx) {
                let { user_id, type, group_id} = ctx.params;
                let response = {};
                return new Promise((resolve, reject) => {
                    switch (type) {
                        case 'groups':
                            let _search = { user_id: user_id, group_id: group_id }
                            ctx.broker.call("db.group_map.find", { query: _search, fields: ['type'] })
                                .then((isAdmin) => {
                                    if (isAdmin.length > 0 && isAdmin[0].type == 'admin')
                                        return ctx.call("db.group_map.getBlockedUsersGroup", ctx.params)
                                    else reject("NOT AUTHORISED")
                                })
                                .then((users) => {
                                    response.data = users;
                                    resolve(response)
                                })
                                .catch((err) => {
                                    console.log(err)
                                    reject(err)

                                })
                            break;
                        case 'events':
                            break

                        default:
                            break;
                    }
                })
            }
        },
        registerToken: {
            params: {
                device_token: 'string',
                user_id: 'string'
            },
            handler(ctx) {
                let { user_id, device_id } = ctx.params;
                return new Promise(async(resolve, reject) => {
                    let isTokenExist = [];
                    isTokenExist = await ctx.call("db.device_token.find",{ query:{device_id:device_id, user_id:user_id }, fields:["id"]})
                    let _type = 'create';
                    if(isTokenExist&&isTokenExist.length>0){
                        ctx.params.id = isTokenExist[0].id;
                        _type = 'update';
                    }
                    return ctx.call(`db.device_token.${_type}`, ctx.params)
                    .then(result => {
                        resolve({ message: 'success', data: result });
                    })
                    .catch(e=>reject(e))
                });
            }
        },
        /**
         * get the token devce
         */
        getregisterToken: {
            handler(ctx) {
                return new Promise((resolve, reject) => {
                    ctx.call('db.device_token.getTokens', ctx.params)
                    .then(result => {
                        resolve({ message: 'success', data: result });
                    })
                });
            }
        },
        /**
         * get all group members of my group
         */
        mygroupMember: {
            params: {
                user_id: "string"
            },
            handler(ctx) {
                return new Promise((resolve, reject) => {
                    ctx.call("db.group_map.mygroupMember", ctx.params)
                        .then(result => {
                            resolve({ code: 200, message: 'success', data: result });
                        })
                });
            }
        },
        /**
         * user details on boarding details
         */
        onBoardCheck: {
            params: {
                user_id: "string"
            },
            handler(ctx) {
                let { user_id } = ctx.params;
                let response = {}
                return new Promise((resolve, reject) => {
                    _async.parallel({
                        user: (callback) => {
                            ctx.call("db.users.find", { query: { id: user_id }, fields: ["id", "email", "full_name", "gender", "dob", "origin", "location","propic","phone","createdAt","is_active","is_verified","user_is_blocked","notification","notification_auto_delete"] })
                            .then(res => {
                                callback(null, res);
                            }).catch(err => {
                                console.log("error happened -------- users", err);
                            })
                        },
                        family_count: (callback) => {
                            ctx.call("db.group_map.getFamilyCount", { user_id: user_id })
                            .then(res => {
                                callback(null, res);
                            }).catch(err => {
                                console.log("error happened -------- group_map", err);
                            })
                        },
                        versions: (callback) => {
                            ctx.call("db.versions.find_v",{})
                            .then(res => {
                                callback(null, res);
                            }).catch(err => {
                                console.log("error happened -------- versions", err);
                            })
                        }
                    }, async function (err, results) {
                        if(err) reject(err)
                        let { versions,family_count,user } = results;
                        if(user.length == 0){
                            reject("user not found")
                        }else{
                            let subscriptions = await ctx.call("db.subscriptions.findByUser", { user_id: user[0].id });
                            response = { ...response, subscriptions:subscriptions}
                        }
                        versions.forEach(v => {
                            if(v.type == 'android')
                                response = { ...response,android_version:v.version,android_force:v.force_update }
                            if(v.type == 'ios')
                                response = { ...response,ios_version:v.version,ios_force:v.force_update }
                        });
                        response.family_count = (family_count && family_count.length>0) ? parseInt(family_count[0].count):0;
                        if(user&&user.length>0) response = { ...response, ...user[0]}
                        resolve(response)

                        if(user.length > 0) {
                            //Track user to user history start
                            let historyObj = {
                            user_id : parseInt(user[0].id),
                            login_ip : ctx.params.login_ip ? ctx.params.login_ip : '',
                            login_location : ctx.params.login_location ? ctx.params.login_location : '',
                            login_browser : ctx.params.login_browser ? ctx.params.login_browser : '',
                            login_country : ctx.params.login_country ? ctx.params.login_country : '',
                            login_type : ctx.params.login_type ? ctx.params.login_type : '',
                            login_device : ctx.params.login_device ? ctx.params.login_device : '',
                            app_version : ctx.params.app_version ? ctx.params.app_version : ''
                            }
                            ctx.call('db.user_history.create',historyObj)
                            .then(res=>{
                                console.log("Successfully tracked user history");
                            }).catch(err2=>{
                                console.log("Tracked user history has some error");
                            });
                            //Track user to user history end
                        }
                    })
                });
            }
        },

        birthday_wish: {
            handler(ctx) {
                return new Promise((resolve, reject) => {
                    let day = moment().get('day');
                    let month = moment().get('month');
                    ctx.call('db.users.birthday_wish', { day: day, month: month })
                    .then(users => {
                        users.map(u => {
                            let sendObj = {
                                from_id: u.id,
                                type: 'birthday',
                                propic: u.propic,
                                message: 'Happy Birthday !!',
                                category: 'birthday',
                                message_title: 'familheey',
                                type_id: parseInt(u.id),
                                link_to: u.id,
                                to_id: u.id
                            }
                            ctx.broker.call("notification.sendAppNotification", sendObj)
                        });
                        resolve({ message: 'success' });
                    })
                })
            }
        },
        /**
         * list the pending requests
         */
        pendingRequest: {
            params: {
                type:"string"
            },
            handler(ctx) {
                ctx.params = { ...ctx.params, status:'pending' }
                return new Promise(async(resolve, reject) => {
                    let { admin_id, group_id } = ctx.params;

                    if(admin_id && group_id){
                        let isAdminc = await ctx.call("db.group_map.count", { query: { group_id: group_id, user_id: admin_id }, fields:['type'] })
                        if(isAdminc ==0){
                            reject("Not Authorised");
                            return;
                        }
                    }
                    ctx.call("db.requests.ListRequest", ctx.params)
                    .then((doc) => {
                        resolve({ code:200, data:doc, count:doc.length })
                    })
                    .catch(err => reject(err));
                })
            }
        },

        /**
         * delete requests
         */
        deletePendingRequest: {
            params: {
                id:"string"
            },
            handler(ctx) {
                return new Promise((resolve, reject) => {
                    ctx.call("db.requests.remove", ctx.params)
                    .then((doc) => {
                        resolve({ code:200, data:doc })
                    })
                    .catch(err => reject(err));
                })
            }
        },

        updateFileuploads:{
            handler(ctx) {
                return new Promise((resolve, reject) => {
                    ctx.call("db.documents.find",{ query:{original_name:null}, fields:["id","url"] })
                    .then((doc) => {

                        
                        doc.forEach(e => {
                            let _ext = e.url.substr(e.url.lastIndexOf('/') + 1);
                            let up_data = {
                                id:e.id,
                                original_name:_ext
                            }


                            ctx.call("db.documents.update",up_data)


                        });

                        resolve({ code:200, data:doc })
                    })
                    .catch(err => reject(err));
                })
            }
        },
        /**
         * group exist and inapp exist check on bulk contact select
         */
        PhoneContactStatus: {
            params: {
                group_id:"string"
            },
            handler(ctx) {
                let { contacts, group_id } = ctx.params;
                return new Promise((resolve, reject) => {
                    let i = 1;
                    try{
                        contacts.forEach(async (doc) => {
                            doc.isExistInGroup = false;
                            doc.isExistinApp = false;
                            doc.isReqExist = 'not-send';
                            let rowphone = doc.phone.replace(/[^\d\+]/g, "");
                            let realPhone = '%' + rowphone.substr(rowphone.length - 10) + '%';
                            let _users = await ctx.call("db.groups.userPhoneMulticheck", { multiPhone: realPhone });
                            doc.isExistinApp = (_users.length>0) ? true : false;
                            if(_users.length>0){
                                let { id } = _users[0];
                                doc.user_id = id;
                                let gp_count = await ctx.broker.call("db.group_map.count",{ query:{ group_id:group_id, is_blocked:false, is_removed:false, user_id:id } });
                                doc.isExistInGroup = (gp_count >0) ? true : false;
                                let reqExist = await ctx.call("db.requests.find", { query:{ user_id: id, group_id: group_id, type:'invite' }, fields:['status'], limit:1 })
                                if(reqExist.length>0 ) doc.isReqExist = reqExist[0].status;
                            }else{
                                let reqExist = await ctx.call("db.other_users.otherReqStatus", { queryString: realPhone, is_active: true, group_id: group_id })
                                doc.isReqExist = (reqExist.length>0 && parseInt(reqExist[0].count) >0) ? 'pending' : 'not-send' 
                            }
                            ctx.params.contacts = contacts;
                            if(i == contacts.length) resolve(ctx.params);
                            i++;
                        });
                    } catch(e){
                        reject(e)
                    }
                });
            }
        },

        Custom:{
            params:{},
            handler(ctx){
                return new Promise((resolve, reject) => {
                    let { token } = ctx.params;
                    let v_token = "aMwU3rE6qHdTPHrMMGcmrTl5QfTSN4hlWIU78202hnuAhSct6eaXNY37rDYCpWLguwg4axvbAVDLkBMjElulJ8KTmv5kmWThlx8bu0iDIJU89FEkozPI7Hn7qDDbnJSy"
                    if(token!=v_token) reject('Unauthorized')
                    else{
                        ctx.call("db.users.Custom", ctx.params)
                        .then((doc) => {
                            resolve({ code:200 })
                        })
                        .catch(err => reject(err));
                    }
                })
            }
        },

        isUserExist:{
            params:{},
            handler(ctx){
                console.log('New Build 02 feb 2021');
                return new Promise((resolve, reject) => {
                    ctx.call("db.users.isUserExist", ctx.params)
                    .then((doc) => {
                        let response = {
                            isUserExist:false,
                            data:[]
                        }
                        if(doc.length>0){
                            delete doc[0].password
                            response.isUserExist = true
                            response.data = doc
                        }
                        resolve(response)
                    })
                    .catch(err => reject(err));
                })
            }
        },

        createSubscription:{
            params:{
                user_id:"string"
            },
            handler(ctx){
                return new Promise((resolve, reject) => {
                    ctx.params = { ...ctx.params, subscribe_expiry:moment().add(30,'days') }
                    ctx.call("db.subscriptions.create", ctx.params)
                    .then((doc) => {
                        resolve(doc)
                    })
                    .catch(e=>reject(e))
                })
            }
        },
        get_notification_settings:{
            params:{
                user_id:"string"
            },
            handler(ctx){
                return new Promise((resolve, reject) => {
                    let { user_id } = ctx.params;
                    ctx.call("db.users.find", {query:{id:user_id}, fields:['notification','public_notification','conversation_notification','announcement_notification','family_notification_off','event_notification']})
                    .then(async(res) => {
                        let family_count = await ctx.call('db.groups.notification_settings_familyCount', { user_id: user_id })
                        res[0] = { ...res[0], family_count:family_count[0].count }
                        resolve(res)
                    })
                    .catch(e=>reject(e))
                })
            }    
        },

        fulluserCount:{
            params:{},
            handler(ctx){
                return new Promise((resolve, reject) => {
                    ctx.call("db.users.count")
                    .then((doc) => {
                        resolve({ code:200, message:'user count success', data:{ count:doc } })
                    })
                    .catch(err => reject(err));
                })
            }
        }
    },
    events: {},
    methods: {
        /**
         * call s3 service for fiel upload
         */
        calls3(ctx, params) {
            ctx.call('s3upload.fileUpload', params)
            .then((res) => {
                console.log("== s3uplod success ==", res);
            })
            .catch((err) => {
                console.log(err)
            })
        }
    },
    created() { },
    started() { },
    stopped() { }
}