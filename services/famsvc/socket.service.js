"uses strict";
const server = require('http').createServer();
const io = require('socket.io')(server);
let client_i;
io.on('connection', client => {
    client_i = client;

    client.on('disconnect', () => {
        console.log('disconnect');
    });
    client.on('connect', () => {
        console.log('connect');
    });
});
server.listen(3001);

module.exports = {
    name: "socket_service",
    settings: {},
    actions: {
        sendMessage: {
            handler: (ctx) => {
                // console.log(io);
                // console.log(ctx.params);
                io.emit(ctx.params.topic, ctx.params.data);
                // io.emit('broadcast', ctx.params.data);
            }
        }
    },
    events: {},
    methods: {},
    created() {

    },
    started() { },
    stopped() { }
};