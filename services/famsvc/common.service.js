"use strict"
const moment = require('moment');
const async = require('async');
const getUrls = require('get-urls');
const request = require('request');
const urlMetadata = require('url-metadata');
const scrape = require('html-metadata');
const CryptoJS = require("crypto-js");
const key = process.env.SECRET_KEY;

const linkPreviewGenerator = require("link-preview-generator");
const chrono = require('chrono-node');

module.exports={
    name:"common_methods",
    actions:{
        date_filter:{
            params:{
                filter:"string"
            },
            handler:(ctx)=>{
                let {filter} = ctx.params;
                let start_date,end_date;
                return new Promise((resolve,reject)=>{
                    switch(filter){
                        case 'today':
                                start_date = moment().startOf('day').unix();
                                end_date = moment().endOf('day').unix();
                                resolve({start_date:start_date,end_date:end_date})
                        break;
                        case 'tomorrow':
                                start_date = moment().startOf('day').add(1,'day').unix();
                                end_date = moment().endOf('day').add(1,'day').unix();
                                resolve({start_date:start_date,end_date:end_date})                        
                        break;
                        case 'week':
                                start_date = moment().startOf('week').unix();
                                end_date = moment().endOf('week').unix();
                                resolve({start_date:start_date,end_date:end_date})                        
                        break;
                        case 'month':
                                start_date = moment().startOf('month').unix();
                                end_date = moment().endOf('month').unix();
                                resolve({start_date:start_date,end_date:end_date})                        
                        break;
                    }
                })
            }
        },
        sort_dates_update:{
            params:{
                type:"string",
                type_id:"any"
            },
            handler(ctx){
                //Check already typeid has data
                let{type,type_id} = ctx.params;
                ctx.call("db.sort-datas.find",{query:{type:type,type_id:type_id}, fields:["id"]})
                .then((res)=>{
                    let currentTimestamp = moment.utc();
                    ctx.params.sort_date = currentTimestamp
                    if(res.length > 0) {
                        ctx.params.id = res[0].id;
                        ctx.call("db.sort-datas.update",ctx.params)
                        .then((res2)=>{
                            console.log('Sort date updated successfully',res2);   
                        })
                        .catch((err) => {
                            console.log(err);
                            reject('Sort date update has some error:',err);
                        });
                    } else {
                        ctx.call("db.sort-datas.create",ctx.params)
                        .then((res3)=>{
                            console.log('Sort date added successfully',res3);   
                        })
                        .catch((err) => {
                            console.log(err);
                            reject('Sort date added has some error:',err);
                        });
                    }
                })
                .catch((err) => {
                    console.log(err);
                    reject('get sort date has some error:',err);
                });
            }
        },
        retriveUrls:{
            params:{
                text:"string"       
            },
            handler(ctx){
                return new Promise(async(resolve, reject) => {
                    let text = ctx.params.text ? ctx.params.text : "";
                    let urls = getUrls(text);
                    let arrayUrls = Array.from(urls);
                    let is_scrap = ctx.params.is_scrap ? ctx.params.is_scrap : true;
                    let resultObj = {};
                    if(is_scrap == true && arrayUrls.length == 1) {
                    // Retrive meta data from urls start
                    let errorobj = {};
                    async.parallel({
                        // scrapeResult: function (callback) {
                        //     var options =  {
                        //         url: arrayUrls[0],
                        //         jar: request.jar(), // Cookie jar
                        //         headers: {
                        //             'User-Agent': 'webscraper'
                        //         }
                        //     };
                        //     scrape(options, function(error, metadata){
                        //         if(metadata) {
                        //             callback(null, metadata);    
                        //         } else if(error) {
                        //             console.log("Fetch metadata scraper has error", error);
                        //             errorobj.error = "Error";
                        //             callback(null, errorobj); 
                        //         }
                        //     });
                        // },
                        urlMetadataResult: function (callback) {
                            urlMetadata(arrayUrls[0]).then(
                            function (metadata) { // success handler
                                resultObj = {
                                    url:metadata.url,
                                    title:metadata.title,
                                    image:metadata.image,
                                    description:metadata.description,
                                    source:metadata.source
                                }
                                callback(null, resultObj);
                            },
                            function (error) { // failure handler
                                console.log("Fetch metadata urlMetadata has error", error);
                                errorobj.error = "Error";
                                callback(null, null); 
                            })
                        },
                        // linkPreviewResult: function(callback) {
                        //     linkPreviewGenerator(arrayUrls[0], ['--no-sandbox', '--disable-setuid-sandbox']).then(
                        //     // linkPreviewGenerator(arrayUrls[0]).then(
                        //     function (metadata) { // success handler
                        //         callback(null, metadata);
                        //     },
                        //     function (error) { // failure handler
                        //         console.log("Fetch metadata linkPreviewResult has error", error);
                        //         errorobj.error = "Error";
                        //         callback(null, errorobj); 
                        //     })   
                        // }
                        
                    },function(err,results) {
                        if (err) {
                            console.log('fetch metadata failed ==> ', err);
                            reject(err);
                        } else {
                            results.urls = arrayUrls;
                            resolve(results);
                        }
                    });
                    // Retrive meta data from urls start end
                    } else {
                        resultObj.urls = arrayUrls;
                        resolve(resultObj);    
                    }
                })
            }   
        },
        sentence_extract:{
            params:{
                text:"string"
            },
            handler:(ctx)=>{
                let {text} = ctx.params;
                return new Promise(async(resolve,reject)=>{
                    //Get hyperlinks from sentences
                    let urls = getUrls(text);
                    let arrayUrls = Array.from(urls);
                    //Split sentence to array
                    let basearray = await text.split(/\r?\n/);
                    //Remove blank arrays
                    basearray = await basearray.filter(item => item);
                    //Remove unwanted space from start & end
                    let finalBaseArray = await basearray.map(i=>i.trim());
                    let finalArray = [];
                    let tempArray = [];
                    let tempObj = {};
                    for (let i = 0; i < finalBaseArray.length; i++) { 
                        //Split sentences with ":"
                        tempArray = await finalBaseArray[i].split(":");
                        if(tempArray.length > 1) {
                            //Create dynamic objects from split array of ":"
                            let valueTexts = '';
                            //Append ":" along with texts from 2nd position EX: "Jun 24, 2020 09:00 PM Mumbai, Kolkata, New Delhi"
                            for (let j = 1; j < tempArray.length; j++) {
                                if(j == 1) {
                                    valueTexts = valueTexts.concat(tempArray[j]);
                                } else {
                                    valueTexts = valueTexts.concat(":"+tempArray[j]);    
                                } 
                            }
                            tempObj[tempArray[0]] = valueTexts;
                            //Extract date & time start
                            let extractDateval = await chrono.parse(valueTexts);
                            if(extractDateval && extractDateval != '') {
                                tempObj["Date_"+tempArray[0]] = extractDateval[0].text;
                            }
                            //Extract date & time end
                        } else if(tempArray.length > 0) {
                            if(tempArray[0] != '') {
                                tempObj["index"+i] = tempArray[0]; 
                            }  
                        }
                    }
                    tempObj.urls = arrayUrls;
                    finalArray.push(tempObj);
                    resolve(finalArray);   
                })
            }
        },
        publish_post:{
            params:{
                publish_type:"string",
                publish_id:"string"
            }, 
            handler(ctx){   
                let {publish_type,publish_id,publish_mention_users,publish_mention_items} = ctx.params;
                return new Promise(async(resolve,reject)=>{
                    switch (publish_type) {
                        case 'request':
                        ctx.broker.call('db.post.find', {query:{publish_type:publish_type,publish_id:publish_id}, fields:["id","publish_mention_users","type","publish_mention_items"]})
                        .then(async(postRes) => {
                            if(postRes.length > 0) {
                                for (var j = 0; j < postRes.length; j++) {
                                    let postId = postRes[j].id;
                                    let getPublishUsers = postRes[j].publish_mention_users ? postRes[j].publish_mention_users : [];
                                    let getRequestitem = postRes[j].publish_mention_items ? postRes[j].publish_mention_items : [];
                                    ctx.params.id = postId.toString();
                                    ctx.params.publish_id = parseInt(publish_id);
                                    ctx.params.type = postRes[j].type;
                                    ctx.params.is_active = true;

                                    //Add new user id to array if it not exist start
                                    if(publish_mention_users.length > 0 && getPublishUsers.length > 0) {
                                        for (var i = 0; i < publish_mention_users.length; i++) {
                                            // let checkVal = getPublishUsers.includes(publish_mention_users[i].toString())
                                            let checkVal = objExists(publish_mention_users[i].user_id,getPublishUsers,"user_id");
                                            if (checkVal) {
                                                console.log('user already exist',publish_mention_users[i]);
                                            } else {
                                                getPublishUsers.unshift(publish_mention_users[i]);
                                            }
                                        }
                                        ctx.params.publish_mention_users = getPublishUsers; 
                                    }
                                    //Add new user id to array if it not exist end

                                    //Check & update request items start
                                    if(publish_mention_items.length > 0 && getRequestitem.length > 0) {
                                        for (var k = 0; k < publish_mention_items.length; k++) {
                                            let checkVal = objExists(publish_mention_items[k].item_id,getRequestitem,"item_id");
                                            if (checkVal) {
                                                console.log('item already exist',publish_mention_items[k]);
                                            } else {
                                                getRequestitem.unshift(publish_mention_items[k]);
                                            }
                                        }
                                        ctx.params.publish_mention_items = getRequestitem; 
                                    } 
                                    //Check & update request items end

                                    //Function check array of object value check start
                                    function objExists(checkVal,checkArray,checkKey) {
                                        return checkArray.some(function(el) {
                                            let tempCheckArray = el;
                                            return tempCheckArray[checkKey] === checkVal;
                                        }); 
                                    }
                                    //Function check array of object value check end

                                    //Update existing post start
                                    await ctx.broker.call("posts.update", ctx.params) 
                                    .then((postupdateRes) => {
                                        console.log("successfully updated post");

                                        //Acknowledge Status update start
                                        let cn_publish_mention_item_id = publish_mention_items[0].item_id;
                                        let cn_publish_mention_user_id = publish_mention_users[0].user_id;
                                        let updateObj = {
                                            request_item_id:cn_publish_mention_item_id.toString(),
                                            contribute_user_id:cn_publish_mention_user_id.toString(),
                                            update_type:"acknowledge"
                                        }
                                        ctx.broker.call("post_needs.contributionStatusUpdation", updateObj) 
                                        .then((statusUpdateRes) => {
                                            resolve("successfully updated acknowledge status");
                                        })
                                        .catch((err) => {
                                            console.log("update acknowledge status has error", err);
                                            reject(err);
                                        })
                                        //Acknowledge Status update end
                                        //Post create Status update start
                                        updateObj.update_type = "post";
                                        ctx.broker.call("post_needs.contributionStatusUpdation", updateObj) 
                                        .then((statusUpdateRes) => {
                                            resolve("successfully updated post create status");
                                        })
                                        .catch((err) => {
                                            console.log("update post create status has error", err);
                                            reject(err);
                                        })
                                        //Post create Status update end

                                    })
                                    .catch((err) => {
                                        console.log("Update posts has error", err);
                                        reject(err);
                                        return;
                                    })
                                    //Update existing post end
                                }
                                resolve("successfully updated all posts");
                                //return;
                            } else {
                                ctx.params.category_id = ctx.params.category_id ? ctx.params.category_id : 3;
                                ctx.params.type = ctx.params.type ? ctx.params.type : "post";
                                ctx.params.post_type = ctx.params.post_type ? ctx.params.post_type : "only_groups";
                                ctx.params.privacy_type = ctx.params.privacy_type ? ctx.params.privacy_type : "public"; 
                                //Create post start
                                ctx.broker.call("posts.create", ctx.params) 
                                .then((postcreateRes) => {
                                    resolve("successfully created all posts");

                                    //Acknowledge Status update start
                                    let cn_publish_mention_item_id = publish_mention_items[0].item_id;
                                    let cn_publish_mention_user_id = publish_mention_users[0].user_id;
                                    let updateObj = {
                                        request_item_id:cn_publish_mention_item_id.toString(),
                                        contribute_user_id:cn_publish_mention_user_id.toString(),
                                        update_type:"acknowledge"
                                    }
                                    ctx.broker.call("post_needs.contributionStatusUpdation", updateObj) 
                                    .then((statusUpdateRes) => {
                                        resolve("successfully updated acknowledge status");
                                    })
                                    .catch((err) => {
                                        console.log("update acknowledge status has error", err);
                                        reject(err);
                                    })
                                    //Acknowledge Status update end

                                    //Post create Status update start
                                    updateObj.update_type = "post";
                                    ctx.broker.call("post_needs.contributionStatusUpdation", updateObj) 
                                    .then((statusUpdateRes) => {
                                        resolve("successfully updated post create status");
                                    })
                                    .catch((err) => {
                                        console.log("update post create status has error", err);
                                        reject(err);
                                    })
                                    //Post create Status update end
                                    return;
                                })
                                .catch((err) => {
                                    console.log("Create posts has error", err);
                                    reject(err);
                                    return;
                                })
                                //Create post end
                            }
                        })
                        .catch((err) => {
                            console.log("Search posts has error", err);
                            reject(err);
                            return;
                        })
                        break;

                        case 'albums':
                            this.publishPost(ctx)
                        break;
                        case 'documents':
                            this.publishPost(ctx)
                        break;

                        default:
                        break;
                    }
                });
            }
        },
        createFirebaseLink:{
            params:{
                type:"string",
                type_id:"string"
            }, 
            handler(ctx){ 
                let {type,type_id} = ctx.params;
                return new Promise(async(resolve,reject)=>{
                    type_id = parseInt(type_id);
                    let f_link = '';
                    //let db_n = '';
                    switch (type) {
                        case 'family':
                            var gId = CryptoJS.AES.encrypt(type_id.toString(), key);
                                gId = encodeURIComponent(gId);
                            f_link  = `${process.env.FRONTEND_BASE_URL}familyview/${gId}?type="${type}"&type_id=${type_id}`;
                        break;
        
                        case 'event':
                            //db_n = 'events';
                            var eId = CryptoJS.AES.encrypt(type_id.toString(), key);
                                eId = encodeURIComponent(eId);
                            f_link  = `${process.env.FRONTEND_BASE_URL}event-details/${eId}?type="${type}"&type_id=${type_id}`;
                        break;
        
                        case 'post':
                            //db_n = 'post';
                            var pId = CryptoJS.AES.encrypt(type_id.toString(), key);
                                pId = encodeURIComponent(pId);
                            f_link  = `${process.env.FRONTEND_BASE_URL}post_details/${pId}?type="${type}"&type_id=${type_id}`;
                        break;
                        
                        default:
                        break;
                    }
                    var options = {
                        'method': 'POST',
                        'url': `https://firebasedynamiclinks.googleapis.com/v1/shortLinks?key=${process.env.FIREBASE_KEY}`,
                        'headers': {
                        'Content-Type': 'application/json'
                        },
                        body: JSON.stringify({
                            "dynamicLinkInfo":{
                                "domainUriPrefix":`${process.env.FIREBASE_DOMAIN_URI}`,
                                "link":`${f_link}`,
                                "androidInfo":{
                                    "androidPackageName":`${process.env.ANDROID_PACKAGE_NAME}`
                                },
                                "iosInfo":{
                                    "iosBundleId":`${process.env.IOS_PACKAGE_NAME}`
                                }
                            },
                            "suffix": {
                                "option": "UNGUESSABLE" 
                            }
                        })
                    };
                    request(options, function (error, response) {
                        if(error) {
                            reject(error);
                        } else {
                            let { shortLink } = JSON.parse(response.body);
                            resolve(shortLink);    
                        }
                    });
                })
            }    
        },
        GetKeys: {
            params:{
                user_id:"string"
            },
            handler(ctx){ 
                return new Promise(async(resolve,reject)=>{
                    let keysObj = {
                        google_api_key : process.env.frontend_google_place_api_key,
                        stripe : process.env.frontend_stripe_key,
                        elastic_search_url : process.env.ELASTIC_SEARCH_URL,
                        elastic_access_key : process.env.ELASTIC_ACCESS_KEY,
                        elastic_secert_key : process.env.ELASTIC_SECERT_KEY,
                        elastic_region : process.env.ELASTIC_REGION,
                        s3_base_url : process.env.AWS_S3_BASE_URL
                    }
                    resolve(keysObj);
                })
            }     
        },
        GetBanners: {
            params:{
                
            },
            handler(ctx){ 
                return new Promise(async(resolve,reject)=>{
                    if(ctx.params.user_id && ctx.params.user_id == "125") {
                        reject();
                        return;   
                    } else {
                        ctx.call("db.banners.find",{query:{visibility:true}, fields:["banner_type","visibility","banner_url","link_type","link_subtype","link_id"]})
                        .then((res)=>{
                            resolve(res);    
                        })
                        .catch((err) => {
                            console.log(err);
                            reject('get banners date has some error:',err);
                        });
                    }
                })
            }     
        },

        paymentNotifyEmail: {
            params: {
                Enotify_type: 'string'
            },
            handler(ctx) {
                let { Enotify_type } = ctx.params;
                return new Promise(async (resolve, reject) => {
                    switch (Enotify_type) {
                        case 'partial_payment':
                            let emailData = [];
                            let { user_id, email_data } = ctx.params;
                            let useris = await ctx.call("db.users.find", { query: { id: user_id }, fields: ["full_name", "email"] });
                            let { full_name, email } = useris[0];
                            emailData.push({ key: 'name', value: full_name });
                            for (var k in email_data) {
                                emailData.push({ key: k, value: email_data[k] });
                            }
                            ctx.params = { ...ctx.params, email_data: emailData }
                            var _html = this.emailTemplate(ctx);
                            // Email template end
                            ctx.broker.call("notification.sendEmail", {
                                toAddresses: [email],
                                message: _html,
                                subject: "Familheey :: Membership"
                            })
                            break;
                        case 'condition_next': //for sonarqube fix remove and add new case if needed
                            console.log(ctx.params);
                            break;
                        case 'condition_next2': //for sonarqube fix remove and add new case if needed
                            console.log(ctx.params);
                            break
                        default:
                            break;
                    }
                });
            }
        },

        /**
         * url open inside app get details without going outside application
         */
        openAppGetParams: {
            params: {
                url: 'string'
            },
            handler(ctx) {
                return new Promise(async (resolve, reject) => {
                    let { url } = ctx.params;
                    let typeIs = url.split(`page/`);
                    let SubtypeIs = typeIs[1].split(`/`);
                    let typeString = SubtypeIs[1];
                    let sendObj = {}
                    switch (SubtypeIs[0]) {
                        case 'events':
                            ctx.call("db.events.find", { query: { event_page: typeString }, fields: ["id"] })
                                .then((event) => {
                                    if (event.length == 0) { reject("No event found"); return; }
                                    sendObj = {
                                        ...sendObj,
                                        type: 'event',
                                        type_id: parseInt(event[0].id),
                                        sub_type:'event'
                                    }
                                    resolve({ code: 200, message: "data is", data: sendObj })
                                }).catch((err) => {
                                    reject(err)
                                });
                            break;
                        case 'posts':
                            sendObj = {
                                ...sendObj,
                                type_id: parseInt(typeString),
                                type: 'home',
                                sub_type: 'familyfeed'
                            }
                            resolve({ code: 200, message: "data is", data: sendObj })
                            break;
                        case 'groups':
                            ctx.call("db.groups.find", { query: { f_text: typeString }, fields: ["id"] })
                                .then((groups) => {
                                    if (groups.length == 0) { reject("No groups found"); return; }
                                    sendObj = {
                                        ...sendObj,
                                        type: 'family',
                                        type_id: parseInt(groups[0].id),
                                        sub_type:'family_link'
                                    }
                                    resolve({ code: 200, message: "data is", data: sendObj })
                                }).catch((err) => {
                                    reject(err)
                                });
                            break;
                        default:
                            reject("Url not valid")
                            break;
                    }
                })
            }
        },

        manuallyUpdateElasticIndex:{
            params:{
                index:'string'
            },
            handler(ctx){
                let { index, limit, offset } = ctx.params;
                return new Promise(async(resolve, reject) => {
                    switch (index) {
                        case 'post':
                            limit  = limit  ? limit  : 50;
                            offset = offset ? offset : 0;
                            ctx.params = { ...ctx.params, limit:limit, offset:offset }
                            ctx.call("db.post.forElastic",ctx.params)
                            .then((posts)=>{
                                if(posts.length == 0 ){ resolve(" all done "); return; }
                                var i=0;
                                posts.forEach(elem => {
                                    let { elasticsearch_id } = elem

                                    let elasticObj = {
                                        index : 'post',
                                        id : elasticsearch_id,
                                        data : elem
                                    } 
                                    ctx.broker.call('es_service.updateRecord', elasticObj)
                                    .then((elasticRes) => {  
                                        console.log('Successfully updated elastic search',elasticRes);   
                                    })
                                    .catch((err) => {
                                        console.log("Elastic search updation has some error",err);
                                    });
                                    i++;
                                    if(i== posts.length ) 
                                        resolve(` success next offset ${ limit + offset } `)
                                })
                            }).catch((err)=>{
                                reject(err)
                            })
                            
                            break;
                        case 'condition_next': //for sonarqube fix remove and add new case if needed
                            console.log(ctx.params);
                            break;
                        case 'condition_next2': //for sonarqube fix remove and add new case if needed
                            console.log(ctx.params);
                            break
                    
                        default:
                            break;
                    }
                });
            }
        }
    },
    events: {},
    methods: {
        publishPost(ctx) {
            let _this = this;
            return new Promise(async(resolve,reject)=>{
            let StartTime = moment().startOf('day').format('YYYY-MM-DD HH:MM:ss')
            let endTime = moment().endOf('day').format('YYYY-MM-DD HH:MM:ss')
            ctx.params = { ...ctx.params, StartTime: StartTime, endTime: endTime, is_shareable: false }
            ctx.broker.call('db.post.postForFolder', ctx.params)
            .then(async (postRes) => {
                ctx.params.created_by = ctx.params.user_id;
                let publishIdInt = parseInt(ctx.params.publish_id);
                ctx.params.publish_id = publishIdInt;
                ctx.params.is_active = true;
                if (postRes.length > 0) {
                    for (var j = 0; j < postRes.length; j++) {
                        let postId = postRes[j].id;
                        ctx.params.id = postId.toString();
                        ctx.params.type = postRes[j].type;
                        let existingFiles = postRes[j].post_attachment;
                        //Add new file to array if it not exist start
                        let newAttachedFile = ctx.params.document_files;
                        let checkArray = Array.isArray(newAttachedFile);
                        if (checkArray) {
                            let tempAttachFile = [];
                            for (var m = 0; m < newAttachedFile.length; m++) {
                                let _filename = newAttachedFile[m].filename ? newAttachedFile[m].filename : newAttachedFile[m].file_name
                                let tempobj = {
                                    filename: _filename,
                                    original_name: newAttachedFile[m].original_name ? newAttachedFile[m].original_name : _filename,
                                    height: newAttachedFile[m].height ? newAttachedFile[m].height : 400,
                                    width: newAttachedFile[m].width ? newAttachedFile[m].width : 400,
                                    is_play: newAttachedFile[m].is_play ? newAttachedFile[m].is_play : false,
                                    type: newAttachedFile[m].file_type ? newAttachedFile[m].file_type : "image/png",
                                    video_thumb: newAttachedFile[m].video_thumb ? newAttachedFile[m].video_thumb : ''
                                }
                                tempAttachFile.unshift(tempobj);
                            }
                            newAttachedFile = tempAttachFile;
                        }

                        if (newAttachedFile && newAttachedFile != '') {
                            for (var i = 0; i < newAttachedFile.length; i++) {
                                existingFiles.unshift(newAttachedFile[i]);
                            }
                            ctx.params.post_attachment = existingFiles;
                        }
                        ctx.params.snap_description = `${ctx.params.post_attachment.length} ${ctx.params.snap_description}`;
                        //Add new file id to array if it not exist end
                        //Update existing post start
                        await ctx.broker.call("db.post.update", ctx.params)
                            .then((postupdateRes) => {
                                console.log("successfully updated post");
                            })
                            .catch((err) => {
                                console.log("Update posts has error", err);
                                reject(err);
                                return;
                            })
                        //Update existing post end 
                    }
                    resolve("successfully updated all posts");
                    return;
                } else {
                    //ctx.broker.call('db.documents.find', {query:{folder_id:ctx.params.publish_id}, fields:["group_id"]})
                    ctx.broker.call('db.folders.find', { query: { id: ctx.params.publish_id }, fields: ["group_id"] })
                        .then(async (documentRes) => {
                            //Remove null group_id datas start
                            let toGroupArray = [];
                            for (var n = 0; n < documentRes.length; n++) {
                                if (documentRes[n].group_id && documentRes[n].group_id != '') {
                                    //Get group post create permission check start
                                    let checkSettObj = {
                                        group_id: documentRes[n].group_id,
                                        user_id: ctx.params.user_id
                                    }
                                    toGroupArray = await _this.getgroupSetting(checkSettObj,ctx);
                                    //Get group post create permission check end 
                                }
                            }

                            //Remove duplicate object from array start
                            let uniq = {}
                            toGroupArray = toGroupArray.filter(obj => !uniq[obj.id] && (uniq[obj.id] = true));
                            //Remove duplicate object from array end
                            //Remove null group_id datas end

                            if (toGroupArray.length > 0) {
                                ctx.params.selected_groups = toGroupArray;
                                ctx.params.category_id = ctx.params.category_id ? ctx.params.category_id : "3";
                                ctx.params.type = ctx.params.type ? ctx.params.type : "post";
                                ctx.params.post_type = ctx.params.post_type ? ctx.params.post_type : "only_groups";
                                ctx.params.privacy_type = ctx.params.privacy_type ? ctx.params.privacy_type : "public";
                                ctx.params.post_info = ctx.params.post_info ? ctx.params.post_info : {};
                                ctx.params.snap_description = ctx.params.snap_description ? ctx.params.snap_description : "";

                                // Set up post_attachment array start
                                let newAttachedFile = ctx.params.document_files;
                                let checkArray = Array.isArray(newAttachedFile);
                                if (checkArray) {
                                    let tempAttachFile = [];
                                    for (var m2 = 0; m2 < newAttachedFile.length; m2++) {
                                        let _filename = newAttachedFile[m2].filename ? newAttachedFile[m2].filename : newAttachedFile[m2].file_name
                                        let tempobj = {
                                            filename: _filename,
                                            original_name: newAttachedFile[m2].original_name ? newAttachedFile[m2].original_name : _filename,
                                            height: newAttachedFile[m2].height ? newAttachedFile[m2].height : 400,
                                            width: newAttachedFile[m2].width ? newAttachedFile[m2].width : 400,
                                            is_play: newAttachedFile[m2].is_play ? newAttachedFile[m2].is_play : false,
                                            type: newAttachedFile[m2].file_type ? newAttachedFile[m2].file_type : "image/png",
                                            video_thumb: newAttachedFile[m2].video_thumb ? newAttachedFile[m2].video_thumb : ''
                                        }
                                        tempAttachFile.unshift(tempobj);
                                    }
                                    newAttachedFile = tempAttachFile;
                                }
                                // Set up post_attachment array end

                                ctx.params.post_attachment = newAttachedFile;
                                ctx.params.snap_description = `${ctx.params.post_attachment.length} ${ctx.params.snap_description}`;
                                //Create post start
                                ctx.broker.call("posts.create", ctx.params)
                                    .then((postcreateRes) => {
                                        resolve(postcreateRes);
                                        return;
                                    })
                                    .catch((err) => {
                                        console.log("Create posts has error", err);
                                        reject(err);
                                        return;
                                    })
                                //Create post end  
                            } else {
                                console.log("No groups find for creating posts");
                                resolve("No groups find for creating posts");
                                return;
                            }
                        })
                        .catch((err) => {
                            console.log("get document groups has some error", err);
                            reject(err);
                            return;
                        })
                }
            })
            .catch((err) => {
                console.log("Search posts has error", err);
                reject(err);
                return;
            })
        })
        },
        getgroupSetting(checkSettObj,ctx){
            return new Promise((resolve, reject) => {
                let togrpTempObj = {};
                let temptoGroupArray = [];
                ctx.broker.call('db.groups.getGroupSetting', checkSettObj)
                .then((groupStteingRes) => {
                    if (groupStteingRes && groupStteingRes.length > 0) {
                        if (groupStteingRes[0].post_create == 6 || groupStteingRes[0].user_type == "admin") {
                            console.log("user have permission to continue");
                            togrpTempObj = {
                                id: checkSettObj.group_id,
                                post_create: 6
                            }
                            temptoGroupArray.push(togrpTempObj);
                            resolve(temptoGroupArray);
                        } else {
                            console.log('User didnt have permission to create post');
                            reject();
                        }
                    } else {
                        console.log('User didnt have permission to create post');
                        reject();
                    }
                })
                .catch((err) => {
                    console.log("get group settings has some error", err);
                    reject(err);
                })
            })
        },

        emailTemplate(ctx) {
            let { email_data } = ctx.params;
            let html =
            `<html>
                <style>
                    table, th, td {
                        border: 1px solid black;
                        border-collapse: collapse;
                    }
                </style>
                    <body>
                    <div class="tableid">
                        <table cellspacing="0" border-collapse="collapse">`;
                        email_data.forEach(elem => {
                        html += 
                            `<tr><td> ${elem.key.replace(/_/g, " ")}: </td> <td> ${elem.value}</td></tr>`
                        });
                        html += 
                        `</table>
                    <br/>
                    Regards,<br/>
                    Familheey Team
                    </div>
                </body>
            </html>`;
            return (html)
        }
    },
    created() { },
    started() { },
    stopped() { }
}