"use strict";
var async = require('async');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
var fs = require('fs');
var moment = require('moment');
const REDIS = require("redis");
const CLIENT_REDIS = REDIS.createClient({
    host:process.env.REDIS_HOST,
    port:process.env.REDIS_PORT
});
CLIENT_REDIS.on("connect", function(error) {
    console.error("---redis connected---**************************");
});
CLIENT_REDIS.on("error", function(error) {
console.error('error in redis---',error);
});
module.exports = {
    name: "posts",
    settings: {},
    actions: {
        /**
         * create a new group
         */
        activatePost:{
            params:{
                user_id:"string",
                post_id:"number",
                device_type:"string" //'web' || 'android' || 'ios'
            },
            handler(ctx) {
                let redisKey = "post_active_"+ctx.params.post_id+"_"+ctx.params.user_id+"_"+ctx.params.device_type;
                CLIENT_REDIS.set(redisKey,1,'EX',120); //2 minut expiry
                return 200;
            }

        },
        deactivatePost:{
            params:{
                user_id:"string",
                post_id:"number",
                device_type:"string" //'web' || 'android' || 'ios'
            },
            handler(ctx) {
                let redisKey = "post_active_"+ctx.params.post_id+"_"+ctx.params.user_id+"_"+ctx.params.device_type;
                CLIENT_REDIS.del(redisKey,1); 
                return 200;
            }
        },
        activateTopic:{
            params:{
                user_id:"string",
                topic_id:"number",
                device_type:"string" //'web' || 'android' || 'ios'
            },
            handler(ctx) {
                let redisKey = "topic_active_"+ctx.params.topic_id+"_"+ctx.params.user_id+"_"+ctx.params.device_type;
                CLIENT_REDIS.set(redisKey,1,'EX',120); //2 minut expiry
                return 200;
            }

        },
        deactivateTopic:{
            params:{
                user_id:"string",
                topic_id:"number",
                device_type:"string" //'web' || 'android' || 'ios'
            },
            handler(ctx) {
                let redisKey = "topic_active_"+ctx.params.topic_id+"_"+ctx.params.user_id+"_"+ctx.params.device_type;
                CLIENT_REDIS.del(redisKey,1); 
                return 200;
            }
        },
        create: {
            params: {
                category_id: "string",
                snap_description: "string",
                created_by: "string",
                type: "string",
                post_type: "string",
                privacy_type: "string",
            },
            async handler(ctx) {
                //let _this = this;
                let { post_info, type, created_by } = ctx.params;
                let postActive = ctx.params.is_active ? ctx.params.is_active : true;
                ctx.params.is_approved = 1;
                if (typeof post_info == 'string') ctx.params = { ...ctx.params, post_info: JSON.parse(post_info) }
                //let response = {}
                let urltxt = {};
                ctx.params.post_ref_id = this.keyGen();

                // Space replace from url start
                let postAttachement = ctx.params.post_attachment;
                if(ctx.params.post_attachment && postAttachement.length > 0) {
                    urltxt.is_scrap = false;
                    await postAttachement.forEach(element => {
                        if(element.type.indexOf('image') != -1){
                            element.height = (element.height) ? element.height : 100;
                        }
                        if(element.filename) {
                            element.filename = replace_space(element.filename); 
                        } else if(element.file_name) {
                            element.file_name = replace_space(element.file_name);    
                        }
                    })
                    ctx.params.post_attachment = postAttachement;
                }
                function replace_space(url){
                    var str = url.replace(/\s/g,'%20');
                    return str; 
                }
                // Space replace from url end

                // Seperate valid urls from description texts start
                if(ctx.params.snap_description && ctx.params.snap_description != '') {
                    urltxt.text = ctx.params.snap_description;
                    await ctx.broker.call("common_methods.retriveUrls", urltxt) 
                    .then(result => {
                        if(result) {
                            ctx.params.valid_urls = result.urls;
                            ctx.params.url_metadata = result;
                        } 
                    })
                    .catch(err => {
                    }); 
                }
                // Seperate valid urls from description texts end

                return new Promise((resolve, reject) => {
                    ctx.params.ntype = `${type}_create`;
                    delete ctx.params.sort_date;
                    let indpost_id = '';
                    if (ctx.meta && ctx.meta.file) {
                        ctx.meta.file.forEach(element => {
                            ctx.params.file_type = element.mimetype; //file mimetype
                            ctx.params[element.fieldname] = ctx.params[element.fieldname] ? (ctx.params[element.fieldname] + ',' + element.filename) : element.filename;
                            this.calls3(ctx, element);
                        });
                    }
                    if (ctx.params.post_type == 'all_family') {
                        let togrpId = [];
                        let postArr = [];
                        ctx.call("posts.getFamilyListForPost", { user_id: ctx.params.created_by, type:type }).then(result => {
                            if(result.data.length == 0) {
                                resolve({ message: 'You have no permission to create post', data: result.data });
                                return;   
                            }
                            result.data.map(v => {
                                let post_item = Object.assign({}, ctx.params);
                                post_item.to_group_id = v.id
                                postArr.push(post_item);
                                togrpId.push({ id: v.id })
                            });
                            ctx.call('db.post.bulkCreate', postArr)
                                .then(async result2 => {
                                    resolve({ message: 'success', data: result2 });
                                    for (var i = 0; i < result2.length; i++) {
                                        //Track data start
                                        let trackerData = {
                                            user_id: parseInt(ctx.params.created_by),
                                            type: ctx.params.type,
                                            type_id: parseInt(result2[i]['dataValues'].id),
                                            activity: ('created a ' + ctx.params.type)
                                        };
                                        ctx.broker.call('user_activities.trackUserActivities', trackerData);
                                        //Track data end

                                        //Generate & update firebase link start
                                        indpost_id = result2[i]['dataValues'].id;
                                        let linkobj = {
                                            type : "post",
                                            type_id : indpost_id.toString()
                                        }
                                        await ctx.call('common_methods.createFirebaseLink', linkobj)
                                        .then(async (linkRes) => {
                                            if(linkRes && linkRes != '') {
                                                await ctx.call('db.post.update',{ id:indpost_id, firebase_link:linkRes })
                                                .then((linkUpdate) => {
                                                    console.log("Successfully updated firebase link");
                                                })
                                                .catch((err) => {
                                                    console.log("Firebase link updation to post has some error", err);
                                                })
                                            }
                                        })
                                        .catch((err) => {
                                            console.log("Firebase link generation has some error", err);
                                        })
                                        //Generate & update firebase link end

                                        ctx.params.type_id = parseInt(result2[i]['dataValues'].id)
                                    }
                                    
                                    ctx.params.shared_user_id = created_by;
                                    ctx.params.to_group_id = togrpId;
                                    ctx.params.ntype = `${type}_create`;
                                    if(postActive) {
                                        this.appNotify(ctx);
                                    }
                                })
                                .catch(err => {
                                });
                        });
                    } else if (ctx.params.post_type == 'only_groups') {
                        let postArr = []; let togrpId = [];
                        if (!ctx.params.selected_groups) {
                            return reject({ message: 'group id is missing' });
                        }
                        ctx.params.selected_groups.map(v => {
                            let post_item = Object.assign({}, ctx.params);
                            post_item.to_group_id = v.id;
                            post_item.is_approved = (v.post_create == 20 ? 0 : 1);
                            postArr.push(post_item);
                            togrpId.push({ id: v.id })
                        });
                        try {
                            ctx.call('db.post.bulkCreate', postArr)
                                .then(async result => {
                                    ctx.params.ntype = `${type}_create`;
                                    ctx.params.shared_user_id = created_by;
                                    ctx.params.to_group_id = togrpId;
                                    resolve({ message: 'success', data: result });

                                    for (var i = 0; i < result.length; i++) {
                                        //Track Data Start
                                        let trackerData = {
                                            user_id: parseInt(ctx.params.created_by),
                                            type: ctx.params.type,
                                            type_id: parseInt(result[i]['dataValues'].id),
                                            activity: ('created a ' + ctx.params.type)
                                        };
                                        ctx.broker.call('user_activities.trackUserActivities', trackerData);
                                        //Track Data End
                                        ctx.params.type_id = parseInt(result[i]['dataValues'].id);

                                        //Generate & update firebase link start
                                        indpost_id = result[i]['dataValues'].id;
                                        let linkobj = {
                                            type : "post",
                                            type_id : indpost_id.toString()
                                        }
                                        await ctx.call('common_methods.createFirebaseLink', linkobj)
                                        .then(async (linkRes) => {
                                            if(linkRes && linkRes != '') {
                                                await ctx.call('db.post.update',{ id:indpost_id, firebase_link:linkRes })
                                                .then((linkUpdate) => {
                                                    console.log("Successfully updated firebase link");
                                                })
                                                .catch((err) => {
                                                    console.log("Firebase link updation to post has some error", err);
                                                })
                                            }
                                        })
                                        .catch((err) => {
                                            console.log("Firebase link generation has some error", err);
                                        })
                                        //Generate & update firebase link end
                                    }
                                    
                                    togrpId.map(async (grpids, j) => {
                                        await ctx.call('db.post.find', { query: { post_ref_id: ctx.params.post_ref_id, to_group_id: grpids.id } })
                                            .then(posts => {
                                                if (posts[0]) {
                                                    try {
                                                        ctx.params.to_group_id = [grpids];
                                                        ctx.params.type_id = parseInt(posts[0].id);
                                                        if(postActive) {
                                                            this.appNotify(ctx);
                                                        }
                                                    } catch (error) {
                                                        console.log('posts = >?', error);
                                                    }
                                                }
                                            });
                                    })
                                    //this.appNotify(ctx);
                                })
                                .catch(err => {
                                });
                        } catch (error) {
                            console.log(error);
                        }
                    } else if (ctx.params.post_type == 'only_users') {
                        let postArr = [];
                        if (!ctx.params.selected_users) {
                            return reject({ message: 'user id is missing' });
                        }
                        ctx.params.selected_users.map(v => {
                            let post_item = Object.assign({}, ctx.params);
                            post_item.to_user_id = v
                            postArr.push(post_item);
                        });
                        ctx.call('db.post.bulkCreate', postArr)
                            .then(async result => {
                                resolve({ message: 'success', data: result });
                                for (var i = 0; i < result.length; i++) {
                                    //Tract Data start
                                    let trackerData = {
                                        user_id: parseInt(ctx.params.created_by),
                                        type: ctx.params.type,
                                        type_id: parseInt(result[i]['dataValues'].id),
                                        activity: ('created a ' + ctx.params.type)
                                    };
                                    ctx.broker.call('user_activities.trackUserActivities', trackerData);
                                    //Tract Data end

                                    //Generate & update firebase link start
                                    indpost_id = result[i]['dataValues'].id;
                                    let linkobj = {
                                        type : "post",
                                        type_id : indpost_id.toString()
                                    }
                                    await ctx.call('common_methods.createFirebaseLink', linkobj)
                                    .then(async (linkRes) => {
                                        if(linkRes && linkRes != '') {
                                            await ctx.call('db.post.update',{ id:indpost_id, firebase_link:linkRes })
                                            .then((linkUpdate) => {
                                                console.log("Successfully updated firebase link");
                                            })
                                            .catch((err) => {
                                                console.log("Firebase link updation to post has some error", err);
                                            })
                                        }
                                    })
                                    .catch((err) => {
                                        console.log("Firebase link generation has some error", err);
                                    })
                                    //Generate & update firebase link end
                                }
                                
                                let post_dataz = await ctx.call('db.post.find', { query: { post_ref_id: ctx.params.post_ref_id }, fields:["id","to_user_id","created_by"] })
                                post_dataz.forEach(element => {
                                    ctx.params = { ...ctx.params, 
                                        to_user_id:element.to_user_id, 
                                        post_id: parseInt(element.id),
                                        shared_user_id:element.created_by,
                                        ntype: 'post_create_connection'
                                    }
                                    if(postActive) {
                                        this.appNotify(ctx);
                                    }
                                });
                                
                            })
                            .catch(err => {
                            });
                    } else if (ctx.params.post_type == 'public') {
                        ctx.call('db.post.create', ctx.params)
                            .then(async (result) => {
                                //push data to elastic search start
                                let elasticObj = {
                                    index_name : 'post',
                                    data : result
                                }
                                ctx.broker.call('es_service.addRecords', elasticObj)
                                .then((elasticRes) => {
                                    //Update post with elastic search ID start
                                    ctx.call('db.post.update',{ id:result.id, elasticsearch_id:elasticRes._id })
                                    .then((linkUpdate) => {
                                        console.log("Successfully updated elastic search ID");
                                    })
                                    .catch((err) => {
                                        console.log("search ID updation to post has some error", err);
                                    })
                                    //Update post with elastic search ID end
                                })
                                .catch((err) => {
                                    console.log("Add post data to elastic search has some error", err);
                                })
                                //push data to elastic search end

                                let trackerData = {
                                    user_id: parseInt(ctx.params.created_by),
                                    type: ctx.params.type,
                                    type_id: parseInt(result.id),
                                    activity: ('created a ' + ctx.params.type)
                                };
                                ctx.broker.call('user_activities.trackUserActivities', trackerData);
                                let togrpId = await ctx.call("db.group_map.find", { query: { user_id: created_by,is_blocked:false, is_removed:false }, fields: ['group_id'] })
                                ctx.params.to_group_id = togrpId;
                                ctx.params.type_id = parseInt(result.id);
                                ctx.params.shared_user_id = created_by;
                                if(postActive) {
                                    this.appNotify(ctx);
                                }
                                resolve({ message: 'success', data: [result] });

                                //Generate & update firebase link start
                                let linkobj = {
                                    type : "post",
                                    type_id : result.id.toString()
                                }
                                ctx.call('common_methods.createFirebaseLink', linkobj)
                                .then((linkRes) => {
                                    if(linkRes && linkRes != '') {
                                        ctx.call('db.post.update',{ id:result.id, firebase_link:linkRes })
                                        .then((linkUpdate) => {
                                            console.log("Successfully updated firebase link");
                                        })
                                        .catch((err) => {
                                            console.log("Firebase link updation to post has some error", err);
                                        })
                                    }
                                })
                                .catch((err) => {
                                    console.log("Firebase link generation has some error", err);
                                })
                                //Generate & update firebase link end

                            }).catch((err) => {
                                console.log(err);
                                reject({ message: 'error', data: err });
                            });
                    } else if (ctx.params.post_type == 'private') {
                        ctx.call('db.post.create', ctx.params)
                        .then((result) => {
                            let trackerData = {
                                user_id: parseInt(ctx.params.created_by),
                                type: ctx.params.type,
                                type_id: parseInt(result.id),
                                activity: ('created a ' + ctx.params.type)
                            };
                            ctx.broker.call('user_activities.trackUserActivities', trackerData);
                            resolve({ message: 'success', data: [result] });

                            //Generate & update firebase link start
                            let linkobj = {
                                type : 'post',
                                type_id : result.id.toString()
                            }
                            ctx.call('common_methods.createFirebaseLink', linkobj)
                            .then((linkRes) => {
                                if(linkRes && linkRes != '') {
                                    ctx.call('db.post.update',{ id:result.id, firebase_link:linkRes })
                                    .then((linkUpdate) => {
                                        console.log("Successfully updated firebase link");
                                    })
                                    .catch((err) => {
                                        console.log("Firebase link updation to post has some error", err);
                                    })
                                }
                            })
                            .catch((err) => {
                                console.log("Firebase link generation has some error", err);
                            })
                            //Generate & update firebase link end

                        }).catch((err) => {
                            console.log(err);
                            reject({ message: 'error', data: err });
                        });
                    }
                });
            }
        },
        update_post: {
            params: {
                post_ref_id: 'string',
                created_by: 'string',
                type: "string",
                update_type: "string",
                privacy_type: "string",
                snap_description: "string",
            },
           async handler(ctx) {
                let { id } = ctx.params;
                //let response = {}
                let urltxt = {};

                // Space replace from url start
                let postAttachement = ctx.params.post_attachment;
                if(ctx.params.post_attachment && postAttachement.length > 0) {
                    urltxt.is_scrap = false;
                    await postAttachement.forEach(element => {
                        element.filename = replace_space(element.filename); 
                    })
                    ctx.params.post_attachment = postAttachement;
                }
                function replace_space(url){
                    var str = url.replace(/\s/g,'%20');
                    return str; 
                }
                // Space replace from url end

                // Seperate valid urls from description texts start
                if(ctx.params.snap_description && ctx.params.snap_description != '') {
                    urltxt.text = ctx.params.snap_description;
                    await ctx.broker.call("common_methods.retriveUrls", urltxt) 
                    .then(result => {
                        if(result) {
                            ctx.params.valid_urls = result.urls;
                            ctx.params.url_metadata = result;
                        }   
                    })
                    .catch(err => {
                    }); 
                }
                // Seperate valid urls from description texts end

                return new Promise((resolve, reject) => {
                    if (ctx.meta && ctx.meta.file) {
                        ctx.meta.file.forEach(element => {
                            ctx.params.file_type = element.mimetype; //file mimetype
                            ctx.params[element.fieldname] = element.filename;
                            this.calls3(ctx, element);
                        });
                    }
                    delete ctx.params.id;
                    let up_data = {
                        params: ctx.params,
                        where: { post_ref_id: ctx.params.post_ref_id, created_by: ctx.params.created_by }
                    };
                    if (ctx.params.update_type != 'multiple') {
                        if (typeof ctx.params.to_group_id == 'object') {
                            delete up_data.params.to_group_id;
                        }
                        console.log('dsfsdfs', typeof ctx.params.to_group_id);
                        ctx.call('db.post.updateByParams', up_data)
                            .then((result) => {
                                console.log('update data', result)
                                let trackerData = {
                                    user_id: parseInt(ctx.params.created_by),
                                    type: 'post',
                                    type_id: parseInt(result.id),
                                    activity: ('updated post')
                                };
                                // console.log(trackerData);
                                ctx.broker.call('user_activities.trackUserActivities', trackerData);

                                resolve({ message: 'success', data: result });

                            }).catch((err) => {
                                console.log(err);
                                // reject({ message: 'error', data: err });
                            });
                    } else {
                        async.parallel({
                            // Update post function
                            data: function (callback) {
                                up_data = {
                                    params: ctx.params,
                                    //where: { post_ref_id: ctx.params.post_ref_id, created_by: ctx.params.created_by },
                                    where: { id: id, created_by: ctx.params.created_by }
                                };
                                ctx.call('db.post.updateByParams', up_data)
                                    .then((result) => {
                                        let trackerData = {
                                            user_id: parseInt(ctx.params.created_by),
                                            type: 'post',
                                            type_id: parseInt(ctx.params.type_id),
                                            activity: ('updated post')
                                        };
                                        ctx.broker.call('user_activities.trackUserActivities', trackerData);
                                        console.log('Successfully updated post');
                                        callback(null, "Successfully updated post");
                                    }).catch((err) => {
                                        console.log('Update Post Error : ', err);
                                        callback(null, "Failed to updated post");
                                    });
                            },
                            // Remove post function
                            delete: function (callback) {
                                if (ctx.params.delete_post && ctx.params.delete_post.length > 0) {
                                    ctx.call('db.post.bulk_update_post', ctx.params)
                                        .then((result) => {
                                            console.log('Successfully removed post');
                                            callback(null, "Successfully removed post");
                                        }).catch((err) => {
                                            console.log('remove post error', err);
                                            callback(null, "Successfully updated post");
                                        });
                                } else {
                                    callback(null, "Successfully removed post");
                                }
                            },
                            // Add post function
                            add: function (callback) {
                                if (ctx.params.to_group_id_array && ctx.params.to_group_id_array.length > 0) {
                                    let idArray = ctx.params.inactive_active_array;
                                    let toGroupIdArray = ctx.params.to_group_id_array;
                                    ctx.call('db.post.find', { query: { id: ctx.params.type_id } })
                                        .then((resultArray) => {
                                            var result = resultArray[0];
                                            var dataObjArray = [];
                                            var toRenewArray = [];
                                            var dataObj = {
                                                category_id: result.category_id,
                                                orgin_id: null,
                                                from_id: null,
                                                post_ref_id: ctx.params.post_ref_id ? ctx.params.post_ref_id : result.post_ref_id,
                                                to_user_id: null,
                                                shared_user_id: null,
                                                created_by: ctx.params.created_by ? parseInt(ctx.params.created_by) : result.created_by,
                                                title: null,
                                                snap_description: ctx.params.snap_description ? ctx.params.snap_description : result.snap_description,
                                                post_attachment: ctx.params.post_attachment ? ctx.params.post_attachment : result.post_attachment,
                                                post_info: result.post_info,
                                                file_type: result.file_type,
                                                is_shareable: ctx.params.is_shareable ? ctx.params.is_shareable : result.is_shareable,
                                                conversation_enabled: ctx.params.conversation_enabled ? ctx.params.conversation_enabled : result.conversation_enabled,
                                                privacy_type: ctx.params.privacy_type ? ctx.params.privacy_type : result.privacy_type,
                                                type: ctx.params.type ? ctx.params.type : result.type,
                                                is_active: true,
                                                is_approved: result.is_approved,
                                                valid_urls : ctx.params.valid_urls ? ctx.params.valid_urls : result.valid_urls,
                                                url_metadata : ctx.params.url_metadata ? ctx.params.url_metadata : result.url_metadata
                                            };
                                            for (var i = 0; i < toGroupIdArray.length; i++) {
                                                var obj = toGroupIdArray[i];
                                                //var params = { is_active: true };
                                                if (idArray.includes(obj.id)) {
                                                    toRenewArray.push(obj.id);
                                                } else {
                                                    var tempObj = { ...dataObj };
                                                    tempObj.to_group_id = obj.id;
                                                    dataObjArray.push(tempObj);
                                                }
                                            }
                                            let up_data_renew = {
                                                params: params,
                                                where: { post_ref_id: ctx.params.post_ref_id, created_by: ctx.params.created_by, to_group_id: { [Op.in]: toRenewArray } }
                                            };
                                            ctx.call('db.post.updateByParams', up_data_renew)
                                                .then((result2) => {
                                                    resolve({ message: 'success', data: result2 });
                                                }).catch((err) => {
                                                    console.log(err);
                                                });
                                            console.log('KiD --> ', dataObjArray);
                                            ctx.call('db.post.bulkCreate', dataObjArray)
                                                .then((result3) => {
                                                    resolve({ message: 'success', data: result3 });
                                                }).catch((err) => {
                                                    console.log(err);
                                                });
                                            callback(null, "Successfully added post");
                                        }).catch((err) => {
                                            console.log('remove post error', err);
                                            callback(null, "Failed to add post");
                                        });
                                } else {
                                    callback(null, "Successfully added post");
                                }
                            }
                        }, function (err, results) {
                            if (err) {
                                console.log('Update post is failed ==> ', err);
                                reject(err);
                            } else {
                                results.message = "success";
                                resolve(results);
                            }
                        });
                    }
                });

            }
        },
        update: {
            params: {
                id: "string",
            },
            async handler(ctx) {
                //let response = {}
                let urltxt = {};

                // Space replace from url start
                let postAttachement = ctx.params.post_attachment;
                if(ctx.params.post_attachment && postAttachement.length > 0) {
                    urltxt.is_scrap = false;
                    await postAttachement.forEach(element => {
                        element.filename = replace_space(element.filename); 
                    })
                    ctx.params.post_attachment = postAttachement;
                }
                function replace_space(url){
                    var str = url.replace(/\s/g,'%20');
                    return str; 
                }
                // Space replace from url end

                // Seperate valid urls from description texts start
                if(ctx.params.snap_description && ctx.params.snap_description != '') {
                    urltxt.text = ctx.params.snap_description;
                    await ctx.broker.call("common_methods.retriveUrls", urltxt) 
                    .then(result => {
                        if(result) {
                            ctx.params.valid_urls = result.urls;
                            ctx.params.url_metadata = result;
                        }   
                    })
                    .catch(err => {
                    }); 
                }
                // Seperate valid urls from description texts end

                return new Promise((resolve, reject) => {
                    if (ctx.meta && ctx.meta.file) {
                        ctx.meta.file.forEach(element => {
                            ctx.params.file_type = element.mimetype; //file mimetype
                            ctx.params[element.fieldname] = element.filename;
                            this.calls3(ctx, element);
                        });
                    }
                    ctx.call('db.post.update', ctx.params)
                        .then((result) => {
                            console.log('create', result)
                            let trackerData = {
                                user_id: parseInt(result.created_by),
                                type: ctx.params.type,
                                type_id: parseInt(ctx.params.id),
                                activity: ('updated ' + ctx.params.type)
                            };
                            // console.log(trackerData);
                            ctx.broker.call('user_activities.trackUserActivities', trackerData);

                            resolve({ message: 'success', data: result });

                        }).catch((err) => {
                            console.log(err);
                            reject({ message: 'error', data: err });
                        });
                });

            }
        },
        getByID: {
            params: {
                id: "string",
            },
            handler(ctx) {
                //let response = {}
                return new Promise((resolve, reject) => {
                    ctx.call('db.post.getByID', ctx.params)
                        .then((result) => {
                            // console.log(result);
                            if (result.length == 0) {
                                return resolve({ message: 'success ', data: {} });
                            } else {
                                result[0].inactive_active_array = result[0].inactive_active_array && result[0].inactive_active_array.length > 0 ? result[0].inactive_active_array : [];
                            }
                            ctx.call('db.post.getFamilyList', result[0])
                                .then((familyList) => {
                                    result[0].familyList = familyList;
                                    resolve({ message: 'success', data: result });
                                });

                        }).catch((err) => {
                            console.log(err);
                            reject({ message: 'error', data: err });
                        });
                });

            }
        },
        getActiveNotificationCount: {
            params: {
                post_id: 'string'
            },
            handler(ctx) {
                return new Promise((resolve, reject) => {

                });
            }
        },
        /**
         * conversation on a post
         */
        postComment: {
            params: {
                post_id: 'string',
            },
            handler(ctx) {

                //let response = {}
                let { group_id, commented_by, post_id } = ctx.params;
                return new Promise(async (resolve, reject) => {
                    let responseArr = [];
                    let _this = this;
                    if (ctx.meta && ctx.meta.file) {
                        await ctx.meta.file.forEach(async element => {
                            ctx.params.file_type = element.mimetype; //file mimetype
                            ctx.params[element.fieldname] = ctx.params[element.fieldname] ? (ctx.params[element.fieldname] + ',' + element.filename) : element.filename;
                            var res1 = {
                                type: element.mimetype,
                                filename: element.filename
                            }
                            responseArr.push(res1);
                            await this.calls3(ctx, element, function () {

                            });
                        });
                    }
                    let attachData = ctx.params.attachment ? ctx.params.attachment : [];
                    if(ctx.params.attachment && attachData.length > 0) {
                        //ctx.params.attachment = ctx.params.attachment; 
                    } else {
                        ctx.params.attachment = responseArr;
                    }
                    var chat_message = '';

                    let _group = [];
                    if (group_id && group_id != 'null') _group = await ctx.broker.call('db.groups.find', { query: { id: group_id }, fields: ["id", "group_name", "logo", "created_by"] })

                    ctx.call("db.users.find", { query: { id: ctx.params.commented_by } })
                        .then(user_info => {
                            let topic = 'post_channel_' + ctx.params.post_id;
                            var comment_data = Object.assign({}, ctx.params);
                            comment_data.full_name = user_info[0].full_name;
                            comment_data.propic = user_info[0].propic;
                            comment_data.type = 'add comment';
                            comment_data.createdAt = moment.utc();
                            chat_message = comment_data;
                            // ctx.call('socket_service.sendMessage', { topic: topic, data: [comment_data] });

                            // COMMENT  
                            ctx.call('db.post.checkPostActiveStatus', ctx.params)
                                .then(async(activeRecords) => {
                                    //if (true) {
                                            let redisKeyprefix = "post_active_"+ctx.params.post_id+"_";
                                            let active_users = await _this.getActiveUsers(redisKeyprefix+'*');
                                            ctx.call('db.post.find', { query: { id: ctx.params.post_id } })
                                            .then(async posts => {
                                                let { to_user_id, to_group_id } = posts[0];
                                                var created_by = posts[0].created_by;
                                                let _created_by = await ctx.call("db.users.find", { query: { id: posts[0].created_by }, fields: ["id", "full_name"] })
                                                try {
                                                    posts[0].post_id = ctx.params.post_id;
                                                    if (to_group_id) { 
                                                        ctx.call('db.post.getAllUserFromGrp', ctx.params)
                                                            .then((result) => {
                                                                result.map(v => {
                                                                        let userbasedkey = redisKeyprefix+v.user_id;
                                                                        if(active_users.indexOf(userbasedkey+'_web') < 0 &&
                                                                            active_users.indexOf(userbasedkey+'_android') < 0 &&
                                                                            active_users.indexOf(userbasedkey+'_ios') < 0
                                                                        ) {
                                                                            let message = {
                                                                                topic: 'notification_' + v.user_id,
                                                                                data: {
                                                                                    type: 'chat_notification',
                                                                                    message: 'Active Conversation Found in ',
                                                                                    group_id: v.group_id,
                                                                                    result: posts,
                                                                                    comment_data: chat_message
                                                                                }
                                                                            };
                                                                            ctx.call('socket_service.sendMessage', message);
                                                                            let _grpname = (_group.length > 0) ? _group[0].group_name : ''
                                                                            ctx.params = {
                                                                                ...ctx.params,
                                                                                to_id: v.user_id,
                                                                                type_id: parseInt(posts[0].id),
                                                                                link_to: posts[0].id,
                                                                                message_title: 'familheey',
                                                                                body: `${comment_data.full_name} has sent a message on ${_created_by[0].full_name}'s ${posts[0].type} in ${_grpname}`,
                                                                                message: `<b>${comment_data.full_name}</b> has sent a message on <b>${_created_by[0].full_name}</b>'s <b>${posts[0].type}</b> in <b>${_grpname}</b>`,
                                                                                sub_type: 'conversation',
                                                                                // type: `${posts[0].type}`,
                                                                                type: 'post',
                                                                                propic: comment_data.propic,
                                                                                from_id: user_info.length > 0 ? user_info[0].id : '',
                                                                                category: 'conversation',
                                                                            }

                                                                            if (v.user_id == created_by) {
                                                                                ctx.params = {
                                                                                    ...ctx.params,
                                                                                    body: `${comment_data.full_name} has sent a message on your ${posts[0].type} in ${_grpname}`,
                                                                                    message: `<b>${comment_data.full_name}</b> has sent a message on your <b>${posts[0].type}</b> in <b>${_grpname}</b>`
                                                                                }
                                                                            }
                                                                            if (commented_by != v.user_id) {
                                                                                delete ctx.params.to_group_id;
                                                                                ctx.params = { ...ctx.params, to_user_id: v.user_id, ntype: 'postComment' }
                                                                                this.pushNotify(ctx, ctx.params);
                                                                                ctx.broker.call("notification.sendAppNotification", ctx.params);
                                                                            }
                                                                    }
                                                                    
                                                                });
                                                            }).catch((err) => {
                                                                reject({ message: 'error' });
                                                            });
                                                    } else if (to_user_id) {
                                                        let commented_users = [created_by,to_user_id];
                                                        commented_users.forEach(elem => {
                                                            let userbasedkey = redisKeyprefix+elem;
                                                            if(active_users.indexOf(userbasedkey+'_web') < 0 &&
                                                                active_users.indexOf(userbasedkey+'_android') < 0 &&
                                                                active_users.indexOf(userbasedkey+'_ios') < 0
                                                            ) {
                                                                let message = {
                                                                    topic: 'notification_' + elem,
                                                                    data: {
                                                                        type: 'chat_notification',
                                                                        message: 'Active Conversation Found in ',
                                                                        result: posts,
                                                                        comment_data: chat_message
                                                                    }
                                                                };
                                                                ctx.call('socket_service.sendMessage', message);
                                                                ctx.params = {
                                                                    ...ctx.params,
                                                                    to_id: elem,
                                                                    type_id: parseInt(posts[0].id),
                                                                    link_to: posts[0].id,
                                                                    message_title: 'familheey',
                                                                    body: `${comment_data.full_name} has sent a message on ${_created_by[0].full_name}'s ${posts[0].type}`,
                                                                    message: `<b>${comment_data.full_name}</b> has sent a message on <b>${_created_by[0].full_name}</b>'s ${posts[0].type}`,
                                                                    sub_type: 'conversation',
                                                                    category: 'conversation',
                                                                    type: 'post',
                                                                    propic: comment_data.propic,
                                                                    from_id: user_info.length > 0 ? user_info[0].id : ''
                                                                }

                                                                if (elem == created_by) {
                                                                    ctx.params = {
                                                                        ...ctx.params,
                                                                        body: `<b>${comment_data.full_name}</b> has sent a message on your <b>${posts[0].type}</b>`,
                                                                        message: `<b>${comment_data.full_name}</b> has sent a message on your <b>${posts[0].type}</b>`
                                                                    }
                                                                }
                                                                if (commented_by != elem) {
                                                                    delete ctx.params.to_group_id;
                                                                    ctx.params = { ...ctx.params, to_user_id: elem, ntype: 'postComment' }
                                                                    ctx.broker.call("notification.sendAppNotification", ctx.params);
                                                                    this.pushNotify(ctx, ctx.params);
                                                                }
                                                            }
                                                            
                                                        });
                                                    }else{//public post
                                                        let _commentedBy = await ctx.call("db.post_comments.getCommentedUser",ctx.params)
                                                        let commentedUsers = _commentedBy.map(v => v.commented_by);
                                                        if(commentedUsers.indexOf(created_by) == -1) commentedUsers.push(created_by)
                                                        ctx.params = {
                                                            ...ctx.params,
                                                            type_id: parseInt(posts[0].id),
                                                            link_to: posts[0].id,
                                                            message_title: 'familheey',
                                                            body: `${comment_data.full_name} has sent a message on ${_created_by[0].full_name}'s ${posts[0].type}`,
                                                            message: `<b>${comment_data.full_name}</b> has sent a message on <b>${_created_by[0].full_name}</b>'s ${posts[0].type}`,
                                                            sub_type: 'conversation',
                                                            category: 'conversation',
                                                            type: 'post',
                                                            propic: comment_data.propic,
                                                            from_id: user_info.length > 0 ? user_info[0].id : ''
                                                        }
                                                        console.log('---------------',active_users);
                                                        commentedUsers.forEach(u => {
                                                            let userbasedkey = redisKeyprefix+u;
                                                            if(active_users.indexOf(userbasedkey+'_web') < 0 &&
                                                                active_users.indexOf(userbasedkey+'_android') < 0 &&
                                                                active_users.indexOf(userbasedkey+'_ios') < 0
                                                            ) {
                                                                ctx.params.to_id = u;
                                                                if (u != commented_by) {
                                                                    ctx.broker.call("notification.sendAppNotification", ctx.params);
                                                                    this.pushNotify(ctx, ctx.params);
                                                                }
                                                            }
                                                            
                                                        });
                                                    }
                                                } catch (error) {
                                                    console.log(error);
                                                }
                                            })
                                    //}
                                    ctx.call('db.post_comments.create', ctx.params)
                                        .then(async(comment) => {
                                            comment_data.comment_id = comment.id;
                                            ctx.call('socket_service.sendMessage', { topic: topic, data: [comment_data] });
                                            let existRead = await ctx.call("db.post_comment_read.find",{ query:{post_id:post_id, user_id:commented_by} })
                                            if(existRead && existRead.length>0){
                                                ctx.call("db.post_comment_read.update",{ id:existRead[0].id, last_read_message_id:comment.id })
                                            }else{
                                                let save_data = {
                                                    last_read_message_id:comment.id,
                                                    post_id:post_id, 
                                                    user_id:commented_by
                                                }
                                                ctx.call("db.post_comment_read.create",save_data)
                                            }
                                            resolve({ message: 'success', data: comment });
                                            //Post sort date update start
                                            let sortdateObj = {
                                                post_id:post_id
                                            };
                                            ctx.broker.call('posts.post_sortdate_update', sortdateObj);
                                            //Post sort date update end
                                        }).catch((err) => {
                                            reject({ message: 'error' });
                                        });
                                });
                            // END
                        });
                });
            }
        },

        /**
         * conversation on a post
         */
        topicComment: {
            params: {
                topic_id: 'string',
            },
            handler(ctx) {
                //let response = {}
                let { commented_by, topic_id } = ctx.params;
                return new Promise(async (resolve, reject) => {
                    let responseArr = [];
                    let _this = this;
                    if (ctx.meta && ctx.meta.file) {
                        await ctx.meta.file.forEach(async element => {
                            ctx.params.file_type = element.mimetype; //file mimetype
                            ctx.params[element.fieldname] = ctx.params[element.fieldname] ? (ctx.params[element.fieldname] + ',' + element.filename) : element.filename;
                            var res1 = {
                                type: element.mimetype,
                                filename: element.filename
                            }
                            responseArr.push(res1);
                            await this.calls3(ctx, element, function () {

                            });
                        });
                    }
                    let attachData = ctx.params.attachment;
                    if(ctx.params.attachment && attachData.length > 0) {
                        //ctx.params.attachment = ctx.params.attachment; 
                    } else {
                        ctx.params.attachment = responseArr;
                    }
                    var chat_message = '';

                    ctx.call("db.users.find", { query: { id: ctx.params.commented_by } })
                        .then(user_info => {
                            let topic = 'topic_channel_' + ctx.params.topic_id;
                            var comment_data = Object.assign({}, ctx.params);
                            comment_data.full_name = user_info[0].full_name;
                            comment_data.propic = user_info[0].propic;
                            comment_data.type = 'add comment';
                            comment_data.createdAt = moment.utc();
                            chat_message = comment_data;
                            // ctx.call('socket_service.sendMessage', { topic: topic, data: [comment_data] });

                            // COMMENT  
                            ctx.call('db.post.checkPostActiveStatus', ctx.params)
                                .then(async(activeRecords) => {
                                    //if (true) {
                                            let redisKeyprefix = "topic_active_"+ctx.params.topic_id+"_";
                                            let active_users = await _this.getActiveUsers(redisKeyprefix+'*');
                                            ctx.call('db.topics.get_topic_withusers', ctx.params)
                                            .then(async posts => {
                                                let { to_user_id } = posts[0];
                                                var created_by = posts[0].created_by;
                                                let _created_by = await ctx.call("db.users.find", { query: { id: posts[0].created_by }, fields: ["id", "full_name"] })
                                                try {
                                                    posts[0].topic_id = ctx.params.topic_id;
                                                    
                                                    if (to_user_id) {
                                                        let commented_users = to_user_id;
                                                        if(commented_users.indexOf(created_by) !== -1){
                                                            console.log('created user already exist');
                                                        } else{
                                                            commented_users.push(created_by);
                                                        }
                                                        commented_users.forEach(elem => {
                                                            let userbasedkey = redisKeyprefix+elem;
                                                            if(active_users.indexOf(userbasedkey+'_web') < 0 &&
                                                                active_users.indexOf(userbasedkey+'_android') < 0 &&
                                                                active_users.indexOf(userbasedkey+'_ios') < 0
                                                            ) {
                                                                let message = {
                                                                    topic: 'notification_' + elem,
                                                                    data: {
                                                                        type: 'chat_notification',
                                                                        message: 'Active Conversation Found in ',
                                                                        result: posts,
                                                                        comment_data: chat_message
                                                                    }
                                                                };
                                                                ctx.call('socket_service.sendMessage', message);
                                                                ctx.params = {
                                                                    ...ctx.params,
                                                                    to_id: elem,
                                                                    type_id: parseInt(posts[0].id),
                                                                    link_to: posts[0].id,
                                                                    message_title: 'familheey',
                                                                    body: `${comment_data.full_name} added a message in the conversation`,
                                                                    message: `<b>${comment_data.full_name}</b> added a message on <b>${_created_by[0].full_name}</b>'s conversation`,
                                                                    sub_type: 'conversation',
                                                                    category: 'conversation',
                                                                    type: 'topic',
                                                                    propic: comment_data.propic,
                                                                    from_id: user_info.length > 0 ? user_info[0].id : ''
                                                                }

                                                                if (elem == created_by) {
                                                                    ctx.params = {
                                                                        ...ctx.params,
                                                                        body: `<b>${comment_data.full_name}</b> added a message in the conversation</b>`,
                                                                        message: `<b>${comment_data.full_name}</b> added a message in the conversation</b>`
                                                                    }
                                                                }
                                                                if (commented_by != elem) {
                                                                    ctx.params = { ...ctx.params, to_user_id: elem, ntype: 'postComment' }
                                                                    ctx.broker.call("notification.sendAppNotification", ctx.params);
                                                                    this.pushNotify(ctx, ctx.params);
                                                                }
                                                            }
                                                            
                                                        });
                                                    }else{//public post
                                                        let _commentedBy = await ctx.call("db.post_comments.getCommentedUser",ctx.params)
                                                        let commentedUsers = _commentedBy.map(v => v.commented_by);
                                                        if(commentedUsers.indexOf(created_by) == -1) commentedUsers.push(created_by)
                                                        ctx.params = {
                                                            ...ctx.params,
                                                            type_id: parseInt(posts[0].id),
                                                            link_to: posts[0].id,
                                                            message_title: 'familheey',
                                                            body: `${comment_data.full_name} added a message in the conversation`,
                                                            message: `<b>${comment_data.full_name}</b> added a message on <b>${_created_by[0].full_name}</b>'s conversation`,
                                                            sub_type: 'conversation',
                                                            category: 'conversation',
                                                            type: 'topic',
                                                            propic: comment_data.propic,
                                                            from_id: user_info.length > 0 ? user_info[0].id : ''
                                                        }
                                                        console.log('---------------',active_users);
                                                        commentedUsers.forEach(u => {
                                                            let userbasedkey = redisKeyprefix+u;
                                                            if(active_users.indexOf(userbasedkey+'_web') < 0 &&
                                                                active_users.indexOf(userbasedkey+'_android') < 0 &&
                                                                active_users.indexOf(userbasedkey+'_ios') < 0
                                                            ) {
                                                                ctx.params.to_id = u;
                                                                if (u != commented_by) {
                                                                    ctx.broker.call("notification.sendAppNotification", ctx.params);
                                                                    this.pushNotify(ctx, ctx.params);
                                                                }
                                                            }
                                                            
                                                        });
                                                    }
                                                } catch (error) {
                                                    console.log(error);
                                                }
                                            })
                                    //}
                                    ctx.call('db.post_comments.create', ctx.params)
                                        .then(async(comment) => {
                                            comment_data.comment_id = comment.id;
                                            ctx.call('socket_service.sendMessage', { topic: topic, data: [comment_data] });
                                            let existRead = await ctx.call("db.post_comment_read.find",{ query:{topic_id:topic_id, user_id:commented_by} })
                                            if(existRead && existRead.length>0){
                                                ctx.call("db.post_comment_read.update",{ id:existRead[0].id, last_read_message_id:comment.id })
                                            }else{
                                                let save_data = {
                                                    last_read_message_id:comment.id,
                                                    topic_id:topic_id, 
                                                    user_id:commented_by
                                                }
                                                ctx.call("db.post_comment_read.create",save_data)
                                            }
                                            resolve({ message: 'success', data: comment });
                                            //Update date to sort-dates table start
                                            let sortdateObj = {
                                                type:'topic',
                                                type_id:parseInt(topic_id)
                                            };
                                            ctx.broker.call('common_methods.sort_dates_update', sortdateObj);
                                            //Update date to sort-dates table end
                                        }).catch((err) => {
                                            reject({ message: 'error' });
                                        });
                                });
                            // END
                        });
                });
            }
        },

        getCommentsByPost: {
            params: {
                user_id: "string"
            },
            handler(ctx) {
                let response = {}
                return new Promise((resolve, reject) => {
                    let{post_id,topic_id,user_id} = ctx.params;
                    if(post_id || topic_id) {
                        ctx.call('db.post_comments.getCommentsByPost', ctx.params)
                        .then((result) => {
                            response.data = result;
                            resolve(response);
                            // Last read comment update start
                            let maxCommentId = Math.max.apply(Math, result.map(function(o) { return o.comment_id; }));
                            if(maxCommentId && maxCommentId!=null && maxCommentId > 0) {
                                let updateCommentObj = {
                                    last_read_message_id:maxCommentId.toString(),
                                    user_id:user_id
                                }
                                if(topic_id && topic_id!=null && topic_id!= '') {
                                    updateCommentObj.topic_id = topic_id;
                                } else {
                                    updateCommentObj.post_id = post_id;
                                }
                                ctx.call("posts.updateLastReadMessage",updateCommentObj)
                                .then((res)=>{
                                    console.log('Updated Last Comment successfully');
                                })
                                .catch((err) => {
                                    console.log(err);
                                    reject('Updated Last Comment has some error:',err);
                                });
                            }
                            // Last read comment update start
                        })
                        .catch((err) => {
                            reject(err);
                        });
                    } else {
                        reject("Parameter topic_id/post_id has been missing");   
                    }
                });
            }
        },
        spam_report: {
            params: {
                type_id: 'string',
                user_id: 'string',
                spam_page_type: 'string',
                description: 'string',
            },
            handler(ctx) {
                //let response = {}
                return new Promise((resolve, reject) => {
                    ctx.call('db.spam_report.create', ctx.params)
                        .then((result) => {
                            ctx.call('db.spam_report.check', { post_id: ctx.params.type_id, user_id: ctx.params.user_id })
                                .then((result2) => {
                                    resolve({ message: 'success', data: result2 });
                                });

                        }).catch((err) => {
                            console.log(err);
                            reject({ message: 'error', data: err });
                        });
                });

            }
        },
        get_my_post: {
            handler(ctx) {
                return new Promise((resolve, reject) => {
                    let { query, post_id } = ctx.params;
                    let offsetval = ctx.params.offset ? ctx.params.offset : 0;
                    let limitval = ctx.params.limit ? ctx.params.limit : 20;
                    let grpIdOriginIdArr = {};

                    if(post_id) {
                        //Check user permission
                        ctx.call('db.post.checkUserPostPermission', ctx.params)
                        .then((checkStatus) => {
                            if(checkStatus != null && checkStatus.length > 0){
                                get_post_by_user(ctx);     
                            } else {
                                resolve({ message: 'success', data: [] });
                            }
                        })
                        .catch((err) => {
                            console.log(err);
                            reject({ message: 'error', data: err });
                        });
                    } else {
                        get_post_by_user(ctx);    
                    }

                    function get_post_by_user(ctxFun) {
                        ctx.call('db.post.post_by_user', { user_id: ctx.params.user_id, post_id: post_id, type: ctx.params.type, txt: query, offset:offsetval, limit:limitval })
                        .then((result) => {
                            result.map(v => {
                                if ((!grpIdOriginIdArr[v.post_ref_id + '_']) || v.post_ref_id == null) {
                                    grpIdOriginIdArr[v.post_ref_id + '_'] = {
                                        group_name: v.group_name,
                                        user_name: v.to_user_name
                                    };
                                    v.flag = true;

                                    // return true;
                                } else {
                                    if (v.group_name) {
                                        grpIdOriginIdArr[v.post_ref_id + '_'].group_name = grpIdOriginIdArr[v.post_ref_id + '_'].group_name + ',' + v.group_name;
                                    }
                                    if (v.group_name) {
                                        grpIdOriginIdArr[v.post_ref_id + '_'].user_name = grpIdOriginIdArr[v.post_ref_id + '_'].user_name + ',' + v.to_user_name;
                                    }
                                }
                                v.other_group_names = grpIdOriginIdArr[v.post_ref_id + '_'].group_name;
                                v.other_user_names = grpIdOriginIdArr[v.post_ref_id + '_'].user_name;

                                // v.other_group_names = grpIdOriginIdArr[v.post_ref_id + '_'].group_name

                            });
                            result = result.filter(v => {
                                v.other_group_names = grpIdOriginIdArr[v.post_ref_id + '_'].group_name
                                return v.flag;
                            }); 
                            resolve({ message: 'success', data: result });
                        });
                    }
                    
                });
            }
        },
        get_my_post_aggregate: {
            handler(ctx) {
                return new Promise((resolve, reject) => {
                    let { query, post_id,post_ref_id} = ctx.params;
                    let offsetval = ctx.params.offset ? ctx.params.offset : 0;
                    let limitval = ctx.params.limit ? ctx.params.limit : 20;

                    ctx.call('db.post.post_by_user', { user_id: ctx.params.user_id, post_id: post_id, type: ctx.params.type, txt: query, offset:offsetval, limit:limitval,post_ref_id:post_ref_id })
                    .then((result) => {
                        resolve({ message: 'success', data: result });      
                    });
                });
            }
        },
        post_by_user: {
            handler(ctx) {
                return new Promise((resolve, reject) => {
                    let { query, post_id } = ctx.params;
                    //let grpIdOriginIdArr = {};
                    ctx.call('db.post.post_by_user', { user_id: ctx.params.user_id, type: ctx.params.type, txt: query, post_id: post_id })
                        .then((result) => {
                            // result.map(v => {
                            //     if ((!grpIdOriginIdArr[v.post_ref_id + '_']) || v.post_ref_id == null) {
                            //         grpIdOriginIdArr[v.post_ref_id + '_'] = {
                            //             group_name: v.group_name
                            //         };
                            //         v.flag = true;

                            //         // return true;
                            //     } else {
                            //         grpIdOriginIdArr[v.post_ref_id + '_'].group_name = grpIdOriginIdArr[v.post_ref_id + '_'].group_name + ',' + v.group_name;
                            //     }
                            //     v.other_group_names = grpIdOriginIdArr[v.post_ref_id + '_'].group_name;

                            //     // v.other_group_names = grpIdOriginIdArr[v.post_ref_id + '_'].group_name

                            // });
                            // result = result.filter(v => {
                            //     v.other_group_names = grpIdOriginIdArr[v.post_ref_id + '_'].group_name
                            //     return v.flag;
                            // });
                            resolve({ message: 'success', data: result });
                        });
                });
            }
        },
        getSidebarCount: {
            handler(ctx) {
                return new Promise((resolve, reject) => {
                    //let { query } = ctx.params;
                    async.parallel({
                        post_count: function (callback) {
                            ctx.call('db.post.post_feed_count', { user_id: ctx.params.user_id, type: 'post', })
                                .then((result) => {
                                    callback(null, result[0].count);
                                })
                        },
                        announcement_count: function (callback) {
                            ctx.call('db.post.post_feed_count', { user_id: ctx.params.user_id, type: 'announcement' })
                                .then((result) => {
                                    callback(null, result[0].count);
                                    // resolve({ message: 'success', data: result });

                                }).catch((err) => {
                                    console.log(err);
                                    reject({ message: 'error', data: err });
                                });
                        },
                        family_count: function (callback) {
                            ctx.call('db.group_map.family_count', { user_id: ctx.params.user_id })
                                .then((result) => {
                                    let count = 0;
                                    if (result && result.length > 0) {
                                        count = result[0].count;
                                    }
                                    callback(null, count);
                                });
                        },
                        my_event_count: function (callback) {
                            ctx.call('events.createdByMe', {
                                user_id: ctx.params.user_id
                            })
                            .then((result) => {
                                let { my_event_list } = result.data;
                                let _count = my_event_list['created_by_me'].length + my_event_list['rsvp_yes'].length + my_event_list['rsvp_maybe'].length
                                callback(null, _count);
                            });
                        },
                        requests_count: function (callback) {
                            ctx.call('db.post_requests.countRequestNeed', {
                                user_id: ctx.params.user_id
                            })
                            .then((result) => {
                                let { count } = result[0];
                                callback(null, parseInt(count) );
                            });
                        }

                    }, function (err, results) {
                        if (err) {
                            console.log('Post by family is failed ==> ', err);
                            reject(err);
                        } else {
                            // console.log('@#@@@@@@@@@', results);
                            // results.data = results.by_family.concat(results.public);
                            resolve(results);
                        }
                    });
                })
            }
        },
        global_search_feed: {
            handler(ctx) {
                return new Promise((resolve, reject) => {
                    //let { query } = ctx.params;
                    async.parallel({
                        by_family: function (callback) {
                            if (!ctx.params.requestFrom) {
                                let postCallFunction = 'db.post.post_by_family';
                                if(ctx.params.type == 'announcement') {
                                    postCallFunction = 'db.post.announcement_by_family';
                                }
                                ctx.call(postCallFunction, { to_group_id: ctx.params.group_id, user_id: ctx.params.user_id, type: ctx.params.type, txt: ctx.params.txt, offset: ctx.params.offset, limit: ctx.params.limit })
                                    .then((result) => {
                                        callback(null, result);
                                    })
                            } else {
                                callback(null, []);
                            }
                        },
                        by_public_feed: function (callback) {
                            ctx.call('db.post.publicFeed', { user_id: ctx.params.user_id, type: ctx.params.type, txt: ctx.params.txt, offset: ctx.params.offset, limit: ctx.params.limit })
                                .then((result) => {
                                    callback(null, result);
                                })
                        }
                    }, function (err, results) {
                        if (err) {
                            console.log('Post by family is failed ==> ', err);
                            reject(err);
                        } else {
                            results.data = results.by_family.concat(results.by_public_feed);
                            resolve(results);
                        }
                    });
                })
            }
        },
        unread: {
            params: {
                post_id: 'string',
                user_id: 'string',
                read_status: 'string'
            },
            handler(ctx) {
                let up_data = {
                    params: ctx.params,
                    where: { user_id: ctx.params.user_id, post_id: ctx.params.post_id }
                };
                return new Promise((resolve, reject) => {
                    ctx.call('db.post_view.find', { query: { post_id: ctx.params.post_id, user_id: ctx.params.user_id } })
                    .then(result => {
                        if (result.length == 0) {
                            ctx.call('posts.addViewCount', ctx.params)
                            .then((post_view) => {
                                resolve({ message: 'success', data: post_view });
                            });
                        } else {
                            ctx.call('db.post_view.update_post_views', up_data)
                            .then((post_view) => {
                                resolve({ message: 'success', data: post_view });
                            });
                        }
                    })
                    .catch((err) => {
                        reject(err)
                    });
                })
            }
        },
        announcement_list: {
            handler(ctx) {
                return new Promise((resolve, reject) => {
                    let { query, user_id, group_id, read_status, offset, limit } = ctx.params;

                    ctx.params = {
                        ...ctx.params,
                        txt:query,
                        offset:offset?offset:0,
                        limit:limit?limit:50
                    }

                    if(group_id) ctx.params = { ...ctx.params, to_group_id: group_id }

                    let response = {
                        data:[],
                        read_announcement:[],
                        unread_announcement:[]
                    }

                    ctx.call('db.post.announcement_by_family', ctx.params )
                    .then((results) => {
                        if(read_status===true){
                            response = { ...response, read_announcement:results }
                        }else if(read_status===false){
                            response = { ...response, unread_announcement:results }
                        }else{
                            let _unread = results.filter(res=>res.read_status === true) 
                            let _read   = results.filter(res=>res.read_status !== true) 
                            response = { ...response, read_announcement:_read, unread_announcement:_unread }
                        }
                        resolve(response)
                        ctx.call('posts.addUpdateAnnounceSeen', { user_id: user_id });
                    })
                    .catch((err)=>{
                        reject(err)
                    });
                })
            }
        },

        post_by_family: {
            handler(ctx) {
                //let response = {}
                return new Promise(async(resolve, reject) => {
                    let { query, group_id } = ctx.params;
                    console.log('KiD --> Start : ', new Date());

                    let _Group = [] 
                    if(group_id) _Group = await ctx.call("db.groups.find",{ query:{ id:ctx.params.group_id }, fields:["post_visibilty","sticky_post"] })
                    async.parallel({
                        data: function (callback) {
                            let grpIdOriginIdArr = {};
                            console.log('KiD --> DB Call : ', new Date());
                            let postCallFunction = 'db.post.post_by_family';
                            if(ctx.params.type == 'announcement') {
                                postCallFunction = 'db.post.announcement_by_family';
                            }
                            ctx.call(postCallFunction, { to_group_id: ctx.params.group_id, user_id: ctx.params.user_id, type: ctx.params.type, txt: query, post_visibilty:(_Group.length>0)?_Group[0].post_visibilty:'', offset: ctx.params.offset, limit: ctx.params.limit,sticky_post:(_Group.length>0)?_Group[0].sticky_post:'', read_status:ctx.params.read_status })
                                .then((result) => {
                                    console.log('KiD --> DB Response : ', new Date());
                                    result.map(v => {
                                        if ((!grpIdOriginIdArr[v.orgin_id + '_' + v.to_group_id + '_' + v.to_user_id]) || v.orgin_id == null) {
                                            grpIdOriginIdArr[v.orgin_id + '_' + v.to_group_id + '_' + v.to_user_id] = true;
                                            v.flag = true;
                                            return true;
                                        }
                                    });
                                    result = result.filter(v => {
                                        return v.flag;
                                    });

                                    callback(null, result);

                                }).catch((err) => {
                                    console.log(err);
                                });
                        },
                        // announcement_count: function (callback) {
                        //     ctx.call('db.post.getNewAnnouncementCount', { user_id: ctx.params.user_id })
                        //         .then((result) => {
                        //             callback(null, result);
                        //         }).catch((err) => {
                        //             console.log(err);
                        //         });
                        // }
                    }, function (err, results) {
                        if (err) {
                            console.log('Post by family is failed ==> ', err);
                            reject(err);
                        } else {
                            console.log('KiD --> Resolve : ', new Date());
                            resolve(results);
                        }
                    });

                });

            }
        },
        publicFeed: {
            handler(ctx) {
                //let response = {}
                return new Promise((resolve, reject) => {
                    let { query } = ctx.params;
                    ctx.call('db.post.publicFeed', { user_id: ctx.params.user_id, type: ctx.params.type, txt: query, offset: ctx.params.offset, limit: ctx.params.limit })
                    .then((result) => {
                        resolve({ message: 'success', data: result });

                    }).catch((err) => {
                        console.log(err);
                        reject({ message: 'error', data: err });
                    });
                });

            }
        },
        listAll: {

            handler(ctx) {
                //let response = {}
                return new Promise((resolve, reject) => {
                    ctx.call('db.post.listAll', {})
                        .then((result) => {
                            // console.log('getByID', result)
                            resolve({ message: 'success', data: result });

                        }).catch((err) => {
                            console.log(err);
                            reject({ message: 'error', data: err });
                        });
                });

            }
        },
        like: {
            params: {
                post_id: 'string',
                user_id: 'string'
            },
            handler(ctx) {
                //let response = {}
                return new Promise((resolve, reject) => {
                    ctx.call('db.post_like.find', { user_id: ctx.params.user_id, post_id: ctx.params.post_id })
                        .then(res => {
                            if (res.length > 0) {
                                res[0].like_status = !res[0].like_status;
                                ctx.call('db.post_like.update', { id: res[0].id, like_status: res[0].like_status })
                                    .then((result) => {
                                        // console.log('getByID', result)
                                        resolve({ message: 'success', data: result });

                                    }).catch((err) => {
                                        console.log('err ====================');
                                        console.log(err);
                                        reject({ message: 'error', data: err });
                                    });
                            } else {
                                ctx.call('db.post_like.create', ctx.params)
                                    .then((result) => {
                                        // console.log('getByID', result)
                                        resolve({ message: 'success', data: result });

                                    }).catch((err) => {
                                        console.log(err);
                                        reject({ message: 'error', data: err });
                                    });
                            }
                        });
                })

            }
        },

        /**
         * shares a post to multiple groups/ multiple users
         */
        postShare: {
            params: {
                shared_user_id: "string",
                post_id: "string"
            },
            handler(ctx) {
                let { post_id, to_group_id, to_user_id, shared_user_id } = ctx.params;
                return new Promise(async(resolve, reject) => {
                    ctx.call('db.post.find', { query: { id: post_id } })
                    .then((post) => {
                        let createPermission; let postArr = []; let i=0;
                        if (post && post.length > 0) {
                            if (!post[0].orgin_id) { ctx.params.orgin_id = post_id }
                            ctx.params.from_id = post_id;
                            ctx.params = { ...ctx.params, ntype: `${post[0].type}_share` }
                            ctx.params.created_by = ctx.params.shared_user_id;
                            ctx.params.post_type=post[0].post_type;  //this line is added by sarath on 25/02/2021 for adding the value of post_type to the parameters
                            if (to_group_id && to_group_id.length>0) {
                                to_group_id.forEach(async e => {
                                    let groupSettings = await ctx.broker.call("db.groups.getGroupSettings", { group_id: parseInt(e.id) });
                                    let usertype = await ctx.call("db.group_map.find", { query: { group_id: e.id, user_id: shared_user_id }, fields:["type"] });
                                    let tmp = { ...post[0], ...ctx.params }
                                    let save_data = Object.assign({}, tmp);
                                    delete save_data.id; delete save_data.createdAt; delete save_data.updatedAt; delete save_data.sort_date
                                    save_data.to_group_id = e.id;
                                    save_data.to_user_id = null;
                                    save_data.is_approved = 0;
                                    if(post[0].type == 'post'){
                                        createPermission = groupSettings.filter((el) => el.key === "POST_CREATE");
                                        if(usertype[0].type == 'admin' && createPermission[0].id == 7 ){
                                            save_data.is_approved = 1;
                                        }
                                        if( ( usertype[0].type == 'admin' || usertype[0].type == 'member') && createPermission[0].id == 6 ){
                                            save_data.is_approved = 1;
                                        }
                                    }else if(post[0].type == 'announcement'){
                                        createPermission = groupSettings.filter((el) => el.key === "ANNOUNCEMENT_CREATE");
                                        if(usertype[0].type == 'admin' && createPermission[0].id == 17 ){
                                            save_data.is_approved = 1;
                                        }
                                        if( ( usertype[0].type == 'admin' || usertype[0].type == 'member') && createPermission[0].id == 18 ){
                                            save_data.is_approved = 1;
                                        }
                                    }
                                    save_data.is_approved && postArr.push(save_data);
                                    i++;
                                    if( i==to_group_id.length ){
                                        ctx.params = { ...ctx.params, postArr:postArr }
                                        resolve(await this.postShareFn(ctx));
                                    }
                                }); 
                            }
                            else if (to_user_id && to_user_id.length > 0) { console.log("== user = = =")
                                i=0;
                                let tmp = { ...post[0], ...ctx.params }
                                let save_data = Object.assign({}, tmp);
                                delete save_data.id; delete save_data.createdAt; delete save_data.updatedAt; delete save_data.sort_date
                                to_user_id.forEach(async e => {
                                    save_data.to_user_id = e;
                                    save_data.to_group_id = null;
                                    postArr.push(save_data);
                                    i++;
                                    if(i==to_user_id.length){
                                        ctx.params = { ...ctx.params, postArr:postArr }
                                        resolve(await this.postShareFn(ctx));
                                    }
                                });
                            }
                        } else {
                            reject("Post not found")
                        }
                    })
                    .catch((err) => {
                        reject(err);
                    });
                });
            }
        },

        list_post_views: {
            params: {
                post_id: "string",
            },
            handler(ctx) {
                return new Promise((resolve, reject) => {
                    ctx.call('db.post.list_post_views', ctx.params)
                        .then((post_view) => {
                            resolve({ message: 'success', data: post_view });
                        });
                })
            }
        },
        addViewCount: {
            params: {
                user_id: "string",
                post_id: "string",
            },
            handler(ctx) {
                let { read_status, user_id, post_id } = ctx.params;
                return new Promise((resolve, reject) => {
                    ctx.call('db.post_view.find', { query: { post_id: post_id, user_id: user_id } })
                    .then(result => {
                        let params_are = ctx.params;
                        let _Type = `create`;
                        if (result.length != 0) {
                            params_are = {
                                params: { read_status: read_status },
                                where: { user_id: user_id, post_id: post_id }
                            };
                            _Type = `update_post_views`;
                        }
                        ctx.call(`db.post_view.${_Type}`, params_are)
                        .then((post_view) => {
                            resolve({ message: 'success', data: post_view });
                        });
                    })
                    .catch((err_avc)=>{
                        reject(err_avc)
                    })
                });
            },
        },
        mute_conversation: {
            params: {
                post_id: "string",
                user_id: "string",
            },
            handler(ctx) {
                return new Promise((resolve, reject) => {
                    ctx.call('db.post_notification_settings.find', { query: { post_id: ctx.params.post_id, user_id: ctx.params.user_id } })
                        .then(result => {
                            if (result.length == 0) {
                                ctx.call('db.post_notification_settings.create', ctx.params)
                                    .then(result2 => {
                                        resolve({ message: 'success', data: result2 });
                                    });
                            } else {
                                ctx.params.id = result[0].id;
                                ctx.params.is_active = !result[0].is_active;
                                ctx.call('db.post_notification_settings.update', ctx.params)
                                    .then(result3 => {
                                        resolve({ message: 'success', data: result3 });
                                    });
                            }
                        });
                });
            }
        },
        updateLastReadMessage: { 
            params: {
                last_read_message_id: "string",
                user_id: "string",
            },
            handler(ctx) {
                return new Promise((resolve, reject) => {
                    let{post_id,topic_id} = ctx.params
                    let querObj = {};
                    if(post_id || topic_id) { 
                        if(topic_id && topic_id != null) {
                            querObj = {
                                topic_id: ctx.params.topic_id,
                                user_id: ctx.params.user_id   
                            }
                        } else {
                            querObj = {
                                post_id: ctx.params.post_id,
                                user_id: ctx.params.user_id   
                            }
                        }
                        
                        ctx.call('db.post_comment_read.find', {
                            query: querObj
                        })
                        .then(duplicate => {
                            if (duplicate.length == 0) {
                                ctx.call('db.post_comment_read.create', ctx.params)
                                .then(result => {
                                    resolve({ message: 'success', data: result });
                                });
                            } else {
                                ctx.params.id = duplicate[0].id;
                                ctx.call('db.post_comment_read.update', ctx.params)
                                .then(result => {
                                    resolve({ message: 'success', data: result });
                                });
                            }
                        });
                    }
                });
            }
        },
        getSharedUserList: {
            params: {
                post_id: "string",
                group_id: "string",
            },
            handler(ctx) {
                return new Promise((resolve, reject) => {
                    ctx.call('db.post.getSharedUserList', ctx.params)
                        .then(result => {
                            resolve(result);
                        });
                });
            }
        },
        getCommonSharedUserList: {
            params: {
                origin_post_id: "string",
            },
            handler(ctx) {
                return new Promise((resolve, reject) => {
                    ctx.call('db.post.getCommonSharedUserList', ctx.params)
                        .then(result => {
                            resolve(result);
                        });
                });
            }
        },
        getSharedUserListByUserid: {
            params: {
                post_id: "string",
                user_id: "string",
            },
            handler(ctx) {
                return new Promise((resolve, reject) => {
                    ctx.call('db.post.getSharedUserListByUserid', ctx.params)
                        .then(result => {
                            resolve(result);
                        })
                        .catch((err) => {
                            console.log(err);
                            reject(err);
                        });
                });
            }
        },
        generateSignedUrl :{
            params: {
                filename: "string",
                filetype:"string"
            },
            handler(ctx) {
                let _this = this;
                return new Promise((resolve, reject) => {
                    ctx.params.filename=ctx.params.filename.replace(/ /g,"_");
                    let file_nm_dt = ctx.params.filename.split('.');
                    console.log(file_nm_dt);
                    let file_nm = file_nm_dt.slice(0, -1).join('.') + '_' +_this.generateFilekey(5) + '.' + file_nm_dt.slice(-1)[0];
                    var params = {
                        Bucket: process.env.AWS_BUCKET,
                        Key: file_nm,
                        Expires: 200, // in seconds
                        ContentType: ctx.params.filetype
                    };
                    ctx.call('s3upload.generateUrl', params)
                    .then((res) => {
                        resolve(res);
                    });
                }).catch((err) => {
                    console.log(err);
                });
            }
        },
        uploadFileToS3: {
            params: {
                name: "string"
            },
            handler(ctx) {
                return new Promise((resolve, reject) => {
                    let _this = this;
                    let responseArr = [];
                    let count=0;
                    if (ctx.meta && ctx.meta.file) {
                        ctx.meta.file.forEach(element => { 
                            let is_video = element.mimetype.split("/")[0]
                            var filename = element.filename.split("file-");
                            ctx.params.file_type = element.mimetype; //file mimetype
                            element.fieldname = ctx.params.name
                            element.filename = ctx.params.name + '-' + filename[1]
                            element.video_thumb = `default_video.jpg`;

                            async.parallel({
                                video_thumb: (callback) => {
                                    if (is_video == 'video')
                                        ctx.broker.call('s3upload.generateVideoThumb', element)
                                            .then(res => {
                                                element.video_thumb = res;
                                                callback(null, res);
                                            }).catch(err => {
                                                callback(null, '');
                                                console.log("error happened -------- familyMembers", err);
                                            })
                                    else
                                        callback(null, '');
                                },
                                _calls3: (callback) => {
                                    _this.calls3(ctx, element, function (err, res) {
                                        if (res) {
                                            res = { ...res,message: res.message,type: element.mimetype,filename: element.filename }
                                            fs.unlinkSync(element.path)
                                            callback(null, res);
                                        } else callback(null, []);
                                    })
                                }
                            }, function (err, result) {
                                count++;
                                if(result.video_thumb!=''){
                                    result._calls3 = {...result._calls3, video_thumb:result.video_thumb }
                                }
                                responseArr.push(result._calls3)
                                let _response = { message:'success',status:"done", data:responseArr }
                                if(err) reject(err)
                                else{
                                    if(count == ctx.meta.file.length) resolve(_response)      
                                }
                            });  
                        });
                    }
                })
            }
        },
        checkPostPermission: {
            params: {
                user_id: "string"
            },
            handler(ctx) {
                return new Promise((resolve, reject) => {
                    var datalength = 0;
                    ctx.call('db.post.check_post_permission', ctx.params)
                        .then((permissionData) => {
                            if (permissionData.length) {
                                datalength = permissionData.length
                            }
                            resolve({ message: 'success', data: permissionData, datalength: datalength });
                        })
                        .catch((err) => {
                            console.log(err);
                            reject({ message: 'error', data: err });
                        });
                })
            }
        },
        deletePost: {
            params: {
                id: "string",
                user_id:"string"
            },
            handler(ctx) {
                return new Promise(async(resolve, reject) => {
                    let { user_id } = ctx.params;
                    let isAdmin = await ctx.call("db.post.find",{ query:{created_by:user_id} })
                    if(isAdmin && isAdmin.length>0){
                        ctx.params = { ...ctx.params, is_active:false }
                        ctx.call('db.post.update', ctx.params)
                        .then(result => {
                            resolve("Success");
                            //Delete post from elastic search start
                            let { elasticsearch_id } = result;
                            //ctx.call("db.post.find",{query:{id:id},fields: ["elasticsearch_id"]})
                            //.then(elasticId => {
                                //if(elasticId.length > 0) {
                                    let elasticObj = {
                                        index : 'post',
                                        id : elasticsearch_id,
                                        data : result
                                    } 
                                    ctx.broker.call('es_service.updateRecord', elasticObj)
                                    .then((elasticRes) => {  
                                        console.log('Successfully updated elastic search',elasticRes);   
                                    })
                                    .catch((err) => {
                                        console.log("Elastic search updation has some error",err);
                                    });
                                //}
                            //})
                            //.catch((err) => {
                               // console.log("Getting elastic search id has some error",err);
                           // });
                            //Delete post from elastic search end
                        })
                        .catch((err) => {
                            console.log(err);
                            reject(err);
                        });
                    }else{
                        reject("Not Authorised")
                    }
                })
            }
        },
        remove_post: {
            params: {
                post_id: "string",
                user_id: "string"
            },
            handler(ctx) {
                return new Promise((resolve, reject) => {
                    ctx.call('db.user_removed_post.create', ctx.params)
                    .then(result => {
                        resolve("Success");
                    })
                    .catch((err) => {
                        console.log(err);
                        reject(err);
                    });
                })
            }
        },
        getFamilyListForPost: {
            params: {
                user_id: 'string'
            },
            handler(ctx) {
                return new Promise((resolve, reject) => {
                    ctx.call('db.post.getFamilyListForPost', ctx.params)
                    .then(result => {
                        resolve({ message: "Success", data: result });
                    })
                    .catch((err) => {
                        console.log(err);
                        reject(err);
                    });
                });
            }
        },
        pendingApprovals: {
            params: {
                group_id: 'string',
                type: 'string'
            },
            handler(ctx) {
                return new Promise((resolve, reject) => {
                    ctx.call('db.post.pendingApprovals', ctx.params)
                    .then(result => {
                        resolve({ message: "Success", data: result });
                    })
                    .catch((err) => {
                        console.log(err);
                        reject(err);
                    });
                });
            }
        },
        approve_post: {
            params: {
                post_id: 'string',
                is_approved: 'string'
            },
            handler(ctx) {
                return new Promise((resolve, reject) => {
                    ctx.call('db.post.update', { id: ctx.params.post_id, is_approved: parseInt(ctx.params.is_approved) })
                        .then(result => {
                            resolve({ message: "Success", data: result });
                        })
                        .catch((err) => {
                            console.log(err);
                            reject(err);
                        });
                });
            }
        },
        delete_comment: {
            params: {
                comment_id: 'array'
            },
            handler(ctx) {
                return new Promise((resolve, reject) => { 
                    let{post_id,topic_id} = ctx.params
                    if(post_id || topic_id) {
                        ctx.call('db.post_comments.bulkCommentDelete', ctx.params)
                        .then(result => {
                            resolve({ message: "Successfully deleted comments", data: result });
                            /* socket function start */
                            // ctx.params.comment_id.forEach(element => {
                            //     let delTopic = 'post_channel_' + ctx.params.post_id;
                            //     let delSocket_data = {
                            //         type: 'delete_comment',
                            //         message: 'Deleted comment',
                            //         delete_id: [element]
                            //     };
                            //     ctx.call('socket_service.sendMessage', { topic: delTopic, data: [delSocket_data] });
                            // });
                            let delTopic = '';
                            if(topic_id && topic_id != null) {
                                delTopic = 'topic_channel_' + ctx.params.topic_id;   
                            } else {
                                delTopic = 'post_channel_' + ctx.params.post_id;
                            }
                            let delSocket_data = {
                                type: 'delete_comment',
                                message: 'Deleted comment',
                                delete_id: ctx.params.comment_id
                            };
                            ctx.call('socket_service.sendMessage', { topic: delTopic, data: [delSocket_data] });
                            /* socket function end */
                        })
                        .catch((err) => {
                            console.log(err);
                            reject(err);
                        });
                    }
                }); 
            }
        },

        addUpdateAnnounceSeen: {
            params: {
                user_id:'string'
            },
            handler(ctx) {
                return new Promise((resolve,reject) => {
                    ctx.call("db.announcement_banner.find", { query: { user_id: ctx.params.user_id }, fields: ["id", "last_seen_date"] })
                    .then(res => {
                        if(res.length > 0) {
                            let todayTimestamp = moment.utc();
                            ctx.params.last_seen_date = todayTimestamp;
                            ctx.params.id = res[0].id;
                            ctx.call("db.announcement_banner.update", ctx.params)
                            .then(res2 => {
                                resolve( {message:"Successfully updated announcement banner"});
                            })
                            .catch(err => {
                                console.log(err);
                                reject(err);   
                            }) 
                        } else {
                            ctx.call("db.announcement_banner.create", { user_id: ctx.params.user_id })
                            .then(res3 => {
                                resolve({message:'Successfully created announcement banner'});
                            })
                            .catch(err => {
                                console.log(err);
                                reject(err);   
                            })
                        }
                    })
                    .catch(err => {
                        console.log(err);
                        reject(err);   
                    })
                })  
            } 
        },

        announceBannerCount: {
            params: {
                user_id:'string'
            },
            handler(ctx) {
                return new Promise((resolve,reject) => {
                    ctx.call("db.announcement_banner.find", { query: {user_id: ctx.params.user_id}, fields: ["last_seen_date"] })
                    .then(res => {
                        let lastSeenDate = res.length > 0 ? res[0].last_seen_date : '';
                        if(lastSeenDate) {
                            ctx.call("db.post.announceBannerCount", { user_id: ctx.params.user_id,last_seen_date:moment(lastSeenDate).format('YYYY-MM-DD HH:mm:ss') })
                            .then(countData => {
                                resolve(countData);
                            })
                            .catch(err => {
                                console.log(err);
                                reject(err);   
                            })  
                        } else {
                            ctx.call("db.post.announceBannerCount", { user_id: ctx.params.user_id,last_seen_date:'' })
                            .then(countData => {
                                resolve(countData);
                            })
                            .catch(err => {
                                console.log(err);
                                reject(err);   
                            })  
                        } 
                    })
                    .catch(err => {
                        console.log(err);
                        reject(err);   
                    })
 
                })    
            }
        },
        getPostdefaultImage: {
            params: {
                post_type:'string'
            },
            handler(ctx) {
                return new Promise((resolve,reject) => {
                    let {post_type} = ctx.params;
                    ctx.call("db.post_default_image_lookup.find", { query: {post_type: post_type}, fields: ["id","filename","post_type","height","width","type"] })
                    .then(res => {
                        resolve(res);
                    })
                    .catch(err => {
                        console.log(err);
                        reject(err);   
                    })
                });
            }  
        },
        post_sortdate_update: {
            params:{
                post_id:"any"
            },
            handler(ctx){
                return new Promise((resolve,reject) => {
                    let currentTimestamp = moment.utc();
                    let updateObj = {
                        id : ctx.params.post_id,
                        sort_date : currentTimestamp
                    };
                    ctx.call("db.post.update",updateObj)
                    .then((res)=>{
                        resolve('Post sort date updated successfully');   
                    })
                    .catch((err) => {
                        console.log(err);
                        reject('Post sort date update has some error:',err);
                    });
                });
            }
        },
        /**
         * create/update the sticky 
         * for post
         */
        stickyPost:{
            params:{
                post_id:"string",
                group_id:"string",
                type:'string'
            },
            handler(ctx){
                let { post_id, group_id, type } = ctx.params;
                return new Promise((resolve,reject)=>{
                    ctx.call("db.groups.find",{ query:{ id:group_id }, fields:["id","sticky_post"] })
                    .then((groups)=>{
                        let { id,sticky_post } = groups && groups[0]; console.log(" sticky_post ", sticky_post)
                        post_id = parseInt(post_id);
                        var valueExist = sticky_post && sticky_post.indexOf(post_id);

                        switch (type) {
                            case 'stick':
                                if(sticky_post && sticky_post.length>0){
                                    if(valueExist!=-1){
                                        reject("already exist as sticky post");
                                        return;
                                    } 
                                    let len = sticky_post.length;
                                    if(len==10){
                                        sticky_post.pop();// remove alst index value if array has  10 value
                                    }
                                }else{
                                    sticky_post = []; 
                                }
                                sticky_post.unshift(post_id);
                                
                            break;
                            case 'unstick':
                                if(valueExist!=-1){
                                    sticky_post.splice(valueExist, 1);
                                } 
                            break;
                        
                            default:
                            break;
                        }
                        let params = { id:id, sticky_post:sticky_post }
                        return ctx.call("db.groups.update",params)
                    }).then((doc)=>{
                        resolve({ code:200, message:"success", data:doc })
                    }).catch((err)=>{
                        reject(err)
                    })
                })
            }
        },
        post_sticky_by_family: {
            params:{
                user_id:"string",
                group_id:"string"
            },
            handler(ctx){
                return new Promise(async(resolve,reject) => {
                    let {group_id} = ctx.params;
                    let _Group = [] 
                    if(group_id) _Group = await ctx.call("db.groups.find",{ query:{ id:ctx.params.group_id }, fields:["post_visibilty","sticky_post"] })
                    ctx.params.post_visibilty = _Group.length > 0 ? _Group[0].post_visibilty:'';
                    ctx.call("db.groups.find",{ query:{ id:group_id }, fields:["post_visibilty","sticky_post"] })
                    .then((groupRes)=>{
                        if(groupRes.length > 0) {
                            if(groupRes[0].sticky_post && groupRes[0].sticky_post.length > 0) {
                                ctx.params.sticky_post = groupRes[0].sticky_post;
                                ctx.call("db.post.post_sticky_by_family",ctx.params)
                                .then((res)=>{
                                    resolve(res);   
                                })
                                .catch((err) => {
                                    reject(err);
                                });  
                            } else {
                                resolve([]);
                                //reject("No sticky post in this family");     
                            }
                        } else {
                            reject('Group not exist');   
                        }  
                    })
                    .catch((err) => {
                        reject(err);
                    });       
                });
            }
        },
        get_user_commented_post:{
            params:{
                user_id:"string"
            },
            handler(ctx){
                return new Promise(async(resolve,reject) => {
                    //let {user_id} = ctx.params;
                    ctx.call("db.post.get_user_commented_post",ctx.params)
                    .then(async(res)=>{
                        //Sort based on last commented date start
                        await res.sort((a, b) => {
                            if (a.sort_id > b.sort_id) {
                                return -1;
                            } else if(a.sort_id < b.sort_id) {
                                return 1 ;  
                            } else {
                                return 0;
                            }
                        })
                        //Sort based on last commented date end
                        resolve(res);   
                    })
                    .catch((err) => {
                        reject('Get commented post has some error:',err);
                    });
                })
            }   
        },
        auto_update_publicpost_elasticSearch: {
            params:{

            },
            handler(ctx){
                return new Promise(async(resolve,reject) => {
                    ctx.call("db.post.get_null_elasticSearch_public_post",ctx.params)
                    .then(async(res)=>{
                        res.forEach(async elem => {
                            //push data to elastic search start
                            let elasticObj = {
                                index_name : 'post',
                                data : elem
                            }
                            ctx.broker.call('es_service.addRecords', elasticObj)
                            .then((elasticRes) => {
                                //Update post with elastic search ID start
                                ctx.call('db.post.update',{ id:elem.id, elasticsearch_id:elasticRes._id })
                                .then((linkUpdate) => {
                                    console.log("Successfully updated elastic search ID");
                                    resolve("Data sync started successfully");
                                })
                                .catch((err) => {
                                    console.log("search ID updation to post has some error", err);
                                })
                                //Update post with elastic search ID end
                            })
                            .catch((err) => {
                                console.log("Add post data to elastic search has some error", err);
                            })
                            //push data to elastic search end
                        })   
                    })
                    .catch((err) => {
                        reject('Get elestic search null post has some error:',err);
                    });
                })
            }   
        },
        get_notification_post_data:{
            params:{},
            handler(ctx){
                return new Promise((resolve,reject) => {
                    ctx.call("db.post.get_notification_post_data",ctx.params)
                    .then((res)=>{
                        resolve(res);   
                    })
                    .catch((err) => {
                        reject(err)
                    });
                });
            }
        },

        getpostsbyPostAttachment:{
            params:{
                filename:'string'
            },
            handler(ctx){
                return new Promise((resolve,reject) => {
                    ctx.call("db.post.getpostsbyPostAttachment",ctx.params)
                    .then((res)=>{
                        resolve(res);   
                    })
                    .catch((err) => {
                        reject(err)
                    });
                });
            }
        },

        /** for background fileupload updates the post */
        postUpdateAttachment: {
            params: {
                id: 'string'
            },
            handler(ctx) {
                let { id, post_attachment: P1 } = ctx.params;
                return new Promise((resolve, reject) => {
                    ctx.call("db.post.find", { query: { id: id }, fields: ["post_attachment"] })
                    .then((post) => {
                        if (post.length>0) {
                            let { post_attachment: P2 } = post[0];
                            ctx.params = {
                                ...ctx.params,
                                post_attachment: P1.concat(P2)
                            }
                            resolve({ code: 200, message: "Post Updated" })
                            ctx.call("db.post.update", ctx.params)
                            .then((result)=>{
                                //Elastic search updation start
                                let elasticSearchId = result.elasticsearch_id ? result.elasticsearch_id : '';
                                if(elasticSearchId != '' && elasticSearchId != null) {
                                    let elasticObj = {
                                        index : 'post',
                                        id : elasticSearchId,
                                        data : result
                                    } 
                                    ctx.broker.call('es_service.updateRecord', elasticObj)
                                    .then((elasticRes) => {  
                                        console.log('Successfully updated elastic search',elasticRes);   
                                    })
                                    .catch((err) => {
                                        console.log("Elastic search updation has some error",err);
                                    });
                                }
                                //Elastic search updation start 
                                //Notification start
                                if(ctx.params.is_active == true && P2.length == 0) {
                                    ctx.params.post_id = result.id;
                                    ctx.params.to_group_id = [result.to_group_id];
                                    ctx.params.to_user_id = result.to_user_id;
                                    ctx.params.shared_user_id = result.created_by;
                                    ctx.params.ntype = "post_create";
                                    ctx.params.post_ref_id = result.post_ref_id;
                                    ctx.params.post_type = result.privacy_type;
                                    this.appNotify(ctx);
                                }
                                //Notification end
                            })
                            .catch((err) => {
                                reject(err)
                            });
                        } else {
                            reject("Post not found!");
                        }
                    })
                    .catch((err) => {
                        reject(err)
                    });
                });
            }
        }
    },
    events: {},
    methods: {
        /**
         * call s3 service for fiel upload
         */
        calls3(ctx, params, callback) {
            ctx.call('s3upload.fileUpload', params)
                .then((res) => {
                    console.log("== s3uplod success ==", res);
                    callback('', res)
                })
                .catch((err) => {
                    callback(err, '')
                    console.log(err)
                })
        },
        keyGen() {
            var result = '';
            var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
            var charactersLength = characters.length;
            for (var i = 0; i < 10; i++) {
                result += characters.charAt(Math.floor(Math.random() * charactersLength));
            }
            return result + Date.now();
        },
        appNotify(ctx) {
            let _this = this;
            let { post_id, to_group_id, to_user_id, shared_user_id, ntype, post_ref_id, post_type, type_id } = ctx.params;
            async.parallel({
                fromUser: (callback) => {
                    if (shared_user_id)
                        ctx.call("db.users.find", { query: { id: shared_user_id }, fields: ["id", "full_name", "propic"] })
                            .then(res => {
                                callback(null, res);
                            }).catch(err => {
                                console.log("error happened -------- fromUser", err);
                            })
                    else callback(null, []);
                },
                tousers: function (callback) { // get memebers inside a group
                    if (to_group_id && to_group_id.length>0) {
                        let group_ids = to_group_id.map(elem => elem.id || elem.group_id)
                        ctx.broker.call('db.group_map.eventInviteGroup', { group_id: group_ids })
                            .then((res) => {
                                if (post_type == 'public') {
                                    const _unique = [...new Set(res.map(({ user_id }) => user_id))].map(e => res.find(({ user_id }) => user_id == e));
                                    callback(null, _unique);
                                }
                                else callback(null, res);
                            })
                            .catch((err) => {
                                console.log("to_group_id Search Error", err);
                            })
                    }
                    if (to_user_id && to_user_id.length>0) {
                        ctx.broker.call('db.users.find', { query: { id: to_user_id }, fields: ["id", "full_name", "propic"] })
                        .then((res) => {
                            callback(null, res);
                        })
                        .catch((err) => {
                            callback(null, []);
                            console.log("User Search Error", err);
                        })
                    }
                },
            }, function (err, result) {
                let { fromUser, tousers } = result;
                tousers.forEach(async elem => {
                    let sendObj = {
                        from_id: fromUser.length > 0 ? fromUser[0].id : '',
                        type: 'home',
                        propic: fromUser.length > 0 ? fromUser[0].propic : '',
                        to_id: elem.user_id ? elem.user_id : elem.id,
                        sub_type: 'familyfeed',
                        category: ntype,
                        message_title: 'familheey',
                        type_id: '',
                        link_to: post_id,
                        post_type:post_type,
                        group_id: elem.group_id ? elem.group_id : ''
                    }
                    var _params = { post_ref_id:post_ref_id };
                    var grpname;
                    var Post
                    switch (ntype) {
                        case 'post_share':
                            grpname = (to_group_id && to_group_id.length > 0) ? `to <b>${elem.group_name}</b>` : ''
                            sendObj.message = `<b>${fromUser[0].full_name}</b> has shared a post ${grpname}`
                            sendObj.sub_type = 'familyfeed';
                            sendObj.message_title = 'familheey';
                            sendObj.type_id = parseInt(post_id);
                            sendObj.link_to = parseInt(post_id);
                            break;
                        case 'announcement_share':
                            grpname = (to_group_id && to_group_id.length > 0) ? `to <b>${elem.group_name}</b>` : ''
                            sendObj.message = `<b>${fromUser[0].full_name}</b> has shared an announcement ${grpname}`
                            sendObj.sub_type = 'familyfeed';
                            sendObj.message_title = 'familheey';
                            sendObj.type_id = parseInt(post_id);
                            sendObj.link_to = parseInt(post_id);
                            sendObj.type = "announcement"
                            break;

                        case 'announcement_create':
                            if(post_type=='only_users') _params.to_user_id = elem.id;
                            if(post_type=='only_groups' || post_type=='all_family') _params.to_group_id = elem.group_id;
                            Post = await ctx.call("db.post.find",{query:_params, fields:['id']})
                            sendObj.type = 'announcement';
                            sendObj.message = `<b>${fromUser[0].full_name}</b> has added an announcement to <b>${elem.group_name}</b>`
                            sendObj.sub_type = '';
                            sendObj.message_title = 'familheey';
                            sendObj.type_id = parseInt(Post[0].id);
                            sendObj.link_to = parseInt(Post[0].id);
                            break;

                        case 'post_create':
                            if(post_type=='only_users') _params.to_user_id = elem.id;
                            if(post_type=='only_groups' || post_type=='all_family') _params.to_group_id = elem.group_id;
                            Post = await ctx.call("db.post.find",{query:_params, fields:['id',"post_type"]});
                            sendObj.privacy_type = Post.length>0 && Post[0].post_type == 'only_groups' ? 'private':'public';
                            sendObj.type = 'home';
                            sendObj.message = `<b>${fromUser[0].full_name}</b> has added a post to <b>${elem.group_name}</b>`
                            sendObj.sub_type = (to_group_id && to_group_id.length > 0) ? `familyfeed` : `publicfeed`;
                            sendObj.message_title = 'familheey';
                            if (post_type == 'public') {
                                sendObj.message = `${fromUser[0].full_name} has added a public post`
                            }
                            sendObj.type_id = parseInt(Post[0].id);
                            sendObj.link_to = parseInt(Post[0].id);
                            break;
                        case 'postComment':
                            sendObj.type = 'home';
                            sendObj.message = ctx.params.message;
                            sendObj.sub_type = ctx.params.sub_type;
                            sendObj.message_title = 'familheey';
                            sendObj.type_id = parseInt(type_id);
                            sendObj.link_to = parseInt(type_id);
                            break;
                        case 'post_create_connection':
                            sendObj.type = 'home';
                            sendObj.message = `<b>${fromUser[0].full_name}</b> has sent a post to you</b>`
                            sendObj.sub_type = (to_group_id && to_group_id.length > 0) ? `familyfeed` : `publicfeed`;
                            sendObj.message_title = 'familheey';
                            sendObj.type_id = parseInt(post_id);
                            sendObj.link_to = parseInt(post_id);
                            break;

                        default:
                            break;
                    }
                    if (fromUser[0].id != elem.user_id) {
                        // let pushnotifyType = ctx.params.post_type ? ctx.params.post_type : '';
                        ctx.broker.call("notification.sendAppNotification", sendObj);
                        let _user_notify = await ctx.call("db.users.find", { query: { id: sendObj.to_id }, fields: ["notification"] })
                        if (_user_notify.length > 0 && _user_notify[0].notification) {
                            _this.pushNotify(ctx, sendObj);
                        }
                    }
                });
            });
        },
        /**
         * push notification 
         */
        pushNotify(ctx, params) {
            ctx.broker.call("userops.getregisterToken", { user_id: params.to_id, is_active: true })
                .then((_devices) => {
                    _devices.data.forEach(elem => {
                        params = {
                            ...params,
                            r_id: elem.id,
                            token: elem.device_token,
                            device_type: elem.device_type.toLowerCase(),
                            title: params.message_title,
                            body: this.removeTags(params.message),
                            sub_type: params.sub_type ? params.sub_type : ''
                        }
                        ctx.broker.call("notification.sendPushNotification", params)
                    })
                })
                .catch(e => e)
        },
        removeTags(str) {
            if ((str === null) || (str === '')) return '';
            else return str.toString().replace(/(<([^>]+)>)/ig, '');
        },
        generateFilekey(len = 5) {
            var text = '';
            var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
          
            for (var i = 0; i < len; i++) {
              text += possible.charAt(Math.floor(Math.random() * possible.length));
            }
        
            return text;
        },
        getActiveUsers(redisKey) {
                return new Promise((resolve, reject) => {
                    CLIENT_REDIS.keys(redisKey,function(err,res) {
                        if(res) {
                            resolve(res);
                        } else {
                            resolve([]);
                        }
                    });
                    setTimeout(()=> {
                        resolve([]);
                    },5000);
               });
        },

        postShareFn(ctx) {
            let { postArr } = ctx.params;
            return new Promise((resolve, reject) => {
                ctx.call('db.post.bulkCreate', postArr)
                .then(result => {
                    for (var i = 0; i < result.length; i++) {
                        //Tract Data start
                        ctx.params = { ...ctx.params, post_id: parseInt(result[i]['dataValues'].id) }
                        let trackerData = {
                            user_id: parseInt(ctx.params.created_by),
                            type: postArr[0].type,
                            type_id: parseInt(result[i]['dataValues'].id),
                            activity: ('share  ' + postArr[0].type)
                        };
                        ctx.broker.call('user_activities.trackUserActivities', trackerData);
                        //Tract Data end
                    }
                    resolve({ message: 'success', data: result, code: 200 });
                    this.appNotify(ctx);
                })
                .catch(err => {
                    reject(err)
                });
            });
        }
    },
    created() { },
    started() { },
    stopped() { }
}
