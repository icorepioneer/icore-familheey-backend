"uses strict";
module.exports = {
    name:"error_handling",
    settings:{},
    actions:{
        errorCode:{
            params:{
                error_name:"string"
            },
            handler:(ctx)=>{
                let {error_name} = ctx.params;
                let response ;
                switch(error_name){
                    case "SequelizeUniqueConstraintError":
                        response = "ALREADY EXISTS"
                        break;
                    case 'condition_next': //for sonarqube fix remove and add new case if needed
                        console.log(ctx.params);
                        break;
                    case 'condition_next2': //for sonarqube fix remove and add new case if needed
                        console.log(ctx.params);
                        break
                }
                return response;
            }
        }
    },
    events:{},
    methods:{},
    created(){},
    started(){},
    stopped(){}
}