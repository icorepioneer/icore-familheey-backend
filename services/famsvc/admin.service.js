"use strict";

var _async = require('async');
var rq = require('request');
const CryptoJS = require("crypto-js");
const key = process.env.SECRET_KEY;
module.exports = {
    name: "admin",
    settings: {},
    actions: {
        /**
         * list all request to admin
         * member request , family linking request
         */
        viewMemberRequest: {
            params: {},
            handler(ctx) {
                let { group_id } = ctx.params;
                var response = {}
                return new Promise((resolve, reject) => {
                    ctx.call("db.requests.findAllRequests", { group_Id: group_id })
                        .then((doc) => {
                            response.data = doc;
                            response.request = doc;
                            return ctx.call("db.familyLinks.adminListLinkRequest", ctx.params)
                        })
                        .then((fetchlinks) => {
                            response.linkRequest = fetchlinks;
                            fetchlinks.map((elem) => { elem['type'] = 'fetch_link' });
                            response.data = response.data.concat(fetchlinks);
                            response = { ...response, code: 200 }
                            resolve(response);
                        })
                        .catch(err => reject(err));
                })
            }
        },
        adminRequestAction: {
            params: {
                type: "string",
                status: "string"
            },
            handler(ctx) {
                let { type } = ctx.params;
                return new Promise(async(resolve, reject) => {
                    var _path;
                    let resolve_message = ""; 
                    let action_message = "";
                    let is_force_update = ctx.params.is_force_update ? ctx.params.is_force_update : false;
                    if (type == "fetch_link") {
                        _path = "admin.updateFamilyLinks"
                        //Resolve if already accepted/rejected link request start
                        if(!is_force_update) {
                            let linkQueryParams = {};
                            if(ctx.params.id) {
                                linkQueryParams = {id:ctx.params.id};  
                            } else {
                                linkQueryParams = { from_group:ctx.params.from_group, to_group:ctx.params.to_group };
                            }
                            await ctx.call("db.familyLinks.find", { query:linkQueryParams,fields:["id","status"] })
                            .then((resLink) => {
                                if(resLink[0].status != 'pending') {
                                    resolve_message = "This request is already " + resLink[0].status;
                                    action_message = resLink[0].status;
                                }
                            })
                            .catch(err => {
                                reject(err);
                            })
                        }
                        //Resolve if already accepted/rejected link request end
                    } else if (type == "request") {
                        _path = "admin.updateRequestsdB"
                        //Resolve if already accepted/rejected start
                        if(!is_force_update) {
                            let requQueryParams = {};
                            if(ctx.params.id) {
                                requQueryParams = {id:ctx.params.id};   
                            } else {
                                requQueryParams = { user_id:ctx.params.user_id, group_id:ctx.params.group_id };
                            }
                            await ctx.call("db.requests.find", { query:requQueryParams,fields:["status","responded_by"] })
                            .then(async(res) => {
                                //Get responded user start
                                let respondUser = '';
                                if(res[0].responded_by == ctx.params.responded_by) {
                                    respondUser = " by you";   
                                } else {
                                    await ctx.call("db.users.find", { query:{id:res[0].responded_by},fields:["full_name"] })
                                    .then((userRes) => { 
                                        respondUser = " by " + userRes[0].full_name;
                                    })
                                    .catch(err => {
                                        console.log("Geting user has some error");
                                    })  
                                }
                                //Get responded user end
                                if(res[0].status != 'pending') {
                                    resolve_message = "This request is already " + res[0].status + respondUser;
                                    action_message = res[0].status;
                                }
                            })
                            .catch(err => {
                                reject(err);
                            })
                        }
                        //Resolve if already accepted/rejected end
                    }
                    if(resolve_message == "") {
                        ctx.call(_path, ctx.params)
                        .then((res) => {
                            resolve(res);
                        })
                        .catch(err => {
                            reject(err);
                        })
                    } else {
                        resolve({"data": [{"message":resolve_message,"action":action_message}]});
                    }
                })
            }
        },
        /**
        * update family links 
        * db
        */
        updateFamilyLinks: {
            params: {
                //id: "string"
            },
            handler(ctx) {
                return new Promise(async(resolve, reject) => {
                    let { type } = ctx.params;
                    if(!ctx.params.id || ctx.params.id == '') {
                        let { from_group, to_group } = ctx.params;
                        let updateId = await ctx.call("db.familyLinks.find", { query:{ from_group:from_group, to_group:to_group },fields:["id"] });
                        if(updateId.length>0) ctx.params.id = updateId[0].id;
                        delete ctx.params.from_group; delete ctx.params.to_group;
                    }
                    ctx.call("db.familyLinks.update", ctx.params)
                    .then((res) => {
                        ctx.params = { ...ctx.params, ...res , type:type, grpmembers:[{ user_id: res.requested_by}] }
                        this.appNotify(ctx)
                        resolve({ data: [res] });
                    })
                    .catch((err) => {
                        reject(err);
                    })
                });
            }
        },

        /**
        * update requests table
        * and add user to group_mpas to particular group
        */
        updateRequestsdB: {
            params: {
                type: "string",
                status: "string"
            },
            handler(ctx) {
                //delete ctx.params.type;
                return new Promise(async (resolve, reject) => {
                    if(!ctx.params.id || ctx.params.id == '') {
                        let { user_id, group_id } = ctx.params;
                        let updateId = await ctx.call("db.requests.find", { query:{ user_id:user_id, group_id:group_id },fields:["id"] });
                        if(updateId.length>0) ctx.params.id = updateId[0].id;
                        delete ctx.params.user_id; delete ctx.params.group_id;
                    }
                    let { type, status, id } = ctx.params;
                    var updata = ctx.params;
                    delete updata.from_user; delete updata.to_user;
                    let all_req = await ctx.call("db.requests.find", { query: { id: id }, fields: ["user_id", "group_id"] })
                    if (all_req && all_req.length > 0) {
                        all_req.map((_el) => { _el['status'] = status });
                    }
                    ctx.call("db.requests.update", updata)
                        .then((result) => {
                            ctx.call("db.requests.update_requests", all_req[0])
                            ctx.params = { ...ctx.params, ...result }
                            if (result && type == "request") { //add to groupmaps if type=accepted
                                delete result.id;
                                result.following = true;
                                result.type = "member";
                                let { group_id, user_id } = result;
                                ctx.params.group_id = group_id;
                                ctx.call("db.group_map.find", { query: { group_id: group_id, user_id: user_id } })
                                    .then(async res => {
                                        ctx.params = { ...ctx.params, group_id: group_id, user_id: user_id, type: type }

                                        if (res.length > 0) {
                                            let groupmapUpdateObj = {};
                                            if(status == 'accepted') {
                                                groupmapUpdateObj = {
                                                    id: res[0].id, 
                                                    is_removed: false, 
                                                    is_blocked:false, 
                                                    type: 'member'
                                                };
                                            } else if(status == 'rejected') {
                                                groupmapUpdateObj = {
                                                    id: res[0].id, 
                                                    is_removed: true                                            };
                                            }
                                            
                                            ctx.call("db.group_map.update", groupmapUpdateObj)
                                                .then((gmp) => {
                                                    this.appNotify(ctx);
                                                })
                                        } else if(status == 'accepted') {
                                            // Default membership added start
                                            let _Group = await ctx.broker.call("db.groups.find", { query: { id: group_id }, fields: ["default_membership"] });
                                            result.membership_period_type = "Life time";
                                            let groupDefaultMembership = parseInt(_Group[0].default_membership);
                                            result.membership_id = _Group[0].default_membership ? groupDefaultMembership : null
                                            // Default membership added end
                                            ctx.call("db.group_map.create", result)
                                                .then((gmp) => {
                                                    this.appNotify(ctx);
                                                })
                                        }
                                    })
                            }
                            resolve({ data: [result] });
                        })
                        .catch((err) => {
                            reject(err);
                        })
                });
            }
        },
        getOverviewDB: {
            params: {

            },
            handler(ctx) {
                return new Promise(async (resolve, reject) => {
                    ctx.call("db.users.getOverviewDB", ctx.params)
                    .then((res) => {
                        if(res && res.length > 0) {
                            resolve(res[0]);
                        } else {
                            resolve("No Data");
                        }
                    })
                    .catch((err) => {
                        reject(err);
                    })
                })
            }
        },

        generateFirebaseUrl: {
            params: {
                type:"string"
            },
            handler(ctx) {
                let _this = this;
                let { type } = ctx.params;
                return new Promise(async (resolve, reject) => {
                    switch (type) {
                        case 'groups':
                            ctx.call("db.groups.list",{ query:{ firebase_link:'' }, fields:["id","f_text"], page: 1, pageSize: 30, sort:"-id" })
                            .then((res) => {
                                let { rows } = res;
                                if(rows.length>0) {
                                    rows.forEach(async (elem,i) => {
                                        elem = { ...elem, type:'family' }
                                        await _this.generatefirebaseLink(ctx,elem);
                                        if(i<rows.length) resolve(`${rows.length} success`)
                                    });
                                } else {
                                    resolve("completed all");
                                }
                            })
                            .catch((err) => {
                                reject(err);
                            })
                        break;

                        case 'events':
                            ctx.call("db.events.list",{ query:{ firebase_link:'' }, fields:["id"], page: 1, pageSize: 30, sort:"-id" })
                            .then((res) => {
                                let { rows } = res;
                                if(rows.length>0) {
                                    rows.forEach(async (elem,i) => {
                                        elem = { ...elem, type:'event' }
                                        await _this.generatefirebaseLink(ctx,elem);
                                        if(i<rows.length) resolve(`${rows.length} success`)
                                    });
                                } else {
                                    resolve("completed all");
                                }
                            })
                            .catch((err) => {
                                reject(err);
                            })
                        break;

                        case 'posts':
                            ctx.call("db.post.list",{ query:{ firebase_link:'' }, fields:["id","type"], page: 1, pageSize: 30, sort:"-id" })
                            .then((res) => {
                                let { rows } = res;
                                if(rows.length>0) {
                                    rows.forEach(async (elem,i) => {
                                        await _this.generatefirebaseLink(ctx,elem);
                                        if(i<rows.length) resolve(`${rows.length} success`)
                                    });
                                } else {
                                    resolve("completed all");
                                }
                            })
                            .catch((err) => {
                                reject(err);
                            })
                        break;

                        default:
                        break;
                    }
                })
            }
        },

        createExcel:{
            params:{},
            handler(ctx){
                let { index } = ctx.params;

                return new Promise((resolve, reject) => {
                    if(index == 3) reject(ctx.params)
                    else resolve(ctx.params)
                })
            }
        }


    },
    events: {},
    methods: {
        appNotify(ctx) {
            let { grpmembers, status, responded_by, group_id, user_id, type, from_id, from_group, to_group  } = ctx.params;
            if(from_id)      user_id = from_id; //from request
            if(responded_by && type!='request') user_id = responded_by; //from family_link
            _async.parallel({
                user: (callback) => {
                    if(user_id) {
                        ctx.call("db.users.find", { query: { id: user_id }, fields: ["id", "full_name", "propic"] })
                        .then(res => {
                            callback(null, res);
                        }).catch(err => {
                            console.log("error happened -------- familyMembers", err);
                        })
                    } else {
                        callback(null, []);
                    }
                },
                group_members: (callback) => {
                    if (group_id && !grpmembers) {
                        ctx.call("db.group_map.find", { query: { group_id: group_id, is_blocked: false, is_removed: false }, fields: ["user_id"] })
                        .then(res => {
                            callback(null, res);
                        }).catch(err => {
                            console.log("error happened -------- familyMembers", err);
                        })
                    } else if (grpmembers) {
                          callback(null, grpmembers);
                    } else {
                        callback(null, []);
                    }
                },
                to_group: (callback) => {
                    if (to_group)
                        ctx.call("db.groups.find", { query: { id: to_group, is_active: true }, fields: ["id", "group_name", "logo"] })
                        .then(res => {
                            callback(null, res);
                        }).catch(err => {
                            console.log("error happened -------- familyMembers", err);
                        })
                    else
                        callback(null, []);
                },
                from_group: (callback) => {
                    if (from_group)
                        ctx.call("db.groups.find", { query: { id: from_group, is_active: true }, fields: ["id", "group_name", "logo"] })
                        .then(res => {
                            callback(null, res);
                        }).catch(err => {
                            console.log("error happened -------- familyMembers", err);
                        })
                    else
                        callback(null, []);
                },
                group: (callback) => {
                    if (group_id)
                        ctx.call("db.groups.find", { query: { id: group_id, is_active: true }, fields: ["id", "group_name", "logo"] })
                        .then(res => {
                            callback(null, res);
                        }).catch(err => {
                            console.log("error happened -------- familyMembers", err);
                        })
                    else
                        callback(null, []);
                },
                responded_by: (callback) => {
                    ctx.call("db.users.find", { query: { id: responded_by }, fields: ["id", "full_name", "propic"] }).then(res => {
                        callback(null, res);
                    }).catch(err => {
                        console.log("error happened -------- familyMembers", err);
                    })
                },
            }, function (err, result) {
                let { user, group_members, group } = result;
                responded_by = result.responded_by;
                to_group = result.to_group;
                from_group = result.from_group;
                console.log("=== =  == group_members = == ", group_members)
                group_members.forEach(async elem => {
                    user_id  = elem.user_id;
                    let sendObj = {
                        type_id: parseInt(elem.user_id),
                        to_id: elem.user_id,
                        from_id: user.length > 0 ? user[0].id : '',
                        type: type ? type:'',
                        propic: user.length > 0 ? user[0].propic : '',
                        category: type ? type : '',
                        message_title: 'familheey'
                    }
                    if (status == 'accepted') {
                        switch (type) {
                            case "fetch_link":
                                sendObj.type = 'family';
                                sendObj.sub_type = 'fetch_link';
                                sendObj.type_id = (to_group && to_group.length>0) ? parseInt(to_group[0].id) : '';
                                sendObj.link_to = (to_group && to_group.length>0) ? parseInt(to_group[0].id) : '';
                                sendObj.message = `Your request to link <b>${from_group[0].group_name}</b> with <b>${to_group[0].group_name}</b> was approved`;
                            break;
                            case "request":
                                sendObj.type = 'family';
                                sendObj.type_id  = parseInt(group_id);
                                sendObj.link_to  = parseInt(group_id);
                                sendObj.sub_type = ''
                                sendObj.propic = `${responded_by[0].propic}`
                                sendObj.message = `<b>${responded_by[0].full_name}</b> has added <b>${user[0].full_name}</b> to <b>${group[0].group_name}</b> `;
                                if (elem.user_id == ctx.params.user_id) {
                                    sendObj.message = `Your request to join <b>${group[0].group_name}</b> was approved`;
                                }
                            break;
                            default:
                            break;
                        }
                        if(responded_by[0].id!==elem.user_id)
                            ctx.broker.call("notification.sendAppNotification", sendObj);
                        let _user_notify = await ctx.call("db.users.find",{ query:{id:user_id}, fields:["notification"] })
                        if(_user_notify.length>0 && _user_notify[0].notification){
                            ctx.broker.call("userops.getregisterToken", { user_id: elem.user_id, is_active:true })
                            .then((_devices) => {
                                _devices.data.forEach(elem2 => {
                                    sendObj = {
                                        ...sendObj,
                                        r_id: elem2.id,
                                        token: elem2.device_token,
                                        device_type: elem2.device_type.toLowerCase(),
                                        title: sendObj.message_title,
                                        body: sendObj.message.toString().replace( /(<([^>]+)>)/ig, '')
                                    }
                                    if(responded_by[0].id!==elem2.user_id)
                                        ctx.broker.call("notification.sendPushNotification", sendObj)
                                });
                            })
                            .catch(e => e)
                        }
                    }
                });
            });
        },

        generatefirebaseLink(ctx,doc){
            let { id, type } = doc;
            let f_link = '';
            let db_n = 'groups'
            
            switch (type) {
                case 'family':
                    var gId = CryptoJS.AES.encrypt(id.toString(), key);
                        gId = encodeURIComponent(gId);
                    f_link  = `${process.env.FRONTEND_BASE_URL}familyview/${gId}?type="${type}"&type_id=${id}`;
                break;
        
                case 'event':
                    db_n = 'events';
                    var eId = CryptoJS.AES.encrypt(id.toString(), key);
                        eId = encodeURIComponent(eId);
                    f_link  = `${process.env.FRONTEND_BASE_URL}event-details/${eId}?type="${type}"&type_id=${id}`;
                break;
        
                case 'post':
                    db_n = 'post';
                    var pId = CryptoJS.AES.encrypt(id.toString(), key);
                        pId = encodeURIComponent(pId);
                    f_link  = `${process.env.FRONTEND_BASE_URL}post_details/${pId}?type="${type}"&type_id=${id}`;
                break;
                
                default:
                break;
            }
            
            var options = {
                'method': 'POST',
                'url': `https://firebasedynamiclinks.googleapis.com/v1/shortLinks?key=${process.env.FIREBASE_KEY}`,
                'headers': {
                  'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    "dynamicLinkInfo":{
                        "domainUriPrefix":`${process.env.FIREBASE_DOMAIN_URI}`,
                        "link":`${f_link}`,
                        "androidInfo":{
                            "androidPackageName":`${process.env.ANDROID_PACKAGE_NAME}`
                        },
                        "iosInfo":{
                            "iosBundleId":`${process.env.IOS_PACKAGE_NAME}`,
                            "iosAppStoreId":`${process.env.IOS_APP_STORE_ID}`
                        }
                    },
                    "suffix": {
                        "option": "UNGUESSABLE" 
                    }
                })
            };
            rq(options, function (error, response) {
                if (error) throw new Error(error);
                else{
                    let { shortLink } = JSON.parse(response.body);
                    return new Promise((resolve, reject) => {
                        ctx.call(`db.${db_n}.update`,{ id:id, firebase_link:shortLink })
                        .then((doc2)=>{
                            resolve(doc2)
                        }).catch(err=>reject(err))
                    })
                }
            });
        }
    },
    created() { },
    started() { },
    stopped() { }
}