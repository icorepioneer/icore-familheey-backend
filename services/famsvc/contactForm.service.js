"use strict";
module.exports = {
    name: "contactForm",
    settings: {},
    actions: {
        receive: {
            params: {
                firstname: "string",
                lastname: "string",
                email: "string"
            },
            handler(ctx) {
                let { firstname, lastname, phone, discuss, email } = ctx.params;
                return new Promise((resolve, reject) => {
                    var html = `
                                <html>
                                    <body>
                                        <div>
                                            <table cellspacing="0" border-collapse="collapse" style="bordercolor=#00F,font-size:12px;">
                                                <tr>
                                                    <td> Name: </td> <td> ${firstname} ${lastname}</td>
                                                </tr>
                                                <tr>
                                                    <td>Contact Email:</td> <td>${email}</td>
                                                </tr>
                                                <tr>
                                                    <td>Contact Number:</td> <td>${phone}</td>
                                                </tr>
                                                <tr>
                                                    <td>Like to discuss:</td> <td>${discuss}</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </body>
                                </html>`;
                    // Email template end
                    // Send email function start
                    ctx.broker.call("notification.sendEmail", {
                        toAddresses: ['contact@familheey.com'],
                        // ccAddress: ['nissanth.s@iinerds.com,angelachinweze@icloud.com,ken.chinweze@gmail.com,ajithkumar15@hotmail.com,anshad@innovationincubator.com'],
                        //ccAddress: ['nissanth.s@iinerds.com','naveen.kh@iinerds.com'],
                        message: html,
                        subject: "Familheey :: Contact Form"
                    })
                    .then((res) => {
                        console.log("contact mail send successfully")
                        resolve("contact mail send successfully");
                    })
                    .catch((err) => {
                        console.log("contact mail send Error", err);
                        reject("contact mail send Error", err);
                    })
                })
            }
        }
    },
    events: {},
    methods: {},
    created() { },
    started() { },
    stopped() { }
}
