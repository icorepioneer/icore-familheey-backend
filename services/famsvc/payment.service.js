"use strict";
var _async = require('async');
var moment = require('moment');

//const secret_key =  "sk_test_51Fd78QHMcY2OIUvPu4jACJpuejgL0SG1PHFHD0q2ETyNndHycqDe2mGacHVRDesbJU5Rhbq1SKyVejxrjIgV4c8Y00ysnP3T5W";
//const client_id = "ca_HT4gdaG230g40TWcDC7YVcAA2I2UZ9I5";
const secret_key = process.env.STRIPE_SECRET_KEY;
const client_id = process.env.STRIPE_CLIENT_ID;
const stripe = require('stripe')(secret_key, {apiVersion: ''});

//Unickey generation function start
const unicKeyGen = (length,id) => {
  let result = id+'-';
  let characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  let charactersLength = characters.length;
  for (let i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}
//Unickey generation function end

module.exports = {
    name: "payment",
    settings: {},
    actions: {
        stripeCreateCustomerId:{
            params:{
        
            },
            handler(ctx) {
                return new Promise(async(resolve, reject) => {
                    const customer = await stripe.customers.create({
                        email : ctx.params.email ? ctx.params.email : '',
                        name : ctx.params.name ? ctx.params.name : ''
                    });
                    if(customer) {
                        resolve(customer);   
                    } else {
                        resolve("Cant create customer",customer);     
                    }
                })
            }
        },
        getCustomerId:{
            params:{
                user_id : "string"
            },
            handler(ctx) {
                let user_id = ctx.params.user_id;
                return new Promise(async(resolve, reject) => {
                    //Get customer id from user table start
                    ctx.call("db.users.find",{query:{id:user_id},fields:["payment_customer_id","full_name","email"]})
                    .then((res)=>{
                        let cust_id = res[0].payment_customer_id;
                        if(cust_id && cust_id != '') {
                            resolve(cust_id);
                        } else {
                            // Create stripe customer id start
                            ctx.params.name = res[0].full_name ? res[0].full_name : '';
                            ctx.params.email = res[0].email ? res[0].email : '';
                            ctx.call("payment.stripeCreateCustomerId",ctx.params)
                            .then((stripeRes)=>{
                                if(stripeRes) {
                                    //Update stripe customer id to user table start
                                    let userUpdateObj = {
                                        payment_customer_id : stripeRes.id,
                                        payment_customer_object : stripeRes,
                                        id : user_id 
                                    } 
                                    ctx.call("db.users.update",userUpdateObj) 
                                    .then((userUpdateRes)=>{
                                        if(userUpdateRes) {
                                            resolve(userUpdateRes.payment_customer_id);
                                        }
                                    }) 
                                    .catch((err) => {
                                        console.log(err);
                                        reject('update stripe customer id has some error:',err);
                                    });
                                    //Update stripe customer id to user table end
                                }
                            })
                            .catch((err) => {
                                console.log(err);
                                reject('create stripe customer id has some error:',err);
                            });
                            // Create stripe customer id end
                        }
                    })
                    .catch((err) => {
                        console.log(err);
                        reject('Get customer Id from users table has some error:',err);
                    });
                    //Get customer id from user table end
                })
            }    
        },
        stripeCreatePaymentIntent:{
            params:{
                amount: "number",
                user_id : "string",
                group_id : "string"
            },
            handler(ctx) {
                let {amount,user_id,group_id,card_id,contributionId} = ctx.params;
                let commission_percent = 2;
                //Including stripe commission to payment fee start
                let stripe__percent = 2.9;
                //actual stripe fixed fee is 0.3 but we consider amount*100 so fixed fee is 0.3*100 = 30
                let stripe_fixed_fee = 30.00;
                commission_percent = commission_percent + stripe__percent;
                //Including stripe commission to payment fee end
                let commission_amount = ((commission_percent/ 100) * amount).toFixed(2);
                commission_amount = parseInt(commission_amount);
                commission_amount = commission_amount + stripe_fixed_fee;
                let transfer_amount = amount - commission_amount;
                return new Promise(async(resolve, reject) => {
                    let currency = ctx.params.currency ? ctx.params.currency : "usd";
                    let receipt_email = ctx.params.email ? ctx.params.email : "";
                    //let to_account_id = "acct_1GuwS3HtDhnh47ab";
                    let to_account_id = "";
                    let family_name = "";
                    //Get account id from group table start
                    await ctx.call("db.groups.find",{query:{id:group_id},fields:["stripe_account_id","group_name"]})
                    .then((accountId)=>{
                        if(accountId && accountId.length > 0) {
                            family_name = accountId[0].group_name ? accountId[0].group_name : '';
                            if(accountId[0].stripe_account_id && accountId[0].stripe_account_id != '') {
                                to_account_id = accountId[0].stripe_account_id;
                            } else {
                                reject('There is no stripe account mapped in this account');
                                return;      
                            }
                        } else {
                            reject('There is no stripe account mapped in this account');
                            return;   
                        }
                    })
                    .catch((err) => {
                        console.log(err);
                        reject('getting account id from groups has some error:',err);
                        return;
                    });
                    //Get account id from group table end
                    
                    let customer_id = '';
                    await ctx.call("payment.getCustomerId",ctx.params)
                    .then((customerId)=>{
                        customer_id = customerId;
                    })
                    .catch((err) => {
                        console.log(err);
                        reject('create stripe customer id has some error:',err);
                    });

                    // Item contribution creation start
                    let contribution_id = "";
                    if(contributionId && contributionId != '') {
                        contribution_id = contributionId;   
                    } else {
                        if(ctx.params.to_type != 'membership') {   
                            let contribution_amt = amount/100;
                            let contributionObj = {
                                post_request_item_id : ctx.params.to_subtype_id ? ctx.params.to_subtype_id : '',
                                contribute_item_quantity : contribution_amt.toString(),
                                is_active : false,
                                notification_off : 1,
                                group_id : group_id,
                                admin_id : ctx.params.admin_id ? parseInt(ctx.params.admin_id) : null,
                                phone_no : ctx.params.phone_no ? ctx.params.phone_no : '',
                                paid_user_name : ctx.params.paid_user_name ? ctx.params.paid_user_name : '',
                            }
                            //Check phone number start
                            let useris = [];
                            if(ctx.params.phone_no) {
                                useris = await ctx.call("db.users.find",{ query: { phone:ctx.params.phone_no }, fields:["id"] });
                                contributionObj.contribute_user_id = useris.length>0 ? useris[0].id.toString() : user_id    
                            } else {
                                contributionObj.contribute_user_id = user_id;    
                            }
                            //Check phone number end
                            if(ctx.params.is_anonymous && ctx.params.is_anonymous != '') {
                                contributionObj.is_anonymous = ctx.params.is_anonymous;   
                            }
                            await ctx.call("post_needs.contributionCreate",contributionObj)
                            .then((contributionObj2)=>{
                                contribution_id = contributionObj2.data.id;
                            })
                            .catch((err) => {
                                console.log(err);
                                reject('create item contribution has some error:',err);
                            });
                        }
                    }
                    // Item contribution creation end

                    // Construct metadata for application reference start
                    let metadata = {
                        payment_from_user : user_id,
                        to_account_id : to_account_id,
                        to_type : ctx.params.to_type ? ctx.params.to_type : 'request',
                        to_type_id : ctx.params.to_type_id ? ctx.params.to_type_id : '',
                        to_subtype : ctx.params.to_subtype ? ctx.params.to_subtype : 'item',
                        to_subtype_id : ctx.params.to_subtype_id ? ctx.params.to_subtype_id : '',
                        to_group_id : ctx.params.group_id ? ctx.params.group_id : '',
                        contribution_id : contribution_id,
                        admin_id : ctx.params.admin_id ? parseInt(ctx.params.admin_id) : null,
                        phone_no : ctx.params.phone_no ? ctx.params.phone_no : '',
                        paid_user_name : ctx.params.paid_user_name ? ctx.params.paid_user_name : '',
                    }
                    // Construct metadata for application reference end
                    let paymentObj = {
                        description : 'Payment for ' + metadata.to_type + ' to family ' + family_name,
                        amount: amount,
                        currency: currency,
                        //application_fee_amount: commission_amount,
                        customer: customer_id,
                        on_behalf_of: to_account_id,
                        metadata: metadata,
                        transfer_data: {
                            amount: transfer_amount, 
                            destination: to_account_id,
                        },
                        payment_method_options: {
                            card: {
                                request_three_d_secure: "any"
                            }
                        },
                        payment_method_types: [
                            "card"
                        ],
                    }
                    if(card_id && card_id != '') {
                        paymentObj.payment_method = card_id 
                        paymentObj.off_session = false
                        paymentObj.confirm = true 
                    }
                    if(receipt_email && receipt_email != '') {
                        paymentObj.receipt_email = receipt_email     
                    } else {
                        await ctx.call("db.users.find",{query:{id:user_id},fields:["email"]})
                        .then((customerEmail)=>{
                            if(customerEmail.length > 0 && customerEmail[0].email && customerEmail[0].email != '') {
                                paymentObj.receipt_email = customerEmail[0].email;
                            }
                        })
                        .catch((err) => {
                            console.log(err);
                        });     
                    }
                    // let connect_accountObj = {
                    //     stripeAccount: to_account_id,
                    // }
                    const paymentIntent = await stripe.paymentIntents.create(paymentObj);
                    if(paymentIntent) {
                        let from_os = ctx.params.os ? ctx.params.os : 'ios';
                        let from_type = metadata.to_type;
                        let from_type_id = metadata.to_type_id;
                        let response = {
                            contributionId:contribution_id,
                            client_secret : paymentIntent.client_secret,
                            id : paymentIntent.id,
                            status : paymentIntent.status,
                            weburl : process.env.FRONTEND_BASE_URL + "payment/client_id?amount=" + amount + "&client_id=" + paymentIntent.client_secret + "&os=" + from_os + "&type=" + from_type + "&type_id=" + from_type_id
                        }
                        resolve(response);
                    } else {
                        resolve("No client secret getting for account");  
                    }
                })
            }    
        },
        striptCreateCard:{
            params:{
                user_id : "string"
            },
            handler(ctx) {
                let user_id = ctx.params.user_id;
                return new Promise(async(resolve, reject) => {
                    let customer_id = '';
                    await ctx.call("payment.getCustomerId",ctx.params)
                    .then((customerId)=>{
                        customer_id = customerId;
                    })
                    .catch((err) => {
                        console.log(err);
                        reject('create stripe customer id has some error:',err);
                    });
                    let source = ctx.params.source ? ctx.params.source : "tok_visa";
                    stripe.customers.createSource(
                        customer_id,
                        {source: source},
                        function(err, card) {
                            if(card) {
                                resolve(card);
                                //Update new card to users table start
                                let cardIdsArrar = [];
                                ctx.call("db.users.find",{query:{id:user_id},fields:["payment_card_ids"]})
                                .then((cardIds)=>{
                                    if(cardIds && cardIds[0].payment_card_ids != null) {
                                        cardIdsArrar = cardIds[0].payment_card_ids;
                                    }
                                    cardIdsArrar.push(card.id); 
                                    //Update stripe cards object to user table start
                                    let userUpdateObj = {
                                        payment_card_ids : cardIdsArrar,
                                        id : user_id 
                                    } 
                                    ctx.call("db.users.update",userUpdateObj) 
                                    .then((userUpdateRes)=>{
                                        if(userUpdateRes) {
                                            console.log("Successfully updated card details");
                                        }
                                    }) 
                                    .catch((err2) => {
                                        console.log(err2);
                                        reject('update stripe customer id has some error:',err2);
                                    });
                                    //Update stripe cards object to user table end   
                                })
                                .catch((err3) => {
                                    console.log(err3);
                                })
                                //Update new card to users table end
                                return;
                            }
                            if(err) {
                                console.log("ERROR",err);
                                resolve(err);
                            }
                        }
                    );
                })
            }   
        },
        retriveCard:{
            params:{
                user_id : "string",
                card_id : "string"  
            }, 
            handler(ctx) {
                return new Promise(async(resolve, reject) => {
                    let customer_id = '';
                    await ctx.call("payment.getCustomerId",ctx.params)
                    .then((customerId)=>{
                        customer_id = customerId;
                    })
                    .catch((err) => {
                        console.log(err);
                        reject('create stripe customer id has some error:',err);
                    });
                    let card_id = ctx.params.card_id ? ctx.params.card_id : "";
                    stripe.customers.retrieveSource(
                        customer_id,
                        card_id,
                        function(err, card) {
                            if(card) {
                                resolve(card);
                                return;
                            }
                            if(err) {
                                console.log("ERROR",err);
                                resolve(err);
                            }
                        }
                    );
                })
            }   
        },
        deleteCard:{
            params:{
                user_id : "string",
                card_id : "string"
            },
            handler(ctx) {
                return new Promise(async(resolve, reject) => {
                    let customer_id = '';
                    await ctx.call("payment.getCustomerId",ctx.params)
                    .then((customerId)=>{
                        customer_id = customerId;
                    })
                    .catch((err) => {
                        console.log(err);
                        reject('create stripe customer id has some error:',err);
                    });
                    let card_id = ctx.params.card_id ? ctx.params.card_id : "";
                    stripe.customers.deleteSource(
                        customer_id,
                        card_id,
                        function(err, confirmation) {
                            if(confirmation) {
                                resolve(confirmation);
                                return;
                            }
                            if(err) {
                                console.log("ERROR",err);
                                resolve(err);
                            }
                        }
                    );
                })
            }    
        },
        listAllCards:{
            params:{
                user_id : "string"
            },
            handler(ctx) {
                return new Promise(async(resolve, reject) => {
                    let customer_id = '';
                    await ctx.call("payment.getCustomerId",ctx.params)
                    .then((customerId)=>{
                        customer_id = customerId;
                    })
                    .catch((err) => {
                        console.log(err);
                        reject('create stripe customer id has some error:',err);
                    });
                    // stripe.customers.listSources(
                    //     customer_id,
                    //     {
                    //         object: 'card', 
                    //         limit: 10
                    //     },
                    //     function(err, cards) {
                    //         if(cards) {
                    //             resolve(cards);
                    //             return;
                    //         }
                    //         if(err) {
                    //             console.log("ERROR",err);
                    //             resolve(err);
                    //         }
                    //     }
                    // );
                    stripe.paymentMethods.list(
                        {customer: customer_id, type: 'card', limit: 10},
                        function(err, cards) {
                            if(cards) {
                                let cardData = cards.data;
                                if(cardData.length > 0) {
                                    let cardArray = [];
                                    let tempcardObj = {};
                                    for(let i=0 ; i < cardData.length ; i++){
                                        tempcardObj = {
                                            id : cardData[i].id,
                                            brand : cardData[i].card.brand,
                                            exp_month : cardData[i].card.exp_month,
                                            exp_year : cardData[i].card.exp_year,
                                            last4 : cardData[i].card.last4,
                                            created : cardData[i].created,
                                            customer : cardData[i].customer,
                                            type : cardData[i].type
                                        }
                                        cardArray.push(tempcardObj);
                                    }
                                    let response = {data:cardArray};
                                    resolve(response);
                                    return;
                                } else {
                                    resolve(cards);
                                    return;
                                }
                            }
                            if(err) {
                                console.log("ERROR",err);
                                resolve(err);
                            }
                        }
                    );
                })
            }  
        },
        createConnectAccount:{
            params:{
                user_id : "string"
            },
            handler(ctx) {
                return new Promise(async(resolve, reject) => {
                    stripe.accounts.create(
                        {
                            type: 'standard',
                            country: 'US',
                            email: 'nissanth.s@iinerds.com',
                            requested_capabilities: [
                                'card_payments',
                                'transfers',
                            ],
                            external_account: "btok_1GuyggHMcY2OIUvPfDphz1nM",
                            business_type: "individual"
                        },
                        function(err, account) {
                            if(account) {
                                resolve(account);
                                return;
                            }
                            if(err) {
                                console.log("ERROR",err);
                                reject(err);
                            }
                        }
                    );
                })
            }    
        },
        updateConnectAccount:{
            params:{
                user_id : "string",
                account_id : "string"
            },
            handler(ctx) {
                let {account_id} = ctx.params;
                return new Promise(async(resolve, reject) => {
                    stripe.accounts.update(
                        account_id,
                        {
                            tos_acceptance: {
                                date: Math.floor(Date.now() / 1000),
                                ip: ctx.meta.clientIp, // Assumes you're not using a proxy
                            },   
                        },
                        function(err, account) {
                            if(account) {
                                resolve(account);
                                return;
                            }
                            if(err) {
                                console.log("ERROR",err);
                                reject(err);
                            }
                        }
                    );
                })
            }
        },
        createAccountToken:{
            params:{
                user_id : "string"
            }, 
            handler(ctx) {
                return new Promise(async(resolve, reject) => {
                    stripe.tokens.create(
                        {
                            bank_account: {
                            country: 'US',
                            currency: 'usd',
                            account_holder_name: 'Nissanth S',
                            account_holder_type: 'individual',
                            routing_number: '110000000',
                            account_number: '000123456789',
                            },
                        },
                        function(err, token) {
                            if(token) {
                                resolve(token);
                                return;
                            }
                            if(err) {
                                console.log("ERROR",err);
                                reject(err);
                            }
                        }
                    );
                })
            }   
        },
        stripe_oauth: {
            params:{

            },
            handler(ctx) {
                return new Promise(async(resolve, reject) => {
                    let { code, state } = ctx.params;
                    stripe.oauth.token({
                        grant_type: 'authorization_code',
                        code
                    })
                    .then(
                        (response) => {
                            var connected_account_id = response.stripe_user_id;
                            ctx.call("db.groups.find",{query:{stripe_oauth_ref_key:state},fields:["id"]})
                            .then((res) => {
                                if(res && res.length > 0) {
                                    // save account id to group table start
                                    let accountUpdateObj = {
                                        id : res[0].id,
                                        stripe_account_id : connected_account_id
                                    }
                                    ctx.call("db.groups.update",accountUpdateObj)
                                    .then((groupUpdateRes)=>{
                                        console.log('Account ID successfull updated');
                                    })
                                    .catch((err) => {
                                        console.log(err);
                                        //Error notification mail start
                                        ctx.broker.call("notification.sendEmail", {
                                            toAddresses: [`naveen.kh@iinerds.com`],
                                            ccAddress: ['nissanth.s@iinerds.com'],
                                            message: `code : ${code} <br/> state : ${state} <br/> connected_account_id : ${connected_account_id} <br/><br/>
                                            Stripe account didnt mapped to any groups`,
                                            subject: `Stripe account mapping error`
                                        })
                                        //Error notification mail end
                                    });
                                    // save account id to group table end     
                                } else {
                                    console.log("No group for updating account ID");
                                    //Error notification mail start
                                    ctx.broker.call("notification.sendEmail", {
                                        toAddresses: [`naveen.kh@iinerds.com`],
                                        ccAddress: ['nissanth.s@iinerds.com'],
                                        message: `code : ${code} <br/> state : ${state} <br/> connected_account_id : ${connected_account_id} <br/><br/>
                                        Stripe account didnt mapped to any groups`,
                                        subject: `Stripe account mapping error`
                                    })
                                    //Error notification mail end
                                }
                            })
                            .catch((err) => {
                                console.log("Topic users getting Error",err);
                            })
                        },
                        (err) => {
                            if (err.type === 'StripeInvalidGrantError') {
                                reject('Invalid authorization code: ', code);
                            } else {
                                reject('An unknown error occurred.');
                            }
                        }
                    );
                })
            }       
        },
        stripe_oauth_link_generation:{
            params:{
                group_id : "string"
            },
            handler(ctx) {
                return new Promise(async(resolve, reject) => {
                    let group_id = ctx.params.group_id;
                    let email = ctx.params.email ? ctx.params.email : '';
                    let url = ctx.params.url ? ctx.params.url : '';

                    //Check state key already exist start
                    let state_key = '';
                    await ctx.call("db.groups.find",{query:{id:group_id},fields:["stripe_oauth_ref_key"]})
                    .then((groupObj)=>{
                        if(groupObj && groupObj.length > 0) {
                            if(groupObj[0].stripe_oauth_ref_key == null || groupObj[0].stripe_oauth_ref_key == '') {
                                state_key = unicKeyGen(10,group_id);    
                            } else {
                                state_key = groupObj[0].stripe_oauth_ref_key;
                            } 
                        } else {
                            state_key = unicKeyGen(10,group_id);
                        }    
                    })
                    .catch((err) => {
                        console.log(err);
                        state_key = unicKeyGen(10,group_id);
                    });
                    //Check state key already exist end

                    // let redirect_uri = "localhost:4000/api/v1/familheey/payment/stripe_oauth";
                    // save state id to group table start
                    let stateUpdateObj = {
                        id : group_id,
                        stripe_oauth_ref_key : state_key
                    }
                    ctx.call("db.groups.update",stateUpdateObj) 
                    .then((groupUpdateRes)=>{
                        if(groupUpdateRes) {
                            //Create & send dynamic link start 
                            let dynamic_link = `https://connect.stripe.com/oauth/authorize?client_id=${client_id}&state=${state_key}&scope=read_write&response_type=code&stripe_user[email]=${email}&stripe_user[url]=${url}`;
                            let response = {
                                link : dynamic_link,
                                state : state_key
                            }
                            resolve(response);
                            //Create & send dynamic link end   
                        }
                    }) 
                    .catch((err) => {
                        console.log(err);
                        reject('Link updation has some error:',err);
                    });
                    // save state id to group table end
                })
            }   
        },
        stripeGetaccountById:{
            params:{
                group_id : "string"
            },
            handler(ctx) {
                return new Promise(async(resolve, reject) => {
                    let {group_id} = ctx.params;
                    //Get account id from group table start
                    await ctx.call("db.groups.find",{query:{id:group_id},fields:["stripe_account_id"]})
                    .then((accountId)=>{
                        if(accountId && accountId.length > 0) {
                            if(accountId[0].stripe_account_id && accountId[0].stripe_account_id != '') {
                                let account_id = accountId[0].stripe_account_id;
                                stripe.accounts.retrieve(
                                    account_id,
                                    function(err, account) {
                                        if(account) {
                                            resolve(account);
                                            return;
                                        }
                                        if(err) {
                                            console.log("ERROR",err);
                                            reject(err);
                                        }
                                    }
                                );
                            } else {
                                reject('There is no stripe account mapped in this account');
                                return;      
                            }
                        } else {
                            reject('There is no stripe account mapped in this account');
                            return;   
                        }
                    })
                    .catch((err) => {
                        console.log(err);
                        reject('getting account id from groups has some error:',err);
                        return;
                    });
                    //Get account id from group table end

                    
                })
            }    
        },
        stripeWebhook:{
            params:{},
            handler(ctx){
                let { type, data } = ctx.params;
                let { object } = data;
                let { metadata,amount,payment_method_types } = object;
                //Convert cent to dollar start
                amount = amount/100;
                //Convert cent to dollar end
                let { contribution_id, payment_from_user, to_group_id, to_subtype, to_subtype_id,to_account_id, to_type, to_type_id,membership_payment_notes,membership_customer_notes } = metadata;
                let _params = {};
                let payments = {};
                return new Promise(async(resolve, reject) => {
                    switch (type) {
                        case "payment_intent.succeeded":
                            //If type request updation contribution table update start
                            if(to_type == "request") {
                                ctx.params.ntype = "request";
                                _params = {
                                    id:parseInt(contribution_id),
                                    is_acknowledge:true,
                                    payment_status:"success",
                                    is_active:true,
                                    payment_type:'card'
                                }
                                ctx.call("db.post_requests_item_contributions.update", _params)
                                .then((res) => {resolve(true)})
                                .catch((err)=>reject(err))
                            }
                            //If type request updation contribution table update end

                            //If type membership updation group_map table update start
                            if(to_type == "membership") {
                                ctx.params.ntype = "membership";
                                let currentTimestamp = moment.utc().unix();

                                _params = {
                                    id:parseInt(contribution_id),
                                    membership_payment_type:"online",
                                    membership_paid_on:currentTimestamp,
                                    payment_object:object
                                }

                                //Group map update with calculated data start
                                ctx.call('db.group_map.find', {query:{id:contribution_id}})
                                .then((getRes) => {
                                    if(getRes && getRes.length > 0) {
                                        //Payment calculations start
                                        let membershipAmount = amount ? amount : 0;
                                        let membershipFee = getRes[0].membership_fees;
                                        if(membershipFee == '' || membershipFee == null) {
                                            membershipFee = 0;   
                                        }
                                        let totalPayedAmount = parseInt(getRes[0].membership_total_payed_amount) + parseInt(membershipAmount);
                                        let outstandingAmount = membershipFee - totalPayedAmount;

                                        let email_data = {
                                            payment_type: _params.membership_payment_type,
                                            payment_method: ctx.params.membership_payment_method,
                                            outstanding_amount:outstandingAmount,
                                            payed_amount: membershipAmount,
                                            total_payed_amount: getRes[0].membership_total_payed_amount
                                        }
            
                                        ctx.params = { ...ctx.params, ...getRes[0], email_data:email_data }
                                        
                                        _params.membership_total_payed_amount = totalPayedAmount;
                                        //Payment calculations end

                                        //Create member ship id if it is null start
                                        if(!getRes[0].membership_ref_id || getRes[0].membership_ref_id == null || getRes[0].membership_ref_id == '') {
                                            ctx.params.membership_ref_id = unicKeyGen(10,getRes[0].group_id);
                                        }
                                        //Create member ship id if it is null end

                                        if(outstandingAmount <= 0) {
                                            //outstandingAmount = 0;
                                            _params.membership_payment_status = 'Completed';   
                                        } else {
                                            _params.membership_payment_status = 'Partial';
                                            ctx.params = { ...ctx.params, Enotify_type:'partial_payment', Enotify_subtype:ctx.params.membership_payment_status }    
                                            ctx.call("common_methods.paymentNotifyEmail",ctx.params)  
                                        }

                                        ctx.call("db.group_map.update", _params)
                                        .then((res) => {
                                            resolve(true);

                                            //Add payment history start
                                            let historyObj = {
                                                user_id : res.user_id ? parseInt(res.user_id) : null,
                                                group_id : res.group_id ? parseInt(res.group_id) : null,
                                                membership_payed_amount : membershipAmount ? parseInt(membershipAmount) : null,
                                                membership_id : res.membership_id ? parseInt(res.membership_id) : null,
                                                membership_from : res.membership_from ? parseInt(res.membership_from) : null,
                                                membership_to : res.membership_to ? parseInt(res.membership_to) : null,
                                                membership_fees : res.membership_fees ? parseInt(res.membership_fees) : null,
                                                membership_paid_on : res.membership_paid_on ? parseInt(res.membership_paid_on) : null,
                                                membership_duration : res.membership_duration ? parseInt(res.membership_duration) : null,
                                                membership_payment_status : res.membership_payment_status ? res.membership_payment_status : '',
                                                membership_period_type : res.membership_period_type ? res.membership_period_type : '',
                                                membership_payment_notes : membership_payment_notes ? membership_payment_notes : null,
                                                membership_customer_notes : membership_customer_notes ? membership_customer_notes : '',
                                                payment_object : res.payment_object ? res.payment_object : '',
                                                membership_payment_type : res.membership_payment_type ? res.membership_payment_type : '',
                                                membership_payment_method : res.membership_payment_method ? res.membership_payment_method : '',
                                                membership_total_payed_amount : res.membership_total_payed_amount ? parseInt(res.membership_total_payed_amount) : 0,
                                                membership_ref_id : res.membership_ref_id ? res.membership_ref_id : '',
                                            }
                                            ctx.call("db.membership_payment_history.create", historyObj)
                                            .then(res2 => {
                                                console.log('Successfully added to payment history');
                                            })
                                            .catch((err) => console.log('membership payment history has some error'))
                                            //Add payment history end

                                            //Track user_membership_history start
                                            let historyObj2 = {
                                                group_id : parseInt(to_group_id),
                                                member_user_id : parseInt(payment_from_user),
                                                paid_amount : parseInt(amount),
                                                payment_status : "success",
                                                membership_id : getRes[0].membership_id,
                                                current_data : getRes[0],
                                                payment_object:object
                                            }
                                            ctx.call('db.user_membership_histories.create', historyObj2)
                                            .then((historRresult) => {
                                                console.log("Successfully added user_membership_history");   
                                            }).catch((err) => {
                                                console.log("user_membership_history creation has some error",err);   
                                            });
                                            //Track user_membership_history end

                                        })
                                        .catch((err)=>reject(err))
                                    }
                                }).catch((err) => {
                                    console.log("Group_maps get has some error",err);
                                });
                                //Group map update with calculated data start
                            }
                            //If type membership updation group_map table update end

                            payments = {
                                from_user:payment_from_user,
                                to_account_id:to_account_id,
                                group_id:to_group_id,
                                type:to_type,
                                type_id:to_type_id,
                                subtype:to_subtype ? to_subtype : '',
                                payment_status:"Completed",
                                payment_object:object
                            }
                            if(to_subtype_id && to_subtype_id != '') {
                                payments.subtype_id = to_subtype_id    
                            }
                            ctx.call("db.payment.create", payments);
                            ctx.params = { ...ctx.params, payments:payments, user_id:payment_from_user }
                            ctx.params.user_id = payment_from_user;
                            ctx.params.group_id = to_group_id;
                            ctx.params.payment_method_types = payment_method_types;
                            this.appNotify(ctx);
                            // ctx.call('notification.sendEmail', {
                            //     toAddresses: ['contact@familheey.com'],
                            //     message: payments.toString(),
                            //     subject: `Payment Success`
                            // }).then((res) => { console.log(res) }).catch((err) => { console.log(err) })   
                        break;

                        case "payment_intent.payment_failed":
                            //If type request updation contribution table update start
                            if(to_type == "request") {
                                _params = {
                                    id:contribution_id,
                                    is_acknowledge:false,
                                    payment_status:"failed",
                                }
                                ctx.call("db.post_requests_item_contributions.update", _params)
                                .then((res) => {resolve(true)})
                                .catch((err)=>reject(err))
                            }
                            //If type request updation contribution table update end

                            //If type membership updation group_map table update start
                            if(to_type == "membership") {
                                let currentTimestamp = moment.utc();
                                _params = {
                                    id:contribution_id,
                                    membership_payment_status:"failed",
                                    membership_paid_on:currentTimestamp
                                }
                                ctx.call("db.group_map.update", _params)
                                .then((res) => {resolve(true)})
                                .catch((err)=>reject(err))
                            }
                            //If type membership updation group_map table update end
                            
                            payments = {
                                from_user:payment_from_user,
                                to_account_id:to_account_id,
                                group_id:to_group_id,
                                type:to_type,
                                type_id:to_type_id ? to_type_id : '',
                                subtype:to_subtype ? to_subtype : '',
                                subtype_id:to_subtype_id ? to_subtype_id : '',
                                payment_status:"failed",
                                payment_object:object
                            }
                            
                            ctx.call("db.payment.create", payments);
                            ctx.call('notification.sendEmail', {
                                toAddresses: ['contact@familheey.com'],
                                message: payments,
                                subject: `Payment Failure`
                            }).then((res) => { console.log(res) }).catch((err) => { console.log(err) })   
                        break;
                    
                        default:
                            resolve(true)
                        break;
                    }
                })
            }
        },
        paymentHistoryByUserid:{
            params:{
                user_id:'string',
                type:'string' // request /membership
            },
            handler(ctx){
                let { type } = ctx.params;
                return new Promise(async(resolve, reject) => {

                    switch (type) {
                        case 'request':
                            ctx.call("db.payment.getrequestPaymentHistory",ctx.params)
                            .then((doc)=>{
                                for(let i=0 ; i < doc.length ; i++){
                                    if(doc[i].is_acknowledge == true) {
                                        doc[i].payment_status = 'success';    
                                    }
                                }
                                resolve({ code:200, message:'fetch success', data:doc })
                            })
                            .catch((err)=>{
                                reject(err)
                            })
                        break;

                        case 'membership':
                            ctx.call("db.membership_payment_history.getMembershipPaymentHistory",ctx.params)
                            .then((doc)=>{
                                resolve({ code:200, message:'fetch success', data:doc })
                            })
                            .catch((err)=>{
                                reject(err)
                            })
                        break;
                    
                        default:
                            reject("Type is incorrect")
                        break;
                    }
                })
            }
        },
        paymentHistoryByGroupid:{
            params:{
                group_id:'string',
                type:'string' // request /membership
            },
            handler(ctx){
                let { type } = ctx.params;
                return new Promise(async(resolve, reject) => {

                    switch (type) {
                        case 'request':
                            ctx.call("db.payment.getrequestPaymentHistoryByGroup",ctx.params)
                            .then((doc)=>{
                                for(let i=0 ; i < doc.length ; i++){
                                    if(doc[i].user_id == 2) {
                                        doc[i].full_name = doc[i].paid_user_name;    
                                    }
                                }
                                resolve({ code:200, message:'fetch success', data:doc })
                            })
                            .catch((err)=>{
                                reject(err)
                            })
                        break;

                        case 'membership':
                            ctx.call("db.membership_payment_history.getMembershipPaymentHistoryByGroup",ctx.params)
                            .then((doc)=>{
                                for(let i=0 ; i < doc.length ; i++){
                                    if(doc[i].mphrefid != doc[i].gmrefid) {
                                        doc[i].payment_status = 'Completed';    
                                    }
                                }
                                resolve({ code:200, message:'fetch success', data:doc })
                            })
                            .catch((err)=>{
                                reject(err)
                            })
                        break;
                    
                        default:
                            reject("Type is incorrect")
                        break;
                    }
                })
            }
        }
    },
    events: {},
    methods: {
        appNotify(ctx) {
            let { to_user_id, user_id, ntype, payments,group_id,payment_method_types } = ctx.params;
            let _this = this;
            _async.parallel({
                tousers: function (callback) {
                    if (to_user_id) {
                        ctx.broker.call('db.users.find', { query: { id: to_user_id }, fields: ["id", "full_name", "propic"] })
                        .then((res) => {
                            callback(null, res);
                        })
                        .catch((err) => {
                            callback(null, []);
                            console.log("User Search Error", err);
                        })
                    }else if(payments && payments.group_id){
                        ctx.broker.call('db.group_map.find', { query: { group_id: payments.group_id, is_blocked:false, type:'admin' }, fields: ["user_id"] })
                        .then((res) => {
                            callback(null, res);
                        })
                        .catch((err) => {
                            callback(null, []);
                            console.log("User Search Error", err);
                        })
                    }
                },
                fromUser: (callback) => {
                    ctx.call("db.users.find", { query: { id: user_id }, fields: ["id", "full_name", "propic"] })
                    .then(res => {
                        callback(null, res);
                    }).catch(err => {
                        console.log("error happened -------- familyMembers", err);
                    })
                },
                item_details:(callback) => {
                    if(payments && payments.subtype_id){
                    ctx.call("db.post_request_items.find", { query: { id: payments.subtype_id }, fields: ["id", "request_item_title"] })
                    .then(res => {
                        callback(null, res);
                    }).catch(err => {
                        callback(null,[])
                        console.log("error happened -------- familyMembers", err);
                    })
                    }else{
                        callback(null,[])
                    }
                }
                
            },
            function (err, results) {
                let { tousers, fromUser, item_details } = results;
                if(payment_method_types && payment_method_types.length > 0) {
                    payment_method_types = payment_method_types[0];  
                }
                tousers.forEach(async elem => {
                    let sendObj = {
                        from_id: fromUser.length > 0 ? fromUser[0].id : '',
                        type: 'family',
                        propic: fromUser.length > 0 ? fromUser[0].propic : '',
                        to_id: elem.user_id ? parseInt(elem.user_id) : parseInt(elem.id),
                        sub_type: '',
                        category: ntype,
                        message_title: 'familheey',
                        type_id: group_id ? group_id : '',
                        link_to: group_id ? group_id : ''
                    }
                    switch (ntype) {
                        case 'request':
                            let request_item_title =  item_details.length>0 ? item_details[0].request_item_title : '';
                            sendObj.message  = `${fromUser[0].full_name} offered to help against ${request_item_title} `
                            sendObj.sub_type = 'requestFeed';
                            sendObj.type = 'home';
                            sendObj.type_id  = parseInt(payments.type_id);
                            sendObj.link_to  = parseInt(payments.type_id);
                        break;

                        case 'membership':
                            sendObj.message  = `${fromUser[0].full_name} have paid the membership via ${payment_method_types}`;
                            sendObj.sub_type = 'member';
                            sendObj.type = 'family';
                            sendObj.type_id  = parseInt(group_id);
                            sendObj.link_to  = parseInt(group_id);
                        break;

                        default:
                        break;
                    }
                    if ( parseInt(fromUser[0].id) != parseInt(sendObj.to_id) ) {
                        ctx.broker.call("notification.sendAppNotification", sendObj);
                        let _user_notify = await ctx.call("db.users.find", { query: { id: sendObj.to_id }, fields: ["notification"] })
                        if (_user_notify.length > 0 && _user_notify[0].notification) {
                            _this.pushNotify(ctx, sendObj);
                        }
                    }
                })
            })
        },
        pushNotify(ctx, params) {
            ctx.broker.call("userops.getregisterToken", { user_id: params.to_id, is_active: true })
            .then((_devices) => {
                _devices.data.forEach(elem => {
                    params = {
                        ...params,
                        token: elem.device_token,
                        device_type: elem.device_type.toLowerCase(),
                        title: params.message_title,
                        body: this.removeTags(params.message),
                        sub_type: params.sub_type ? params.sub_type : ''
                    }
                    ctx.broker.call("notification.sendPushNotification", params)
                })
            })
            .catch(e => e)
        },
        removeTags(str) {
            if ((str === null) || (str === '')) return '';
            else return str.toString().replace(/(<([^>]+)>)/ig, '');
        }  
    },
    created() { },
    started() { },
    stopped() { }
}
