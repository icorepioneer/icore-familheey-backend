"use strict";
const E = require("moleculer-web").Errors;

const ApiGateway = require("moleculer-web");
const path = require('path')
const uploadDir = path.join(__dirname, "../../public/uploads");
const multer = require("multer");
var _async = require('async');
var storage = multer.diskStorage({
	destination: function (req, file, cb) {
		cb(null, uploadDir)
	},
	filename: function (req, file, cb) {
		let fieldname;
		// /v1/familheey/uploadFile?user_id=1081&&folder_id=369&&is_sharable=true
		let eext = `.${file.originalname.split('.').reverse()[0]}`;
		// cb(null, `${file.fieldname==="file"?"Documents":file.fieldname}` + '-' + Date.now() + eext)
		if(req.url.includes("/v1/familheey/uploadFile?user_id=")&&req.url.includes("&&is_sharable=true")){
			fieldname = file.fieldname==="file"?"Documents":file.fieldname 
		}
		cb(null, `${fieldname?fieldname:file.fieldname}` + '-' + Date.now() + eext)
	}
});
var _upload = multer({ storage: storage })
function test() {
	return function test12(req, res, next) {
		if (!req.headers.token && req.method != 'OPTIONS') {
			// res.setHeader("Content-Type", "application/json");
			// res.writeHead(500);
			// res.end("{message:'invalid'}");
			// console.log(res);
			// return res;
			// throw new MoleculerError("Something happened", 501, "ERR_SOMETHING", { a: 5, nodeID: "node-666" });
		}
		//console.log(req.headers.authorization);
		next();
	}
}

module.exports = {
	name: "api",
	mixins: [ApiGateway],
	// More info about settings: https://moleculer.services/docs/0.13/moleculer-web.html
	settings: {
		cors: {
			// Configures the Access-Control-Allow-Origin CORS header.
			origin: "*",
		},
		port: process.env.PORT || 5000,
		routes: [
			{
				path: "/api",
				use: [
					_upload.any(),
					test()
				],

				whitelist: [
					// Access to any actions in all services under "/api" URL
					"**"
				],
				callOptions: {
					timeout: 120 * 10000,
					// retries: 3,
					// fallbackResponse(ctx, err) { ... }
				},
				/**
				 * middleware token varification
				 * true :: enable
				 */
				authorization: true,

				aliases: {
					//cognito
					"POST /v1/familheey/register": "cognito.addUser",
					"POST /v1/familheey/login": "cognito.loginUser",
					"POST /v1/familheey/confirmotp": "cognito.verifyOTP",
					"POST /v1/familheey/resendOTP": "cognito.resendOTP",
					"POST /v1/familheey/loginWithOtp": "cognito.loginWithOtp",
					"POST /v1/familheey/getAccessToken": "cognito.getAccessToken",
					//userOps
					"POST /v1/familheey/edit_profile": "userops.updateProfile",
					"POST /v1/familheey/user_profile_details": "userops.userProfileDetails",
					"POST /v1/familheey/get_connections": "userops.getConnections",
					//groups
					"POST /v1/familheey/create_family": "groups.createGroup",
					"POST /v1/familheey/update_family": "groups.updateGroup",
					"POST /v1/familheey/viewFamily": "groups.viewFamily",
					"POST /v1/familheey/fetch_group": "groups.fetchGroup",
					"POST /v1/familheey/view_request": "admin.viewRequest",
					"POST /v1/familheey/view_profile": "userops.viewProfile",
					"POST /v1/familheey/addToGroup": "groups.addMemberTogroup",
					"POST /v1/familheey/search": "groups.memberSearch",
					"POST /v1/familheey/joinFamily": "groups.joinFamily",
					"POST /v1/familheey/approveReq": "groups.approveRequest",
					"POST /v1/familheey/rejectReq": "groups.rejectRequest",
					"POST /v1/familheey/unfollow": "groups.unfollowGroup",
					"POST /v1/familheey/getFamilyById": "groups.getFamilyById",
					"POST /v1/familheey/viewMembers": "groups.viewMembers",
					"POST /v1/familheey/updateSettings": "groups.updateGroupSettings",
					"POST /v1/familheey/listAllMembers": "groups.listAllMembers",
					"POST /v1/familheey/sendInvitation": "groups.sendInvitation",
					"POST /v1/familheey/invitationResp": "groups.invitationStatusbyUser",
					"POST /v1/familheey/follow": "groups.followGroup",
					"POST /v1/familheey/getGroupsToAddMember": "groups.getAllGroupsBasedOnUserId",
					"POST /v1/familheey/listallInvitation": "groups.listallInvitation",
					"POST /v1/familheey/inviteViaSms": "groups.inviteViaSms",
					"POST /v1/familheey/view_family_details": "groups.viewFamilyDetails",
					"POST /v1/familheey/leave_family": "groups.leaveFamily",
					//notifications
					"POST /v1/familheey/sendSms": "notification.sendSms",
					"POST /v1/familheey/sendEmail": "notification.sendEmail",
					"POST /v1/familheey/twilioSms": "notification.sendTwilioSms",
					//relation
					"POST /V1/familheey/addRelation": "relation.addRelation",
					"POST /v1/familheey/updateRelation": "relation.updateRelation",
					"POST /v1/familheey/getallRelations": "relation.getallRelations",
					//admin
					"POST /v1/familheey/admin_view_requests": "admin.viewMemberRequest",
					"POST /v1/familheey/addUserHistory": "user_history.addUserHistory",
					"POST /v1/familheey/globalsearch": "globalsearch.globalsearch",
					"POST /v1/familheey/fetch_link_family": "groups.fetchLinkFamily",
					"POST /v1/familheey/request_link_family": "groups.requestToLinkFamily",
					"POST /v1/familheey/admin_link_request": "groups.adminListLinkRequest",
					"POST /v1/familheey/admin_action_linking": "groups.adminActionLinking",
					"POST /v1/familheey/update_groupmaps": "groups.updateGroupmaps",
					"POST /v1/familheey/admin_request_action": "admin.adminRequestAction",
					//resources
					"POST /v1/familheey/createFolder": "resources.createFolder",
					"POST /v1/familheey/listUserFolders": "resources.listUserFolders",
					"POST /v1/familheey/listGroupFolders": "resources.listGroupFolders",
					"POST /v1/familheey/listEventFolders": "resources.listEventFolders",
					"POST /v1/familheey/updateFolder": "resources.updateFolder",
					"POST /v1/familheey/uploadFile": "resources.addDocuments",
					"POST /v1/familheey/viewContents": "resources.viewDocuments",
					"POST /v1/familheey/removeFolder": "resources.deleteFolder",
					//status
					"POST /v1/familheey/create_status": "userops.createStatus",
					"POST /v1/familheey/fetch_status": "userops.fetchStatus",
					"POST /v1/familheey/update_status": "userops.updateStatus",
					//events
					"POST /v1/familheey/create_events": "events.createEvents",
					"POST /v1/familheey/update_events": "events.updateEvents",
					"POST /v1/familheey/get_events": "events.getEvents",
					"POST /v1/familheey/respondToEvents": "events.respondToEvents",
					"POST /v1/familheey/addEventSpeaker": "events.addEventSpeaker",
					"POST /v1/familheey/addEventContact": "events.addEventContact",
					"POST /v1/familheey/event_shareor_invite": "events.eventShareOrinvite",//invites or share event to a user/group/others
					"POST /v1/familheey/add_event_signupitems": "events.addEventSignupItems",
					"POST /v1/familheey/update_event_signupitems": "events.updateEventSignupItems",
					"POST /v1/familheey/delete_event_signupitems": "events.deleteEventSignupItems",
					"POST /v1/familheey/event_share_list": "events.eventShareList",
					"POST /v1/familheey/event_deleteorcancel": "events.eventDeleteOrCancel",
					"POST /v1/familheey/event_reminder": "events.event_reminder",
					"POST /v1/familheey/birthday_wish": "userops.birthday_wish",
					"POST /v1/familheey/get_event_basedOnEventPage": "events.getEventBasedOnEVentPage",
					//"POST /v1/familheey/share_event": "events.shareEvent",
					"POST /v1/familheey/get_eventitems": "events.getEventItems",
					"POST /v1/familheey/get_event_list": "events.getEventList",
					"POST /v1/familheey/update_follow_status": "events.updateFollowStatus",
					"POST /v1/familheey/event_details": "events.eventDetails",
					"POST /v1/familheey/add_eventitems_contribution": "events.addEventItemsContribution",
					"POST /v1/familheey/update_eventitems_contribution": "events.updateEventItemsContribution",
					"POST /v1/familheey/delete_eventitems_contribution": "events.deleteEventItemsContribution",
					"POST /v1/familheey/event_invitation_list": "events.eventInvitationList",
					"POST /v1/familheey/user_event_invitation_list": "events.userInvitationEventList",
					"POST /v1/familheey/fetch_category": "events.fetchCategory",
					"POST /v1/familheey/block_event": "events.blockEvent",
					"POST /v1/familheey/unblock_event": "events.unBlockEvent",
					"POST /v1/familheey/create_agenda": "events.addAgenda",
					"POST /v1/familheey/update_agenda": "events.updateAgenda",
					"POST /v1/familheey/list_agenda": "events.listAgenda",
					"POST /v1/familheey/explore_events": "events.exploreEvents",
					"POST /v1/familheey/event_invitations": "events.eventInvitations",
					"POST /v1/familheey/created_by_me": "events.createdByMe",
					"POST /v1/familheey/get_guests_count": "events.getguestsCount",
					"POST /v1/familheey/get_event_byId": "events.getEventById",
					"POST /v1/familheey/get_going_guest_details": "events.getGoingGuestDetails",
					"POST /v1/familheey/getRSVP_guest_details": "events.getRSVPGuestDetails",
					"POST /v1/familheey/get_invited_user_details": "events.getInvitedUserDetails",
					/** calender */
					"POST /v1/familheey/saveto_calender": "calender.savetoCalender",
					"POST /v1/familheey/update_calender": "calender.updateCalender",
					"POST /v1/familheey/fetch_calender": "calender.fetchCalender",
					//"POST /v1/familheey/search_calender": "calender.fetchCalender",

					"POST /v1/familheey/list_linked_family": "groups.listLinkedFamily",
					"POST /v1/familheey/action_linked_family": "groups.actionLinkedFamily",

					"POST /v1/familheey/event_viewed": "events.eventViewed",
					"POST /v1/familheey/check_link": "events.checkEventLink",

					"POST /v1/familheey/user_group_members": "groups.userGroupMembers",
					/** reminders :: events */
					"POST /v1/familheey/create_reminder": "events.createReminder",
					"POST /v1/familheey/update_reminder": "events.updateReminder",
					"POST /v1/familheey/fetch_reminder": "events.fetchReminder",

					"POST /v1/familheey/group_events": "groups.groupEvents",
					"POST /v1/familheey/make_pic_cover": "resources.makePicCover",

					"POST /v1/familheey/item_contributor_list": "events.itemContributorList",
					"POST /v1/familheey/delete_file": "resources.deleteFile",
					/** post */
					"POST /v1/familheey/posts/activatePost": "posts.activatePost",
					"POST /v1/familheey/posts/deactivatePost": "posts.deactivatePost",
					"POST /v1/familheey/posts/create": "posts.create",
					"POST /v1/familheey/posts/update": "posts.update",
					"POST /v1/familheey/posts/update_post": "posts.update_post",
					"POST /v1/familheey/posts/getByID": "posts.getByID",
					"POST /v1/familheey/posts/listall": "posts.listAll",
					"POST /v1/familheey/posts/post_by_family": "posts.post_by_family",
					"POST /v1/familheey/posts/announcement_list": "posts.announcement_list",
					"POST /v1/familheey/posts/post_by_user": "posts.post_by_user",
					"POST /v1/familheey/posts/get_my_post": "posts.get_my_post",
					"POST /v1/familheey/posts/public_feed": "posts.publicFeed",
					"POST /v1/familheey/posts/like": "posts.like",
					"POST /v1/familheey/posts/getSidebarCount": "posts.getSidebarCount",
					"POST /v1/familheey/posts/post_share": "posts.postShare",
					"POST /v1/familheey/posts/getCommentsByPost": "posts.getCommentsByPost",
					"POST /v1/familheey/posts/post_comment": "posts.postComment",
					"POST /v1/familheey/posts/delete_comment": "posts.delete_comment",
					"POST /v1/familheey/posts/add_view_count": "posts.addViewCount",
					"POST /v1/familheey/posts/list_post_views": "posts.list_post_views",
					"POST /v1/familheey/posts/getSharedUserList": "posts.getSharedUserList",
					"POST /v1/familheey/posts/getCommonSharedUserList": "posts.getCommonSharedUserList",
					"POST /v1/familheey/posts/getSharedUserListByUserid": "posts.getSharedUserListByUserid",
					"POST /v1/familheey/posts/unread": "posts.unread",
					"POST /v1/familheey/posts/get_my_post_aggregate": "posts.get_my_post_aggregate",

					"POST /v1/familheey/posts/updateLastReadMessage": "posts.updateLastReadMessage",
					"POST /v1/familheey/posts/mute_conversation": "posts.mute_conversation",
					"POST /v1/familheey/posts/get_active_notification_count": "posts.getActiveNotificationCount",
					"POST /v1/familheey/posts/deletePost": "posts.deletePost",
					"POST /v1/familheey/posts/remove_post": "posts.remove_post",
					"POST /v1/familheey/posts/pending_approvals": "posts.pendingApprovals",
					"POST /v1/familheey/posts/approve_reject_post": "posts.approve_post",

					"POST /v1/familheey/posts/getFamilyListForPost": "posts.getFamilyListForPost",
					"POST /v1/familheey/posts/addUpdate_Announcement_Seen": "posts.addUpdateAnnounceSeen",
					"POST /v1/familheey/posts/get_announcement_banner_count": "posts.announceBannerCount",
					"POST /v1/familheey/posts/post_sticky_by_family": "posts.post_sticky_by_family",

					"POST /v1/familheey/blocked_users": "userops.blockedUsers",
					"POST /v1/familheey/receive_feedback": "feedback.receiveFeedback",
					"POST /v1/familheey/change_folder_permission": "resources.changeFolderPermission",

					"POST /v1/familheey/event_invited_byUser": "events.eventInvitedByUser",
					"POST /v1/familheey/getMutualConnections": "groups.getMutualConnections",
					"POST /v1/familheey/getMutualConnectionsList": "groups.getMutualConnectionsList",

					"POST /v1/familheey/get_family_mutual_list": "groups.getFamilyMutualList",
					"POST /v1/familheey/other_users": "cognito.otherUsersList",
					"POST /v1/familheey/get": "redis.deleteKeys",
					"POST /v1/familheey/upload_file_tos3": "posts.uploadFileToS3",
					"POST /v1/familheey/generateSignedUrl":"posts.generateSignedUrl",
					"POST /v1/familheey/spam_report": "posts.spam_report",
					"POST /v1/familheey/check_post_permission": "posts.checkPostPermission",
					"POST /v1/familheey/registerToken": "userops.registerToken",
					"POST /v1/familheey/sendPushNotification": "notification.sendPushNotification",

					"POST /v1/familheey/editEventContact": "events.editEventContact",
					"POST /v1/familheey/deleteEventContact": "events.deleteEventContact",
					"POST /v1/familheey/delete_from_s3": "s3upload.deleteFile",
					"GET /v1/familheey/cronForReminder": "cronjob.cronForReminder",
					"POST /v1/familheey/copy_file_between_bucket": "s3upload.CopyFileBetweenBucket",

					"POST /v1/familheey/forgot_password": "cognito.forgotPassword",//aws send an otp
					"POST /v1/familheey/change_password": "cognito.changePassword",//resetwithotp
					"POST /v1/familheey/reset_password": "cognito.resetPassword",//reset a password

					"POST /v1/familheey/family_link_exist": "groups.familyLinkExist",
					"POST /v1/familheey/user_mutual_group": "groups.userMutualGroup",
					"POST /v1/familheey/updateFilesData": "resources.updateFilesData",
					"POST /v1/familheey/mygroupMember": "userops.mygroupMember",
					"POST /v1/familheey/onBoardCheck": "userops.onBoardCheck",
					"POST /v1/familheey/vimeouploadvideo": "video_service.vimeo_upload_video",

					//bulk upload
					"POST /v1/familheey/family_link_create": "bulk.familyLinkCreate",
					"POST /v1/event_invitation" : "bulk.eventInvitation",
					"POST /v1/add_family_member": "bulk.addFamilyMember",
					"POST /v1/familheey/mob_bulk": "bulk.mobBulk",
					//dummy
					"POST /v1/familheey/dummy": "resources.dummyAPI",
					"POST /v1/familheey/checkUserGroups": "groups.checkUserGroups",
					"POST /v1/familheey/cancelRequest": "groups.cancelRequest",
					"POST /v1/familheey/getUserActivities": "user_activities.getUserActivities",
					"POST /v1/familheey/update_file_name": "resources.updateFileName",
					"POST /v1/familheey/pending_requests": "userops.pendingRequest",
					"POST /v1/familheey/delete_pending_requests": "userops.deletePendingRequest",

					"POST /v1/familheey/updateFileuploads": "userops.updateFileuploads",
					"POST /v1/familheey/signedUrl": "s3upload.signedUrl",
					"POST /v1/familheey/clearNotification": "notification.clearNotification",

					"POST /v1/familheey/PhoneContactStatus": "userops.PhoneContactStatus",

					"POST /v1/familheey/custom": "userops.Custom",
					"POST /v1/familheey/isUserExist": "userops.isUserExist",
					"POST /v1/familheey/createSubscription": "userops.createSubscription",

					/** Post Needs **/
					"POST /v1/familheey/needs/create": "post_needs.create",
					"POST /v1/familheey/needs/post_need_list": "post_needs.post_need_list",
					"POST /v1/familheey/needs/post_need_detail": "post_needs.post_need_detail",
					"POST /v1/familheey/needs/update": "post_needs.update",
					"POST /v1/familheey/needs/delete": "post_needs.delete",

					"POST /v1/familheey/needs/create_items": "post_needs.createItems",
					"POST /v1/familheey/needs/update_items": "post_needs.updateItems",
					"POST /v1/familheey/needs/delete_items": "post_needs.deleteItems",

					"POST /v1/familheey/needs/contribution_create": "post_needs.contributionCreate",
					"POST /v1/familheey/needs/contribution_update": "post_needs.contributionUpdate",
					"POST /v1/familheey/needs/contribution_delete": "post_needs.contributionDelete",
					"POST /v1/familheey/needs/contribution_list": "post_needs.contributionListOrCount",
					"POST /v1/familheey/needs/get_contribute_itemAmount_split": "post_needs.getContributeItemAmountSplit",
					"POST /v1/familheey/needs/admin_user_selection": "post_needs.admin_user_selection",
					"POST /v1/familheey/needs/add_member_contribution_ByAdmin": "post_needs.add_member_contribution_ByAdmin",
					"POST /v1/familheey/needs/get_contribution_list_Byadmin": "post_needs.get_contribution_list_Byadmin",

					/** Topics **/
					"POST /v1/familheey/topic_create": "topic.createTopic",
					"POST /v1/familheey/add_users_to_topic": "topic.addUsersToTopic",
					"POST /v1/familheey/topic_list": "topic.topiclist",
					"POST /v1/familheey/delete_topic": "topic.deleteTopic",
					"POST /v1/familheey/remove_users_from_topic": "topic.removeUserFromTopic",
					"POST /v1/familheey/update_topic": "topic.updateTopic",
					"POST /v1/familheey/topic_users_list": "topic.topicUsersList",
					"POST /v1/familheey/topic_comment": "posts.topicComment",
					"POST /v1/familheey/activateTopic": "posts.activateTopic",
					"POST /v1/familheey/deactivateTopic": "posts.deactivateTopic",
					"POST /v1/familheey/topic_detail": "topic.topicdetail",
					"POST /v1/familheey/accept_user_topic": "topic.acceptUserTopic",
					"POST /v1/familheey/reject_user_topic": "topic.rejectUserTopic",
					"POST /v1/familheey/commonTopicListByUsers": "topic.commonTopicListByUsers",
					"POST /v1/familheey/add_membership_lookup": "membership.add_membership_lookup",
					"POST /v1/familheey/update_membership_lookup": "membership.update_membership_lookup",
					"POST /v1/familheey/user_membership_update": "membership.user_membership_update",
					"POST /v1/familheey/list_membership_lookup": "membership.list_membership_lookup",
					"POST /v1/familheey/membership_dashboard": "membership.membership_dashboard",
					"POST /v1/familheey/getMembershiptypeById": "membership.getMembershiptypeById",
					"POST /v1/familheey/Membership_reminder": "membership.Membership_reminder",
					"POST /v1/familheey/get_membership_lookup_periods": "membership.get_membership_lookup_periods",
					"POST /v1/familheey/publish_post": "common_methods.publish_post",
					"POST /v1/familheey/getThankPostusers": "post_needs.getThankPostusers",
					"POST /v1/familheey/contributionlistByUser": "post_needs.contributionlistByUser",
					"POST /v1/familheey/contributionStatusUpdation": "post_needs.contributionStatusUpdation",

					"POST /v1/familheey/payment/stripeCreatePaymentIntent": "payment.stripeCreatePaymentIntent",
					"POST /v1/familheey/payment/striptCreateCard": "payment.striptCreateCard",
					"POST /v1/familheey/payment/retriveCard": "payment.retriveCard",
					"POST /v1/familheey/payment/deleteCard": "payment.deleteCard",
					"POST /v1/familheey/payment/listAllCards": "payment.listAllCards",
					"POST /v1/familheey/payment/createConnectAccount": "payment.createConnectAccount",
					"POST /v1/familheey/payment/stripeCreateCustomerId": "payment.stripeCreateCustomerId",
					"POST /v1/familheey/payment/createAccountToken": "payment.createAccountToken",
					"POST /v1/familheey/payment/updateConnectAccount": "payment.updateConnectAccount",
					"POST /v1/familheey/payment/stripe_oauth_link_generation": "payment.stripe_oauth_link_generation",
					"POST /v1/familheey/payment/stripe_oauth": "payment.stripe_oauth",
					"POST /v1/familheey/payment/stripeGetaccountById": "payment.stripeGetaccountById",

					"POST /v1/familheey/get_post_default_image": "posts.getPostdefaultImage",

					"POST /v1/familheey/get_suggested_group_newUser": "groups.getSuggestedGroupNewUser",

					"POST /v1/familheey/send_app_notification": "notification.sendAppNotification",
					"GET /v1/familheey/admin/get_overview_db": "admin.getOverviewDB",

					"GET /v1/familheey/needExpiryReminder": "cronjob.needExpiryReminder",

					"POST /v1/familheey/contactForm": "contactForm.receive",

					"POST /v1/familheey/retriveUrls": "common_methods.retriveUrls",

					"POST /v1/familheey/sentence_extract": "common_methods.sentence_extract",

					"POST /v1/familheey/generateFirebaseUrl": "admin.generateFirebaseUrl",
					"POST /v1/familheey/stripe_webhook": "payment.stripeWebhook",

					"POST /v1/familheey/getUserAdminGroups": "groups.getUserAdminGroups",
					"POST /v1/familheey/groupMapUpdate": "groups.groupMapUpdate",

					"POST /v1/familheey/GetKeys": "common_methods.GetKeys",
					"POST /v1/familheey/GetBanners": "common_methods.GetBanners",
					"POST /v1/familheey/paymentHistoryByUserid": "payment.paymentHistoryByUserid",
					"POST /v1/familheey/paymentHistoryByGroupid": "payment.paymentHistoryByGroupid",
					"POST /v1/familheey/createExcel": "admin.createExcel",

					"POST /v1/familheey/post/stickyPost": "posts.stickyPost",
					"POST /v1/familheey/get_user_commented_post": "posts.get_user_commented_post",
					/** Elastic search  */
					"POST /v1/familheey/elastic/getRecords": "es_service.getRecords",
					"POST /v1/familheey/get_other_invited_users_by_group": "groups.get_other_invited_users_by_group",
					"POST /v1/familheey/auto_update_publicpost_elasticSearch": "posts.auto_update_publicpost_elasticSearch",
					"POST /v1/familheey/adminAddUserManually": "cognito.adminAddUserManually",
					"POST /v1/familheey/DeleteOneNotification": "notification.DeleteOneNotification",
					"POST /v1/familheey/get_notification_settings": "userops.get_notification_settings",

					"POST /v1/familheey/openAppGetParams": "common_methods.openAppGetParams",
					"POST /v1/familheey/manuallyUpdateElasticIndex": "common_methods.manuallyUpdateElasticIndex",
					"POST /v1/familheey/clearNotificationCroneJob": "notification.clearNotificationCroneJob",
					"POST /v1/familheey/fulluserCount": "userops.fulluserCount",
					"POST /v1/familheey/clearFullNotificationEnv": "notification.clearFullNotificationEnv",
					"POST /v1/familheey/readNotifications": "notification.readNotifications",
					"POST /v1/familheey/testPushNotification": "notification.testPushNotification",
					"POST /v1/familheey/postUpdate": "posts.postUpdateAttachment",
				},
				
				onBeforeCall(ctx, route, req, res) {
					// Check user authorization start
					let user_authorization_urls = [
						'/v1/familheey/edit_profile',
						'/v1/familheey/create_family',
						'/v1/familheey/createFolder',
						'/v1/familheey/create_events',
						'/v1/familheey/posts/create',
						'/v1/familheey/topic_create',
						'/v1/familheey/update_family',
						'/v1/familheey/removeFolder',
						'/v1/familheey/update_events',
						'/v1/familheey/respondToEvents',
						'/v1/familheey/add_event_signupitems',
						'/v1/familheey/event_deleteorcancel',
						'/v1/familheey/create_agenda',
						'/v1/familheey/event_invitations',
						'/v1/familheey/needs/create',
						'/v1/familheey/needs/delete',
						'/v1/familheey/needs/create_items',
						'/v1/familheey/needs/update_items',
						'/v1/familheey/needs/delete_items',
						'/v1/familheey/delete_topic',
						'/v1/familheey/remove_users_from_topic',
						'/v1/familheey/accept_user_topic',
						'/v1/familheey/reject_user_topic',
						'/v1/familheey/payment/stripeCreatePaymentIntent',
						'/v1/familheey/payment/updateConnectAccount',
						'/v1/familheey/DeleteOneNotification',
						'/v1/familheey/readNotifications',
						'/v1/familheey/event_shareor_invite',
						'/v1/familheey/add_eventitems_contribution',
						'/v1/familheey/needs/contribution_create',
						'/v1/familheey/needs/contribution_update',
						'/v1/familheey/needs/contribution_delete',
						'/v1/familheey/posts/post_comment',
						'/v1/familheey/needs/add_member_contribution_ByAdmin',
						'/v1/familheey/user_membership_update'
					];
					let checkArray = user_authorization_urls.includes(req.url);
					if(checkArray) {
						// if(req.body.id != req.headers.logined_user_id)	{
						// 	return Promise.reject(new E.UnAuthorizedError(E.UnAuthorizedError));	
						// }

						switch (req.url) {
							case '/v1/familheey/edit_profile':
							if(req.body.id != req.headers.logined_user_id)	{
								return Promise.reject(new E.UnAuthorizedError(E.UnAuthorizedError));	
							}
							break;
							case '/v1/familheey/create_family':
							case '/v1/familheey/createFolder':
							case '/v1/familheey/create_events':
							case '/v1/familheey/posts/create':
							case '/v1/familheey/topic_create':
							if(req.body.created_by != req.headers.logined_user_id)	{
								return Promise.reject(new E.UnAuthorizedError(E.UnAuthorizedError));	
							}
							break;
							case '/v1/familheey/update_family':
							case '/v1/familheey/removeFolder':
							case '/v1/familheey/update_events':
							case '/v1/familheey/respondToEvents':
							case '/v1/familheey/add_event_signupitems':
							case '/v1/familheey/event_deleteorcancel':
							case '/v1/familheey/create_agenda':
							case '/v1/familheey/event_invitations':
							case '/v1/familheey/needs/create':
							case '/v1/familheey/needs/delete':
							case '/v1/familheey/needs/create_items':
							case '/v1/familheey/needs/update_items':
							case '/v1/familheey/needs/delete_items':
							case '/v1/familheey/delete_topic':
							case '/v1/familheey/remove_users_from_topic':
							case '/v1/familheey/accept_user_topic':
							case '/v1/familheey/reject_user_topic':
							case '/v1/familheey/payment/updateConnectAccount':
							case '/v1/familheey/DeleteOneNotification':
							case '/v1/familheey/readNotifications':
							if(req.body.user_id != req.headers.logined_user_id)	{
								return Promise.reject(new E.UnAuthorizedError(E.UnAuthorizedError));	
							}
							break;
							case '/v1/familheey/payment/stripeCreatePaymentIntent':
							if(req.body.user_id != req.headers.logined_user_id && req.body.user_id != 2)	{
								return Promise.reject(new E.UnAuthorizedError(E.UnAuthorizedError));	
							}
							break;
							case '/v1/familheey/event_shareor_invite':
							if(req.body.from_user != req.headers.logined_user_id)	{
								return Promise.reject(new E.UnAuthorizedError(E.UnAuthorizedError));	
							}
							break;
							case '/v1/familheey/add_eventitems_contribution':
							case '/v1/familheey/needs/contribution_create':
							case '/v1/familheey/needs/contribution_update':
							case '/v1/familheey/needs/contribution_delete':
							if(req.body.contribute_user_id != req.headers.logined_user_id)	{
								return Promise.reject(new E.UnAuthorizedError(E.UnAuthorizedError));	
							}
							break;
							case '/v1/familheey/posts/post_comment':
							if(req.body.commented_by != req.headers.logined_user_id)	{
								return Promise.reject(new E.UnAuthorizedError(E.UnAuthorizedError));	
							}
							break;
							case '/v1/familheey/needs/add_member_contribution_ByAdmin':
							if(req.body.admin_id != req.headers.logined_user_id)	{
								return Promise.reject(new E.UnAuthorizedError(E.UnAuthorizedError));	
							}
							break;
							case '/v1/familheey/user_membership_update':
							if(req.body.edited_user_id != req.headers.logined_user_id)	{
								return Promise.reject(new E.UnAuthorizedError(E.UnAuthorizedError));	
							}
							break;

							default:
                            break;
						}
					}
					// Check user authorization start end

					ctx.meta.clientIp = req.connection.remoteAddress;
					if (req.files) { ctx.meta.file = req.files }
					if (req.url == '/v1/familheey/posts/post_comment') {
						// let topic = 'post_channel_' + req.body.post_id;
						// ctx.call('socket_service.sendMessage', { topic: topic, data: req.body });
					}
				},
				onAfterCall(ctx, route, req, res, data) {
					return data;
				}
			},
			{
				path: "/page",
				whitelist: [
					// Access to any actions in all services under "/page" URL
					"**"
				],
				aliases: {
					"GET /events/:text": "events.eventUrl",
					"GET /posts/:text": "events.postUrl",
					"GET /groups/:group_id": "events.groupUrl"

				},
				callOptions: {
					timeout: 120 * 10000,
					// retries: 3,
					// fallbackResponse(ctx, err) { ... }
				}
			}
		],

		// Serve assets from "public" folder
		assets: {
			folder: "public"
		}
	},

	methods: {
		/**
		 * token validation
		 * user validtaion
		 */
		async authorize(ctx, route, req, res) {
			//return new Promise(async(resolve, reject) => {
				//let appInfo = req.headers["app_info"] ? JSON.parse(req.headers["app_info"]) : null
				let auth = req.headers["authorization"];
				let user_id = req.headers["logined_user_id"];
				let exception_urls = [
					'/v1/familheey/register',
					'/v1/familheey/login',
					'/v1/familheey/confirmotp',
					'/v1/familheey/resendOTP',
					'/v1/familheey/loginWithOtp',
					'/v1/familheey/isUserExist',
					'/v1/familheey/forgot_password',
					'/v1/familheey/change_password',
					'/v1/familheey/reset_password',
					'/v1/familheey/payment/stripe_oauth',
					'/v1/familheey/stripe_webhook',
					'/v1/familheey/getAccessToken',
					'/v1/familheey/registerToken',
					"/v1/familheey/testPushNotification"
				];
				let checkArray = exception_urls.includes(req.url);
				//comment below line to enable for all [ios,android]
				//now enabled for web only
				//if(appInfo && appInfo.os != "Web"){ resolve(ctx); return; }
				if(checkArray) {
					return Promise.resolve(ctx);
				}
				if (auth && auth.startsWith("Bearer") && user_id) {
					let token = auth.slice(7);
					let _User = await ctx.call("db.users.find", { query: { id: user_id }, fields: ["phone","user_is_blocked"] })
					return ctx.call("cognito.verifyAuthToken",{ token:token })
					.then((verifyres)=>{
						let { UserAttributes } = verifyres;
						let _Phone = UserAttributes.filter(function(obj) {
							return obj.Name == 'phone_number';
						});
						if(_Phone[0]['Value'].toString() ==  _User[0].phone.toString()){
							return Promise.resolve(ctx)
						} else{ 						
							//reject(new E.UnAuthorizedError(E.ERR_INVALID_TOKEN))
							return Promise.reject(new E.UnAuthorizedError(E.ERR_INVALID_TOKEN));
						}
					})
					.catch((err)=>{ 
						//reject(err);
						return Promise.reject(new E.UnAuthorizedError(E.ERR_INVALID_TOKEN));
					})
				} else {
					//No token
					//reject(new E.UnAuthorizedError(E.ERR_NO_TOKEN));
					return Promise.reject(new E.UnAuthorizedError(E.ERR_INVALID_TOKEN));
				}
			//})
		}
	}
};
