"use strict";
const moment = require('moment');
const _async = require('async');

module.exports = {
    name: "template",
    settings: {},
    actions: {
        templateSendMail: {
            params: {},
            handler(ctx) {
                let { view } = ctx.params;
                var returnVal;
                switch (view) {
                    case 'template1':
                    returnVal =  this.template_one(ctx);
                    return returnVal;

                    case 'template2':
                    returnVal =  this.template_two(ctx);
                    return returnVal;

                    default:
                    break;
                }
            }
        }
    },
    events: {},
    methods: {
        async template_one(ctx) {
            let { result,from_user, type, type_value } = ctx.params;
            let { event,group_members, fromUser } = result;
            event.event_image = (!event.event_image) ? 'https://familheey.s3.amazonaws.com/email_template/banner.png' : `https://familheey.s3.amazonaws.com/event_image/${event.event_image}`
            group_members.forEach(async value => {
                let isUser = await ctx.broker.call('db.users.find', { query: { phone: value.phone } })
                if (isUser && isUser.length == 0){
                    let invitez = {
                        group_id:value.group_id,
                        user_id:value.user_id,
                        event_id:event.id,
                        type:type,
                        invited_by:from_user,
                        from_user:from_user,
                        type_value:type_value ? type_value : 'invite'
                    }
                    await ctx.broker.call("db.events_invitation.create",invitez)
                    let o_users = await ctx.broker.call("db.other_users.create",{...invitez,full_name:value.full_name,email:value.email,phone:value.phone})
                    
                    if(value.phone){
                        let smsHtml = this.getSmsText({event:event, otheruser:o_users });
                        ctx.broker.call('notification.sendSms', {
                            message:smsHtml,
                            PhoneNumber: value.phone,
                            //PhoneNumber: '+918089628210',                            
                            subject: "Familheey event"
                        })
                    }
    
                    if(value.email){
                        let html = this.getEmailText({event:event, otheruser:o_users, fromUser:fromUser});
                        ctx.broker.call("notification.sendEmail", {
                            toAddresses: [`${value.email}`],
                            //toAddresses: ['deepdil.sp@iinerds.com'],
                            message: html,
                            subject: `Event Invitation:${event.event_name}`
                        })
                    }
                }
            }) 
        },

        async template_two(ctx) {
            let { result, type, from_user } = ctx.params;
            let { group_members, fromUser, group } = result;
            //let _image =  'https://familheey.s3.amazonaws.com/email_template/banner.png';
            //if(event && event.event_image) _image = `https://familheey.s3.amazonaws.com/event_image/${event.event_image}`
            //if(group && group.logo) _image = `https://familheey.s3.amazonaws.com/logo/${group.logo}`
            group_members.forEach(async value => {
                let invitez = {
                    group_id:value.group_id ? value.group_id :  group.id,
                    user_id:value.user_id ? value.user_id : null,
                    type: type,
                    type_value:'invite',
                    invited_by:from_user
                }

                /*if(event){
                    invitez = { ...invitez, event_id:event.id }
                    await ctx.broker.call("db.events_invitation.create",invitez)
                }*/
                let o_users = await ctx.broker.call("db.other_users.create",{...invitez,full_name:value.full_name,email:value.email,phone:value.phone})
                if(value.phone){
                    let smsHtml = '';
                    if(group) {
                        smsHtml = `You are invited to join the ${group.group_name} family in Familheey. Download the app and stay connected with your family :) click here : ${group.f_link}`
                    }
                    //if(event) smsHtml = this.getSmsText({event:event, otheruser:o_users });
                    ctx.broker.call('notification.sendSms', {
                        message:smsHtml,
                        PhoneNumber: value.phone,
                        //PhoneNumber: '+918089628210',
                        subject: "Familheey event"
                    })
                }
    
                if(value.email){

                    //let html = this.getEmailText({event:event, otheruser:o_users, fromUser:fromUser});
                    /*let html = `Dear ${value.full_name},<br/>
                    ${fromUser[0].full_name} has invited you to join the ${group.group_name} family in Familheey - The social networking app for families!<br/>
                    <a href='${group.f_link}'>Download</a> the  app and stay connected with your family :)
                    Best regards,<br/>
                    ${fromUser[0].full_name}`*/

                    let html = 
                    `<html>
                        <body>
                            <table>
                                <tr>
                                    <td>Dear ${value.full_name},</td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>${fromUser[0].full_name}</b> has invited you to join the <b> ${group.group_name} </b> family in Familheey - The social networking app for families!<br/>
                                        <a href='${group.f_link}'>Download</a> the app and stay connected with your family :)<br/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Best regards,</td>
                                </tr>
                                <tr>
                                    <td>Team Familheey</td>
                                </tr>
                            </table>
                        </body>
                    </html>`;
                    ctx.broker.call("notification.sendEmail", {
                        toAddresses: [`${value.email}`],
                        //toAddresses: ['deepdil.sp@iinerds.com'],
                        message: html,
                        subject: `Group Invitation:${group.group_name}`
                    })
                }
            }) 
        },
        
        /**
         * 
         * get the text for send a email
         */
        getEmailText(params){
            let { event, otheruser, fromUser } = params;

            var html = `<!doctype html>
            <html>
            <head>
                <meta name="viewport" content="width=device-width">
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                <title>Invite</title>
            </head>
            <body style="background-color: #f6f6f6; font-family: Helvetica,sans-serif; -webkit-font-smoothing: antialiased; font-size: 14px; line-height: 1.4; margin: 0; padding: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
            
                <table border="0" cellpadding="0" cellspacing="0"
                    style="border-collapse: separate; font-family: Helvetica,sans-serif;mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background-color: #FFF;width: 100%;max-width: 600px;margin: 25px auto;box-shadow: 0 0 10px rgba(0,0,0,0.2);">
                    <tbody>
                        <tr>
                            <td style="padding: 15px 5px 15px 20px;font-family: Helvetica, sans-serif;font-size: 13px;color: #170b2b;opacity: 0.85;line-height: 1;vertical-align: middle;">${event.event_name ? 'public' : 'private'} <i style=" width: 8px; height: 8px; background-color: #464646; display: inline-block; vertical-align: middle; opacity: 0.7; border-radius: 50%; margin: 0 5px; "></i> ${event.event_type}</td>
                            <td style="padding: 15px 20px 15px 5px;text-align: right;"><a href="${event.event_page}?rsvp=going&&osid=${otheruser.id}" style="font-family: Helvetica, sans-serif; background-color: #7e57c2; color: #FFF; display: inline-block; vertical-align: top; text-decoration: none; border-radius: 6px; padding: 10px 15px; font-size: 17px; line-height: 1; outline: none;">I'm Going</a></td>
                        </tr>
                        <tr>
                            <td colspan="2" style="padding: 0;">
                                <img src='${event.event_image}'" alt="Banner" style="display: inline-block;vertical-align: top;width: 100%;max-width: 100%;" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="padding: 15px 20px;">
                                <span style=" background-color: #ffdd72; color: #242424; font-size: 16px; border-radius: 8px; padding: 9px 15px; display: inline-block; vertical-align: top; font-family: Helvetica, sans-serif; font-weight: 400; line-height: 1; ">${event.ticket_type}</span>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="padding: 0 20px;">
                                <h2 style=" color: #3a2262; font-family: Helvetica, sans-serif; margin: 0; display: inline-block; vertical-align: top; width: 100%; line-height: 1; font-size: 40px; ">${event.event_name}</h2>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="padding: 20px 20px 0 20px;vertical-align: middle;">
                                <img style=" width: 25px; vertical-align: bottom; margin-right: 8px; " src="https://familheey.s3.amazonaws.com/email_template/date.png" alt="Date" />
                                <h4 style=" display: inline-block; font-family: Helvetica, sans-serif; font-size: 17px; font-weight: 400; color: #170b2b; line-height: 1.5; margin: 0; "> 
                                ${ moment.unix(event.from_date).tz("America/Los_Angeles").format("MM/DD/YYYY hh:mm A") } :
                                ${ moment.unix(event.to_date).tz("America/Los_Angeles").format("MM/DD/YYYY hh:mm A") }
                                </h4>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="padding: 10px 20px 0 15px;vertical-align: middle;">
                                <img style="width: 20px;vertical-align: bottom;margin-right: 10px;margin-left: 3px;" src="https://familheey.s3.amazonaws.com/email_template/place.png" alt="Place" />
                                <h4 style=" display: inline-block; font-family: Helvetica, sans-serif; font-size: 17px; font-weight: 400; color: #170b2b; line-height: 1.5; margin: 0; "> ${event.location}</h4>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="padding: 10px 20px 0 20px;vertical-align: middle;">
                                <img style=" width: 25px; vertical-align: bottom; margin-right: 8px; " src="https://familheey.s3.amazonaws.com/email_template/host.png" alt="Host" />
                                <h4 style=" display: inline-block; font-family: Helvetica, sans-serif; font-size: 17px; font-weight: 400; color: #170b2b; line-height: 1.5; margin: 0; "> ${event.event_host}</h4>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="padding: 25px 20px;">
                                <hr style=" border-top: none; border-bottom-color: #f1f1f1; margin: 0;" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="padding: 0 20px;">
                            <p style="vertical-align: top;color: #3e3e3e; display: inline-block; width: 100%; font-weight: 400; font-size: 16px; line-height: 1.5; margin: 0 0 10px; font-family: Helvetica, sans-serif; ">Dear ${otheruser.full_name},</p>
                            <p style="vertical-align: top;color: #3e3e3e; display: inline-block; width: 100%; font-weight: 400; font-size: 16px; line-height: 1.5; margin: 0 0 10px; font-family: Helvetica, sans-serif; ">
                            I invited you to join ${event.event_name} on ${ moment.unix(event.from_date).tz("America/Los_Angeles").format("MM/DD/YYYY hh:mm A") } to
                            ${ moment.unix(event.to_date).tz("America/Los_Angeles").format("MM/DD/YYYY hh:mm A") } at ${event.location}. <br/>
                            Please click <a href='${event.event_page}?osid=${otheruser.id}'>link</a> to accept the invitation.</p>

                                <p style="vertical-align: top; display: inline-block; width: 100%; font-weight: 400; font-size: 16px; line-height: 1.5; margin: 0 0 10px; font-family: Helvetica, sans-serif; ">Regards,<br />${fromUser[0].full_name}</p>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="padding: 15px 20px 0 20px;"><a href="${event.event_page}?rsvp=going&&osid=${otheruser.id}" style="outline: none; font-family: Helvetica, sans-serif; background-color: #7e57c2; color: #FFF; display: inline-block; vertical-align: top; text-decoration: none; border-radius: 6px; padding: 12px 15px; font-size: 22px; line-height: 1; width: 100%; text-align: center; box-sizing: border-box; ">I'm Going</a></td>
                        </tr>
                        <tr>
                            <td style="padding: 15px 10px 15px 20px;"><a href="${event.event_page}?rsvp=interested&&osid=${otheruser.id}" style="font-family: Helvetica, sans-serif;background-color: #FFF;color: #7e57c2;display: inline-block;vertical-align: top;text-decoration: none;border-radius: 6px;padding: 12px 10px;font-size: 16px;line-height: 1;width: 100%;text-align: center;box-sizing: border-box;border: 2px solid #c6adf1; outline: none;">Interested</a></td>
                            <td style="width:50%;padding: 15px 20px 15px 10px;"><a href="${event.event_page}?rsvp=not-going&&osid=${otheruser.id}" style="font-family: Helvetica, sans-serif;background-color: #FFF;color: #7e57c2;display: inline-block;vertical-align: top;text-decoration: none;border-radius: 6px;padding: 12px 10px;font-size: 16px;line-height: 1;width: 100%;text-align: center;box-sizing: border-box;border: 2px solid #c6adf1; outline: none;">Not Interested</a></td>
                        </tr>
                        <tr>
                            <td colspan="2" style="padding: 25px 20px;">
                                <hr style=" border-top: none; border-bottom-color: #f1f1f1; margin: 0;" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style=" padding: 0 15px 20px; ">
                                <p style=" display: inline-block; vertical-align: top; width: 100%; text-align: center; font-size: 13px; line-height: 1.5; margin: 0; color: #5a5a5a; font-family: Helvetica, sans-serif; ">&copy; 2019 Familheey LLC. All rights reserved.</p>
                            </td>
                        </tr>
                    </tbody>
                </table>
                </body>
            </html>`;
            return html;
        },

        /**
         * 
         * get the text to send sms
         */
        getSmsText(params){
            let { event, otheruser } = params;
            let _smsHtml = 
            `Event Name : ${event.event_name}
            Venue : ${event.location}
            From: ${ moment.unix(event.from_date).tz("America/Los_Angeles").format("MM/DD/YYYY hh:mm A") }
            To: ${ moment.unix(event.to_date).tz("America/Los_Angeles").format("MM/DD/YYYY hh:mm A") }
            click here =>${event.event_page}?osid=${otheruser.id}`;
            //_smsHtmlOnlineEvent is created by sarath on 09-02-2021 for the sms text of online event to avoid venue where venue is not needed and it is returned if event type is Online
            let _smsHtmlOnlineEvent=
            `Event Name : ${event.event_name}
            From: ${ moment.unix(event.from_date).tz("America/Los_Angeles").format("MM/DD/YYYY hh:mm A") }
            To: ${ moment.unix(event.to_date).tz("America/Los_Angeles").format("MM/DD/YYYY hh:mm A") }
            click here =>${event.event_page}?osid=${otheruser.id}`;
            if(event.event_type=="Online"){
                return _smsHtmlOnlineEvent;
            }
            //--------------------------------------//
            //_smsHtml is returned in else condition for regular event and sign up event where venue is needed. _smsHtml is placed in else by Sarath on 09-02-2021
            else{
            return _smsHtml;
            }
            //--------------------------------------//
            // return _smsHtml;   //this line is commented by Sarath on 09-02-2021
        }
    },
    created() { },
    started() { },
    stopped() { }
}