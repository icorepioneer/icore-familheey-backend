"use strict";
let csv = require('csv-parser');
let fs  = require('fs');
let _async = require('async')

module.exports = {
  name: "bulk",
  settings: {},
  actions: {
    
    eventInvitation:{
      params:{
        from_user:"string",
        event_id:"string",
        type:"string"
      },
      handler(ctx){
        let results = [];
        return new Promise((resolve, reject) => {
          if(ctx.meta && ctx.meta.file){
            ctx.meta.file.forEach(elem  => {
              let Ext = elem.filename.split(".")[1];
              if(Ext != 'csv'){
                reject("uploaded file not supported.!")
              }
              fs.createReadStream(`${elem.path}`)
              .pipe(csv())
              .on('data',(data)=>{
                results.push(data)
              })
              .on('end',()=> {
                _async.each(results, (others, callback) => {
                  others.country_code = others.country_code.indexOf("+") == '-1' ? `+${others.country_code}` : `${others.country_code}`
                  others.phone = `${others.country_code}${others.phone}`
                  callback();
                }, async(err) => {
                  let phnz = results.map(item => item.phone);
                  let _users = await ctx.broker.call("db.users.find",{ query:{ phone:phnz }, fields:['phone','email'] })
                  let _users_phone = _users.map(item => item.phone);
                  
                  var finalArr = [];
                  var userExist = [];

                  results.forEach(e => {
                    if (_users_phone.indexOf(e.phone) == -1) finalArr.push(e);//not in application
                    else userExist.push(e)//registered user list
                  });

                  ctx.params = { ...ctx.params, others:finalArr,userExist:userExist, notType:"bulk_event" };
                  this.appNotify(ctx)
                });
                resolve("success")
              });
            });
          }
        })
      }
    },

    /**
     * bulk family invitation 
     * add user to group
     */
    addFamilyMember:{
      params:{
        group_id:"string",
        from_user:"string",
        type:"string"
      },
      handler(ctx){
        let results = [];
        let  { group_id } = ctx.params;
        return new Promise((resolve, reject) => {
          if(ctx.meta && ctx.meta.file){
            ctx.meta.file.forEach(elem  => {
              let Ext = elem.filename.split(".")[1];
              if(Ext != 'csv'){
                reject("uploaded file not supported.!")
              }
              fs.createReadStream(`${elem.path}`)
              .pipe(csv())
              .on('data',(data)=>{
                results.push(data)
              })
              .on('end',()=> {
                _async.each(results, (others, callback) => {
                  if(others.country_code == '00') {
                    //others.country_code = others.country_code;
                  } else {
                    others.country_code = others.country_code.indexOf("+") == '-1' ? `+${others.country_code}` : `${others.country_code}`
                  }
                  others.phone = `${others.country_code}${others.phone}`
                  callback();
                }, async (err) => {
                  let phnz = results.map(item => item.phone);
                  let _users = await ctx.broker.call("db.users.find",{ query:{ phone:phnz }, fields:['phone','email'] })
                  let _users_phone = _users.map(item => item.phone);
                  
                  var finalArr = [];
                  var userExist = [];

                  results.forEach(e => {
                    if (_users_phone.indexOf(e.phone) == -1) finalArr.push(e);//not in application
                    else userExist.push(e);//exist in application
                  });

                  ctx.params = { ...ctx.params, others:finalArr,userExist:userExist, notType:"bulk_group" };
                  if(typeof group_id == "string"){
                    ctx.params['group_id'] = [group_id]
                  }
                  this.appNotify(ctx)
                });
                resolve("success");
              });
            });
          }
        })
      }
    },

    /**
     * mobile contact bulk upload
     */
    mobBulk:{
      params:{
        from_user:"string",
        country_code:"string",
        type:"string"
      },
      handler(ctx){
        let  { group_id, phone, country_code, type } = ctx.params;
        return new Promise((resolve, reject) => {
          let results = [];
          _async.each(phone, (others, callback) => {
            let new_no = others.replace(/^0+/, `${country_code}`);//remove all 0's from front and add "+"
            if(new_no.indexOf("+") == -1){ //if + not in string add "+"
              new_no = `${country_code}${others}`;
            }
            results.push({ phone:new_no })
            callback();
          }, async (err) => {
            if(err) reject(err)
            let phnz = results.map(item => item.phone);
            let _users = await ctx.broker.call("db.users.find",{ query:{ phone:phnz }, fields:['phone','email'] })
            let _users_phone = _users.map(item => item.phone);
            
            var finalArr  = [];
            var userExist = [];
            
            results.forEach(e => {
              if (_users_phone.indexOf(e.phone) == -1) finalArr.push(e);//not in application
              else userExist.push(e);//exist in application
            });

            ctx.params = { ...ctx.params, others:finalArr,userExist:userExist };

            if(type=='group') {
              ctx.params.notType = "bulk_group";
              ctx.params['group_id'] =  [group_id]
            }
            if(type=='event') ctx.params.notType = "bulk_event"
            this.appNotify(ctx)
            resolve({ code:200, message:"success" })
          });
        })
      }
    },

    familyLinkCreate:{
      params:{},
      handler(ctx){
        return new Promise((resolve, reject) => {
          ctx.broker.call("db.groups.find",{ query:{ f_text : null }, fields:['id']})
          .then((ids)=>{
            ids.forEach( async doc => {
              let { id } = doc;
              let sdata = { id:id, f_text:  this.randomString(6) }
              ctx.broker.call("db.groups.update",sdata)
            });
            resolve("ok")
          })
          .catch(err => reject(err))
        })
      }
    }
  },
  events: {},
  methods: {

    appNotify(ctx){
      let { event_id, others, from_user, type, group_id, user_id, notType, userExist } = ctx.params;
      _async.parallel({
        fromUser: (callback) => {
          ctx.call("db.users.find", { query: { id: from_user }, fields: ["id","full_name","propic"] }).then(res => {
            callback(null, res);
          }).catch(err => {
            console.log("error happened -------- fromUser", err);
          })
        },
        group_members: function (callback) { // get memebers inside a group
          /*if (group_id) {
            ctx.broker.call('db.group_map.eventInviteGroup', ctx.params)
            .then((res) => {
              callback(null, res);
            })
            .catch((err) => {
              console.log("User Search Error 1", err);
            })
          }*/
          if (user_id) {
            ctx.broker.call('db.users.find', { query: { id: user_id } })
            .then((res) => {
              callback(null, res);
            })
            .catch((err) => {
              console.log("User Search Error 2", err);
            })
          }
          if (others) {
            callback(null, others);
          }
        },
        event: (callback) => {
          if(event_id) {
            ctx.call("db.events.find", { query: { id: event_id } })
            .then(res => {
              var _events = {};
              if (res && res.length > 0) {
                _events = res[0];
                if (_events.event_page) {
                  _events.event_page = `${process.env.SHARE_BASE_URL}page/events/${_events.event_page}`;
                }
              }
              callback(null, _events);
            }).catch(err => {
              console.log("error happened -------- event", err);
            })
          } else {
            callback(null, []);
          }
        },
        group: (callback) => {
          if(group_id) {
            ctx.call("db.groups.find", { query: { id: group_id } })
            .then(res => {
              var _group = {};
              if (res && res.length > 0) {
                _group = res[0];
                if (_group.f_text) {
                  _group.f_link = `${process.env.SHARE_BASE_URL}page/groups/${_group.f_text}`;
                  _group.event_page = _group.f_text;
                }
              }
              callback(null, _group);
            }).catch(err => {
              console.log("error happened -------- event", err);
            })
          } else {
            callback(null, [])
          }
        }
      }, function (err, results) {

        let { fromUser, group, event } = results;
        /** other invite */
        let data = {
          view:'template2',
          from_user:from_user,
          result:results,
          type:type,
          notType:notType
        }
        if(notType=='bulk_event') {
          data.view = 'template1'; 
        }
        if(notType=='bulk_group') {
          data.view = 'template2'; 
        } 
        
        /** In app notification for exist Users */

     
          if(userExist && userExist.length>0) { 
              appNotifyExistuser(userExist);   
          }

          function appNotifyExistuser(funUserExist){
              funUserExist.forEach(async usr => {

                let sendObj = {
                  from_id: parseInt(from_user),
                  type: 'family',
                  propic: fromUser.length>0 ? fromUser[0].propic : '',
                  message: fromUser.length > 0 ? `<b>${fromUser[0].full_name}</b> has invited you to join  <b>${group.group_name}</b>` : "",
                  category: 'family',
                  message_title: 'familheey',
                  type_id: parseInt(group_id),
                  link_to: parseInt(group_id)
                }

                var user = [];
                var isExist = [];

                switch (notType) {
                  case 'bulk_group':
                    user = await ctx.call("db.users.find", { query: { phone: usr.phone }, fields: ["id","full_name","propic","notification"] })
                    isExist = await ctx.call("db.group_map.find", { query: { group_id: group_id, user_id: user[0].id } })
                    if(isExist.length == 0){
                      ctx.call("db.requests.create",{ group_id: group_id, user_id: user[0].id, from_id:from_user, type:'invite' })
                      sendObj.to_id = user[0].id;
                      sendObj.type_id = parseInt(user[0].id);
                      sendObj.link_to = parseInt(user[0].id);
                      sendObj.type ='user';
                      sendObj.sub_type ='request';
                      ctx.broker.call("notification.sendAppNotification", sendObj);
                      if(user.length>0 && user[0].notification){
                        ctx.broker.call("userops.getregisterToken", { user_id: user[0].id, is_active:true })
                        .then((_devices) => {
                          if(_devices.length>0) {
                            _devices.data.forEach(elem => {
                              sendObj.r_id = elem.id;
                              sendObj.token = elem.device_token;
                              sendObj.device_type = elem.device_type.toLowerCase();
                              sendObj.title = sendObj.message_title;
                              sendObj.body = sendObj.message.toString().replace( /(<([^>]+)>)/ig, '')
                              ctx.broker.call("notification.sendPushNotification", sendObj)
                            }); 
                          }
                          // _devices.length>0 && _devices.data.forEach(elem => {
                          //   sendObj.r_id = elem.id,
                          //   sendObj.token = elem.device_token,
                          //   sendObj.device_type = elem.device_type.toLowerCase(),
                          //   sendObj.title = sendObj.message_title,
                          //   sendObj.body = sendObj.message.toString().replace( /(<([^>]+)>)/ig, '')
                          //   ctx.broker.call("notification.sendPushNotification", sendObj)
                          // });
                        })
                        .catch(e => e)
                      }
                    }
                  break;

                  case 'bulk_event':
                    user = await ctx.call("db.users.find", { query: { phone: usr.phone }, fields: ["id","full_name","propic","notification"] })
                    isExist = await ctx.call("db.events_share.find", { query: { event_id: event.id, user_id: user[0].id, shared_by:from_user } })

                    if(isExist.length ==0){
                      sendObj.to_id = user[0].id;
                      sendObj.type  = 'event';
                      sendObj.category  = 'event';
                      sendObj.type  = 'event';
                      sendObj.type_id = parseInt(event.id);
                      sendObj.link_to = parseInt(event.id);
                      ctx.call("db.events_share.create", { event_id: event.id, user_id: user[0].id, shared_by:from_user, type:'event' })
                      ctx.broker.call("notification.sendAppNotification", sendObj);
                      if(user.length>0 && user[0].notification){
                        ctx.broker.call("userops.getregisterToken", { user_id: user[0].id, is_active:true })
                        .then((_devices) => {
                          if(_devices.length>0) {
                            _devices.data.forEach(elem => {
                              sendObj.r_id = elem.id;
                              sendObj.token = elem.device_token;
                              sendObj.device_type = elem.device_type.toLowerCase();
                              sendObj.title = sendObj.message_title;
                              sendObj.body = sendObj.message.toString().replace( /(<([^>]+)>)/ig, '');
                              ctx.broker.call("notification.sendPushNotification", sendObj)
                            });  
                          }
                          // _devices.length>0 && _devices.data.forEach(elem => {
                          //   sendObj.r_id = elem.id,
                          //   sendObj.token = elem.device_token,
                          //   sendObj.device_type = elem.device_type.toLowerCase(),
                          //   sendObj.title = sendObj.message_title,
                          //   sendObj.body = sendObj.message.toString().replace( /(<([^>]+)>)/ig, '')
                          //   ctx.broker.call("notification.sendPushNotification", sendObj)
                          // });
                        })
                        .catch(e => e)
                      }
                    }
                  break;
                  
                  default:
                  break;
                }
              });
          }
        /** End  */

        if(others){
          return new Promise((resolve, reject) => {
            ctx.broker.call('template.templateSendMail', data)
            .then(res => resolve("success"))
            .catch(errs => reject(errs))
          })
        }
        /**End of others invite */

      });
    },

    randomString(length) {
      var result = '';
      var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
      var charactersLength = characters.length;
      for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
      }
      return result;
    }
  },
  created() { },
  started() { },
  stopped() { }
}