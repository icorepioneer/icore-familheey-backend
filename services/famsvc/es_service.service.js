"uses strict";
var elasticsearch = require('elasticsearch');

var elastic_search_client = new elasticsearch.Client({
    host: `${process.env.ELASTIC_SEARCH_URL}`,
    log: 'error',
    // connectionClass: connectionClass
});

elastic_search_client.ping({
    requestTimeout: 10000
}, function (error) {
    if (error) {
        console.log(error);
        console.log("error in elastic search connection");

    } else {
        console.log("elastic search connected");
    }
});

module.exports = {
    name: "es_service",
    settings: {},
    actions: {
        addRecords: {
            handler: (ctx) => {
                return new Promise((resolve, reject) => {
                    elastic_search_client.index({
                        index: ctx.params.index_name,
                        type: ctx.params.index_name,
                        body: ctx.params.data
                    }, function (err, resp, status) {
                        if (err) {
                            // t.commit();
                            reject(err);
                        } else {
                            // t.rollback();
                            resolve(resp)
                        }
                    });
                })
            }
        },
        updateRecord: {
            handler: (ctx) => {
                return new Promise((resolve, reject) => {
                    let {data,id,index} = ctx.params;
                    let sourceData = {
                        doc: data
                    }
                    var docParam = {
                        id: id,
                        index:index,
                        type: index,
                        body: sourceData
                    };

                    elastic_search_client.update(docParam)
                    .then((elasticRes) => {
                        resolve(elasticRes);
                    })
                    .catch((err) => {
                        console.log("Update elastic search has some error", err);
                        reject(err);
                    })
                })
            }    
        },
        /**
         * elastic seafrch get results
         */
        getRecords:{
            params:{
                index:'string',
                user_id:'string'
            },
            handler(ctx){
                let { index,user_id,offset,limit } = ctx.params;
                let _query={}, _sort=[];
                return new Promise((resolve, reject) => {

                    switch (index) {
                        case 'post':
                            _query = {
                                bool: {
                                    must: [{
                                        match: { is_active: true }
                                    },{
                                        match: { type: 'post' }
                                    },{
                                        match:{ post_type: 'public'}
                                    },{
                                        exists: { field: "post_attachment.filename" }
                                    },{
                                        query_string: { query: "post_attachment.type:video OR post_attachment.type:image" }
                                    }],
                                    must_not: { //will not show posts created by the searching user
                                        term: { created_by : user_id }
                                    }
                                }
                            }
                            //_fields = [ "description"]; 
                            _sort = [ //sort the data
                                { sort_date: { "order": "desc" } }
                            ]
                            
                            break;
                        case 'condition_next': //for sonarqube fix remove and add new case if needed
                            console.log(ctx.params);
                            break;
                        case 'condition_next2': //for sonarqube fix remove and add new case if needed
                            console.log(ctx.params);
                            break
                    
                        default:
                        break;
                    }

                    let params = {
                        index: index,
                        userid: user_id,
                        _body: {
                            sort: _sort,
                            query: _query
                        }
                    }
                    let { _body } = params;
                    // if( search_text && search_text!=''){ //search posts
                    //     _body = { ..._body,
                    //         multi_match : {
                    //             query:search_text,
                    //             fields:_fields
                    //         }
                    //     }
                    // }
                    params = { ...params, _body:_body }

                    elastic_search_client.search({
                        index : index,
                        from  : offset ? offset : 0,
                        size  : limit  ? limit  : 50,
                        body  : params._body
                    }).then(res => {
                        console.log('gobalSearch: Response -> ', res.hits);
                        resolve(res.hits);
                    }).catch(err => {
                        console.log('gobalSearch: Error', err);
                        resolve({ hits: [] });
                    });
                });
            }
        }
    },
    events: {},
    methods: {},
    created() {

    },
    started() { },
    stopped() { }
}