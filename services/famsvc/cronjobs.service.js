"use strict";

let moment = require('moment');
module.exports = {
    name: "cronjob",
    settings: {},
    actions: {
        cronForReminder:{
            params:{},
            handler(ctx){
                return new Promise((resolve, reject) => {
                    let crone_id = 1;
                    var cronefndquery = { id:crone_id };
                    let crnt_time = moment().unix();
                    ctx.call("db.crone_reminder.find", { query: cronefndquery })
                    .then(async res => {
                        let start = res[0].last_crone; 
                        let lastUpdate = moment(res[0].updatedAt).unix();
                        let compareTime = lastUpdate + 1800;
                        if(crnt_time <= start) {  
                            let end   = (start/60)+5;
                                end   = end*60;
                            if(res[0].crone_running == false || compareTime <= crnt_time) {
                                console.log('####### Start Reminder Running ############');
                                let update_data = {
                                    id: crone_id, crone_running: true
                                }
                                await ctx.call("db.crone_reminder.update", update_data)
                                .then(doc => {
                                    console.log('###### Successfully updated reminder running status ##########');

                                }).catch(err => {
                                    console.log(err);
                                })

                                ctx.broker.call("db.events.reminderSend", { start:start, end:end })
                                .then(async doc => {
                                    if(doc && doc.length>0){
                                        doc.forEach(elem  => {

                                            //send push notification start
                                            let pushTitle = "Event Reminder - "+ elem.event_name;
                                            elem.event_type = elem.event_type ? elem.event_type : '';
                                            let pushBody = "";
                                            if(elem.event_location && elem.event_location != null) {
                                                pushBody = "You have new event at" + elem.event_location;   
                                            } else {
                                                pushBody = "You have new " + elem.event_type + "event";   
                                            }
                                            ctx.broker.call("db.device_token.find", { query:{ user_id:elem.user_id }})
                                            .then(tokenData => {
                                                if(tokenData && tokenData.length > 0) {
                                                    tokenData.forEach(dataToken  => {  
                                                        let pushparams = {
                                                            r_id: parseInt(dataToken.id),
                                                            token: dataToken.device_token ? dataToken.device_token : '',
                                                            device_type: dataToken.device_type ? dataToken.device_type.toLowerCase() : '',
                                                            title: pushTitle,
                                                            body: pushBody,
                                                            type : 'event',
                                                            type_id : elem.event_id ? parseInt(elem.event_id) : '',
                                                            to_id:parseInt(elem.user_id)
                                                        }
                                                        ctx.broker.call("notification.sendPushNotification", pushparams)
                                                    });  
                                                }
                                            }).catch(err => {
                                                reject(err);
                                            })
                                            //send push notification end

                                            // let message = `Event ${elem.event_name} is scheduled as ${ moment.unix(elem.from_date).tz("America/Los_Angeles").format("MMM DD YYYY hh:mm A") } :
                                            // ${ moment.unix(elem.to_date).tz("America/Los_Angeles").format("MMM DD YYYY hh:mm A") } `
                                            let message = `You have an upcoming event - ${elem.event_name}  
                                            hosted by ${elem.event_host}  ${process.env.SHARE_BASE_URL}page/events/${res[0].event_page}`
                                            if(elem.email && elem.email != ''){
                                                ctx.broker.call("notification.sendEmail", {
                                                    toAddresses: [`${elem.email}`],
                                                    message: `Hi ${elem.full_name}, <br/>${message} <br/>`,
                                                    subject: "Event Reminder"
                                                })
                                            }
                                            if(elem.phone && elem.phone != ''){
                                                ctx.call('notification.sendSms', {
                                                    message: `${message} `,
                                                    PhoneNumber: elem.phone,
                                                    subject: "Event Reminder"
                                                })
                                            }
                                        });
                                    }
                                    
                                    let end_update_data = {
                                        id: crone_id, crone_running: false, last_crone:end 
                                    }
                                    await ctx.call("db.crone_reminder.update", end_update_data)
                                    .then(doc2 => {
                                        console.log('Successfully updated reminder running status as false');
                                    }).catch(err => {
                                        console.log(err);
                                    })

                                    resolve()
                                }).catch(err => {
                                    reject(err);
                                })
                            } else {
                                console.log('Reminder Already Running');
                                resolve('Reminder Already Running');
                            }
                        } else {
                            resolve('########### Last crone time is greater than current time');
                        }
                    }).catch(err => {
                        reject(err);
                    });
                })
            }
        },

        /**
         * needs exipiry notification
         */
        needExpiryReminder:{
            params:{},
            handler(ctx){
                let _this = this;
                return new Promise((resolve, reject) => {
                    let crnt_time = moment().unix();
                    let nxt_time = moment().add(12,'hours').unix();
                    ctx.call("db.post_requests_item_contributions.cronJob", { crnt_time:crnt_time, nxt_time:nxt_time})
                    .then(_items => {
                        resolve("== cron sucecss ==")
                        _items.forEach(async elem => {
                            let gmaps = await ctx.call("db.group_map.fetchGroup_members", { group_id: elem.group_id, type:'admin' })
                            let sendObj = {
                                from_id:  '',
                                type: 'home',
                                propic: '',
                                sub_type: 'requestFeed',
                                category: 'Notifications',
                                message_title: 'familheey',
                                type_id: parseInt(elem.id),
                                link_to: parseInt(elem.id)
                            }
                            
                            gmaps.forEach(async elem2 => {
                                sendObj.to_id = parseInt(elem2.id);
                                sendObj.message = `${elem2.full_name} a request of a member is yet to be fully helped`
                                ctx.broker.call("notification.sendAppNotification", sendObj);
                                let _user_notify = await ctx.call("db.users.find", { query: { id: sendObj.to_id }, fields: ["notification"] })
                                if (_user_notify.length > 0 && _user_notify[0].notification) {
                                    _this.pushNotify(ctx, sendObj);
                                }
                            });
                        });
                    })
                    .catch(e=>reject(e))
                })
            }
        }
    },
    events: {},
    methods: {
        
        /**
         * push notification 
         */
        pushNotify(ctx, params) {
            ctx.broker.call("userops.getregisterToken", { user_id: params.to_id, is_active: true })
                .then((_devices) => {
                    _devices.data.forEach(elem => {
                        params = {
                            ...params,
                            r_id: elem.id,
                            token: elem.device_token,
                            device_type: elem.device_type.toLowerCase(),
                            title: params.message_title,
                            body: params.message,
                            sub_type: params.sub_type ? params.sub_type : ''
                        }
                        ctx.broker.call("notification.sendPushNotification", params)
                    })
                })
                .catch(e => e)
        },

    },
    created() { },
    started() { },
    stopped() { }
}