"uses strict";
module.exports = {
    name: "redis",
    settings: {},
    actions: {
        deleteKeys: {
            handler(ctx) {
                console.log(ctx.params,"ctx.params");
                for (let i = 0; i < ctx.params.length; i++) {
                    let key = ctx.params[i] ;
                    console.log('key =========',key);
                    ctx.broker.cacher.clean(key);
                }
            }
        },
        createCustomCacheKey: {
            handler(ctx) {
                let {key,data} = ctx.params;
                console.log(key,data);
                return new Promise((resolve,reject)=>{
                    ctx.broker.cacher.set(`${key}`,data).then(()=>{
                        resolve(true);
                    }).catch(err=>{
                        console.log("failed to cache data provided",err)
                    });
                });   
            }
        },
        getCachedResult:{
             handler(ctx){
                let {key} = ctx.params;
                return new Promise(async (resolve,reject)=>{
                    console.log(await ctx.broker.cacher.get(`MOL-familheey-${key}`));
                        ctx.broker.cacher.get(`${key}`).then(res=>{
                            console.log("res,,,,,,,,",res);
                            resolve(res);
                        }).catch(err=>{reject(err);});
                });
            }
        }
    },
    events: {},
    methods: {},
    created() { },
    started() { },
    stopped() { }
};