"use strict";
module.exports = {
    name: "calender",
    settings: {},
    actions: {
        /**
         * save data to calender
         */
        savetoCalender:{
            params:{
                user_id:"string"
            },
            handler(ctx){
                return new Promise((resolve, reject)=>{
                    var response = {}
                    ctx.broker.call("db.calender.find",{ query:ctx.params })
                    .then((doc) =>{ 
                        if(doc && doc.length>0){
                            reject({ code:500, message:"Already exist" })
                        }else{
                            ctx.broker.call("db.calender.create",ctx.params)
                        }
                    })
                    .then((doc)=>{
                        response.data = doc;
                        resolve(response);
                    })
                    .catch((err)=>{
                        reject(err);
                    })
                })   
            }
        },
        /**
         * update a user's calender data
         * eg:set/update reminder
         */
        updateCalender:{
            params:{
                id:"string"
            },
            handler(ctx){
                return new Promise((resolve, reject)=>{
                    var response = {}
                    ctx.broker.call("db.calender.update",ctx.params)
                    .then((doc)=>{
                        response.data = doc;
                        resolve(response);
                    })
                    .catch((err)=>{
                        reject(err);
                    })
                })   
            }
        },
        /**
         * fetch full calender for a userid
         */
        fetchCalender:{
            params:{
                user_id:"string"
            },
            handler(ctx){
                return new Promise((resolve, reject)=>{
                    var response = {}
                    ctx.broker.call("db.calender.fetchCalender",ctx.params)
                    .then((doc)=>{
                        response.data = doc;
                        resolve(response);
                    })
                    .catch((err)=>{
                        reject(err);
                    })
                })   
            }
        }
    },
    events: {},
    methods: { },
    created() { },
    started() { },
    stopped() { }
}
