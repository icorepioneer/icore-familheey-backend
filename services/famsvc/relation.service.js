"use strict";
var _async = require('async');
module.exports = {
	name: "relation",

	settings: {

	},
	dependencies: [],

	actions: {
		addRelation: {
			params: {
				primary_user: "string",
				secondary_user: "string",
				group_id:"string",
				type:"string"
			},
			handler(ctx) {
				let { relation_id, value, type, group_id, secondary_user, primary_user } = ctx.params;
				return new Promise(async (resolve, reject) => {
					if(type == 'regular' && relation_id == 'others'){
						reject({code:500, message:`${value} cannot be added`})
					}
					
					if(relation_id == 'others' && type == 'role' && value){
						let isRelationExist = await ctx.broker.call('db.relationshipLookup.isExist',{relationship:value})
						if(isRelationExist && isRelationExist.length>0){
							ctx.params.relation_id = isRelationExist[0].id;
						}else{
							if(type=='role'){ type = 'org'}
							let newId = await ctx.call('db.relationshipLookup.create',{ relationship:value, type:type })
							ctx.params.relation_id = newId.id;
						}
					}
					
					let _rlupdate = await ctx.broker.call('db.relationships.isRelationExist',{ group_id:group_id,primary_user:primary_user, secondary_user:secondary_user });

					if(_rlupdate && _rlupdate.length>0){
						ctx.call('db.relationships.update',{ id: _rlupdate[0].id, relation_id:ctx.params.relation_id })
						.then(res => {
							this.appNotify(ctx)
							resolve({ data: res, message: 'successfully updated' });
						}).catch(e=> reject(e))
					} else{
						ctx.call('db.relationships.create', ctx.params)
						.then(response => {
							//if(primary_user != secondary_user) {
								this.appNotify(ctx)
							//}
							resolve({ data: response, message: 'successfully added' });
							// Track user activities start
							let trackerData = {
								user_id : parseInt(primary_user),
								type : 'Family',
								type_id : parseInt(group_id),
								sub_type : 'Relation',
								sub_type_id : parseInt(secondary_user),
								activity : 'added new relation with user'
							};
							ctx.broker.call('user_activities.trackUserActivities', trackerData);
							// Track user activities end
						}).catch(error => {
							reject({ data: error, message: 'error while adding' })
						})
					}					
				})
			}
		},
		updateRelation: {
			params: {
				primary_user: "string",
				secondary_user: "string",
				id: "string",
				type:"string"
			},
			handler(ctx) {
				let { relation_id, value, type, primary_user, secondary_user } = ctx.params;
				return new Promise(async(resolve, reject) => {
					if (ctx.params.relationId) {
						ctx.params.others = null
					}

					if(type == 'regular' && relation_id == 'others'){
						reject({code:500, message:`${value} cannot be added`})
					}
					
					if(relation_id == 'others' && type == 'role' && value){
						let isRelationExist = await ctx.broker.call('db.relationshipLookup.isExist',{relationship:value})
						if(isRelationExist && isRelationExist.length>0){
							ctx.params.relation_id = isRelationExist[0].id;
						}else{
							if(type=='role'){ type = 'org'}
							let newId = await ctx.call('db.relationshipLookup.create',{ relationship:value, type:type })
							ctx.params.relation_id = newId.id;
						}
					}
					delete ctx.params.primary_user; delete ctx.params.secondary_user;

					ctx.call('db.relationships.update', ctx.params)
					.then(res => {
						ctx.params = { ...ctx.params, primary_user:primary_user, secondary_user:secondary_user }
						//if(primary_user != secondary_user) {
							this.appNotify(ctx)
						//}
						resolve({ data: res, message: 'successfully updated' });
						// Track user activities start
						let trackerData = {
							user_id : parseInt(primary_user),
							type : 'Family',
							sub_type : 'Relation',
							sub_type_id : parseInt(secondary_user),
							activity : 'updated relation with user'
						};
						ctx.broker.call('user_activities.trackUserActivities', trackerData);
						// Track user activities end
					}).catch(err => {
						reject({ data: err, message: 'error while updating' });
					})
				})
			}
		},
		getallRelations: {
			params:{
				type:"string"
			},
			handler(ctx) {
				return new Promise((resolve, reject) => {
					ctx.call("db.relationshipLookup.getallRelations",ctx.params)
					.then(res => {
						resolve({ data: res });
					}).catch(err => {
						reject({ data: err, message: 'error while updating' });
					})
				})
			}
		}
	},

	events: {

	},
	methods: { 
		appNotify(ctx){
			let { primary_user,secondary_user, type, group_id, relation_id  } = ctx.params;
			let _this = this;

            _async.parallel({
                fromUser: (callback) => {
                    ctx.call("db.users.find", { query: { id: primary_user }, fields: ["id","full_name","propic","gender"] }).then(res => {
                        callback(null, res);
                    }).catch(err => {
                        console.log("error happened -------- fromUser", err);
                    })
                },
                toUser: (callback) => {
                    ctx.call("db.users.find", { query: { id: secondary_user }, fields: ["id","full_name","propic","gender"] }).then(res => {
                        callback(null, res);
                    }).catch(err => {
                        console.log("error happened -------- toUser", err);
                    })
				},
				group: (callback) => {
                    ctx.call("db.groups.find", { query: { id: group_id }, fields: ["id","group_name","logo"] }).then(res => {
                        callback(null, res);
                    }).catch(err => {
                        console.log("error happened -------- group_id", err);
                    })
				},
				groupAdmins: (callback) => {
                    ctx.call("db.group_map.find", { query: { type:'admin', group_id:group_id }, fields: ["id","user_id"] }).then(res => {
                        callback(null, res);
                    }).catch(err => {
                        console.log("error happened -------- group_id", err);
                    })
				},
				relation: (callback) => {
                    ctx.call("db.relationshipLookup.find", { query: { id:relation_id }, fields: ["id","relationship"] }).then(res => {
                        callback(null, res);
                    }).catch(err => {
                        console.log("error happened -------- group_id", err);
                    })
				}
            }, function (err, result) {
				let { fromUser, toUser, group, groupAdmins, relation } = result;
				let sendObj = {
					from_id: fromUser.length > 0 ? fromUser[0].id : '',
                    type: 'family',
                    propic: fromUser.length > 0 ? fromUser[0].propic : '',
                    message: `<b>${fromUser[0].full_name}</b> has added a <b>${type}</b> with you  `,
                    category: type,
                    message_title: 'familheey',
                    type_id: group.length > 0 ? parseInt(group[0].id) : '',
                    link_to: group.length > 0 ? group[0].id : '',
					to_id: toUser[0].id,
					sub_type:'member'
				}
				let rl_ = 'his(her)';
				if(fromUser.length>0 && fromUser[0].gender){
					rl_ = (fromUser[0].gender == 'male') ? 'his':'her'
				}

				switch (type) {
					case 'role':

					//user himself add role
					if(primary_user == secondary_user){
						
						sendObj.message = `<b>${fromUser[0].full_name}</b> has updated ${rl_} role in <b>${group[0].group_name}</b>`
						if(groupAdmins && groupAdmins.length>0){
							groupAdmins.forEach(elem => {
								sendObj.to_id = elem.user_id;
								if(elem.user_id != sendObj.from_id) ctx.broker.call("notification.sendAppNotification", sendObj)
								ctx.call("db.users.find",{ query:{id:sendObj.to_id}, fields:["notification"] })
								.then((_user_notify)=>{
									if(_user_notify.length>0 && _user_notify[0].notification){
										_this.pushNotify(ctx, sendObj)
									}
								}).catch(e=>console.log(e))
							})
						}
					}
					//admin add role
					if(primary_user != secondary_user){
						sendObj.message = `<b>${fromUser[0].full_name}</b> has updated your role in <b>${group[0].group_name}</b>`
						ctx.broker.call("notification.sendAppNotification", sendObj)
						ctx.call("db.users.find",{ query:{id:sendObj.to_id}, fields:["notification"] })
						.then((_user_notify)=>{
							if(_user_notify.length>0 && _user_notify[0].notification){
								_this.pushNotify(ctx, sendObj)
							}
						}).catch(e=>console.log(e))
					}
					break;

					case 'relation':
						sendObj.message = `<b>${fromUser[0].full_name}</b> added you as  ${rl_} <b>${relation[0].relationship}</b> in <b>${group[0].group_name}</b>`
						if(toUser[0].id != fromUser[0].id){
							ctx.broker.call("notification.sendAppNotification", sendObj);
							ctx.call("db.users.find",{ query:{id:sendObj.to_id}, fields:["notification"] })
							.then((_user_notify)=>{
								if(_user_notify.length>0 && _user_notify[0].notification){
									_this.pushNotify(ctx, sendObj)
								}
							}).catch(e=>console.log(e))
						}
						
					break;
				
					default:
					break;
				}
				
            });
		},
		  /**
         * push notification 
         */
        pushNotify(ctx, params) {
            ctx.broker.call("userops.getregisterToken", { user_id: params.to_id, is_active:true })
			.then((_devices) => {
				_devices.data.forEach(elem => {
					params = {
						...params,
						r_id: elem.id,
						token: elem.device_token,
						device_type: elem.device_type.toLowerCase(),
						title: params.message_title,
						body: this.removeTags(params.message),
						sub_type: params.sub_type ? params.sub_type : ''
					}
					ctx.broker.call("notification.sendPushNotification", params)
				})
			})
			.catch(e => e)
		},
		removeTags(str){ 
            if ((str===null) || (str==='')) return '';
            else return str.toString().replace( /(<([^>]+)>)/ig, '');
        }
	},

	created() {
	},
	started() {

	},
	stopped() {

	}
};