"use strict";
var _async = require('async');
var moment = require('moment');
module.exports = {
    name: "events",
    settings: {
        "users": {
            action: "db.users.get",
            params: {
                fields: "full_name propic location"
            }
        },
    },
    actions: {
        respondToEvents: {
            params: {
                event_id: "string",
                user_id: "string",
                resp: "string"
            },
            handler(ctx) {
                let response = {};
                let { event_id, user_id, resp, guest_count_update } = ctx.params;
                ctx.params = { ...ctx.params, attendee_id: user_id, attendee_type: resp, notifyType: "rsvp" }
                return new Promise(async(resolve, reject) => {
                    let findQ = { attendee_id: user_id, event_id: event_id }
                    let eventis  = await ctx.call("db.events.find",{ query:{ id:event_id }, fields:["is_cancel","is_active","to_date"] })
                    let { is_cancel, is_active, to_date } = eventis[0];
                    let currentTimestamp = moment().unix(); 
                    if(is_cancel) { reject("This event is cancelled"); return; }
                    else if(!is_active){ reject("This event is removed"); return;}
                    else if(currentTimestamp > to_date){ reject("This event has expired"); return; }
                    ctx.call("db.event_rsvp.find", { query: findQ })
                    .then(async(rsvpData) => {
                        if (rsvpData && rsvpData.length > 0) {
                            let up_data = { where: { event_id: event_id, user_id: user_id } }
                            ctx.params = { ...ctx.params, id: rsvpData[0].id }
                            if (resp == 'not-going') {
                                up_data = {
                                    ...up_data,
                                    params: { is_active: false }
                                }  
                            }else{
                                up_data = {
                                    ...up_data,
                                    params: { is_active: true }
                                }
                            }
                            await ctx.call("db.calender.customUpdate", up_data);
                            await ctx.call("db.reminder.customUpdate", up_data);
                            // Track user activities start
                            let trackerData = {
                                user_id: parseInt(user_id),
                                type: 'Event',
                                type_id: parseInt(event_id),
                                activity: 'Responded to event'
                            };
                            ctx.broker.call('user_activities.trackUserActivities', trackerData);
                            // Track user activities end
                            return ctx.call("db.event_rsvp.update", ctx.params);
                        } else {
                            if (resp == 'not-going') {
                                ctx.params.is_active = false;
                            }
                            ctx.broker.call("events.savetoCalender", ctx.params)
                            // Track user activities start
                            let trackerData = {
                                user_id: parseInt(user_id),
                                type: 'Event',
                                type_id: parseInt(event_id),
                                activity: 'Responded to event'
                            };
                            ctx.broker.call('user_activities.trackUserActivities', trackerData);
                            // Track user activities end
                            return ctx.call("db.event_rsvp.create", ctx.params);
                        }
                    })
                    .then(res => {
                        response.data = res;
                        if (resp != 'not-going' && !guest_count_update){
                            this.appNotify(ctx, ctx.params)
                        }
                        resolve(response);
                    }).catch(err => {
                        reject(err)
                    })
                })
            }
        },
        /**
         * create new events
         */
        createEvents: {
            params: {
                event_name: "string",
                event_type: "string",
                category: "string",
                created_by: "string"
            },
            handler(ctx) {
                //let _this = this;
                let { created_by, group_id, event_page, category, is_public } = ctx.params;
                let response = {};
                return new Promise(async (resolve, reject) => {
                    if ((!ctx.params.to_date || ctx.params.to_date == '') || (!ctx.params.from_date || ctx.params.from_date == '')) {
                        reject('date missing')
                    }
                    if (ctx.meta && ctx.meta.file) {
                        ctx.meta.file.forEach(element => {
                            ctx.params[element.fieldname] = element.filename;
                            this.calls3(ctx, element)
                        });
                    }

                    if (!event_page) {
                        ctx.params.event_page = this.makeEventPage(6);
                    }

                    category = category.toLowerCase();
                    let _category = await ctx.call("db.category_lookup.find", { query: { name: category } });
                    if (_category && _category.length == 0) {
                        let _saveCat = { name: category }
                        ctx.call("db.category_lookup.create", _saveCat)
                    }

                    if(ctx.params.meeting_link) {
                        var meeting_string = ctx.params.meeting_link;
                        if(meeting_string. indexOf("webex") > -1) {
                            ctx.params.meeting_logo = 'meeting_logo/logo_webex.png';
                        } else if(meeting_string. indexOf("gotomeeting") > -1) {
                            ctx.params.meeting_logo = 'meeting_logo/logo_gotomeeting.png';   
                        } else if(meeting_string. indexOf("meet") > -1) {
                            ctx.params.meeting_logo = 'meeting_logo/logo_hangouts.png';   
                        } else if(meeting_string. indexOf("zoom") > -1) {
                            ctx.params.meeting_logo = 'meeting_logo/logo_zoom.png';   
                        } else if(meeting_string. indexOf("bluejeans") > -1) {
                            ctx.params.meeting_logo = 'meeting_logo/logo_bluejeans.png';   
                        } else {
                            ctx.params.meeting_logo = 'meeting_logo/logo_default.png';    
                        }
                    }

                    ctx.call("db.events.create", { ...ctx.params })
                        .then((res) => {
                            response.data = res;
                            if ( (is_public == true || is_public == 'true') && !group_id) { 
                                ctx.call('userops.mygroupMember', { user_id: created_by })
                                .then((_users) => {
                                    let { data } = _users;
                                    ctx.params._users = data;
                                    ctx.params = { ...ctx.params, user_id: created_by, event_id: res.id, notifyType: 'public_evt_create' }
                                    delete ctx.params.created_by;
                                    this.appNotify(ctx, ctx.params)
                                })
                            }
                            if (group_id && group_id !='') {
                                let ev_invite = {
                                    event_id: res.id.toString(),
                                    from_user: res.created_by.toString(),
                                    type: 'invitation',
                                    group_id: [parseInt(group_id)],
                                    view:"template1"
                                }
                                ctx.broker.call('events.eventShareOrinvite', ev_invite);

                                // Track user activities start
                                let trackerData = {
                                    user_id: parseInt(res.created_by),
                                    type: 'Event',
                                    type_id: parseInt(res.id),
                                    activity: 'created a event'
                                };
                                ctx.broker.call('user_activities.trackUserActivities', trackerData);
                                // Track user activities end
                            }

                            //Generate & update firebase link start
                            let linkobj = {
                                type : 'event',
                                type_id : res.id.toString()
                            }
                            ctx.call('common_methods.createFirebaseLink', linkobj)
                            .then((linkRes) => {
                                if(linkRes && linkRes != '') {
                                    ctx.call('db.events.update',{ id:res.id, firebase_link:linkRes })
                                    .then((linkUpdate) => {
                                        console.log("Successfully updated firebase link");
                                    })
                                    .catch((err) => {
                                        console.log("Firebase link updation to event has some error", err);
                                    })
                                }
                            })
                            .catch((err) => {
                                console.log("Firebase link generation has some error", err);
                            })
                            //Generate & update firebase link end

                            return ctx.call("db.users.find", { query: { id: created_by }, fields: ['full_name'] })
                        })
                        .then((user) => {
                            response.data['created_by_name'] = (user && user.length > 0) ? user[0].full_name : '';
                            resolve(response)
                        }).catch((err) => reject(err))
                })
            }
        },
        /**
         * update an event using event id
         */
        updateEvents: {
            params: {
                id: "string",
                user_id: "string"
            },
            handler(ctx) {
                let { id, user_id } = ctx.params;
                let response = {}
                return new Promise((resolve, reject) => {
                    ctx.call("db.events.find", { query: { id: id } })
                        .then(res => {
                            if (ctx.meta && ctx.meta.file) {
                                ctx.meta.file.forEach(element => {
                                    ctx.params[element.fieldname] = element.filename;
                                    this.calls3(ctx, element)
                                });
                            }

                            let { from_date, to_date, location, created_by } = res[0];
                            if(ctx.params.meeting_link) {
                                var meeting_string = ctx.params.meeting_link;
                                if(meeting_string. indexOf("webex") > -1) {
                                    ctx.params.meeting_logo = 'meeting_logo/logo_webex.png';
                                } else if(meeting_string. indexOf("gotomeeting") > -1) {
                                    ctx.params.meeting_logo = 'meeting_logo/logo_gotomeeting.png';   
                                } else if(meeting_string. indexOf("meet") > -1) {
                                    ctx.params.meeting_logo = 'meeting_logo/logo_hangouts.png';   
                                } else if(meeting_string. indexOf("zoom") > -1) {
                                    ctx.params.meeting_logo = 'meeting_logo/logo_zoom.png';   
                                } else if(meeting_string. indexOf("bluejeans") > -1) {
                                    ctx.params.meeting_logo = 'meeting_logo/logo_bluejeans.png';   
                                } else {
                                    ctx.params.meeting_logo = 'meeting_logo/logo_default.png';    
                                }
                            }
                            created_by === parseInt(user_id) ? (
                                ctx.call('db.events.update', ctx.params)
                                    .then(async (res2) => {
                                        response.data = res2;
                                        // Track user activities start
                                        let trackerData = {
                                            user_id: parseInt(user_id),
                                            type: 'Event',
                                            type_id: parseInt(id),
                                            activity: 'Updated an event'
                                        };
                                        ctx.broker.call('user_activities.trackUserActivities', trackerData);
                                        // Track user activities end

                                        /** update event send notification */
                                        let _users = await ctx.call("db.event_rsvp.rsvpExceptList", { event_id: id, except: 'not-going' })
                                        let _msg = [];
                                        if ((res2.from_date != from_date) || (res2.to_date != to_date)) _msg.push('date')
                                        if (res2.location != location) _msg.push('location')
                                        if (_msg.length > 0) {
                                            _msg = _msg.join(" and ")
                                        }
                                        ctx.params = {
                                            ...ctx.params,
                                            notifyType: 'edit_event',
                                            _users: _users,
                                            _msg: _msg,
                                            event_id: id
                                        }
                                        if (_msg.length > 0) this.appNotify(ctx, ctx.params);
                                        resolve(response)
                                    }).catch((err) => {
                                        response.err = err;
                                        reject(response);
                                    })
                            ) : (reject("NOT_AUTHORISED"))
                        }).catch(err => reject(err))
                })
            }
        },
        getEvents: {
            params: { user_id: "string" },
            handler(ctx) {
                let { user_id } = ctx.params;
                return new Promise((resolve, reject) => {
                    var response = {}
                    ctx.call("db.events.eventListbyCreator", { created_by: user_id })
                        .then((doc) => {
                            response.data = doc;
                            resolve(response);
                        })
                        .catch((err) => {
                            response.err = err;
                            reject(response);
                        })
                })
            }
        },
        addEventSignupItems: {
            params: {
                event_id: "string",
                user_id: "string",
                slot_title: "string",
                item_quantity: "string",
                start_date: "string"
            },
            handler(ctx) {
                return new Promise((resolve, reject) => {
                    let { user_id, event_id } = ctx.params;
                    var response = {}
                    ctx.call('db.events_signup_items.create', { ...ctx.params })
                        .then((res) => {
                            response.data = [res];
                            // Track user activities start
                            let trackerData = {
                                user_id: parseInt(user_id),
                                type: 'Event',
                                type_id: parseInt(event_id),
                                sub_type: 'sign up item',
                                sub_type_id: parseInt(res.id),
                                activity: 'added sign up item to event'
                            };
                            ctx.broker.call('user_activities.trackUserActivities', trackerData);
                            // Track user activities end
                            resolve(response);
                        })
                        .catch((err) => {
                            response.err = err;
                            reject(response);
                        })
                })
            }

        },
        updateEventSignupItems: {
            params: {
                id: "string"
            },
            handler(ctx) {
                return new Promise((resolve, reject) => {
                    ctx.broker.call('db.events_signup_items.update', ctx.params)
                    .then((res) => {
                        resolve(res);
                        // Track user activities start
                        let trackerData = {
                            user_id: parseInt(user_id),
                            type: 'Event',
                            type_id: parseInt(event_id),
                            sub_type: 'sign up item',
                            sub_type_id: parseInt(res.id),
                            activity: 'Updated sign up item to event'
                        };
                        ctx.broker.call('user_activities.trackUserActivities', trackerData);
                        // Track user activities end
                    })
                    .catch((err) => {
                        reject(err)
                    });
                })
            }
        },
        deleteEventSignupItems: {
            params: {
                id: "string"
            },
            handler(ctx) {
                var response = {}
                return new Promise((resolve, reject) => {
                    ctx.broker.call('db.events_signup_items.remove', ctx.params)
                    .then((res) => {
                        let { user_id, event_id } = ctx.params;
                        response.data = res;
                        // Track user activities start
                        let trackerData = {
                            user_id: parseInt(user_id),
                            type: 'Event',
                            type_id: parseInt(event_id),
                            sub_type: 'sign up item',
                            sub_type_id: parseInt(res.id),
                            activity: 'Deleted sign up item to event'
                        };
                        ctx.broker.call('user_activities.trackUserActivities', trackerData);
                        // Track user activities end
                        resolve(response);

                    })
                    .catch((err) => {
                        console.log(err);
                        reject(err);
                    });
                })
            }
        },
        getEventItems: {
            params: {
                event_id: "string"
            },
            handler(ctx) {
                let response = {}
                return new Promise((resolve, reject) => {
                    ctx.call("db.events_signup_items.getEventItems", ctx.params)
                    .then((res) => {
                        response.data = res[0]
                        resolve(response);
                    })
                    .catch((err) => {
                        console.log(err)
                        reject(err);
                    })
                })
            }
        },
        /**
         * send email to user
         */
        eventShareOrinvite: {
            params: {
                event_id: "string",
                from_user: "string",
                type: "string"
            },
            handler(ctx) {
                return new Promise((resolve, reject) => {
                    let { event_id, group_id, user_id, others, from_user } = ctx.params;
                    _async.parallel({
                        event: function (callback) { //get the events details
                            ctx.call("db.events.find", { query: { id: event_id } })
                                .then((res) => {
                                    var _events = {};
                                    if (res && res.length > 0) {
                                        _events = res[0];
                                        if (_events.event_page) {
                                            _events.event_page = `${process.env.SHARE_BASE_URL}page/events/${_events.event_page}`;
                                        }
                                    }
                                    callback(null, _events);
                                })
                                .catch((err) => {
                                    console.log("events Search Error", err);
                                })
                        },
                        group_members: function (callback) { // get memebers inside a group
                            if (group_id) {
                                ctx.broker.call('db.group_map.eventInviteGroup', ctx.params)
                                    .then((res) => {
                                        // Track user activities start
                                        let trackerData = {
                                            user_id: parseInt(from_user),
                                            type: 'Event',
                                            type_id: parseInt(event_id),
                                            sub_type: 'event invite to group',
                                            sub_type_id: parseInt(group_id),
                                            activity: 'Sent event invitation to group'
                                        };
                                        ctx.broker.call('user_activities.trackUserActivities', trackerData);
                                        // Track user activities end
                                        callback(null, res);
                                    })
                                    .catch((err) => {
                                        console.log("User Search Error", err);
                                    })
                            }
                            if (user_id) {
                                ctx.broker.call('db.users.find', { query: { id: user_id } })
                                    .then((res) => {
                                        // Track user activities start
                                        let trackerData = {
                                            user_id: parseInt(from_user),
                                            type: 'Event',
                                            type_id: parseInt(event_id),
                                            sub_type: 'event invite to user',
                                            sub_type_id: parseInt(user_id),
                                            activity: 'Sent event invitation to user'
                                        };
                                        ctx.broker.call('user_activities.trackUserActivities', trackerData);
                                        // Track user activities end
                                        callback(null, res);
                                    })
                                    .catch((err) => {
                                        console.log("User Search Error", err);
                                    })
                            }
                            if (others) {
                                callback(null, others);
                            }
                        },
                        speakers: function (callback) { // get speakers of an events
                            ctx.call('db.event_speakers.find', { query: { event_id: event_id } })
                                .then((res) => {
                                    callback(null, res);
                                })
                                .catch((err) => {
                                    console.log("speakers Search Error", err);
                                })
                        },
                        contacts: function (callback) { // get contacts for a event
                            ctx.call('db.event_contacts.find', { query: { event_id: event_id } })
                                .then((res) => {
                                    callback(null, res);
                                })
                                .catch((err) => {
                                    console.log("contacts Search Error", err);
                                })
                        },
                        fromUser: function (callback) {
                            ctx.call('db.users.find', { query: { id: from_user } })
                                .then((res) => {
                                    callback(null, res);
                                })
                                .catch((err) => {
                                    console.log("speakers Search Error", err);
                                })
                        },
                    }, function (err, results) {
                        if (err) {
                            console.log('Global search is failed ==> ', err);
                            reject(err);
                        } else {
                            ctx.params.result = results;
                            //fetch created user details
                            ctx.call('db.users.find', { query: { id: results.event.created_by } })
                                .then(async (user) => {
                                    let _user = {}
                                    if (user && user.length > 0) {
                                        _user = user[0]
                                    }
                                    ctx.params.result['user'] = _user;
                                    if (others) {///not in the systesm 
                                        ctx.params.type = 'events';
                                        ctx.params.type_value='invite'
                                        let phnz = others.map(item => item.phone);
                                        let isUser = await ctx.broker.call('db.users.find', { query: { phone: phnz } })
                                        if (isUser && isUser.length == 0)
                                            ctx.broker.call('template.templateSendMail', ctx.params)
                                    } else {//inside application
                                        ctx.params = { ...ctx.params, group_members: results.group_members }
                                        ctx.broker.call("events.sendAppNotification", ctx.params)
                                    }
                                    return true;
                                })
                                .then((res) => {
                                    let response_data = {
                                        code: 200,
                                        message: "Event email sent",
                                        data: []
                                    }
                                    resolve(response_data)
                                })
                                .catch((err2) => {
                                    let response_data = {
                                        code: 500,
                                        message: err2,
                                        data: []
                                    }
                                    reject(response_data);
                                })
                        }
                    });
                })
            }
        },



        addEventSpeaker: {
            params: {
                name: "string",
                description: "string",
                event_id: "string"
            },
            handler(ctx) {
                return new Promise((resolve, reject) => {
                    if (ctx.meta && ctx.meta.file) {
                        ctx.meta.file.forEach(async element => {
                            ctx.params[element.fieldname] = element.filename;
                            await this.calls3(ctx, element).then(res => {
                                if (res == "SUCCESS") {
                                    let params = { ...ctx.params, speaker_image: `https://familheey.s3.amazonaws.com/Speakers/${ctx.meta.file[0].filename}` }
                                    ctx.call("db.event_speakers.create", params).then(response => resolve(response))
                                }
                                else {
                                    reject(res)
                                }

                            }
                            ).catch(err => reject(err))
                        });
                    }
                })
            }
        },

        editEventSpeaker: {
            params: {
                id: "string"
            },
            handler(ctx) {
                return new Promise((resolve, reject) => {
                    if (ctx.meta && ctx.meta.file) {
                        ctx.meta.file.forEach(async element => {
                            ctx.params[element.fieldname] = element.filename;
                            await this.calls3(ctx, element).then(res => {
                                if (res == "SUCCESS") {
                                    let params = { ...ctx.params, speaker_image: `https://familheey.s3.amazonaws.com/Speakers/${ctx.meta.file[0].filename}` }
                                    ctx.call("db.event_speakers.create", params).then(response => resolve(response))
                                }
                                else {
                                    reject(res)
                                }

                            }
                            ).catch(err => reject(err))
                        });
                    }
                })
            }
        },

        deleteEventSpeaker: {
            params: {
                id: "string"
            },
            handler(ctx) {
                return new Promise((resolve, reject) => {
                    if (ctx.meta && ctx.meta.file) {
                        ctx.meta.file.forEach(async element => {
                            ctx.params[element.fieldname] = element.filename;
                            await this.calls3(ctx, element).then(res => {
                                if (res == "SUCCESS") {
                                    let params = { ...ctx.params, speaker_image: `https://familheey.s3.amazonaws.com/Speakers/${ctx.meta.file[0].filename}` }
                                    ctx.call("db.event_speakers.create", params).then(response => resolve(response))
                                }
                                else {
                                    reject(res)
                                }

                            }
                            ).catch(err => reject(err))
                        });
                    }
                })
            }
        },

        addEventContact: {
            params: {
                name: "string",
                email: "string",
                phone: "string",
                event_id: "string"
            },
            handler(ctx) {
                let response = {}
                return new Promise((resolve, reject) => {
                    ctx.call("db.event_contacts.create", { ...ctx.params }).then(res => {
                        response.data = res;
                        resolve(response);
                    }).catch(err => {
                        reject(err);
                    })
                })
            }
        },
        /**
         * update event contact
         */
        editEventContact: {
            params: {
                id: "string"
            },
            handler(ctx) {
                let response = {}
                return new Promise((resolve, reject) => {
                    ctx.call("db.event_contacts.update", ctx.params).then(res => {
                        response = { code: 200, data: res, message: 'updated contact' }
                        resolve(response);
                    }).catch(err => {
                        reject(err);
                    })
                })
            }
        },
        /**
         * delete event contact from events
         */
        deleteEventContact: {
            params: {
                id: "string",
            },
            handler(ctx) {
                //let response = {}
                return new Promise((resolve, reject) => {
                    ctx.call("db.event_contacts.deleteById", ctx.params)
                    .then(res => {
                        resolve({ code: 200, data: res, message: 'deleted contact' });
                    }).catch(err => {
                        reject(err);
                    })
                })
            }
        },
        getEventList: {
            params: {
                group_id: "string"
            },
            handler: (ctx) => {
                // return ctx.params;
                let { group_id } = ctx.params;
                let response = {};
                let flag = group_id == "null";
                return flag ? (new Promise((resolve, reject) => {
                    ctx.call("db.events.find", {
                        query: {
                            is_public: true,
                            is_active: true
                        }
                    }).then(res => {
                        response.data = res;
                        resolve(response)
                    }).catch(err => reject(err))
                })) : (
                        new Promise((resolve, reject) => {
                            ctx.call("db.events_share.find", {
                                query: {
                                    group_id: group_id
                                }
                            }).then(res => resolve(res)).catch(err => reject(err))
                        })
                    )

            }
        },
        shareEvent: {
            params: {
                event_id: "string",
                group_id: "string",
                user_id: "string"
            },
            handler: (ctx) => {
                let response = {};
                let { group_id, user_id, event_id } = ctx.params;
                let customID = event_id + group_id + user_id
                // return customID
                return new Promise((resolve, reject) => {
                    ctx.call("db.group_map.find", { query: { group_id: group_id, user_id: user_id } }).then(res => {
                        res.length > 0 ? (
                            ctx.call("db.events_share.create", { ...ctx.params, id: customID }).then(res3 => {
                                ctx.params.notifyType = "event_share"
                                this.appNotify(ctx, ctx.params);
                                response.data = res3;
                                resolve(res3)
                            }).catch(err => {
                                //let waitResponse = ctx.broker.call("error_handling.errorCode", { error_name: err.name })
                                let error = err.name == "SequelizeUniqueConstraintError" ? "ALREADY EXISTS" : err
                                response.err = error;
                                reject(response.err)
                            })
                        ) : (reject("NOT A MEMBER"))
                    })
                })
            }
        },
        updateFollowStatus: {
            params: {
                user_id: "string",
                event_id: "string"
            },
            handler: (ctx) => {
                let { user_id, event_id } = ctx.params;
                let customID = user_id + event_id;
                let response = {};
                return new Promise((resolve, reject) => {
                    ctx.call("db.events_follow.find", { query: { id: customID } }).then(
                        result => {
                            result.length > 0 ? (ctx.call("db.events_follow.update", { ...ctx.params, id: customID, is_blocked: !result[0].is_blocked }
                            ).then(res => {
                                response.data = res
                                resolve(response);
                            }).catch(err => reject(err))
                            ) : (ctx.call("db.events_follow.create", { ...ctx.params, id: customID, is_blocked: true }).then(res => {
                                response.data = res
                                resolve(response);
                            }))
                        }).catch(err => reject(err))


                    ctx.call("db.events_follow.create", { ...ctx.params, id: customID }).then(res => {
                        response.data = res
                        resolve(response);
                    })
                })
                // let response2 = ctx.params;
                // return response2;
            }
        },
        eventDetails: {
            params: {
                event_id: "string",
                user_id: "string"
            },
            handler: (ctx) => {
                let { event_id, user_id } = ctx.params;
                let response = {};
                //let items_needed = [];
                //let itemsAndQuantityNeeded = [];
                return new Promise((resolve, reject) => {
                    ctx.call("db.events.find", { query: { id: event_id } }).then(res => {
                        response.eventDetails = res
                        let { created_by } = res[0]
                        let flag = created_by === parseInt(user_id)
                        _async.parallel({
                            attendies: (callback) => {
                                ctx.call(`db.event_attendees.${flag ? 'find' : 'count'}`, { query: { event_id: event_id } }
                                ).then(res4 => {
                                    callback(null, res4)
                                }).catch(err => console.log("Error Happened -- Attendies"))
                            },
                            contacts: (callback) => {
                                ctx.call("db.event_contacts.find", { query: { event_id: event_id } }).then(res5 => {
                                    callback(null, res5)
                                }).catch(err => {
                                    console.log("Error Happened -- contacts")
                                })
                            },
                            rsvp: (callback) => {
                                ctx.call(`db.event_rsvp.${flag ? 'getRsvpDetails' : 'getRsvpCount'}`, { event_id: event_id }).then(res6 => {
                                    callback(null, res6)
                                }).catch(err => console.log("Error Happened -- rsvp"))
                            },
                            speakers: (callback) => {
                                ctx.call("db.event_speakers.find", { query: { event_id: event_id } }).then(res7 => {
                                    callback(null, res7);
                                }).catch(err => console.log("Error Happened -- speakers"))
                            },
                            // followers:(callback)=>{
                            //     flag?(console.log("sjcvshjcvhj"),
                            //     callback(null,flag))
                            //     :
                            //     (ctx.call("followersdb.events_follow.count",{query:{event_id:event_id}}).then(res=>{
                            //         callback(null,res);
                            //     }).catch(err=>console.log("Error Happened -- followers ")))
                            // },
                            items: (callback) => {
                                ctx.call("db.events_signup_items.getItemsContribution", { event_id: event_id }).then(res8 => {
                                    callback(null, res8)
                                })
                                    .catch(err => console.log("Error Happened -- followers"))
                            }
                        }, function (err, res9) {
                            if(err) {
                                console.log("error  happened", err);    
                            } else {
                                response = {
                                    ...response,
                                    contacts: res9.contacts,
                                    attendies: res9.attendies,
                                    rsvp: res9.rsvp,
                                    speakers: res9.speakers,
                                    // followers:res.followers,
                                    items: res9.items,
                                };
                                resolve(response);   
                            }

                        })

                    }).catch(err => {
                        reject(err)
                    })
                })
            }
        },
        addEventItemsContribution: {
            params: {
                event_id: "string",
                contribute_user_id: "string",
                contribute_items_id: "string",
                contribute_item_quantity: "string"
            },
            handler(ctx) {
                return new Promise((resolve, reject) => {
                    var response = {}
                    let { contribute_item_quantity, ...rem } = ctx.params;
                    ctx.call('db.events_signup_contributions.find', { query: { ...rem } }).then(res => {
                        resolve(res);
                        res.length <= 0 ? (
                            ctx.call('db.events_signup_contributions.create', { ...ctx.params })
                                .then((res2) => {
                                    response.data = [res2];
                                    resolve(response);
                                    // Track user activities start
                                    let trackerData = {
                                        user_id: parseInt(contribute_user_id),
                                        type: 'Event',
                                        type_id: parseInt(event_id),
                                        sub_type: 'event signup contribution',
                                        sub_type_id: parseInt(contribute_items_id),
                                        activity: 'Contribute event signup to event'
                                    };
                                    ctx.broker.call('user_activities.trackUserActivities', trackerData);
                                    // Track user activities end
                                })
                                .catch((err) => {
                                    response.err = err;
                                    reject(response);
                                })
                        ) : (
                                ctx.call('db.events_signup_contributions.update', {
                                    id: res[0].id,
                                    ...rem,
                                    contribute_item_quantity: `${parseInt(res[0].contribute_item_quantity) + parseInt(contribute_item_quantity)}`
                                })
                                    .then((res2) => {
                                        response.data = [res2];
                                        resolve(response);
                                        // Track user activities start
                                        let trackerData = {
                                            user_id: parseInt(contribute_user_id),
                                            type: 'Event',
                                            type_id: parseInt(event_id),
                                            sub_type: 'event signup contribution',
                                            sub_type_id: parseInt(contribute_items_id),
                                            activity: 'Update contributed event signup to event'
                                        };
                                        ctx.broker.call('user_activities.trackUserActivities', trackerData);
                                        // Track user activities end
                                    })
                                    .catch((err) => {
                                        response.err = err;
                                        reject(response);
                                    })
                            )
                    })

                })
            }

        },
        updateEventItemsContribution: {
            params: {
                id: "string"
            },
            handler(ctx) {
                return new Promise((resolve, reject) => {
                    ctx.broker.call('db.events_signup_contributions.update', ctx.params)
                        .then((res) => {
                            resolve(res);
                            // Track user activities start
                            let trackerData = {
                                user_id: parseInt(res.contribute_user_id),
                                type: 'Event',
                                type_id: parseInt(res.event_id),
                                sub_type: 'event signup contribution',
                                sub_type_id: parseInt(id),
                                activity: 'Update contributed event signup to event'
                            };
                            ctx.broker.call('user_activities.trackUserActivities', trackerData);
                            // Track user activities end
                        })
                        .catch((err) => {
                            console.log(err);
                            reject({ data: err });
                        });

                })
            }
        },
        /**
         * delete an item contribbuted to an event
         */
        deleteEventItemsContribution: {
            params: {
                id: "string"
            },
            handler(ctx) {
                return new Promise((resolve, reject) => {
                    ctx.broker.call('db.events_signup_contributions.remove', ctx.params)
                        .then((res) => {
                            resolve(res);
                            // Track user activities start
                            let trackerData = {
                                user_id: parseInt(res.contribute_user_id),
                                type: 'Event',
                                type_id: parseInt(res.event_id),
                                sub_type: 'event signup contribution',
                                sub_type_id: parseInt(id),
                                activity: 'Deleted contribution event signup to event'
                            };
                            ctx.broker.call('user_activities.trackUserActivities', trackerData);
                            // Track user activities end
                        })
                        .catch((err) => {
                            console.log(err);
                            reject({ data: err });
                        });
                })
            }
        },
        eventInvitationList: {
            params: {},
            handler(ctx) {
                return new Promise((resolve, reject) => {
                    var response = {}
                    ctx.broker.call("db.events_invitation.invitationList", ctx.params)
                        .then((doc) => {
                            response.data = doc;
                            resolve(response);
                        })
                        .catch((err) => {
                            reject(err);
                        })
                })
            }
        },
        userInvitationEventList: {
            params: {},
            handler(ctx) {
                return new Promise((resolve, reject) => {
                    var response = {}
                    ctx.broker.call("db.events_invitation.invitationList", ctx.params)
                        .then((doc) => {
                            response.data = doc;
                            resolve(response);
                        })
                        .catch((err) => {
                            reject(err);
                        })
                })
            }
        },
        /**
         * fetch category data to the events creation category
         */
        fetchCategory: {
            params: {},
            handler(ctx) {
                return new Promise((resolve, reject) => {
                    var response = {}
                    ctx.broker.call("db.category_lookup.find", { sort: 'name' })
                        .then((doc) => {
                            response.data = doc;
                            resolve(response);
                        })
                        .catch((err) => {
                            reject(err);
                        })
                })
            }
        },
        /**
         * save data to calender
         */
        savetoCalender: {
            params: {
                user_id: "string"
            },
            handler(ctx) {
                return new Promise((resolve, reject) => {
                    var response = {}
                    ctx.broker.call("db.calender.create", ctx.params)
                        .then((doc) => {
                            response.data = doc;
                            resolve(response);
                        })
                        .catch((err) => {
                            reject(err);
                        })
                })
            }
        },
        /**
         * update a user's calender data
         * eg:set/update reminder
         */
        updateCalender: {
            params: {
                id: "string"
            },
            handler(ctx) {
                return new Promise((resolve, reject) => {
                    var response = {}
                    ctx.broker.call("db.calender.update", ctx.params)
                        .then((doc) => {
                            response.data = doc;
                            resolve(response);
                        })
                        .catch((err) => {
                            reject(err);
                        })
                })
            }
        },
        /**
         * fetch full calender for a userid
         */
        fetchCalender: {
            params: {
                user_id: "string"
            },
            handler(ctx) {
                return new Promise((resolve, reject) => {
                    var response = {}
                    ctx.broker.call("db.calender.fetchCalender", ctx.params)
                        .then((doc) => {
                            response.data = doc;
                            resolve(response);
                        })
                        .catch((err) => {
                            reject(err);
                        })
                })
            }
        },
        /**
         * search for calender of a user
         */
        searchCalender: {
            params: {
                user_id: "string",
                search: "string"
            },
            handler(ctx) {
                return new Promise((resolve, reject) => {
                    var response = {}
                    ctx.broker.call("db.calender.searchCalender", ctx.params)
                        .then((doc) => {
                            response.data = doc;
                            resolve(response);
                        })
                        .catch((err) => {
                            reject(err);
                        })
                })
            }
        },
        blockEvent: {
            params: {
                event_id: "string",
                user_id: "string",
            },
            handler: (ctx) => {
                let { event_id, user_id, } = ctx.params;
                let response = {};
                return new Promise((resolve, reject) => {
                    ctx.call("db.events.find", { query: { id: event_id, created_by: user_id } }).then(res => {
                        let flag = res.length > 0;
                        flag ? (
                            ctx.call("db.events.update", { id: event_id, is_active: false }).then(res2 => {
                                response.data = res2;
                                resolve(response);
                                // Track user activities start
                                let trackerData = {
                                    user_id: parseInt(user_id),
                                    type: 'Event',
                                    type_id: parseInt(event_id),
                                    activity: 'Blocked event'
                                };
                                ctx.broker.call('user_activities.trackUserActivities', trackerData);
                                // Track user activities end
                            }).catch(err => {
                                console.log(err);
                                response.err = err;
                                reject(response);
                            })
                        ) : (
                                reject("NOT_AUTHORISED")
                            )
                    })

                })
            }
        },
        unBlockEvent: {
            params: {
                event_id: "string",
                user_id: "string",
            },
            handler: (ctx) => {
                let { event_id, user_id, } = ctx.params;
                let response = {}
                return new Promise((resolve, reject) => {
                    ctx.call("db.events.find", { query: { id: event_id, created_by: user_id } }).then(res => {
                        let flag = res.length > 0;
                        flag ? (
                            ctx.call("db.events.update", { id: event_id, is_active: true }).then(res2 => {
                                response.data = res2;
                                resolve(response);
                                // Track user activities start
                                let trackerData = {
                                    user_id: parseInt(user_id),
                                    type: 'Event',
                                    type_id: parseInt(event_id),
                                    activity: 'Unblock event'
                                };
                                ctx.broker.call('user_activities.trackUserActivities', trackerData);
                                // Track user activities end
                            }).catch(err => {
                                console.log(err);
                                response.err = err;
                                reject(response);
                            })
                        ) : (
                                reject("NOT_AUTHORISED")
                            )
                    })

                })
            }
        },
        /**
         * save when a user views a event
         */
        eventViewed: {
            params: {
                event_id: "string",
                user_id: "string",
                group_id: "string"
            },
            handler: (ctx) => {
                let response = {}
                return new Promise((resolve, reject) => {
                    ctx.call("db.viewed.create", ctx.params)
                        .then(res => {
                            response.data = res;
                            resolve(response);
                            // Track user activities start
                            let trackerData = {
                                user_id: parseInt(user_id),
                                type: 'Event',
                                type_id: parseInt(event_id),
                                activity: 'Viewed event'
                            };
                            ctx.broker.call('user_activities.trackUserActivities', trackerData);
                            // Track user activities end
                        }).catch(err => {
                            console.log(err);
                            response.err = err;
                            reject(response);
                        })
                })
            }
        },
        addAgenda: {
            params: {
                event_id: "string",
                user_id: "string",
                title: "string",
                start_date: "string",
                end_date: "string"
            },
            handler(ctx) {
                let response = {};
                return new Promise((resolve, reject) => {
                    if (ctx.meta && ctx.meta.file) {
                        ctx.meta.file.forEach(element => {
                            ctx.params[element.fieldname] = element.filename;
                            this.calls3(ctx, element)
                        });
                    }
                    ctx.call("db.agenda.create", { ...ctx.params }).then(res => {
                        response.data = res;
                        resolve(response);
                        // Track user activities start
                        let trackerData = {
                            user_id: parseInt(user_id),
                            type: 'Event',
                            type_id: parseInt(event_id),
                            sub_type: 'agenda',
                            sub_type_id: parseInt(res.id),
                            activity: 'Added agenda to event'
                        };
                        ctx.broker.call('user_activities.trackUserActivities', trackerData);
                        // Track user activities end
                    }).catch(err => {
                        response.err = err;
                        reject(response)
                    })
                })
            }
        },
        /**
         * custome url handles here
         */
        eventUrl: {
            params: {},
            handler(ctx) {
                let { text, rsvp, osid } = ctx.params;
                let response = {};
                ctx.params = { ...ctx.params, id: osid }
                if (rsvp) {
                    ctx.call("db.other_users.update", ctx.params);
                }
                return new Promise((resolve, reject) => {
                    ctx.call("db.events.find", { query: { event_page: text }, fields: ["id"] }).then(res => {
                        response.data = res;


                        let _HTML = `<html>
                            <head>
                                <title> Custom Title</title>
                                <meta name="description" content="custom Text" />
                                <meta property="og:image" content="https://ahrefs.com/blog/wp-content/uploads/2019/12/fb-how-to-become-an-seo-expert.png" />               
                                </head>
                                <script>alert(" hai aha i")</script>
                                <body> You are in Custom Text </body>
                        </html>`


                        return (_HTML);
                    }).catch(err => {
                        response.err = err;
                        reject(response);
                    })
                })
            }
        },
        /**
         * custome url handles here
         */
        postUrl: {
            params: {},
            handler(ctx) {
                let { text } = ctx.params;
                let response = {
                    data: [{id: text}]
                };
                return response;
            }
        },

        exploreEvents: {
            params: {
                user_id: "string"
            },
            handler: (ctx) => {
                let { user_id, from_date, to_date, query, filter } = ctx.params;
                let response = {};
                return new Promise((resolve, reject) => {
                    _async.parallel({
                        sharedEvents: (callback) => {
                            ctx.call("db.events.shared_events", {
                                user_id: user_id,
                                from_date: from_date,
                                to_date: to_date,
                                txt: query,
                                filter: filter
                            }).then(res => {
                                callback(null, res);
                            }).catch(err => {
                                console.log("error happened -------- sharedEvents", err);
                            })
                        },
                        basedOnLocation: (callback) => {
                            ctx.call("db.events.logined_user", {
                                user_id: user_id,
                                from_date: from_date,
                                to_date: to_date,
                                txt: query,
                                filter: filter
                            }).then(res => {
                                callback(null, res);
                            }).catch(err => {
                                console.log("error happened -------- basedOnLocation", err);
                            })
                        },
                        publicEvents: (callback) => {
                            ctx.call("db.events.public_events", {
                                user_id: user_id,
                                from_date: from_date,
                                to_date: to_date,
                                txt: query,
                                filter: filter
                            }).then(res => {
                                callback(null, res);
                            }).catch(err => {
                                console.log("error happened -------- publicEvents", err);
                            })
                        },
                        // eventInvitations:(callback)=>{
                        //     ctx.call("db.events.event_invitations",{user_id:user_id}).then(res=>{
                        //         callback(null,res);
                        //     }).catch(err=>{
                        //         console.log("error happened -------- publicEvents",err);
                        //     })
                        // },
                        // created_by_me:(callback)=>{
                        //     ctx.call("db.events.find",{created_by:user_id}).then(res=>{
                        //         callback(null,res);
                        //     }).catch(err=>{
                        //         console.log("error happened -------- publicEvents",err);
                        //     })
                        // },
                        // rsvp_yes:(callback)=>{
                        //     ctx.call("db.events.based_on_rsvp",{status:'yes'}).then(res=>{
                        //         callback(null,res);
                        //     }).catch(err=>{
                        //         console.log("error happened -------- publicEvents",err);
                        //     })
                        // },
                        // rsvp_maybe:(callback)=>{
                        //     ctx.call("db.events.based_on_rsvp",{status:'maybe'}).then(res=>{
                        //         callback(null,res);
                        //     }).catch(err=>{
                        //         console.log("error happened -------- publicEvents",err);
                        //     })
                        // }
                    }, function (err, res) {
                        if(err) {
                            console.log("error happened");   
                        } else {
                            response.data = {
                                explore: {
                                    sharedEvents: res.sharedEvents,
                                    basedOnLocation: res.basedOnLocation,
                                    publicEvents: res.publicEvents
                                }
                                // my_event_list:{
                                //     created_by_me:res.created_by_me,
                                //     rsvp_yes:res.rsvp_yes,
                                //     rsvp_maybe:res.rsvp_maybe
                                // }
                            };
                            resolve(response);   
                        }
                    })
                })
            }
        },
        eventInvitations: {
            params: {
                user_id: "string"
            },
            handler: (ctx) => {
                let response = {};
                let { user_id, query, to_date, from_date, filter } = ctx.params;
                return new Promise((resolve, reject) => {
                    ctx.call("db.events.event_invitations", {
                        user_id: user_id,
                        txt: query,
                        filter: filter,
                        from_date: from_date,
                        to_date: to_date
                    })
                        .then(res => {
                            response.data = res;
                            resolve(response)
                        }).catch(err => {
                            response.err = err;
                            reject(response);
                        })
                })
            }
        },
        createdByMe: {
            params: {},
            handler: (ctx) => {
                let response = {};
                let { user_id, query, from_date, to_date, filter } = ctx.params;
                return new Promise((resolve, reject) => {
                    _async.parallel({
                        created_by_me: (callback) => {
                            ctx.call("db.events.eventListbyCreator", {
                                created_by: user_id,
                                txt: query,
                                from_date: from_date,
                                to_date: to_date,
                                filter: filter,
                                expire: 0
                            }).then(res => {
                                callback(null, res);
                            }).catch(err => {
                                console.log("error happened -------- Events", err);
                            })
                        },
                        rsvp_yes: (callback) => {
                            ctx.call("db.events.based_on_rsvp", {
                                status: 'going',
                                user_id: user_id,
                                from_date: from_date,
                                to_date: to_date,
                                txt: query,
                                filter: filter,
                                expire: 0
                            }).then(res => {
                                callback(null, res);
                            }).catch(err => {
                                console.log("error happened -------- Events", err);
                            })
                        },
                        rsvp_maybe: (callback) => {
                            ctx.call("db.events.based_on_rsvp", {
                                status: 'interested',
                                user_id: user_id,
                                from_date: from_date,
                                to_date: to_date,
                                txt: query,
                                filter: filter,
                                expire: 0
                            }).then(res => {
                                callback(null, res);
                            }).catch(err => {
                                console.log("error happened -------- Events", err);
                            })
                        },
                        created_by_me_past: (callback) => {
                            ctx.call("db.events.eventListbyCreator", {
                                created_by: user_id,
                                txt: query,
                                from_date: from_date,
                                to_date: to_date,
                                filter: filter,
                                expire: 1
                            }).then(res => {
                                callback(null, res);
                            }).catch(err => {
                                console.log("error happened -------- pastEvents", err);
                            })
                        },
                        rsvp_yes_past: (callback) => {
                            ctx.call("db.events.based_on_rsvp", {
                                status: 'going',
                                user_id: user_id,
                                from_date: from_date,
                                to_date: to_date,
                                txt: query,
                                filter: filter,
                                expire: 1
                            }).then(res => {
                                callback(null, res);
                            }).catch(err => {
                                console.log("error happened -------- Events", err);
                            })
                        },
                        rsvp_maybe_past: (callback) => {
                            ctx.call("db.events.based_on_rsvp", {
                                status: 'interested',
                                user_id: user_id,
                                from_date: from_date,
                                to_date: to_date,
                                txt: query,
                                filter: filter,
                                expire: 1
                            }).then(res => {
                                callback(null, res);
                            }).catch(err => {
                                console.log("error happened -------- Events", err);
                            })
                        }
                    }, function (err, res) {
                        if(err) {
                            console.log("error happened");  
                        } else {
                            response.data = {
                                my_event_list: {
                                    created_by_me: res.created_by_me,
                                    rsvp_yes: res.rsvp_yes,
                                    rsvp_maybe: res.rsvp_maybe
                                },
                                past_event_list: {
                                    data: res.created_by_me_past.concat(res.rsvp_yes_past, res.rsvp_maybe_past)
                                }
                            };
                            resolve(response);   
                        }
                    })
                })
            }
        },
        /**
         * send app notifications 
         */
        sendAppNotification: {
            params: {},
            handler(ctx) {
                return new Promise(async (resolve, reject) => {
                    let _this = this;
                    let { user_id, group_id, event_id, group_members, from_user, type, result } = ctx.params;
                    let { fromUser, event } = result;
                    let duplicateArr;
                    if (type == 'share') {
                        duplicateArr = await ctx.call("db.events_share.find", { query: { event_id: event_id }, fields: ["event_id", "user_id"] })
                    } else {
                        duplicateArr = await ctx.call("db.events_invitation.find", { query: { event_id: event_id }, fields: ["event_id", "user_id"] })
                    }

                    group_members.forEach(async value => {
                        let _eventShare = {
                            event_id: event_id,
                            type_id: parseInt(event_id)
                        }
                        _eventShare.message_title = 'familheey';
                        _eventShare.message = (type == 'share') ?
                            `<b>${fromUser[0].full_name}</b>  has shared an event <b>${event.event_name}</b>` : `<b>${fromUser[0].full_name}</b> has invited you to <b>${event.event_name}</b>`
                        let _userid;
                        if (user_id) {
                            _eventShare.user_id = value.id;
                            _userid = value.id;
                        } else if (group_id) {
                            _eventShare.user_id = value.user_id;
                            _eventShare.group_id = value.group_id;
                            _userid = value.user_id;
                        }
                        _eventShare.to_id = parseInt(_eventShare.user_id);
                        let _duplicate = false;
                        let findData = { event_id: event_id, user_id: _userid }
                        const filtered = duplicateArr.filter(ele => ele.event_id == event_id && ele.user_id == _userid);
                        if (filtered.length > 0) {
                            _duplicate = true;
                        } else {
                            duplicateArr.push(findData)
                        }

                        let createEv = false;
                        if(value.group_id){
                            let groupSettings = await ctx.broker.call("db.groups.getGroupSettings", { group_id: parseInt(value.group_id) })
                            let usertype = await ctx.call("db.group_map.find", { query: { group_id: value.group_id, user_id: from_user }, fields: ["type"] });
                            let createPermission = groupSettings.filter((el) => el.key === "POST_CREATE");

                            if ((usertype[0].type == 'admin' || usertype[0].type == 'member') && createPermission[0].id == 6 || (usertype[0].type == 'admin' && createPermission[0].id == 7)) {
                                createEv = true;
                            }
                        }
                        if(user_id && user_id.length>0){
                            createEv = true;
                        }
                        if (type && type == 'share' && createEv) {
                            _eventShare.shared_by = from_user;
                            ctx.call("db.events_share.create", { ..._eventShare, duplicate: _duplicate })
                        }
                        if (type && type == 'invitation' && createEv) {
                            _eventShare.invited_by = from_user;
                            _eventShare.type = (group_id) ? 'groups' : 'users';
                            ctx.call("db.events_invitation.create", { ..._eventShare, duplicate: _duplicate })
                        }
                        _eventShare.from_id = from_user;
                        _eventShare.propic = fromUser[0].propic;
                        _eventShare.type = 'event';
                        _eventShare.link_to = parseInt(event_id);
                        if ( parseInt(event.created_by) != _eventShare.to_id && parseInt(from_user) != _eventShare.to_id && createEv) {
                            ctx.broker.call("notification.sendAppNotification", _eventShare);
                            ctx.call("db.users.find", { query: { id: _eventShare.to_id }, fields: ["notification"] })
                            .then((_user_notify) => {
                                if (_user_notify.length > 0 && _user_notify[0].notification) {
                                    _this.pushNotify(ctx, _eventShare)
                                }
                            }).catch((err) => {
                                console.log(err)
                            })
                        }
                    });
                })
            }
        },
        updateAgenda: {
            params: {
                agenda_id: "string",
            },
            handler(ctx) {
                let response = {};
                return new Promise((resolve, reject) => {
                    if (ctx.meta && ctx.meta.file) {
                        ctx.meta.file.forEach(element => {
                            ctx.params[element.fieldname] = element.filename;
                            this.calls3(ctx, element)
                        });
                    }
                    let { agenda_id, ...remParams } = ctx.params;
                    ctx.call("db.agenda.update", { id: agenda_id, ...remParams }).then(res => {
                        response.data = res;
                        resolve(response);
                        // Track user activities start
                        let trackerData = {
                            user_id: parseInt(res.user_id),
                            type: 'Event',
                            type_id: parseInt(res.event_id),
                            sub_type: 'agenda',
                            sub_type_id: parseInt(id),
                            activity: 'Updated agenda to event'
                        };
                        ctx.broker.call('user_activities.trackUserActivities', trackerData);
                        // Track user activities end
                    }).catch(
                        err => {
                            response.err = err;
                            reject(err);
                        }
                    );
                })
            }
        },
        listAgenda: {
            params: {
                event_id: "string"
            },
            handler: (ctx) => {
                let response = {};
                let dateObj = [];
                let dates = new Set();
                return new Promise((resolve, reject) => {
                    ctx.call("db.agenda.listAgenda", { ...ctx.params }).then((res) => {
                        res.map((item) => {
                            dates.add(item.event_date)
                        })
                        dates.forEach((date) => {
                            let dataSet = res.filter(resp_date => resp_date.event_date == date)
                            // dateObj = {...dateObj,[`${date}`]:{date:date,data:dataSet}}
                            dateObj.push({ date: date, data: dataSet })
                            response.data = dateObj;
                        })
                        // response.data;
                        resolve(response);
                    }).catch(err => {
                        response.err = err;
                        reject(response)
                    })
                })
            }
        },
        getEventById: {
            params: {
                event_id: "string",
                user_id: "string"
            },
            handler: (ctx) => {
                let { event_id, user_id } = ctx.params;
                let response = {};
                return new Promise((resolve, reject) => {
                    _async.parallel({
                        event: (callback) => {
                            ctx.call("db.events.getEventbyID", { event_id: event_id }).then(res => {
                                if (res && res.length > 0) {
                                    res[0].event_url = `${process.env.SHARE_BASE_URL}page/events/${res[0].event_page}`
                                }
                                callback(null, res);
                            }).catch(err => {
                                console.log("Error happened ================ event", err);
                            })
                        },
                        contacts: (callback) => {
                            ctx.call("db.event_contacts.getByEventId", { event_id: event_id }).then(res => {
                                callback(null, res);
                            }).catch(err => {
                                console.log("Error happened =================== Contacts", err)
                            })
                        },
                        rsvp: (callback) => {
                            ctx.call("db.event_rsvp.findByUserid", { event_id: event_id, attendee_id: user_id  }).then(res => {
                                callback(null, res);
                            }).catch(err => {
                                console.log("Error happened =================== rsvp", err)
                            })
                        }
                    }, function (err, res) {
                        if(err) {
                            console.log("===== error happened =====");    
                        } else {
                            response.data = res;
                            resolve(response);  
                        }
                    })
                })
            }
        },
        getguestsCount: {
            params: {
                event_id: "string"
            },
            handler: (ctx) => {
                let response = {};
                let { event_id } = ctx.params;
                //let userSet = new Set();
                return new Promise((resolve, reject) => {
                    _async.parallel({
                        getAllcountBasedOnID: (callback) => {
                            // ctx.call("db.events.getAllcounts", { event_id: event_id }).then(res => {
                            ctx.call("db.event_rsvp.countOfEventList", { event_id: event_id, attendee_type: "going" }).then(res => {
                                callback(null, parseInt(res[0].count));
                            }).catch(err => {
                                console.log("error happened =============== getAllcountBasedOnID", err)
                            })
                        },
                        interested: (callback) => {
                            ctx.call("db.event_rsvp.countOfEventList", { event_id: event_id, attendee_type: "interested" }).then(res => {
                                callback(null, parseInt(res[0].count) )
                            }).catch(err => {
                                console.log("error happened =============== interested", err)
                            })
                        },
                        invitationsSend: (callback) => {
                            let count_ = 0;
                            ctx.call("db.events_invitation.invitationsSendCount", { event_id: event_id })
                            .then(res => {
                                count_ = count_+ parseInt(res[0].count);
                                return ctx.call("db.other_users.count", { query: { event_id: event_id } });
                            })
                            .then(res => {
                                callback(null,  count_ /* + res */); // +res is commented by Sarath on 06/02/2021 to fix issue no 26 in ios where number of invitations send is showing wrong after sending invitation to other contacts,issue is with number of others 
                            }).catch(err => {
                                console.log("error happened =============== going", err)
                            })
                        },
                        notGoing: (callback) => {
                            ctx.call("db.event_rsvp.countOfEventList", { event_id: event_id, attendee_type: "not-going" }).then(res => {
                                callback(null, parseInt(res[0].count) )
                            }).catch(err => {
                                console.log("error happened =============== notGoing", err)
                            })
                        }

                    }, function (err, res) {
                        if(err) {
                            console.log("error happened");  
                        } else {
                            response.data = {
                                allCount: res.getAllcountBasedOnID ? res.getAllcountBasedOnID : 0 ,
                                interested: res.interested ? res.interested : 0,
                                invitationsSend: res.invitationsSend ? res.invitationsSend : 0,
                                notGoing: res.notGoing ? res.notGoing : 0
                            };
                            resolve(response);   
                        }
                    })
                })
            }
        },
        /**
         * event link generation check exits/not
         */
        checkEventLink: {
            params: {
                text: "string"
            },
            handler: (ctx) => {
                let { text } = ctx.params;
                //let response = {};
                return new Promise((resolve, reject) => {

                    ctx.call("db.events.find", { query: { event_page: text } })
                        .then(res => {
                            if (res && res.length > 0) {
                                reject("Link already exist")
                            } else {
                                resolve({
                                    name: "success",
                                    message: "Link not found in db",
                                    code: 200
                                })
                            }
                        }).catch(err => {
                            console.log("Error happened ================ event");
                            reject(err)
                        })
                })
            }
        },
        getGoingGuestDetails: {
            params: {
                current_id: "string",
                event_id: "string"
            },
            handler(ctx) {
                let { event_id, current_id, query } = ctx.params;
                return new Promise((resolve, reject) => {
                    ctx.call("db.event_rsvp.getDetailsOfUsersGoing", { user_id: current_id, event_id: event_id, txt: query }).then(res => {
                        resolve(res)
                    }).catch(err => {
                        reject(err);
                    })
                })
            }
        },
        getRSVPGuestDetails: {
            params: {
                current_id: "string",
                event_id: "string",
                rsvp_status: "string"
            },
            handler(ctx) {
                let { event_id, current_id, rsvp_status, query } = ctx.params;
                return new Promise((resolve, reject) => {
                    ctx.call("db.event_rsvp.getDetailsOfUsersBasedOnRSVP", {
                        user_id: current_id,
                        event_id: event_id,
                        rsvp_status: rsvp_status,
                        txt: query
                    }).then(res => {
                        resolve(res)
                    }).catch(err => {
                        reject(err);
                    })
                })
            }
        },
        getInvitedUserDetails: {
            params: {
                event_id: "string"
            },
            handler: (ctx) => {
                let { event_id } = ctx.params;
                let response = {};
                return new Promise((resolve, reject) => {
                    ctx.call("db.events_invitation.getInvitedUsersDetails", { event_id: event_id }).then(res => {
                        response.data = res;
                        resolve(response);
                    }).catch(err => {
                        response.err = err;
                        reject(response);
                    })
                })
            }
        },

        /**
         * create reminder for an going event
         */
        createReminder: {
            params: {
                event_id: "string",
                user_id: "string",
                event_date: "string"
            },
            handler(ctx) {
                let response = {};
                return new Promise((resolve, reject) => {
                    var eventDate = ctx.params.event_date;
                    if(ctx.params.remind_on && ctx.params.remind_on != null) {
                        var onTime = ctx.params.remind_on ? ctx.params.remind_on : 0;
                        let minuseTime = (eventDate/60)-onTime;
                        minuseTime = minuseTime * 60;
                        ctx.params.remind_on = minuseTime
                    } else if(ctx.params.remind_date && ctx.params.remind_date != null) {
                        ctx.params.remind_on = ctx.params.remind_date   
                    }
                    if(ctx.params.reminder_id) {
                        ctx.broker.call("events.deleteReminder", ctx.params)
                        .then(deldata => {
                            console.log('Reminder successfully deleted',deldata);
                        }).catch(err => {
                            console.log('Reminder delete error',err);
                        })
                    }
                    ctx.call("db.reminder.create", ctx.params)
                    .then(res => {
                        response.data = res;
                        resolve(response);
                    }).catch(err => {
                        response.err = err;
                        reject(response);
                    })
                })
            }
        },

        /**
         * create reminder for an going event
         */
        updateReminder: {
            params: {
                id: "string"
            },
            handler(ctx) {
                let response = {};
                return new Promise((resolve, reject) => {
                    ctx.call("db.reminder.update", ctx.params)
                    .then(res => {
                        response.data = res;
                        resolve(response);
                    }).catch(err => {
                        response.err = err;
                        reject(response);
                    })
                })
            }
        },

        deleteReminder: {
            params: {
                reminder_id: "any"
            },
            handler(ctx) {
                let response = {};
                let delobj = {
                    id:ctx.params.reminder_id
                }
                return new Promise((resolve, reject) => {
                    ctx.call("db.reminder.remove", delobj)
                    .then(res => {
                        response.data = res;
                        resolve(response);
                    }).catch(err => {
                        response.err = err;
                        reject(response);
                    })
                })
            }   
        },

        /**
         * create reminder for an going event
         */
        fetchReminder: {
            params: {
                event_id: "string",
                user_id: "string"
            },
            handler(ctx) {
                let response = {};
                return new Promise((resolve, reject) => {
                    ctx.params = { ...ctx.params, is_active: true }
                    ctx.call("db.reminder.find", { query: ctx.params })
                    .then(res => {
                        response.data = res;
                        resolve(response);
                    }).catch(err => {
                        response.err = err;
                        reject(response);
                    });
                });
            }
        },

        /**
         * get the list of users for particular contribution
         */
        itemContributorList: {
            params: {
                event_id: "string",
                item_id: "string"
            },
            handler(ctx) {
                let response = {}
                return new Promise((resolve, reject) => {
                    ctx.call("db.events_signup_items.getItemsContribution", ctx.params)
                    .then((res) => {
                        response.data = res
                        resolve(response);
                    })
                    .catch((err) => {
                        console.log(err)
                        reject(err);
                    })
                })
            }
        },

        /**
         * get the list of invitations send to 
         * users / groups / others
         * by a user(invited_by)
         */
        eventInvitedByUser: {
            params: {
                user_id: "string"
            },
            handler(ctx) {
                //let { user_id } = ctx.params;
                return new Promise((resolve, reject) => {
                    _async.parallel({
                        groups: function (callback) {
                            ctx.call("db.events_invitation.forGroups", ctx.params)
                                .then((res) => {
                                    callback(null, res);
                                })
                                .catch((err) => {
                                    console.log("events Search Error", err);
                                })
                        },
                        users: function (callback) {
                            ctx.broker.call('db.events_invitation.forUsers', ctx.params)
                                .then((res) => {
                                    callback(null, res);
                                })
                                .catch((err) => {
                                    console.log("User Search Error", err);
                                })
                        },
                        others: function (callback) {
                            ctx.call('db.events_invitation.other_users', ctx.params)
                                .then((res) => {
                                    callback(null, res);
                                })
                                .catch((err) => {
                                    console.log("speakers Search Error", err);
                                })
                        }

                    }, function (err, results) {
                        if (err) {
                            console.log('event invitation list is failed ==> ', err);
                            reject(err);
                        } else {
                            resolve({ code: 200, message: "success", data: results })
                        }
                    });
                })
            }
        },
        eventShareList: {
            params: {
                event_id: "string",
                user_id: "string"
            },
            handler: (ctx) => {
                let { event_id, user_id, query } = ctx.params;
                let response = {};
                return new Promise((resolve, reject) => {
                    ctx.call("db.events_share.event_share_list", { event_id: event_id, user_id: user_id, txt: query }).then(res => {
                        response.data = res;
                        resolve(response);
                    }).catch(err => {
                        response.err = err;
                        reject(response);
                    })
                })
            }
        },

        /**
         * custome url handles here
         */
        groupUrl: {
            params: {},
            handler(ctx) {
                let { group_id } = ctx.params;
                let response = {};
                return new Promise((resolve, reject) => {
                    ctx.call("db.groups.find", { query: { f_text: group_id }, fields: ["id"] }).then(res => {
                        response.data = res;
                        resolve(response);
                    }).catch(err => {
                        response.err = err;
                        reject(response);
                    })
                })
            }
        },
        event_reminder: {

            handler(ctx) {
                return new Promise((resolve, reject) => {
                    let _this = this;
                    let start_date = moment().startOf('day').add(1, 'day').unix();
                    let end_date = moment().endOf('day').add(1, 'day').unix();
                    ctx.call("db.events.event_by_date", { from_date: start_date, to_date: end_date })
                        .then(event => {
                            event.map(eve => {
                                ctx.call("db.events.eventInterestedList", { event_id: eve.id })
                                    .then(users => {
                                        users.map(async u => {
                                            let sendObj = {
                                                from_id: u.user_id,
                                                type: 'event',
                                                propic: u.propic,
                                                message: 'You have an event tomorrow',
                                                category: 'Events',
                                                message_title: 'familheey',
                                                type_id: parseInt(eve.id),
                                                link_to: eve.id,
                                                to_id: u.user_id
                                            }
                                            ctx.broker.call("notification.sendAppNotification", sendObj)
                                            let _user_notify = await ctx.call("db.users.find", { query: { id: sendObj.to_id }, fields: ["notification"] })
                                            if (_user_notify.length > 0 && _user_notify[0].notification) {
                                                _this.pushNotify(ctx, sendObj)
                                            }
                                        })
                                    });
                            });
                            resolve(event);
                        });
                });
            }
        },
        eventDeleteOrCancel: {
            params: {
                event_id: "string",
                user_id: "string",
                status: "string"
            },
            handler(ctx) {
                let { event_id, user_id, status } = ctx.params;
                let response = {};
                return new Promise((resolve, reject) => {
                    ctx.call("db.events.find", { query: { id: event_id, created_by: user_id } })
                        .then((_event) => {//find the user is the creator
                            if (_event && _event.length == 0) {
                                reject("Not Authorised")
                            } else { console.log(" ===else ===")
                                response.event = _event[0];
                                let save_data = {};

                                if (status == 'delete') {
                                    save_data = { id: event_id, is_active: false }
                                } else if (status == 'cancel') {
                                    save_data = { id: event_id, is_cancel: true }
                                }
                                return ctx.call("db.events.update", save_data) /** delete|cancel event **/
                            }
                            // Track user activities start
                            let trackerData = {
                                user_id: parseInt(user_id),
                                type: 'Event',
                                type_id: parseInt(event_id),
                                activity: 'Deleted/Canceled event'
                            };
                            ctx.broker.call('user_activities.trackUserActivities', trackerData);
                        })
                        .then(res => {//rsvp except not-going get all list
                            return ctx.call("db.event_rsvp.rsvpExceptList", { event_id: event_id, except: 'not-going' })
                        })
                        .then((rsvpList) => {
                            response.users = rsvpList;
                            return ctx.call("db.other_users.find", { query:{ event_id: event_id, is_active: true } })
                        })
                        .then((other_users) => {
                            response.users = response.users.concat(other_users);
                            if (response.event && response.event.to_date >= moment().unix()) {
                                let _data = {
                                    event_id: event_id,
                                    user_id: user_id,
                                    notifyType: status,
                                    delete_user_notify: response.users
                                }
                                this.appNotify(ctx, _data);
                            }
                            resolve({ code: 200, message: 'success' });
                        })
                        .catch(err => {
                            response.err = err;
                            reject(response);
                        })
                })
            }
        },
        getEventBasedOnEVentPage: {
            params: {
                event_page: "string",
                user_id: "string"
            },
            handler: (ctx) => {
                let { event_page, user_id } = ctx.params;
                return new Promise((resolve, reject) => {
                    ctx.call("db.events.find", { query: { event_page: event_page } }).then(async res => {
                        return res;
                    }).then(res => {
                        ctx.broker.call("events.getEventById", { event_id: `${res[0].id}`, user_id: user_id }).then(res2 => {
                            resolve(res2)
                        })
                    }).catch(err => {
                        reject("EVENT_NOT_FOUND")
                    })
                })
            }
        },
        get_notification_event_data: {
            params: {},
            handler(ctx) {
                return new Promise((resolve, reject) => {
                    ctx.call("db.events.get_notification_event_data", ctx.params)
                    .then(res => {
                        resolve(res);
                    }).catch(err => {
                        reject(err);
                    });
                });
            }
        },
    },
    events: {},
    methods: {
        /**
        * call s3 service for fiel upload
        */
        calls3(ctx, params) {
            return new Promise((resolve, reject) => {
                ctx.call('s3upload.fileUpload', params)
                    .then((res) => {
                        resolve("SUCCESS");
                    })
                    .catch((err) => {
                        reject("ERROR");
                    })
            })
        },
        async asyncForEach(array, callback) {
            for (let index = 0; index < array.length; index++) {
                await callback(array[index]);
            }
        },

        appNotify(ctx, params) {
            let _this = this;
            let { event_id, group_id, user_id, notifyType, created_by, resp, delete_user_notify, _users, _msg } = params;
            _async.parallel({
                fromUser: (callback) => {
                    if (user_id)
                        ctx.call("db.users.find", { query: { id: parseInt(user_id) }, fields: ["id", "full_name", "propic"] })
                            .then(res => {
                                callback(null, res);
                            }).catch(err => {
                                console.log("|| || error happened -------- fromUser", err);
                            })
                    else callback(null, [])
                },
                group: (callback) => {
                    if (group_id)
                        ctx.call("db.groups.find", { query: { id: group_id, is_active: true }, fields: ["id", "group_name", "logo"] }).then(res => {
                            callback(null, res);
                        }).catch(err => {
                            console.log("error happened -------- group", err);
                        })
                    else callback(null, [])
                },
                event: (callback) => {
                    if (event_id)
                        ctx.call("db.events.find", { query: { id: event_id }, fields: ["id", "event_name"] }).then(res => {
                            callback(null, res);
                        }).catch(err => {
                            console.log("error happened -------- event???", err);
                        })
                    else callback(null, [])
                },
                eventAdmin: (callback) => {
                    if (created_by)
                        ctx.call("db.events.find", { query: { created_by: created_by, id: event_id } }).then(res => {
                            callback(null, res)
                        }).catch(err => {
                            console.log("error happened -------- eventAdmin", err)
                        })
                    else callback(null, [])
                },
                calender: (callback) => {
                    if (event_id)
                        ctx.call("db.calender.find", { query: { event_id: event_id }, fields: ["id"] }).then(res => {
                            callback(null, res);
                        }).catch(err => {
                            console.log("error happened -------- calender", err);
                        })
                    else callback(null, [])
                },
            }, async function (err, result) {
                let { fromUser, event, eventAdmin, calender } = result;
                var sendObj = {
                    from_id: fromUser.length > 0 ? fromUser[0].id : '',
                    type: 'event',
                    propic: fromUser.length > 0 ? fromUser[0].propic : '',
                    type_id: eventAdmin.length > 0 ? parseInt(eventAdmin[0].id) : '',
                    link_to: eventAdmin.length > 0 ? parseInt(eventAdmin[0].id) : '',
                    to_id: eventAdmin.length > 0 ? parseInt(eventAdmin[0].created_by) : ''
                }

                switch (notifyType) {
                    case 'rsvp':
                        if(sendObj.type_id == '') {
                            await ctx.call("db.events.find", { query: {id: event_id },fields: ["id","created_by"] }).then(res => {
                                sendObj.type_id = res[0].id;
                                sendObj.link_to = res[0].id;
                                sendObj.to_id = res[0].created_by; 
                            }).catch(err2 => {
                                console.log("error happened -------- getting event", err2)
                            })    
                        }
                        let _response = (resp == 'interested') ? 'interested in' : 'attending'
                        sendObj.message = `<b>${fromUser[0].full_name}</b> is ${_response}  your event <b>${event[0].event_name}</b> `
                        sendObj.category = 'rsvp';
                        sendObj.message_title = 'familheey';
                        sendObj.sub_type = (resp == 'interested') ? 'guest_interested' : 'guest_attending';
                        ctx.broker.call("notification.sendAppNotification", sendObj)
                        ctx.call("db.users.find", { query: { id: sendObj.to_id }, fields: ["notification"] })
                            .then((_user_notify) => {
                                if (_user_notify.length > 0 && _user_notify[0].notification) {
                                    _this.pushNotify(ctx, sendObj)
                                }
                            })
                            .catch(e => console.log(e))
                        
                        break;
                    case 'unfollow':
                        sendObj.message = `<b>${fromUser[0].full_name}</b> has unfollow your family  <b>${toGroup[0].group_name}</b> `
                        sendObj.category = 'unfollow';
                        sendObj.message_title = 'familheey';
                        break;
                    case 'public_evt_create':
                        _users.forEach(async elem => {
                            sendObj.message = `<b>${fromUser[0].full_name}</b> has added a new event <b>${event[0].event_name}</b>`;
                            sendObj.category = 'event';
                            sendObj.message_title = 'familheey';
                            sendObj.sub_type = '';
                            sendObj.to_id = parseInt(elem.user_id);
                            sendObj.type_id = parseInt(event_id)
                            sendObj.link_to = parseInt(event_id);
                            ctx.broker.call("notification.sendAppNotification", sendObj);
                            let _user_notify = await ctx.call("db.users.find", { query: { id: elem.user_id }, fields: ["notification"] })
                            if (_user_notify.length > 0 && _user_notify[0].notification) {
                                sendObj.to_id = parseInt(elem.user_id);
                                _this.pushNotify(ctx, sendObj)
                            }
                        });
                        break;
                    case 'edit_event':
                        _users.forEach(async elem => {
                            sendObj.message = `<b>${fromUser[0].full_name}</b> has updated ${_msg} on event <b>${event[0].event_name}</b>`;
                            sendObj.category = 'event';
                            sendObj.message_title = 'familheey';
                            sendObj.sub_type = '';
                            sendObj.to_id = parseInt(elem.user_id);
                            sendObj.type_id = parseInt(event_id);
                            sendObj.link_to = parseInt(event_id);
                            ctx.broker.call("notification.sendAppNotification", sendObj);
                            ctx.call("db.users.find", { query: { id: sendObj.to_id }, fields: ["notification"] })
                            .then((_user_notify) => {
                                if (_user_notify.length > 0 && _user_notify[0].notification) {
                                    _this.pushNotify(ctx, sendObj)
                                }
                            })
                            .catch(e => console.log(e))
                        });
                        break;
                    default:
                        break;
                }

                if (notifyType == 'cancel' || notifyType == 'delete') {
                    let _userArr = [];
                    delete_user_notify.forEach(async user => {
                        let notifyType_ = notifyType == 'cancel' ? 'cancelled' : 'deleted';
                        let event_name = (event && event.length > 0) ? event[0].event_name : ''
                        sendObj.link_to = parseInt(user.event_id);
                        sendObj.to_id = parseInt(user.user_id);
                        sendObj.type_id = parseInt(event_id); //value of type_id in sendObj is set by Sarath on 08/02/2021 to avoid the null value and set the event_id to type_id for fixing issue no 64 in android where push notification and app notification were not sending at the time of event cancellation or deletion
                        sendObj.message = `<b>${fromUser[0].full_name}</b> has ${notifyType_} the event  <b>${event_name}</b> `
                        sendObj.category = 'delete';
                        sendObj.message_title = 'familheey';
                        if (_userArr.indexOf(user.email) == -1 || _userArr.indexOf(user.phone) == -1) {
                            if (user.email && user.email != '') {
                                ctx.broker.call("notification.sendEmail", {
                                    toAddresses: [`${user.email}`],
                                    message: `Hi ${user.full_name}, <br/>Heads up! ${sendObj.message} <br/>`,
                                    subject: `Event ${notifyType_}`
                                })
                            }
                            if (user.phone && user.phone != '') {
                                ctx.call('notification.sendSms', {
                                    message: `Heads up! ${fromUser[0].full_name} has ${notifyType_} the event ${event_name} `,
                                    PhoneNumber: user.phone,
                                    subject: `Event ${notifyType_}`
                                })
                            }
                        }
                        _userArr.push(user.email); _userArr.push(user.phone);
                        ctx.broker.call("notification.sendAppNotification", sendObj);
                        ctx.call("db.users.find", { query: { id: sendObj.to_id }, fields: ["notification"] })
                        .then((_user_notify) => {
                            if (_user_notify.length > 0 && _user_notify[0].notification) {
                                _this.pushNotify(ctx, sendObj)
                            }
                        })
                        .catch(e => console.log(e))
                    });
                    if (calender && calender.length > 0) {
                        calender.forEach(val => {
                            ctx.call("db.calender.update", { id: val.id, is_active: false })
                                .then((res) => console.log(res)).catch((err3) => console.log(err3))
                        });
                    }
                }
            });
        },
        makeEventPage(length) {
            var result = '';
            var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
            var charactersLength = characters.length;
            for (var i = 0; i < length; i++) {
                result += characters.charAt(Math.floor(Math.random() * charactersLength));
            }
            return result;
        },
        async pushNotify(ctx, params) {
            let _devices = await ctx.broker.call("userops.getregisterToken", { user_id: params.to_id, is_active: true })            
            _devices.data.forEach(async elem => {
                params.to_id = elem.user_id //this line is added by Sarath on 02/03/2021 for passing user_id to parameters
                params.r_id  = elem.id
                params.token = elem.device_token
                params.device_type = elem.device_type.toLowerCase()
                params.title = params.message_title
                params.body  = this.removeTags(params.message)
                params.sub_type = params.sub_type ? params.sub_type : ''
                await ctx.broker.call("notification.sendPushNotification", params)
            });
        },
        removeTags(str) {
            if ((str === null) || (str === '')) return '';
            else return str.toString().replace(/(<([^>]+)>)/ig, '');
        }
    },
    created() { },
    started() { },
    stopped() { }
}
