"use strict";
const { MoleculerError } = require("moleculer").Errors;
const ApiGateway = require("moleculer-web");
const path = require('path')
const uploadDir = path.join(__dirname, "../../public/uploads");
const multer = require("multer");
var storage = multer.diskStorage({
	destination: function (req, file, cb) {
		cb(null, uploadDir)
	},
	filename: function (req, file, cb) {
		let eext = `.${file.originalname.split('.').reverse()[0]}`;
		//let ext  = `.${file.mimetype.split('/')[1]}`;
		cb(null, file.fieldname + '-' + Date.now() + eext)
	}
});
var _upload = multer({ storage: storage })

module.exports = {
	name: "bulkapi",
	mixins: [ApiGateway],
	//More info about settings: https://moleculer.services/docs/0.13/moleculer-web.html
	settings: {
		cors: {
			//Configures the Access-Control-Allow-Origin CORS header.
			origin: "*",
		},
		port: 4001,
		routes: [{
			path: "/bulk",
			use: [
				_upload.any()
			],
			whitelist: [
				//Access to any actions in all services under "/api" URL
				"**"
			],
			aliases: {
				"POST /v1/event_invitation" : "bulk.eventInvitation",
				"POST /v1/add_family_member": "bulk.addFamilyMember",
			},
			onBeforeCall(ctx, route, req, res) {
				if (req.files) { ctx.meta.file = req.files }
			},
			onAfterCall(ctx, route, req, res, data) {
				return data;
			}
		}],     
		
		//Serve assets from "public" folder
		assets: {
			folder: "public"
		}
	}
}