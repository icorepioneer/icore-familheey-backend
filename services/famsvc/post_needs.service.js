"use strict";
var _async = require('async');
var moment = require('moment');

module.exports = {
    name: "post_needs",
    settings: {},
    actions: {
        /**
         * create a need
         */
        create:{
            params:{
                user_id:"string"
            },
            handler(ctx){
                let { to_group_id, items, user_id } = ctx.params;
                return new Promise(async(resolve, reject)=>{
                    /** create main request table */
                    ctx.call("db.post_requests.create",ctx.params)
                    .then((doc)=>{
                        let { id } = doc;   
                        ctx.params = { ...ctx.params, post_request_id:id}                    
                        _async.parallel({
                            /** map the groups for the request to send */
                            groupMaps: (callback) => {
                                ctx.params = { ...ctx.params, ntype:'create_need' }
                                let x=1;

                                if(to_group_id.length>0){ //to selected families
                                    to_group_id.forEach(async g_id => {
                                        let group_maps = { user_id:user_id, post_request_id:id,group_id:g_id }
                                        await this.createGm(ctx,group_maps)
                                        if(to_group_id.length == x){
                                            this.appNotify(ctx);
                                        }
                                        x++;
                                    });
                                    callback(null,[]);
                                }else{ //to all families
                                    ctx.broker.call('db.groups.userGroups', { user_id: user_id })
                                    .then(result => {//fetch all groups of user
                                        to_group_id = result.map(v=>v.group_id);
                                        ctx.params = { ...ctx.params, to_group_id:to_group_id }
                                        to_group_id.forEach(async g_id => {
                                            let group_maps = { user_id:user_id, post_request_id:id,group_id:g_id }
                                            await this.createGm(ctx,group_maps)
                                            if(to_group_id.length == x){
                                                this.appNotify(ctx);
                                            }
                                            x++;
                                        });
                                        callback(null,[]);
                                    })
                                    .catch(e=>console.log(e))
                                }
                            },
                            /** items maps to db with req id */
                            reqItems: (callback) => {
                                let y = 1; let _items = [];
                                items.forEach(async elem => {
                                    let _elem = { ...elem, user_id:user_id, post_request_id:id }
                                    let item_saved = await ctx.call("db.post_request_items.create",_elem);
                                    elem = { ...elem, item_id:item_saved.id, total_contribution:null }
                                    _items.push(elem)
                                    if(items.length == y) callback(null,_items)
                                    y++;
                                });
                            },
                        }, function (err, result) {
                            ctx.params.items = result.reqItems;
                            if(err) reject(err)
                            else{
                                resolve({ code:200, message:"created successfully", data:ctx.params});
                                // Track user activity start
                                let trackerData = {
                                    user_id: parseInt(ctx.params.user_id),
                                    type: 'post request',
                                    type_id: parseInt(id),
                                    activity: ('created post request')
                                };
                                ctx.broker.call('user_activities.trackUserActivities', trackerData);
                                // Track user activity end
                            }      
                        })
                    })
                    .catch(e=>reject(e))
                })   
            }
        },

        /** update the request */
        update:{
            params:{
                user_id:"string",
                post_request_id:"string"
            },
            handler(ctx){
                let { post_request_id, user_id, to_group_id } = ctx.params;
                return new Promise(async(resolve, reject)=>{
                    let isCreator = await this.isCreator(ctx,{ query:{ id:post_request_id, user_id:user_id, is_active:true }, fields:["id","user_id","group_type"] })
                                
                    if(isCreator && isCreator.length==0 ){
                        reject("Not Authorised!");
                        return;
                    }
                    let req_items = { ...ctx.params, id:post_request_id }
                    
                    ctx.call("db.post_requests.update",req_items);//update post requests

                    /** create main request table */
                    ctx.call("db.post_requests_group_map.find",{ query:{ post_request_id:post_request_id }, fields:["id", "group_id"] })
                    .then(async(doc)=>{
                        //let x = 1;
                        let idz = doc.map(value=>value.id);
                        let diffArray = [];
                        ctx.params = { ...ctx.params, ntype:'update_need' };
                        /** delete existing data */
                        if(idz.length>0) await ctx.call("db.post_requests_group_map.bulkDelete", { idz:idz.toString()})
        
                        /** create new data */
                        if(to_group_id.length>0){ //to selected families
                            diffArray = to_group_id.filter(y => !idz.includes(y));
                            let y_c = 0;
                            to_group_id.forEach(async g_id => {
                                let group_maps = { user_id:user_id, post_request_id:post_request_id,group_id:g_id }
                                await this.createGm(ctx,group_maps)
                                if(to_group_id.length == y_c){
                                    
                                    // Notification sent to newely added groups start
                                    var notifyCtx = ctx;
                                    notifyCtx.params.to_group_id = diffArray;
                                    this.appNotify(notifyCtx);
                                    // Notification sent to newely added groups end
                                }
                                y_c++;
                            });
                        }else{ //to all families
                            ctx.broker.call('db.groups.userGroups', { user_id: user_id })
                            .then(result => {//fetch all groups of user
                                to_group_id = result.map(v=>v.group_id);
                                diffArray = to_group_id.filter(z => !idz.includes(z));
                                ctx.params = { ...ctx.params, to_group_id:to_group_id }
                                to_group_id.forEach(async g_id => {
                                    let group_maps = { user_id:user_id, post_request_id:post_request_id,group_id:g_id }
                                    await this.createGm(ctx,group_maps)
                                    if(to_group_id.length == z){
                                        //resolve({ code:200, message:'updated successfully', data:[]})
                                        // Notification sent to newely added groups start
                                        var notifyCtx = ctx;
                                        notifyCtx.params.to_group_id = diffArray;
                                        this.appNotify(ctx);
                                        // Notification sent to newely added groups end

                                        // Track user activity start
                                        let trackerData = {
                                            user_id: parseInt(ctx.params.user_id),
                                            type: 'post request',
                                            type_id: parseInt(post_request_id),
                                            activity: ('updated post request')
                                        };
                                        ctx.broker.call('user_activities.trackUserActivities', trackerData);
                                        // Track user activity end
                                    }
                                    z++;
                                });
                            })
                            .catch(e=>console.log(e))
                        }
                        resolve({ code:200, message:'updated successfully', data:[]})
                    })
                    .catch(e=>reject(e))
                })   
            }
        },
        /**
         * list all neds in request feed
         */
        post_need_list: {
            params: {
                user_id:"string"
            },
            handler(ctx) {
                return new Promise((resolve, reject)=>{
                    let { user_id }  = ctx.params;
                    ctx.call("db.post_requests.get_post_need",ctx.params)
                    .then(async(res)=>{
                        let needList = [];
                        let Limit = 3;
                        async function getItemsLst () {
                            if(res.length > 0) {
                                for (const items of res) {
                                    await getRequestItemlist(items);

                                    // let countOfItems = await ctx.broker.call('db.post_request_items.list_items', { post_request_id:items.post_request_id, typeis:'count' })
                                    // items.item_others_count = parseInt(countOfItems[0].count - Limit);
    
                                    let _count = await ctx.call("db.post_requests_item_contributions.supportersCount",{post_request_id:items.post_request_id})
                                    items.supporters = (_count.length>0) ? parseInt(_count[0].count) : 0;
                                    
                                    let to_groups = await ctx.broker.call('db.post_requests_group_map.requestToGroup', { post_request_id:items.post_request_id,user_id:user_id });
                                    items.to_groups = to_groups;
                                }
                            }
                        }

                        function getRequestItemlist(reqItemParm){
                            ctx.broker.call('db.post_request_items.get_request_item_list', { user_id:user_id, request_id:reqItemParm.post_request_id,limit:Limit,offset:0 })
                                    .then(async contrRes => {
                                        if(contrRes.length > 0) {
                                            reqItemParm.items = contrRes;
                                        } else {
                                            reqItemParm.items = [];    
                                        }
                                        needList.push(reqItemParm);
                                    }) 
                                    .catch((err) => {
                                        console.log(err);
                                        reject(err);
                                    });
                        }

                        await getItemsLst();
                        resolve({ code:200,data:needList });
                    }) 
                    .catch((err) => {
                        console.log(err);
                        reject(err);
                    });
                })
            }
        },
        /** delete a needs request */
        delete:{
            params:{
                post_request_id:"string",
                user_id:"string"
            },
            handler(ctx){
                let { post_request_id, user_id } = ctx.params;
                return new Promise(async(resolve, reject)=>{
                    let isCreator = await this.isCreator(ctx,{ query:{ id:post_request_id, user_id:user_id }, fields:["id","user_id","group_type"] })
                    
                    if(isCreator && isCreator.length==0 ){
                        reject("Not Authorised!");
                        return;
                    }
                    ctx.call("db.post_requests.update",{ id: post_request_id, is_active:false })
                    .then((doc)=>{
                        resolve({ code:200, message:'deleted successfully'});
                        // Track user activity start
                        let trackerData = {
                            user_id: parseInt(ctx.params.user_id),
                            type: 'post request',
                            type_id: parseInt(post_request_id),
                            activity: ('deleted post request')
                        };
                        ctx.broker.call('user_activities.trackUserActivities', trackerData);
                        // Track user activity end
                    })
                    .catch(e=>reject(e))
                })
            }
        },
        /**
         * details page of a need
         */
        post_need_detail: {
            params: {
                post_request_id:"string",
                user_id:"string"
            },
            handler(ctx) {
                let { post_request_id, user_id } = ctx.params;
                let response = {}
                return new Promise(async(resolve, reject)=>{
                    ctx.call("db.post_requests.get_post_need_detail",ctx.params)
                    .then(async(res)=>{
                        if(res && res.length==0){
                            reject("Need not found.");
                            return;
                        }
                        response.data = res[0];
                        return ctx.broker.call('db.post_request_items.get_request_item_list', { request_id:post_request_id, user_id:user_id })
                    }).then(contrRes => {
                        response.data = {
                            ...response.data,
                            items:contrRes
                        }
                        return ctx.call("db.post_requests_item_contributions.supportersCount",{post_request_id:post_request_id}) 
                    })
                    .then((_count)=>{
                        response.data = {
                            ...response.data,
                            supporters:(_count.length>0) ? parseInt(_count[0].count) : 0
                        }
                        return ctx.broker.call('db.post_requests_group_map.requestToGroup', { post_request_id:post_request_id,user_id:user_id });
                    })
                    .then(async(to_groups)=>{
                        response.data = {
                            ...response.data,
                            to_groups:to_groups
                        }
                        if(to_groups.length > 0) {
                            //Check this user is admin for any one of the family which post request shared start
                            await ctx.call("db.post_requests.checkUserAdminRequest",ctx.params)
                            .then((checkUser)=>{
                                const userAdmin = checkUser.some(user => user.type == 'admin');
                                if(userAdmin) {
                                    response.is_admin = true;
                                } else {
                                    response.is_admin = false;   
                                }
                            })
                            .catch(e=>reject(e))
                            //Check this user is admin for any one of the family which post request shared end
                            resolve({ code:200, ...response });
                        } else {
                            reject({ code:400, data:"User didnt have permission to access this request data" });
                        } 
                    })
                    .catch((err) => {
                        console.log(err);
                        reject(err);
                    });
                })
            }
        },

        /**
         * create items for a request
         */
        createItems:{
            params:{
                user_id:"string",
                request_item_title:"string", 
                item_quantity:"string",
                post_request_id:"string"
            },
            handler(ctx){
                let { user_id, post_request_id,item_quantity } = ctx.params;
                return new Promise(async(resolve, reject)=>{
                    /** create main request table */
                    let isCreator = await this.isCreator(ctx,{ query:{ id:post_request_id, user_id:user_id }, fields:["id"] });
                    if(isCreator && isCreator.length==0){
                        reject(" Not Authorized.");
                    }
                    else{
                        ctx.params = { ...ctx.params, post_request_id:isCreator[0].id, item_quantity:parseInt(item_quantity) }
                        ctx.call("db.post_request_items.create",ctx.params)
                        .then((doc)=>{
                            resolve({ code:200, message:'items created', data:doc});
                            let { id } = doc; 
                            // Track user activity start
                            let trackerData = {
                                user_id: parseInt(ctx.params.user_id),
                                type: 'post request item',
                                type_id: parseInt(id),
                                activity: ('added post request item')
                            };
                            ctx.broker.call('user_activities.trackUserActivities', trackerData);
                            // Track user activity end
                        })
                        .catch(e=>reject(e))
                    }
                })   
            }
        },
        /** 
         * update the created items
         */
        updateItems:{
            params:{
                id: "string",
                user_id: "string",
                post_request_id: "string"
            },
            handler(ctx){
                let { user_id, post_request_id } = ctx.params;
                return new Promise(async(resolve, reject)=>{
                    let isCreator = await this.isCreator(ctx,{ query:{ id:post_request_id, user_id:user_id }, fields:["id"] });
                    if(isCreator && isCreator.length==0){
                        reject(" Not Authorized.");
                    }
                    else{
                        ctx.call("db.post_request_items.update",ctx.params)
                        .then((doc)=>{
                            doc = { ...doc, item_quantity: parseInt(doc.item_quantity) }
                            resolve({ code:200, message:'items update', data:doc});
                            // Track user activity start
                            let trackerData = {
                                user_id: parseInt(ctx.params.user_id),
                                type: 'post request',
                                type_id: parseInt(post_request_id),
                                activity: ('updated post request item')
                            };
                            ctx.broker.call('user_activities.trackUserActivities', trackerData);
                            // Track user activity end
                        })
                        .catch(e=>reject(e))
                    }
                })
            }
        },
        /** 
         * delete an item from db
         */
        deleteItems:{
            params:{
                id:"string",
                user_id:"string",
                post_request_id:"string"
            },
            handler(ctx){
                let { user_id,post_request_id, id } = ctx.params;
                return new Promise(async(resolve, reject)=>{
                    let isCreator = await this.isCreator(ctx,{ query:{ id:post_request_id, user_id:user_id }, fields:["id"] });
                    if(isCreator && isCreator.length==0){
                        reject(" Not Authorized.");
                    }
                    else{
                        ctx.call("db.post_request_items.remove",{ id:id })
                        .then((doc)=>{
                            resolve({ code:200, message:'items deleted', data:doc});
                            // Track user activity start
                            let trackerData = {
                                user_id: parseInt(ctx.params.user_id),
                                type: 'post request item',
                                type_id: parseInt(id),
                                activity: ('deleted post request item')
                            };
                            ctx.broker.call('user_activities.trackUserActivities', trackerData);
                            // Track user activity end
                        })
                        .catch(e=>reject(e))
                    }
                });
            }
        },

        /**
         * create contributions
         */
        contributionCreate:{
            params:{
                contribute_user_id:"string",
                post_request_item_id:"string", 
                contribute_item_quantity:"string"
            },
            handler(ctx){
                let { post_request_item_id, contribute_user_id, contributionId} = ctx.params
                let notification = ctx.params.notification_off ? ctx.params.notification_off : '';
                return new Promise(async(resolve, reject)=>{
                    let type = 'create';
                    // if(payment_type && payment_type != '') {
                    //     if(payment_type.toLowerCase() == 'cheque' || payment_type.toLowerCase() == 'cash' || payment_type.toLowerCase() == 'other') {
                    //         ctx.params.is_acknowledge = true
                    //     }
                    // }
                    // if(payment_note && payment_note != '') {
                    //     ctx.params.is_acknowledge = true   
                    // }

                    /** create contribution */
                    // let isExist = await ctx.call("db.post_requests_item_contributions.find",{
                    //     query:{ post_request_item_id:post_request_item_id, contribute_user_id:contribute_user_id   },
                    //     fields:["id","contribute_item_quantity"]
                    // });
                    /** 
                     * if same user already contribute against the item , then update that
                     */
                    // if(isExist && isExist.length>0){
                    //     type = 'update';
                    //     ctx.params = {
                    //         ...ctx.params,
                    //         contribute_item_quantity : parseInt(contribute_item_quantity)+ parseInt(isExist[0].contribute_item_quantity),
                    //         id:isExist[0].id
                    //     }
                    // }

                    //If contributionId exist post_requests_item_contributions entry update instead of create start
                    if(contributionId && contributionId != '') {
                        type = 'update';
                        notification = 1;
                        ctx.params = {
                            ...ctx.params,
                            id:contributionId
                        }   
                    }
                    //If contributionId exist post_requests_item_contributions entry update instead of create end

                    ctx.call(`db.post_requests_item_contributions.${type}`,ctx.params)
                    .then(async(doc)=>{
                        let _sum = await ctx.call("db.post_requests_item_contributions.contributionSumOfaItem",{ post_request_item_id:post_request_item_id })
                        doc = { ...doc, total_contribution : parseInt(_sum[0].total_contribution) }
                        resolve({ code:200, message:'items contributed', data:doc});
                        let needed_user = await ctx.call("db.post_request_items.find",{ query:{  id:post_request_item_id }, fields:["user_id","post_request_id"] })
                        ctx.params = {
                            ...ctx.params,
                            user_id:contribute_user_id,
                            to_user_id:[needed_user[0].user_id],
                            item_id:post_request_item_id,
                            post_request_id: ( needed_user.length>0 ?  needed_user[0].post_request_id : null),
                            ntype:'contribution_create'
                        }
                        if(notification != 1) {
                            this.appNotify(ctx);
                        }
                        // Track user activity start
                        let trackerData = {
                            user_id: parseInt(ctx.params.user_id),
                            type: 'post request item',
                            type_id: parseInt(post_request_item_id),
                            activity: ('added post request item contribution')
                        };
                        ctx.broker.call('user_activities.trackUserActivities', trackerData);
                        // Track user activity end
                    })
                    .catch(err => err)
                })   
            }
        },
        /**
         * update the contribution by contributor only
         */
        contributionUpdate:{
            params:{
                contribute_user_id:"string",
                contribute_item_quantity:"string",
                id: "string"
            },
            handler(ctx){
                return new Promise(async(resolve, reject)=>{
                    let { id, contribute_user_id  } = ctx.params;
                    let isContributor = await ctx.call("db.post_requests_item_contributions.find",{ query:{ id:id, contribute_user_id:contribute_user_id } })

                    if(isContributor&&isContributor.length==0){
                        reject("Not Authorized.");
                    }else{
                        /** update contribution */
                        ctx.call("db.post_requests_item_contributions.update",ctx.params)
                        .then(async(doc)=>{
                            let _sum = await ctx.call("db.post_requests_item_contributions.contributionSumOfaItem",{ post_request_item_id: isContributor[0].post_request_item_id })
                            doc = { ...doc, total_contribution : parseInt(_sum[0].total_contribution) }
                            resolve({ code:200, message:'items updated', data:doc});
                            // Track user activity start
                            let trackerData = {
                                user_id: parseInt(ctx.params.contribute_user_id),
                                type: 'post request item',
                                type_id: parseInt(id),
                                activity: ('updated post request item contribution')
                            };
                            ctx.broker.call('user_activities.trackUserActivities', trackerData);
                            // Track user activity end
                        })
                        .catch(e=>reject(e))
                    }
                })   
            }
        },

        /**
         * delete items contributions
         */
        contributionDelete:{
            params:{
                id:"string",
                contribute_user_id:"string"
            },
            handler(ctx){
                let { id, contribute_user_id } = ctx.params;
                return new Promise(async(resolve, reject)=>{
                    let isContributor = await ctx.call("db.post_requests_item_contributions.find",{ query:{ id:id, contribute_user_id:contribute_user_id } })

                    if(isContributor&&isContributor.length==0){
                        reject("Not Authorized.");
                    }else{
                        /** update contribution */
                        ctx.call("db.post_requests_item_contributions.remove",ctx.params)
                        .then((doc)=>{
                            resolve({ code:200, message:'items removed', data:doc});
                            // Track user activity start
                            let trackerData = {
                                user_id: parseInt(ctx.params.contribute_user_id),
                                type: 'post request item',
                                type_id: parseInt(id),
                                activity: ('deleted post request item contribution')
                            };
                            ctx.broker.call('user_activities.trackUserActivities', trackerData);
                            // Track user activity end
                        })
                        .catch(e=>reject(e))
                    }
                })   
            }
        },

        /**
         * count/list of contributors
         */
        contributionListOrCount:{
            params:{
                user_id:"string",
                post_request_item_id:"string"
            },
            handler(ctx){
                let _this = this;
                let {post_request_item_id} = ctx.params
                return new Promise(async(resolve, reject)=>{
                    _async.parallel({
                        contribution : function(callback) {
                            ctx.call("db.post_requests_item_contributions.contributionListOrCount",ctx.params)
                            .then(async(res) => {
                                //Remove duplicate object from array start
                                //let uniq = {};
                                let contributionArray = _this.check_duplication(res);
                                //Remove duplicate object from array end

                                //Check if any contribution with acknowledge = false or any thank post is pending start
                                for (var j = 0; j < contributionArray.length; j++) {
                                    //Check contribute user is default(2) start
                                    if(contributionArray[j].contribute_user_id == 2 && contributionArray[j].paid_user_name && contributionArray[j].paid_user_name != '') {
                                        contributionArray[j].full_name = contributionArray[j].paid_user_name;
                                        //contributionArray[j].full_name = "Non-App-Members"
                                        //contributionArray[j].paid_user_name = "Non-App-Members"
                                    }
                                    //Check contribute user is default(2) end
                                    let checkAcknowledgeObj = {
                                        contribute_user_id:contributionArray[j].contribute_user_id,
                                        post_request_item_id:contributionArray[j].post_request_item_id,
                                        is_acknowledge:false,
                                        is_active:true,
                                        is_anonymous:contributionArray[j].is_anonymous
                                    }
                                    ctx.params.reqObj = checkAcknowledgeObj;
                                    let checkAcknowledge = await _this.findItemContribution(ctx);
                                    if(checkAcknowledge.length > 0) {
                                        contributionArray[j].is_acknowledge = false;
                                    }
                                    // await ctx.call("db.post_requests_item_contributions.find",{query:checkAcknowledgeObj,fields:["id"]})
                                    // .then((checkRes) => {
                                    //     if(checkRes.length > 0) {
                                    //         contributionArray[j].is_acknowledge = false; 
                                    //     }
                                    // })
                                    // .catch((err) => {
                                    //     console.log("Check if any contribution with acknowledge has some error",err);
                                    // })

                                    // Check if any thankpost is pending , if yes update is_pending_thank_post = true start 
                                    contributionArray[j].is_pending_thank_post = false; 
                                    let checkThankObj = {
                                        contribute_user_id:contributionArray[j].contribute_user_id,
                                        post_request_item_id:contributionArray[j].post_request_item_id,
                                        is_thank_post:false,
                                        is_active:true,
                                        is_anonymous:contributionArray[j].is_anonymous,
                                        skip_thank_post:false
                                    }
                                    ctx.params.reqObj = checkThankObj;
                                    let checkThank = await _this.findItemContribution(ctx);
                                    if(checkThank.length > 0) {
                                        contributionArray[j].is_pending_thank_post = true;
                                    }
                                    // await ctx.call("db.post_requests_item_contributions.find",{query:checkThankObj,fields:["id"]})
                                    // .then((checkRes2) => {
                                    //     if(checkRes2.length > 0) {
                                    //         contributionArray[j].is_pending_thank_post = true; 
                                    //     }
                                    // })
                                    // .catch((err) => {
                                    //     console.log("Check if any contribution with acknowledge has some error",err);
                                    // })
                                    // Check if any thankpost is pending , if yes update is_pending_thank_post = true end 

                                    //Check & get counts seperate for anonymous true & anonymous false start
                                    let contrubutionCountObj = {
                                        post_request_item_id : post_request_item_id,
                                        contribute_user_id : contributionArray[j].contribute_user_id,
                                        paid_user_name : contributionArray[j].paid_user_name
                                    }
                                    if(contributionArray[j].is_anonymous == true) {
                                        contrubutionCountObj.is_anonymous = true;
                                    } else {
                                        contrubutionCountObj.is_anonymous = false;
                                    }
                                    ctx.params.contrubutionCountObj = contrubutionCountObj;
                                    let contAnnounimousCount = await _this.contributionAnnounimousCount(ctx);
                                    if(contAnnounimousCount.length > 0) {
                                        contributionArray[j].total_contribution = contAnnounimousCount[0].total_contribution; 
                                        contributionArray[j].contribution_count = contAnnounimousCount[0].contribution_count;
                                    }
                                    // await ctx.call("db.post_requests_item_contributions.contributionAnonimousBasedCounts",contrubutionCountObj)
                                    // .then((anonymousResp) => {
                                    //     if(anonymousResp.length > 0) {
                                    //         contributionArray[j].total_contribution = anonymousResp[0].total_contribution; 
                                    //         contributionArray[j].contribution_count = anonymousResp[0].contribution_count;
                                    //     }
                                    // })
                                    // .catch((err) => {
                                    //     console.log("Check if any contribution with acknowledge has some error",err);
                                    // })
                                    //Check & get counts seperate for anonymous true & anonymous false end

                                    // //Check if any contribution with is_anonymous = true then update all data with is_anonymous = true start
                                    // await ctx.call("db.post_requests_item_contributions.find",{query:{contribute_user_id:contributionArray[j].contribute_user_id,post_request_item_id:contributionArray[j].post_request_item_id,is_anonymous:true,is_active:true},fields:["id"]})
                                    // .then(async(checkRes3) => {
                                    //     if(checkRes3.length > 0) {
                                    //         contributionArray[j].is_anonymous = true;
                                    //         // Remove is_anonymous = true data from search start 
                                    //         if(ctx.params.query && ctx.params.query != '') {
                                    //             contributionArray.splice(j, 1);
                                    //         }
                                    //         // Remove is_anonymous = true data from search end 
                                    //     } 
                                    // })
                                    // .catch((err) => {
                                    //     console.log("Check if any contribution with is_anonymous has some error",err);
                                    // })
                                    // //Check if any contribution with is_anonymous = true then update all data with is_anonymous = true end
                                }
                                //Check if any contribution with acknowledge = false or any thank post is pending end
                                callback(null, contributionArray);
                            })
                            .catch((err) => {
                                console.log("Get contribution has some error",err);
                                reject(err);
                            })
                        },
                        // thankpost_users : function(callback) {
                        //     // get request id
                        //     ctx.call("db.post_request_items.find",{query:{id:post_request_item_id},fields:["post_request_id"]})
                        //     .then((reqRes) => {
                        //         let reqest_id = reqRes[0].post_request_id;
                        //         //get thank post
                        //         ctx.call("db.post.find",{query:{publish_id:reqest_id,publish_type:"request"},fields:["publish_mention_users"]})
                        //         .then((res) => {
                        //             callback(null, res);
                        //         })
                        //         .catch((err) => {
                        //             console.log("Get contribution has some error",err);
                        //         })    
                        //     })
                        //     .catch((err) => {
                        //         console.log("Get request item has some error",err);
                        //     })  
                        // }
                        isSearchUserAdmin: function(callback) {
                            let { query:Query } = ctx.params;
                            let res_arr = [];
                            if(Query && Query.trim()!=""){
                                ctx.call("db.post_request_items.isSearchUserAdmin",ctx.params)
                                .then((req_Res) => {
                                    res_arr = req_Res;
                                    callback(null, res_arr);
                                })
                                .catch((err_isAdmin) => {
                                    console.log("== err == ", err_isAdmin)
                                    callback(null, res_arr);
                                });
                            }else{
                                callback(null, res_arr);
                            }
                        }
                    }, function(err,results) {
                        if (err) {
                            console.log('Global search is failed ==> ', err);
                            reject(err);
                        } else {
                            let { isSearchUserAdmin } = results;
                            let contributionData = results.contribution;
                            if(isSearchUserAdmin.length>0 && isSearchUserAdmin[0].user_id != ctx.params.user_id){
                                contributionData = contributionData.filter(x=>x.is_anonymous == false)
                            }
                            // let thankpostUserData = results.thankpost_users;
                            // if(thankpostUserData.length > 0) {
                            //     thankpostUserData = thankpostUserData[0].publish_mention_users;
                            // }
                            // for (var i = 0; i < contributionData.length; i++) {
                            //     let checkobj = objExists(contributionData[i].contribute_user_id,thankpostUserData,"user_id");
                            //     if(checkobj) {
                            //         contributionData[i].is_thank_post = true;    
                            //     } else {
                            //         contributionData[i].is_thank_post = false;    
                            //     }
                            // }
                            resolve({ code:200, message:'contributors list', data:contributionData})
                        }
                    });

                    //Function check array of object value check start
                    // function objExists(checkVal,checkArray,checkKey) {
                    //     if(checkArray && checkArray != '' && checkArray.length > 0) {
                    //         return checkArray.some(function(el) {
                    //             let tempCheckArray = el;
                    //             return tempCheckArray[checkKey] === checkVal;
                    //         }); 
                    //     } else {
                    //         return false;
                    //     }
                    // }
                    //Function check array of object value check end

                    // ctx.call("db.post_requests_item_contributions.contributionListOrCount",ctx.params)
                    // .then((doc)=>{
                    //     resolve({ code:200, message:'contributors list', data:doc})
                    // })
                    // .catch(e=>reject(e))
                })   
            }
        },
        getThankPostusers: {
            params:{
                request_id:"string"
            },
            handler(ctx){
                let {request_id} = ctx.params
                return new Promise(async(resolve, reject)=>{
                    ctx.call("db.post.find",{query:{publish_id:request_id,publish_type:"request"},fields:["publish_mention_users"]})
                    .then((userArray)=>{
                        if(userArray.length > 0) {
                            let getuserobjs = userArray[0].publish_mention_users;
                            let usersArray = [];
                            //seperate user ids from array object start
                            for (var i = 0; i < getuserobjs.length; i++) {
                                usersArray.push(getuserobjs[i].user_id)
                            }
                            //seperate user ids from array object end

                            //Get users details start
                            let get_user_obj = {
                                user_ids : usersArray
                            }
                            ctx.call("db.users.getBulkusers",get_user_obj)
                            .then((userData)=>{
                                resolve(userData); 
                            })
                            .catch(err => {
                                console.log(err)
                            })
                            //Get users details end
                        } else {
                            reject("No data found")
                        }
                    })
                    .catch(e=>reject(e))
                })
            }   
        },
        contributionlistByUser:{
            params:{
                request_item_id:"string",
                contribute_use_id:"string"
            },
            handler(ctx){
                return new Promise(async(resolve, reject)=>{
                    ctx.call("db.post_requests.contributionlistByUser",ctx.params)
                    .then(async(res) => {
                        for (var j = 0; j < res.length; j++) {
                            //Check contribute user is default(2) start
                            if(res[j].contribute_user_id == 2 && res[j].paid_user_name && res[j].paid_user_name != '') {
                                res[j].full_name = res[j].paid_user_name;
                            }
                            //Check contribute user is default(2) end
                        }
                        let checkAnonimous = valueExists(true,res);
                        if(checkAnonimous == true) {
                            await res.forEach(obj => obj.is_anonymous = true);
                        }
                        resolve(res);
                    })
                    .catch((err) => {
                        console.log("Get contribution has some error",err);
                        reject(err);
                    }) 

                    function valueExists(value,arr) {
                        return arr.some(function(el) {
                            return el.is_anonymous == value;
                        }); 
                    }   
                })
            }
        },
        contributionStatusUpdation:{
            params:{
                request_item_id:"string",
                contribute_user_id:"string"            
            }, 
            handler(ctx){
                return new Promise(async(resolve, reject)=>{
                    let {request_item_id,contribute_user_id,update_type,skip_thank_post,contribution_id,is_anonymous} = ctx.params;
                    if(!update_type) {
                        update_type = "acknowledge";
                    }
                    let updateObj = {}
                    if(update_type == "acknowledge") {
                        updateObj.is_acknowledge = true;
                        updateObj.payment_status = "success";
                    } else if(update_type == "post") {
                        updateObj.is_thank_post = true; 
                    }
                    if(skip_thank_post) {
                        updateObj.skip_thank_post = skip_thank_post;   
                    }
                    
                    if(contribution_id && contribution_id != '') {
                        updateObj.id = contribution_id; 
                        let data_is = await ctx.call("db.post_requests_item_contributions.find",{ query:{ id: contribution_id },fields:["is_active"] })
                        updateObj.payment_status = (data_is.length>0 && data_is[0].is_active) ? 'success':'pending';
                        ctx.call("db.post_requests_item_contributions.update",updateObj)
                        .then((updateRres) => {
                            console.log("Updated successfully");
                            resolve(updateRres);
                            return;
                        })
                        .catch((err) => {
                            console.log("Update contribution has some error",err);
                            reject(err);
                        })  
                    } else { 
                        let contributionUpdateObj = {
                            post_request_item_id:request_item_id,
                            contribute_user_id:contribute_user_id
                        }
                        if(is_anonymous && is_anonymous != '') {
                            contributionUpdateObj.is_anonymous = true    
                        } else {
                            contributionUpdateObj.is_anonymous = false    
                        }
                        ctx.call("db.post_requests_item_contributions.find",{query:contributionUpdateObj,fields:["id","is_active"]})
                        .then((contributRres) => {
                            if(contributRres.length > 0) {
                                for (var i = 0; i < contributRres.length; i++) {
                                    updateObj.id = contributRres[i].id;
                                    updateObj.payment_status = contributRres[i].is_active ? 'success':'pending';
                                    ctx.call("db.post_requests_item_contributions.update",updateObj)
                                    .then((updateRres) => {
                                        console.log("Updated successfully");
                                        resolve(updateRres);
                                        return;
                                    })
                                    .catch((err) => {
                                        console.log("Update contribution has some error",err);
                                        reject(err);
                                    }) 
                                }
                            } else {
                                console.log("There is no contributions");
                                return;
                            }   
                        })
                        .catch((err) => {
                            console.log("Get contribution has some error",err);
                            reject(err);
                        }) 
                    } 
                })
            }   
        },
        getContributeItemAmountSplit:{
            params:{
                item_id:"string"
            },
            handler(ctx){
                return new Promise(async(resolve, reject)=>{
                    ctx.call("db.post_request_items.contribute_item_amount_split",ctx.params)
                    .then((res) => {
                        resolve(res);
                    })
                    .catch((err) => {
                        console.log("Get contribution amount splitup has error",err);
                        reject(err);
                    })   
                })
            }
        },
        admin_user_selection:{
            params:{
                group_id:"string",
                user_id:"string"
            },
            handler(ctx){
                return new Promise(async(resolve, reject)=>{
                    _async.parallel({
                        admin_user_selection: (callback) => {
                            ctx.call("db.post_requests.admin_user_selection", ctx.params)
                            .then((res) => {
                                callback(null, res);
                            })
                            .catch((err) => {
                                callback(null, []);
                            })
                        },
                        other_users: (callback) => {
                            ctx.call("db.post_requests.admin_otheruser_selection", ctx.params)
                            .then((res) => {
                                callback(null, res);
                            })
                            .catch((err) => {
                                callback(null, []);
                            })
                        }
                    }, function (err, result) {
                        if(err) reject(err);
                        else{
                            let { admin_user_selection, other_users } = result;
                            const others_res = other_users.filter(({phone:a}) => !admin_user_selection.some(({phone:b}) => a === b ));
                            let finalarray = admin_user_selection.concat(others_res);
                            resolve({ code:200, data:finalarray });
                        }
                    }); 
                })
            }
        },
        add_member_contribution_ByAdmin:{
            params:{
                group_id:"string",
                admin_id:"string",
                post_request_item_id:"string", 
                contribute_item_quantity:"string"
            },
            handler(ctx){
                return new Promise(async(resolve, reject)=>{
                    let {group_id,admin_id,post_request_item_id,contribute_item_quantity,intex,phone_no} = ctx.params
                    ctx.params.is_acknowledge = true;
                    ctx.params.payment_status = "success";
                    ctx.params.group_id = parseInt(ctx.params.group_id);
                    ctx.params.admin_id = parseInt(ctx.params.admin_id);
                    ctx.params.post_request_item_id = parseInt(ctx.params.post_request_item_id);
                    ctx.params.contribute_item_quantity = parseInt(ctx.params.contribute_item_quantity);

                    //Check phone number exist in app start
                    async function checkphoneExist(phoneNo){
                        // if(contribute_user_id && contribute_user_id != "") {
                        //     console.log("User already exist");
                        // }
                        if(phone_no && phone_no != '') {
                            await ctx.call("db.users.find", { query: { phone: phoneNo },fields: ["id","full_name"] })
                            .then(userData => {
                                if(userData.length > 0) {
                                    ctx.params.contribute_user_id = userData[0].id;   
                                    ctx.params.full_name = userData[0].full_name;  
                                } else {
                                    //Set default user id to contribute ID
                                    ctx.params.contribute_user_id = 2;   
                                }
                            })
                            .catch(err => {
                                console.log(err);
                            })   
                        }      
                    }
                    await checkphoneExist(phone_no);
                    //Check phone number exist in app end

                    ctx.call("db.post_requests_item_contributions.create",ctx.params)
                    .then(async(res) => {
                        resolve(res);
                        //Success Socket start
                        let aceptTopic = 'admin_contribution_' + admin_id + group_id + post_request_item_id;
                        let acceptSocket_data = {
                            type: 'admin_contribution_',
                            contribute_item_quantity:contribute_item_quantity,
                            intex:intex,
                            status:"Success"
                        };
                        ctx.call('socket_service.sendMessage', { topic: aceptTopic, data: acceptSocket_data })
                        .then((socketRes) => {
                            console.log("Successfully pushed data to socket",socketRes);
                        })
                        .catch((err) => {
                            console.log("push data to socket has error",err);
                        })   
                        //Success Socket end
                        let req_id = await ctx.call("db.post_request_items.find",{ query:{ id:ctx.params.post_request_item_id }, fields:["post_request_id"] })
                        ctx.params.post_request_id = req_id.length>0 ? req_id[0].post_request_id : '';
                        ctx.params.item_id = ctx.params.post_request_item_id;
                        ctx.params = { ...ctx.params, ntype:'request', notifyFor:'admins', user_id: parseInt(ctx.params.admin_id), actionby:"contribute_through_admin" }
                        this.appNotify(ctx);
                    })
                    .catch((err) => {
                        console.log("add contribution has error",err);
                        reject(err);
                        //Failed Socket start
                        let aceptTopic = 'admin_contribution_' + admin_id + group_id + post_request_item_id;
                        let acceptSocket_data = {
                            type: 'admin_contribution_',
                            contribute_item_quantity:contribute_item_quantity,
                            intex:intex,
                            status:"Failed"
                        };
                        ctx.call('socket_service.sendMessage', { topic: aceptTopic, data: acceptSocket_data })
                        .then((socketRes) => {
                            console.log("Successfully pushed data to socket",socketRes);
                        })
                        .catch((err2) => {
                            console.log("push data to socket has error",err2);
                        })   
                        //Failed Socket end
                    })   
                })      
            }
        },
        get_contribution_list_Byadmin:{
            params:{
                group_id:"string",
                admin_id:"string"
            },
            handler(ctx){
                return new Promise(async(resolve, reject)=>{
                    ctx.call("db.post_requests_item_contributions.get_contribution_list_Byadmin",ctx.params)
                    .then((contributlist) => {
                        resolve(contributlist);
                    })  
                    .catch((err) => {
                        console.log("get contribution has error",err);
                        reject(err);
                    })   
                })
            }   
        },

        get_notification_needs_data:{
            params:{},
            handler(ctx){
                return new Promise(async(resolve, reject)=>{
                    ctx.call("db.post_requests.get_notification_needs_data",ctx.params)
                    .then((res) => {
                        resolve(res);
                    })  
                    .catch((err) => {
                        console.log("get contribution has error",err);
                        reject(err);
                    })   
                })
            }   
        }

    },
    events: {},
    methods: {
        /** craete group maps of request item */
        createGm(ctx,params){
            return ctx.call("db.post_requests_group_map.create",params);
        },

        /** find he creator of need */
        isCreator(ctx,params){
            return ctx.call("db.post_requests.find",params);
        },

        /** send notifications */
        appNotify(ctx) {
            let { to_group_id, to_user_id, user_id, ntype, item_id, post_request_id, notifyFor } = ctx.params;
            let _this = this;
            _async.parallel({
                tousers: function (callback) { // get memebers inside a group
                    if (to_group_id) {
                        ctx.broker.call('db.group_map.eventInviteGroup', { group_id: to_group_id, user_id:user_id, reqFrom:"post_request" })
                        .then((res) => {
                            callback(null, res)
                        })
                        .catch((err) => {
                            console.log("to_group_id Search Error", err);
                        })
                    }
                    if (to_user_id) {
                        ctx.broker.call('db.users.find', { query: { id: to_user_id }, fields: ["id", "full_name", "propic"] })
                        .then((res) => {
                            callback(null, res);
                        })
                        .catch((err) => {
                            console.log("User Search Error", err);
                        })
                    }
                    if(notifyFor && notifyFor == 'admins'){
                        ctx.broker.call('db.group_map.find', { query: { group_id: ctx.params.group_id, is_blocked:false, is_removed:false, type:'admin' }, fields: ["user_id"] })
                        .then((res) => {
                            callback(null, res);
                        })
                        .catch((err) => {
                            callback(null, []);
                            console.log("User Search Error", err);
                        })
                    }
                },
                
                fromUser: (callback) => {
                    ctx.call("db.users.find", { query: { id: user_id }, fields: ["id", "full_name", "propic"] })
                    .then(res => {
                        callback(null, res);
                    }).catch(err => {
                        console.log("error happened -------- familyMembers", err);
                    })
                },
                item_details:(callback) => {
                    if(item_id){
                        ctx.call("db.post_request_items.find", { query: { id: item_id }, fields: ["id", "request_item_title", "post_request_id"] })
                    .then(res => {
                        callback(null, res);
                    }).catch(err => {
                        console.log("error happened -------- familyMembers", err);
                    })

                    }else{
                        callback(null,[])
                    }

                }
            }, function (err, results) {
                let { tousers, fromUser, item_details } = results;
                
                tousers.forEach(async elem => {
                    let sendObj = {
                        from_id: fromUser.length > 0 ? fromUser[0].id : '',
                        type: 'home',
                        propic: fromUser.length > 0 ? fromUser[0].propic : '',
                        to_id: elem.user_id ? parseInt(elem.user_id) : parseInt(elem.id),
                        sub_type: '',
                        category: ntype,
                        message_title: 'familheey',
                        type_id: '',
                    }
                    switch (ntype) {
                        case 'create_need':
                        case 'update_need':
                            let typeis = (ntype == 'create_need') ? 'posted' : 'updated'
                            sendObj.message  = `${fromUser[0].full_name} have ${typeis} a need in ${elem.group_name}`
                            sendObj.sub_type = 'requestFeed';
                            sendObj.type_id  = parseInt(post_request_id);
                            sendObj.link_to  = parseInt(post_request_id);
                        break;

                        case 'contribution_create':
                            let request_item_title =  item_details.length>0 ? item_details[0].request_item_title : '';
                            //let request_item_id =  item_details.length>0 ? item_details[0].id : '';
                            sendObj.message  = `${fromUser[0].full_name} offered to help against ${request_item_title} `
                            sendObj.sub_type = 'requestFeed';
                            sendObj.type_id  = parseInt(post_request_id);
                            sendObj.link_to  = parseInt(post_request_id);
                        break;

                        case 'request':
                            let rq_item_title =  item_details.length>0 ? item_details[0].request_item_title : '';
                            let rq_id =  item_details.length>0 ? item_details[0].post_request_id : '';
                            sendObj.message  = `${fromUser[0].full_name} offered to help against ${rq_item_title} `
                            sendObj.sub_type = 'requestFeed';
                            sendObj.type = 'home';
                            sendObj.type_id  = parseInt(rq_id);
                            sendObj.link_to  = parseInt(rq_id);
                            if(ctx.params.actionby && ctx.params.actionby == "contribute_through_admin") sendObj.category  = 'contribution_create';
                        break;
                    
                        default:
                        break;
                    }
                    if ( parseInt(fromUser[0].id)  != parseInt(sendObj.to_id) ) {
                        ctx.broker.call("notification.sendAppNotification", sendObj);
                        let _user_notify = await ctx.call("db.users.find", { query: { id: sendObj.to_id }, fields: ["notification"] })
                        if (_user_notify.length > 0 && _user_notify[0].notification) {
                            _this.pushNotify(ctx, sendObj);
                        }
                    }
                });
               
            });
        },
        pushNotify(ctx, params) {
            ctx.broker.call("userops.getregisterToken", { user_id: params.to_id, is_active: true })
                .then((_devices) => {
                    _devices.data.forEach(elem => {
                        params = {
                            ...params,
                            token: elem.device_token,
                            device_type: elem.device_type.toLowerCase(),
                            title: params.message_title,
                            body: this.removeTags(params.message),
                            sub_type: params.sub_type ? params.sub_type : ''
                        }
                        ctx.broker.call("notification.sendPushNotification", params)
                    })
                })
                .catch(e => e)
        },
        removeTags(str) {
            if ((str === null) || (str === '')) return '';
            else return str.toString().replace(/(<([^>]+)>)/ig, '');
        },
        
        check_duplication(checkObj){
            let contributionArray = [];
            for (let i = 0; i < checkObj.length; i++) {
                let flags = contributionArray.some(checkObjs);
                if(!flags) {
                    contributionArray.push(checkObj[i]);  
                }
                function checkObjs(objs) {
                    if(objs.contribute_user_id == 2) {
                        return (objs.paid_user_name == checkObj[i].paid_user_name && objs.is_anonymous == checkObj[i].is_anonymous);
                    } else {
                        return (objs.contribute_user_id == checkObj[i].contribute_user_id && objs.is_anonymous == checkObj[i].is_anonymous);
                    }
                }
            }
            return contributionArray;
        },
        
        findItemContribution(ctx) {
            return new Promise((resolve, reject) => {
                let { reqObj } = ctx.params;
                let res_Arr = [];
                ctx.call("db.post_requests_item_contributions.find", { query: reqObj, fields: ["id"] })
                .then((checkRes) => {
                    res_Arr = checkRes;
                    resolve(res_Arr);
                })
                .catch((err) => {
                    console.log("Check if any contribution with acknowledge has some error", err);
                    reject(res_Arr);
                })
            })
        },

        contributionAnnounimousCount(ctx) {
            let { contrubutionCountObj } = ctx.params;
            let resArr = [];
            return new Promise((resolve, reject) => {
                ctx.call("db.post_requests_item_contributions.contributionAnonimousBasedCounts", contrubutionCountObj)
                .then((anonymousResp) => {
                    resArr = anonymousResp; 
                    resolve(resArr);
                })
                .catch((err) => {
                    console.log("Check if any contribution with acknowledge has some error", err);
                    reject(resArr);
                })
            })
        }
    },
    created() { },
    started() { },
    stopped() { }
}
