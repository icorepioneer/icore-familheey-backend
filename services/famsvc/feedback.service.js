"use strict";
const moment = require('moment');
module.exports = {
    name: "feedback",
    settings: {},
    actions: {
        /**
         * save data to calender
         */
        receiveFeedback:{
            params:{
                description:"string"
            },
            handler(ctx){
                return new Promise((resolve, reject)=>{
                    if(ctx.meta && ctx.meta.file) {
                        ctx.meta.file.forEach(element => {
                            ctx.params[element.fieldname] = element.filename;
                            this.calls3(ctx, element)
                        });
                    }
                    ctx.call("db.user_feedbacks.create", { ...ctx.params }).then(res => {
                        ctx.call("db.users.find", { query: { id: res.user } }).then(userRes => {
                            // Email template start
                            var version = res.version ? res.version : '';
                            var imgurl = `${process.env.AWS_S3_BASE_URL}image/${res.image}`; 
                            var user_name = userRes[0].full_name ? userRes[0].full_name : 'Un known';
                            var html = `
                            <html>
                                <body>
                                    <div>
                                        <table border="1" cellspacing="0" border-collapse="collapse" style="bordercolor=#00F,font-size:12px;">
                                            <tr>
                                                <td colspan="2">
                                                    <img src="${imgurl}"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Description</td>
                                                <td> ${res.description}</td>
                                            </tr>
                                        
                                            <tr>
                                                <td>Created by</td>
                                                <td> ${user_name} </td>
                                            </tr>
                                            <tr>
                                                <td>Created id</td>
                                                <td> ${res.user} </td>
                                            </tr>
                                            <tr>
                                                <td>Device</td>
                                                <td> ${res.device}</td>
                                            </tr>
                                            <tr>
                                                <td>Type</td>
                                                <td> ${res.type}</td>
                                            </tr>
                                            <tr>
                                                <td>version</td>
                                                <td> ${version}</td>
                                            </tr>
                                            <tr>
                                                <td>Date</td>
                                                <td>
                                                ${ res.createdAt }
                                                </td>
                                            </tr>
                                        </table>
                                        
                                    </div>
                                </body>
                            </html>`;
                            // Email template end
                            // Send email function start
                            ctx.broker.call("notification.sendEmail", {
                                toAddresses: ['naveen.kh@iinerds.com'],
                                // ccAddress: ['nissanth.s@iinerds.com,angelachinweze@icloud.com,ken.chinweze@gmail.com,ajithkumar15@hotmail.com,anshad@innovationincubator.com'],
                                ccAddress: ['nissanth.s@iinerds.com'],
                                message: html,
                                subject: "Familheey Feedback"
                            })
                            .then((res2) => {
                                console.log("Feedback mail send successfully");
                            })
                            .catch((err) => {
                                console.log("Feedback mail send Error", err);
                            })
                            // Send email function end
                            resolve(res);
                        }).catch(err => {
                            reject(err)
                        })

                    }).catch(err => {
                        reject(err)
                    })
                        
                })   
            }
        }
    },
    events: {},
    methods: { 
        calls3(ctx, params) {
            return new Promise((resolve, reject) => {
                ctx.call('s3upload.fileUpload', params)
                    .then((res) => {
                        resolve("SUCCESS");
                    })
                    .catch((err) => {
                        reject("ERROR");
                    })
            })
        }
     },
    created() { },
    started() { },
    stopped() { }
}
