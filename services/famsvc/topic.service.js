"use strict";
var _async = require('async');
var moment = require('moment');

module.exports = {
    name: "topic",
    settings: {},
    actions: {
        createTopic:{
            params:{
                created_by: "string"
            },
            handler(ctx) {
                let { to_user_id, created_by } = ctx.params;
                return new Promise((resolve, reject) => {
                    ctx.call("db.topics.create",ctx.params)
                    .then(async(res)=>{
                        // create topic map entry start
                        ctx.params.topic_id = res.id.toString();
                        ctx.params.from_to_add = 'topic_create';
                        await ctx.call("topic.addUsersToTopic",ctx.params)
                        .then((res2)=>{
                            console.log('Created topic map successfully');
                        })
                        .catch((err) => {
                            console.log(err);
                            reject('Find map creation has some error:',err);
                        });
                        // create topic map entry end
                        res.topic_id = res.id;
                        resolve({ code:200, message:"created successfully", data:res});

                        // Notification start
                        var notificationObj = ctx;
                        notificationObj.params.to_user_id = to_user_id;
                        notificationObj.params.user_id = created_by;
                        notificationObj.params.topic_id = parseInt(res.id);
                        notificationObj.params.ntype = 'create_topic';
                        this.appNotify(notificationObj);
                        // Notification end

                        //Update date to sort-dates table start
                        let sortdateObj = {
                            type:'topic',
                            type_id:parseInt(res.id)
                        };
                        ctx.broker.call('common_methods.sort_dates_update', sortdateObj);
                        //Update date to sort-dates table end

                        // Track user activity start
                        let trackerData = {
                            user_id: parseInt(created_by),
                            type: 'topics',
                            type_id: parseInt(res.id),
                            activity: ('created topic')
                        };
                        ctx.broker.call('user_activities.trackUserActivities', trackerData);
                        // Track user activity end
                    })
                    .catch((err) => {
                        console.log(err);
                        reject(err);
                    });
                });
            }
        },
        addUsersToTopic:{
            params:{
                topic_id: "string"
            },
            handler(ctx) {
                return new Promise(async(resolve, reject) => {
                    let finTopicMapObject = [];
                    let {created_by,topic_id,to_user_id} = ctx.params;
                    // Create map entry object for created user
                    if(ctx.params.from_to_add == 'topic_create') {
                        finTopicMapObject.push(
                            {
                                user_id:created_by,
                                topic_id:topic_id,
                                created_by:created_by
                            }   
                        );
                    }
                    
                    for (var n = 0 ; n < to_user_id.length ; n++) {
                        ctx.params.reqToUserId = to_user_id[n];
                        // Check if entry is duplicate
                        //await topicMapFn(to_user_id[n]);
                        let udata =  await this.topicMapFn(ctx);
                        //console.log("== = udata = = = ", udata)
                        finTopicMapObject.push(udata)
                    }


                    // create topic map entry
                    ctx.call("db.topic_map.bulkCreate",finTopicMapObject)
                    .then((res)=>{
                        resolve({ code:200, message:"added successfully", data:res});

                        // Notification start
                        if(!ctx.params.from_to_add) {
                            var notificationObj = ctx;
                            notificationObj.params.user_id = created_by;
                            notificationObj.params.topic_id = parseInt(topic_id);
                            
                            if(to_user_id && to_user_id.length > 0) {
                                notificationObj.params.to_user_id = to_user_id;
                                notificationObj.params.ntype = 'create_topic';
                                this.appNotify(notificationObj);    
                            }
                            // get all users except newely added and updated user
                            var notNotifyUsers = to_user_id;
                            notNotifyUsers.push(created_by);
                            var getNotifyusers = {
                                topic_id:topic_id,
                                not_in_users:notNotifyUsers
                            }
                            ctx.call("db.topic_map.get_topic_users",getNotifyusers)
                            .then((res4)=>{
                                if(res4.length > 0){
                                    var notify_to_user = res4.map(function (obj) {
                                        return obj.user_id;
                                    });
                                    notificationObj.params.to_user_id = notify_to_user;
                                    notificationObj.params.ntype = 'add_users_to_topic';
                                    this.appNotify(notificationObj);
                                }
                            })
                            .catch((err) => {
                                console.log(err);
                                reject('get user for notification has some error:',err);
                            });
                        }
                        // Notification end

                        // Track user activity start
                        let trackerData = {
                            user_id: parseInt(created_by),
                            type: 'topics',
                            type_id: parseInt(topic_id),
                            activity: ('added users to topic')
                        };
                        ctx.broker.call('user_activities.trackUserActivities', trackerData);
                        // Track user activity end
                    })
                    .catch((err) => {
                        console.log(err);
                        reject('topic map creation has some error:',err);
                    });
                    
                })
            }    
        },
        topiclist:{
            params:{
                user_id: "string"
            },
            handler: (ctx) => {
                return new Promise((resolve, reject) => {
                    ctx.call("db.topics.topic_list",ctx.params)
                    .then((res)=>{
                        resolve({ code:200,data:res});
                    })
                    .catch((err) => {
                        console.log(err);
                        reject('topic list has some error:',err);
                    });
                })
            }
        },
        deleteTopic:{
            params:{
                topic_id:"string",
                user_id:"string"
            },
            handler(ctx){
                return new Promise((resolve, reject) => {
                    ctx.call("db.topics.update",{ id: ctx.params.topic_id, is_active:false })
                    .then((res)=>{
                        resolve({ code:200, message:'deleted successfully'});
                        // Track user activity start
                        let trackerData = {
                            user_id: parseInt(ctx.params.user_id),
                            type: 'topic',
                            type_id: parseInt(ctx.params.topic_id),
                            activity: ('deleted topic')
                        };
                        ctx.broker.call('user_activities.trackUserActivities', trackerData);
                        // Track user activity end
                    })
                    .catch((err) => {
                        console.log(err);
                        reject('topic delete has some error:',err);
                    });
                })   
            }
        },
        removeUserFromTopic:{
            params:{
                topic_id:"string",
                user_id:"string"
            },
            handler(ctx){
                return new Promise((resolve, reject) => {
                    ctx.call("db.topic_map.remove_user_topic",ctx.params)
                    .then((res)=>{
                        resolve({ code:200, message:'removed successfully'}); 
                        // Track user activity start
                        let trackerData = {
                            user_id: parseInt(ctx.params.user_id),
                            type: 'topic',
                            type_id: parseInt(ctx.params.topic_id),
                            activity: ('removed users from topic')
                        };
                        ctx.broker.call('user_activities.trackUserActivities', trackerData);
                        // Track user activity end  
                    })
                    .catch((err) => {
                        console.log(err);
                        reject('remove user frm topic has some error:',err);
                    });
                })
            }   
        },
        updateTopic:{
            params:{
                topic_id:"string",
                updated_by:"string"
            },
            handler(ctx){
                let{updated_by,topic_id,to_user_id} = ctx.params; 
                to_user_id.push(parseInt(updated_by));
                return new Promise(async(resolve, reject) => {
                    if(to_user_id.length > 0) {
                        var existing_users = [];
                        var add_users = [];
                        var delete_users = [];
                        // get current users from topic
                        await ctx.broker.call('db.topic_map.find', { query: { topic_id: topic_id,is_active:true }, fields: ["user_id"] })
                        .then((res) => {
                            if(res.length > 0){
                                existing_users = res.map(function (obj) {
                                    return obj.user_id;
                                });
                                add_users = to_user_id.filter(x => !existing_users.includes(x));
                                delete_users = existing_users.filter(x => !to_user_id.includes(x));
                            } else {
                                add_users = to_user_id;    
                            }
                        })
                        .catch((err) => {
                            console.log("User Search Error", err);
                        }) 
                    }
                    
                    // Update topic table
                    ctx.params.id = topic_id
                    ctx.call("db.topics.update",ctx.params)
                    .then((res)=>{
                        // Deleted unwanted users
                        if(delete_users.length > 0) {
                            var deleteObj = {
                                topic_id:topic_id,
                                user_id:updated_by,
                                delete_user:delete_users
                            }
                            ctx.call("topic.removeUserFromTopic",deleteObj)
                            .then((res2)=>{
                                console.log('removed users from topic successfully');
                            })
                            .catch((err) => {
                                console.log(err);
                                reject('removed users from topic has some error:',err);
                            });
                        }
                        // Added new users to topic
                        if(add_users.length > 0) {
                            var userAddObj = {
                                topic_id:topic_id,
                                created_by:updated_by,
                                to_user_id:add_users
                            }
                            ctx.call("topic.addUsersToTopic",userAddObj)
                            .then((res3)=>{
                                console.log('added users to topic successfully');
                            })
                            .catch((err) => {
                                console.log(err);
                                reject('added users to topic has some error:',err);
                            });
                        }

                        resolve({ code:200, message:'Topic updated successfully'});

                        // Track user activity start
                        let trackerData = {
                            user_id: parseInt(updated_by),
                            type: 'topic',
                            type_id: parseInt(topic_id),
                            activity: ('updated topic')
                        };
                        ctx.broker.call('user_activities.trackUserActivities', trackerData);
                        // Track user activity end
                    })
                    .catch((err) => {
                        console.log(err);
                        reject('topic update has some error:',err);
                    });
                })
            }   
        },
        topicUsersList:{
            params:{
                user_id:"string"
            }, 
            handler(ctx){
                let {topic_id} = ctx.params;
                return new Promise(async(resolve, reject) => {
                    _async.parallel({
                        topicUsers : function(callback) {
                            if(topic_id && topic_id != '') {
                                ctx.broker.call('db.topic_map.get_topic_users_list', ctx.params)
                                .then((res) => {
                                    callback(null, res);
                                })
                                .catch((err) => {
                                    console.log("Topic users getting Error",err);
                                })
                            } else {
                                callback(null, []);
                            }
                        },
                        connectionUsers : function(callback) {
                            ctx.broker.call('db.users.getConnectionUsers', ctx.params)
                            .then((res) => {
                                callback(null, res);
                            })
                            .catch((err) => {
                                console.log("Connection users getting Error",err);
                            }) 
                        },
                        nonConnectionUsers : function(callback) {
                            ctx.broker.call('db.users.getUsersList', ctx.params)
                            .then((res) => {
                                callback(null, res);
                            })
                            .catch((err) => {
                                console.log("nonConnection users getting Error",err);
                            }) 
                        },
                    }, async function(err,results) {
                        if (err) {
                            console.log('Global search is failed ==> ', err);
                            reject(err);
                        } else {
                            //filter duplicate data with topicuser data
                            results.connectionUsers = await resultFilter(results.connectionUsers,results.topicUsers,null);
                            //filter duplicate data with topicuser data and connectionUsers data
                            results.nonConnectionUsers = await resultFilter(results.nonConnectionUsers,results.topicUsers,results.connectionUsers);
                            resolve(results);
                            //filter function start
                            function resultFilter(array1,array2,array3){
                                var filterResult;
                                if(array3 == null) {
                                    filterResult = array1.filter(({user_id}) => !array2.some(x => x.user_id == user_id));
                                } else {
                                    filterResult = array1.filter(({user_id}) => !array2.some(x => x.user_id == user_id) && !array3.some(x => x.user_id == user_id));   
                                }
                                return filterResult;
                            }
                            //filter function end
                        }
                    });
                })    
            }  
        },
        topicdetail:{
            params:{
                user_id: "string",
                topic_id: "string"
            },
            handler: (ctx) => {
                return new Promise((resolve, reject) => {
                    let{user_id,topic_id} = ctx.params;
                    //check user permission in topic
                    ctx.call("db.topic_map.find", { query: { user_id:user_id, topic_id:topic_id, is_active:true }, fields: ["id","is_accept"] })
                    .then((perRes)=>{
                        if(perRes.length > 0) {
                            //Get topic detail
                            ctx.call("db.topics.topic_detail",ctx.params)
                            .then(async(res)=>{
                                res[0].is_accept = perRes[0].is_accept;
                                // get topic users list with details start
                                await ctx.call("db.topic_map.get_topic_users_list",ctx.params)
                                .then((userRes)=>{
                                    res[0].to_users = userRes; 
                                })
                                .catch((err) => {
                                    console.log(err);
                                    reject('get topic users has some error:',err);
                                });
                                // get topic users list with details end
                                resolve({ code:200,data:res});
                            })
                            .catch((err) => {
                                console.log(err);
                                reject('topic detail has some error:',err);
                            });
                        } else {
                           resolve({ code:200,data:"User has no permission in this topic"}); 
                        }
                    })
                    .catch((err) => {
                        console.log(err);
                        reject('check user permission in topic has some error:',err);
                    });
                })
            }
        },
        acceptUserTopic:{
            params:{
                user_id: "string",
                topic_id: "string"
            },
            handler(ctx) {
                return new Promise((resolve, reject) => {
                    let{user_id,topic_id} = ctx.params
                    ctx.broker.call('db.topic_map.find', {query: {user_id:user_id, topic_id:topic_id}, fields: ["id","created_by"] })
                    .then((res) => {
                        if(res && res.length > 0) {
                            //Update topic map start
                            let topUpdateObj = {
                                id:res[0].id,
                                is_accept:true 
                            }
                            ctx.call("db.topic_map.update",topUpdateObj)
                            .then((res2)=>{
                                resolve({ code:200,data:res2});
                                ctx.call("db.users.find", {query: {id: user_id}, fields: ["propic", "full_name"] })
                                .then((userRes)=>{
                                    // Socket start
                                    let aceptTopic = 'topic_channel_' + topic_id;
                                    let acceptSocket_data = {
                                        type: 'topic_accept',
                                        message: 'New user accepted this topic',
                                        topic_id:topic_id,
                                        user_id:user_id,
                                        user_name:userRes[0].full_name,
                                        propic:userRes[0].propic
                                    };
                                    ctx.call('socket_service.sendMessage', { topic: aceptTopic, data: acceptSocket_data });
                                    // Socket end
                                })
                                .catch((err) => {
                                    reject('find user has some error:',err);
                                });
                                // Notification start
                                if(res2.created_by) {
                                    var notificationObj = ctx;
                                    notificationObj.params.to_user_id = res2.created_by;
                                    notificationObj.params.user_id = user_id;
                                    notificationObj.params.topic_id = parseInt(topic_id);
                                    notificationObj.params.ntype = 'accept_topic';
                                    this.appNotify(notificationObj);
                                }
                                // Notification end
                                // Track user activity start
                                let trackerData = {
                                    user_id: parseInt(ctx.params.user_id),
                                    type: 'topic',
                                    type_id: parseInt(ctx.params.topic_id),
                                    activity: ('Accepted user topic conversation')
                                };
                                ctx.broker.call('user_activities.trackUserActivities', trackerData);
                                // Track user activity end 
                            })
                            .catch((err) => {
                                console.log(err);
                                reject('topic accept has some error:',err);
                            });
                            //Update topic map end
                        } else {
                            resolve({ code:500,data:"User id or Topic id is not exist"});  
                        }
                    })
                    .catch((err) => {
                        console.log("get topic map Error", err);
                    })
                })
            }
        },
        rejectUserTopic:{
            params:{
                user_id: "string",
                topic_id: "string"
            },
            handler: (ctx) => {
                let{user_id,topic_id} = ctx.params;
                return new Promise((resolve, reject) => {
                    // remove user from topic start
                    var deleteObj = {
                        topic_id:topic_id,
                        user_id:user_id,
                        delete_user:user_id
                    }
                    ctx.call("topic.removeUserFromTopic",deleteObj)
                    .then((res)=>{
                        resolve({ code:200,data:'Successfully rejected user'});
                        // Track user activity start
                        let trackerData = {
                            user_id: parseInt(ctx.params.user_id),
                            type: 'topic',
                            type_id: parseInt(ctx.params.topic_id),
                            activity: ('rejected user topic conversation')
                        };
                        ctx.broker.call('user_activities.trackUserActivities', trackerData);
                        // Track user activity end 
                    })
                    .catch((err) => {
                        reject('removed users from topic has some error:',err);
                    });
                    // remove user from topic start
                })
            }
        },
        commonTopicListByUsers:{
            params:{
                login_user:"string",
                second_user:"string"
            },
            handler(ctx){
                return new Promise((resolve, reject) => {
                    ctx.call("db.topics.commonTopicListByUsers",ctx.params)
                    .then((res)=>{
                        resolve({ code:200,data:res});
                    })
                    .catch((err) => {
                        console.log(err);
                        reject('topic list has some error:',err);
                    });
                })
            }
        }
    },
    events: {},
    methods: {
        appNotify(ctx) {
            let { to_user_id, user_id, ntype, topic_id } = ctx.params;
            let _this = this;
            _async.parallel({
                tousers: function (callback) {
                    if (to_user_id) {
                        ctx.broker.call('db.users.find', { query: { id: to_user_id }, fields: ["id", "full_name", "propic"] })
                        .then((res) => {
                            callback(null, res);
                        })
                        .catch((err) => {
                            console.log("User Search Error", err);
                        })
                    }
                },
                fromUser: (callback) => {
                    ctx.call("db.users.find", { query: { id: user_id }, fields: ["id", "full_name", "propic"] })
                    .then(res => {
                        callback(null, res);
                    }).catch(err => {
                        console.log("error happened -------- familyMembers", err);
                    })
                },
                topic_details:(callback) => {
                    if(topic_id){
                        ctx.call("db.topics.find", { query: { id: topic_id }, fields: ["id", "title"] })
                    .then(res => {
                        callback(null, res);
                    }).catch(err => {
                        console.log("error happened -------- familyMembers", err);
                    })

                    }else{
                        callback(null,[])
                    }

                }
            },
            function (err, results) {
                let { tousers, fromUser  } = results;
                tousers.forEach(async elem => {
                    let sendObj = {
                        from_id: fromUser.length > 0 ? fromUser[0].id : '',
                        type: 'topic',
                        propic: fromUser.length > 0 ? fromUser[0].propic : '',
                        to_id: elem.user_id ? parseInt(elem.user_id) : parseInt(elem.id),
                        sub_type: '',
                        category: ntype,
                        message_title: 'familheey',
                        type_id: '',
                    }
                    switch (ntype) {
                        case 'create_topic':
                            sendObj.message  = `${fromUser[0].full_name} started a conversation with you`
                            sendObj.sub_type = 'topic';
                            sendObj.type_id  = parseInt(topic_id);
                            sendObj.link_to  = parseInt(topic_id);
                        break;
                        case 'add_users_to_topic':
                            sendObj.message  = `${fromUser[0].full_name} added a new user to conversation`
                            sendObj.sub_type = 'topic';
                            sendObj.type_id  = parseInt(topic_id);
                            sendObj.link_to  = parseInt(topic_id);
                        break;
                        case 'accept_topic':
                            sendObj.message  = `${fromUser[0].full_name} accepted your request, now you can continue your conversation`
                            sendObj.sub_type = 'topic_chat';
                            sendObj.type_id  = parseInt(topic_id);
                            sendObj.link_to  = parseInt(topic_id);
                        break;

                        default:
                        break;
                    }
                    if (fromUser[0].id != elem.user_id) {
                        console.log(sendObj.to_id)
                        ctx.broker.call("notification.sendAppNotification", sendObj);
                        let _user_notify = await ctx.call("db.users.find", { query: { id: sendObj.to_id }, fields: ["notification"] })
                        if (_user_notify.length > 0 && _user_notify[0].notification) {
                            _this.pushNotify(ctx, sendObj);
                        }
                    }
                })
            })
        },
        pushNotify(ctx, params) {
            ctx.broker.call("userops.getregisterToken", { user_id: params.to_id, is_active: true })
            .then((_devices) => {
                _devices.data.forEach(elem => {
                    params = {
                        ...params,
                        token: elem.device_token,
                        device_type: elem.device_type.toLowerCase(),
                        title: params.message_title,
                        body: this.removeTags(params.message),
                        sub_type: params.sub_type ? params.sub_type : ''
                    }
                    ctx.broker.call("notification.sendPushNotification", params)
                })
            })
            .catch(e => e)
        },
        removeTags(str) {
            if ((str === null) || (str === '')) return '';
            else return str.toString().replace(/(<([^>]+)>)/ig, '');
        },

        /**
         * 
         * to map data user with topic
         */
        topicMapFn(ctx) {
            let { topic_id, created_by, reqToUserId } = ctx.params;
            let checkObj = {};
            return new Promise((resolve, reject) => {
                ctx.call("db.topic_map.find", { query: { user_id: reqToUserId, topic_id: topic_id, is_active: true }, fields: ["id"] })
                .then(async (res) => {
                    if (res.length > 0) {
                        console.log('Already exist', reqToUserId);
                    } else {
                        // Check both users are connected
                        checkObj = {
                            user_id: reqToUserId,
                            topic_id: topic_id,
                            login_user: created_by,
                            created_by: created_by
                        }
                        await ctx.call("db.group_map.checkUsersConnected", checkObj)
                            .then((res3) => {
                                if (res3.length > 0) {
                                    console.log('Both users are connected');
                                } else {
                                    console.log('Both users are not connected');
                                    checkObj.is_accept = false;
                                }
                            })
                            .catch((err) => {
                                console.log('check user connection has some error:', err);
                            });

                        //resolve(checkObj)

                        //finTopicMapObject.push(checkObj);
                    }
                    resolve(checkObj)
                })
                .catch((err) => {
                    console.log(err);
                    reject(checkObj);
                });
            })
        }
    },
    created() { },
    started() { },
    stopped() { }
}
