"use strict";


let AWS = require('aws-sdk');
let fs = require('fs');
let zlib = require('zlib');
const cmd = require('node-cmd');
let s3Stream = require('s3-upload-stream')(new AWS.S3({
    accessKeyId: process.env.AWS_ACCESSKEY,
    secretAccessKey: process.env.AWS_SECRETKEY
}));

const S3 = new AWS.S3({
    accessKeyId: process.env.AWS_ACCESSKEY,
    secretAccessKey: process.env.AWS_SECRETKEY,
    region: 'us-east-1'
});


module.exports = {
    name: "s3upload",
    settings: {},
    actions: {
        generateUrl:{
            params: {
                Bucket: "string",
                Key:"string",
                Expires: "number", 
                ContentType: "string"
            },
            handler(ctx) {
                return new Promise((resolve, reject) => {
                    S3.getSignedUrl('putObject', ctx.params, function(err, url) {
                        if (err) {
                            console.log(err);
                            return resolve({error:1});
                        } else {
                            return resolve({signedUrl:url,path:ctx.params.Key,error:0});
                        }
                    });
                });
            }
        },
        fileUpload: {
            params: {},
            handler(ctx) {
                return new Promise((resolve, reject) => {
                    if (ctx.params) {
                        let { filename, fieldname, path } = ctx.params;

                        //  s3 stream upload
                        //https://prod-familheey.s3.amazonaws.com/Documents/Documents-1574695823094.png
                        let fieldname_ = fieldname === "file" ? "Documents" : fieldname;
                        let read = fs.createReadStream(path);
                        //let compress = zlib.createGzip();
                        let upload = s3Stream.upload({
                            "Bucket": process.env.AWS_BUCKET,
                            "Key": `${fieldname_}/${filename}`,
                        });

                        // Optional configuration
                        upload.maxPartSize(20971520); // 20 MB
                        upload.concurrentParts(5);

                        // Handle errors.
                        upload.on('error', function (error) {
                            console.log(error);
                        });

                        /* Handle progress. Example details object:
                           { ETag: '"f9ef956c83756a80ad62f54ae5e7d34b"',
                             PartNumber: 5,
                             receivedSize: 29671068,
                             uploadedSize: 29671068 }
                        */
                        upload.on('part', function (details) {
                        });

                        /* Handle upload completion. Example details object:
                           { Location: 'https://bucketName.s3.amazonaws.com/filename.ext',
                             Bucket: 'bucketName',
                             Key: 'filename.ext',
                             ETag: '"bf2acbedf84207d696c8da7dbb205b9f-5"' }
                        */
                        upload.on('uploaded', function (details) {
                            resolve({ "message": "Successfully uploaded data" });
                            //fs.unlink(path, (err) => {});
                        });

                        // Pipe the incoming filestream through compression, and up to S3.
                        read.pipe(upload);
                        return;
                        //  end
                        // fs.readFile(path, function (err, data) {
                        //     if (err) reject(err); // Something went wrong!
                        //     S3.createBucket(() => {
                        //         var params = {
                        //             Key: `${fieldname}/${filename}`,
                        //             Body: data,
                        //             Bucket: process.env.AWS_BUCKET,
                        //             ACL: 'public-read',
                        //         };
                        //         S3.upload(params, (err, data) => {
                        //             fs.unlink(path, (err) => {
                        //                 if (err) {
                        //                     reject(err);
                        //                 }
                        //             });
                        //             if (err) {
                        //                 console.log(err)
                        //                 reject(err);
                        //             } else {
                        //                 resolve({ "message": "Successfully uploaded data" })
                        //                 //resolve({"message":"Successfully uploaded data"})
                        //                 console.log('Successfully uploaded data');
                        //             }
                        //         });
                        //     });
                        // });
                    } else {
                        resolve();
                    }
                });
            }
        },
        /**
         * delete a file from s3 bucket
         */
        deleteFile: {
            params: {
                filename: "string",
                folder: "string"
            },
            handler(ctx) {
                let { filename, folder } = ctx.params;
                let params = {
                    Bucket: process.env.AWS_BUCKET,
                    Key: `${folder}/${filename}`
                };
                S3.deleteObject(params, function (err, data) {
                    if (err) console.log(err, err.stack); // an error occurred
                    else console.log(data);           // successful respond
                });
            }
        },
        generateVideoThumb: {
            params: {},
            handler(ctx) {
                let { path, filename } = ctx.params;
                return new Promise((resolve, reject) => {
                    // Create video thumb to s3 start
                    let _this = this;
                    let filePath = path;
                    let fileName = `${filename}.png`;
                    cmd.get(`ffmpeg -i ` + filePath + ` -ss 00:00:02 -vframes 1 ` + fileName, function (err, data, stderr) {
                        if (!err) {
                            let _element = {};
                            _element.fieldname = 'video_thumb';
                            _element.filename = fileName;
                            _element.path = fileName;
                            _this.calls3(ctx, _element, function (err2, res) {
                                if (res) {
                                    fs.unlink(fileName, (error) => {
                                        if (!error) {
                                            console.log('File deleted successfully', res);
                                            resolve('video_thumb/' + fileName);
                                        } else {
                                            console.log('Error deleting the file');
                                            reject(error);
                                        }
                                    });
                                } else {
                                    console.log(err2);
                                    fs.unlink(fileName, (error) => {
                                        if (!error) {
                                            console.log('File deleted successfully', res);
                                        } else {
                                            console.log('Error deleting the file');
                                        }
                                    });
                                    reject(err2);
                                }
                            });
                        } else {
                            console.log('error', err);
                            reject(err);
                        }

                    }
                    );
                    // Create video thumb to s3 end
                });
            }
        },
        signedUrl: {
            handler(ctx) {
                let { file_name } = ctx.params;
                let options = {
                    Bucket: process.env.AWS_BUCKET,
                    Key: file_name,
                    ACL: 'public-read'
                };
                let res = '';
                S3.getSignedUrl('putObject', options, function (err, data) {
                    if (err) return res.send('Error with S3');

                    res.json({
                        signed_request: data,
                        url: process.env.AWS_S3_BASE_URL + process.env.AWS_BUCKET + '/' + file_name
                    });
                });
            }
        },
        CopyFileBetweenBucket: {
            params: {
                source_file:"string",
                destination_file:"string"
            },
            handler(ctx) {
                return new Promise((resolve, reject) => {
                    let srcBucket = process.env.AWS_BUCKET;
                    let destBucket = process.env.AWS_BUCKET;
                    let sourceObject = ctx.params.source_file;
                    let destObject = ctx.params.destination_file;  

                    S3.copyObject({ 
                        CopySource: srcBucket + '/' + sourceObject,
                        Bucket: destBucket,
                        Key: destObject
                    }, function(copyErr, copyData){
                        if (copyErr) {
                            console.log("Error: " + copyErr);
                            reject({ message: 'error', data: copyErr });
                        } else {
                            console.log('Copied OK');
                            resolve({ message: 'Successfully copyed file'});
                        } 
                    });
                });
            }
        }
    },
    events: {},
    methods: {
        /**
         * call s3 service for fiel upload
         */
        calls3(ctx, params, callback) {
            ctx.call('s3upload.fileUpload', params)
                .then((res) => {
                    console.log("== s3uplod success ==", res);
                    callback('', res);
                })
                .catch((err) => {
                    callback(err, '');
                    console.log(err);
                });
        }
    },
    created() { },
    started() { },
    stopped() { }
};