"use strict"
const moment = require('moment');
const async = require('async');
const getUrls = require('get-urls');
const request = require('request');
const urlMetadata = require('url-metadata');
const scrape = require('html-metadata');

const linkPreviewGenerator = require("link-preview-generator");

//Unickey generation function start
const unicKeyGen = (length,id) => {
  let result = id+'-';
  let characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  let charactersLength = characters.length;
  for (let i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}
//Unickey generation function end

module.exports={
    name:"membership",
    actions:{
        add_membership_lookup:{
            params:{
                membership_name:"string",
                group_id:"string"
            },
            handler:(ctx)=>{
                let {membership_name,group_id} = ctx.params;
                return new Promise((resolve,reject)=>{
                    ctx.call("db.membership_lookup.find", {query:{membership_name: membership_name, group_id:group_id},fields: ["id"]})
                    .then((membershipData) => {
                        if(membershipData.length > 0) {
                            resolve({ message: 'Membership already exist', data: [] });   
                        } else {
                            ctx.call('db.membership_lookup.create', ctx.params)
                            .then((result) => {
                                resolve({ message: 'success', data: result });
                            }).catch((err) => {
                                reject({ message: 'Membership creation has some error', data: err });
                            });
                        }
                    }).catch((err) => {
                        reject('fetch membership has some error:',err);
                    });  
                })
            }
        },
        update_membership_lookup:{
            params:{
                id:"string"
            },
            handler:(ctx)=>{
                return new Promise((resolve,reject)=>{
                    ctx.call('db.membership_lookup.update', ctx.params)
                    .then((result) => {
                        resolve({ message: 'success', data: result });
                    }).catch((err) => {
                        reject({ message: 'Membership updation has some error', data: err });
                    });
                })
            }
        },
        user_membership_update:{
            params:{
                user_id:"string",
                group_id:"string",
                membership_id:"string"
            }, 
            handler(ctx){
                let {membership_from,membership_id,user_id,group_id,edited_user_id,membership_payment_status} = ctx.params;
                return new Promise(async(resolve,reject)=>{
                    let membershipPeriod = 0;
                    // Get membership period start
                    if(ctx.params.membership_duration && ctx.params.membership_duration != '') {
                        membershipPeriod = ctx.params.membership_duration   
                    } else {
                        await ctx.call('db.membership_lookup.find', {query:{id: membership_id},fields: ["membership_period"]})
                        .then((result) => {
                            if(result && result.length > 0) {
                                membershipPeriod = result[0].membership_period;
                            } else {
                                reject({message: 'Cant get membership period'});   
                            }
                        }).catch((err) => {
                            reject({ message: 'Membership get has some error', data: err });
                        });
                    }
                    // Get membership period end

                    // Calculate membership TO date start
                    if(ctx.params.membership_from && ctx.params.membership_from != '') {
                        let membershipStart = moment(membership_from * 1000).add(1, 'day').unix(); 
                        ctx.params.membership_to = moment(membershipStart * 1000).add(membershipPeriod, 'day').unix();   
                    } else {
                        reject({message: 'Membership from date is missing'});   
                    }
                    // Calculate membership TO date end

                    // Get previous todate start
                    await ctx.call('db.group_map.find', {query:{user_id:user_id,group_id:group_id},fields: ["membership_to","id","membership_id","membership_duration"]})
                    .then((mapRresult) => {
                        if(mapRresult && mapRresult.length > 0) {
                            ctx.params.id = mapRresult[0].id;
                            // if((!membership_from && !membership_to) && (mapRresult[0].membership_id != membership_id || (mapRresult[0].membership_duration != membershipPeriod && membershipPeriod > 0))) {
                            //     if(mapRresult[0].membership_to != null) {
                            //         let resultDate = mapRresult[0].membership_to;
                            //         let membershipStart = moment(resultDate * 1000).add(1, 'day').unix();
                            //         if(!ctx.params.membership_from || ctx.params.membership_from == '') {
                            //             ctx.params.membership_from = membershipStart;
                            //         }
                            //         if(!ctx.params.membership_to || ctx.params.membership_to == '') {
                            //             ctx.params.membership_to = moment(membershipStart * 1000).add(membershipPeriod, 'day').unix();
                            //         }   
                            //     } else {
                            //         if(!ctx.params.membership_from || ctx.params.membership_from == '') {
                            //             ctx.params.membership_from = moment.utc().unix();
                            //         }
                            //         if(!ctx.params.membership_to || ctx.params.membership_to == '') {
                            //             ctx.params.membership_to =  moment().add(membershipPeriod, 'day').unix();
                            //         }     
                            //     }
                            // }
                        } else {
                            reject({ message: 'Group_maps is not available'});   
                        }
                    }).catch((err) => {
                        reject({ message: 'Group_maps get has some error', data: err });
                    });
                    // Get previous todate start

                    let beforeRec;
                    if(membership_payment_status && membership_payment_status.toLowerCase() == 'pending') {
                        ctx.params.membership_ref_id = unicKeyGen(10,group_id);
                        ctx.params.membership_total_payed_amount = 0;
                    }

                    ctx.call('db.group_map.find', {query:{id:ctx.params.id}})
                    .then(rec => { beforeRec = rec[0]; })
                    .then(() => {
                        let _this = this;
                        ctx.call('db.group_map.update', ctx.params)
                        .then((result) => {
                            resolve({ message: 'success', data: result });

                            //If membership_payment_status = Completed, Update membership paid amount automatically start
                            if(membership_payment_status && membership_payment_status.toLowerCase() == 'completed'){
                                let memberFee = parseInt(result.membership_fees);
                                let memberpayed = parseInt(result.membership_total_payed_amount);
                                let memberOutstand = memberFee - memberpayed;
                                if(memberOutstand > 0) {
                                    let autoPaymentUpdateObj = {
                                        id : result.id.toString(),
                                        membership_payed_amount : memberOutstand,
                                        membership_updated_by : 'admin'
                                    };
                                    ctx.call('groups.groupMapUpdate', autoPaymentUpdateObj)
                                    .then((historRresult) => {
                                        console.log("Successfully created auto payment");   
                                    }).catch((err) => {
                                        console.log("auto payment creation has some error",err);   
                                    });
                                }
                            }
                            //If membership_payment_status = Completed, Update membership paid amount automatically end

                            //Send Notification start
                            if(membership_payment_status && membership_payment_status.toLowerCase() == 'pending') {
                                ctx.params.to_user_id = user_id;
                                ctx.params.group_id = group_id;
                                ctx.params.ntype = 'payments';
                                _this.appNotify(ctx);
                            }
                            //Send Notification end

                            //Track user_membership_history start
                            let historyObj = {
                                group_id : parseInt(result.group_id),
                                member_user_id : parseInt(result.user_id),
                                paid_amount : parseInt(result.membership_fees),
                                payment_status : result.membership_payment_status,
                                membership_id : parseInt(result.membership_id),
                                current_data : result,
                                previous_data : beforeRec,
                                changed_data : objectDiff(beforeRec,result)
                            }
                            if(edited_user_id && edited_user_id != '') {
                                historyObj.edited_user_id = parseInt(edited_user_id);   
                            }
                            ctx.call('db.user_membership_histories.create', historyObj)
                            .then((historRresult) => {
                                console.log("Successfully added user_membership_history");   
                            }).catch((err) => {
                                console.log("user_membership_history creation has some error",err);   
                            });
                            //Track user_membership_history end

                            // Track user activity start
                            let trackerData = {
                                user_id: parseInt(user_id),
                                type: 'membership',
                                type_id: parseInt(result.id),
                                activity: 'User membership updated'
                            };
                            ctx.broker.call('user_activities.trackUserActivities', trackerData);
                            // Track user activity end 

                        }).catch((err) => {
                            reject({ message: 'Membership updation to user has some error', data: err });
                        });
                    })

                    //Compare and return difference of two json object start
                    function objectDiff(obj1, obj2) {
                        let diffResult = {};
                        for(var o1 in obj1){
                            if(obj2[o1]){
                                if(obj1[o1].toString() != obj2[o1].toString()) {
                                    diffResult[o1] = obj2[o1];    
                                }
                            }
                        }
                        return diffResult;
                    }   
                    //Compare and return difference of two json object end
                })
            }  
        },
        list_membership_lookup:{
            params:{
                user_id:"string",
                group_id:"string"
            },
            handler(ctx){
                return new Promise(async(resolve,reject)=>{
                    ctx.call("db.membership_lookup.find", {query:{group_id:ctx.params.group_id,is_active:true}, fields: ["id","membership_name","created_by","membership_period","membership_fees","membership_currency","is_active","membership_period_type_id"] }) 
                    .then(async res => {
                        let response = res;
                        for (var n = 0 ; n < response.length ; n++) {
                            ctx.params.periodreq = response[n];
                            response[n].types = await this.membershipPeriodType(ctx);
                        }
                        resolve(response);
                    })
                    .catch(err => {
                        console.log(err);
                        reject(err);   
                    })  
                })       
            }   
        },
        membership_dashboard:{
            params:{
                user_id:"string",
                group_id:"string"   
            },
            handler(ctx){
                return new Promise((resolve,reject)=>{
                    //let errorobj = {};
                    async.parallel({
                        membership_data: function(callback){
                            ctx.call("db.membership_lookup.group_by_membership",ctx.params)
                            .then(membershipData => {
                                callback(null, membershipData);
                            })
                            .catch(err => {
                                console.log(err);
                                callback(err, null);  
                            }) 
                        },
                        membership_counts: function(callback){
                            ctx.call("db.membership_lookup.group_membership_counts",ctx.params)
                            .then(membershipcount => {
                                callback(null, membershipcount);
                            })
                            .catch(err => {
                                console.log(err);
                                callback(err, null);  
                            })     
                        }
                    },function(err,results) {
                        if (err) {
                            console.log('fetch membership by group failed ==> ', err);
                            reject(err);
                        } else {
                            resolve(results);
                        }
                    }); 
                })
            }
        },

        /**
         * get the details of memebership look up yo edit
         */
        getMembershiptypeById:{
            params:{
                membership_id:"string"
            },
            handler(ctx){
                let { membership_id } = ctx.params;
                let response = {}
                return new Promise(async(resolve,reject)=>{
                    ctx.call("db.membership_lookup.find", { query: { id: membership_id } }) 
                    .then(res => {
                        response = { ...response, message:"success", doc: res.length>0 ? res[0]:{} }
                        resolve(response);
                    })
                    .catch(err => {
                        console.log(err);
                        reject(err);   
                    })  
                })       
            }   
        },
        Membership_reminder:{
            params:{
                loggedin_user_id:"string",
                to_userid:"string",
                group_id:"string"
            },  
            handler(ctx){
                return new Promise((resolve,reject)=>{
                    let _this = this;
                    let {loggedin_user_id,to_userid,group_id} = ctx.params;
                    async.parallel({
                        user_data: function(callback){
                            ctx.call("db.users.find", { query: { id: loggedin_user_id },fields: ["propic"] })
                            .then(userData => {
                                callback(null, userData);
                            })
                            .catch(err => {
                                console.log(err);
                                callback(err, null);  
                            }) 
                        },
                        group_data: function(callback){
                            ctx.call("db.groups.find", { query: { id: group_id },fields: ["group_name"] })
                            .then(groupData => {
                                callback(null, groupData);
                            })
                            .catch(err => {
                                console.log(err);
                                callback(err, null);  
                            }) 
                        },
                    },function(err,results) {
                        if (err) {
                            reject(err);
                        } else {
                            let sendObj = {
                                from_id: loggedin_user_id,
                                type: 'family',
                                propic: results.user_data.length > 0 ? results.user_data[0].propic : '',
                                to_id: to_userid,
                                sub_type: 'about',
                                category: 'Familheey Membership reminder',
                                message_title: 'Familheey Membership reminder',
                                type_id: group_id,
                                message:`Your membership with ${results.group_data[0].group_name} is expiring, please get in touch with your admin to renew`,
                                link_to:group_id
                            }
                            resolve('Send successfully');
                            ctx.broker.call("notification.sendAppNotification", sendObj)
                            .then(senddata => {
                                //resolve('Send successfully')
                            })
                            .catch(err2 => {
                                console.log(err2);
                            }) 
                            _this.pushNotify(ctx,sendObj);
                        }
                    });  
                })
            } 
        },
        get_membership_lookup_periods:{
            params:{
            },
            handler(ctx){
                return new Promise((resolve,reject)=>{
                    ctx.call("db.membership_lookup_periods.find",{query: {is_active: true},fields: ["id","membership_lookup_period_type","membership_lookup_period"]}) 
                    .then(res => {
                        resolve(res);
                    })
                    .catch(err => {
                        console.log(err);
                        reject(err);   
                    })  
                });
            }   
        }
    },
    methods:{
        appNotify(ctx) {
            let { to_user_id, edited_user_id, ntype, group_id } = ctx.params;
            let _this = this;
            async.parallel({
                tousers: function (callback) {
                    ctx.broker.call('db.users.find', { query: { id: to_user_id }, fields: ["id", "full_name", "propic"] })
                    .then((res) => {
                        callback(null, res);
                    })
                    .catch((err) => {
                        callback(null, []);
                        console.log("User Search Error", err);
                    })  
                },
                fromUser: (callback) => {
                    ctx.call("db.users.find", { query: { id: edited_user_id }, fields: ["id", "full_name", "propic"] })
                    .then(res => {
                        callback(null, res);
                    }).catch(err => {
                        callback(null, []);
                        console.log("error happened -------- familyMembers", err);
                    })
                },
                groupDetail: (callback) => {
                    ctx.call("db.groups.find", { query: { id: group_id }, fields: ["group_name","created_by","logo"] })
                    .then(res => {
                        callback(null, res);
                    }).catch(err => {
                        callback(null, []);
                        console.log("error happened -------- group", err);
                    })
                }
            },
            function (err, results) {
                let { tousers, fromUser, groupDetail } = results;
                tousers.forEach(async elem => {
                    let sendObj = {
                        from_id: fromUser.length > 0 ? fromUser[0].id : '',
                        type: 'family',
                        propic: fromUser.length > 0 ? fromUser[0].propic : '',
                        to_id: elem.user_id ? parseInt(elem.user_id) : parseInt(elem.id),
                        sub_type: 'about',
                        category: ntype,
                        message_title: 'familheey',
                        type_id: group_id ? group_id : '',
                        link_to: group_id ? group_id : ''
                    }
                    switch (ntype) {
                        case 'payments':
                            sendObj.message  = `Membership dues for ${groupDetail[0].group_name}. Click to view details and complete payment.`;
                            sendObj.sub_type = 'about';
                            sendObj.type = 'family';
                            sendObj.type_id  = parseInt(group_id);
                            sendObj.link_to  = parseInt(group_id);
                            sendObj.from_id = group_id;
                            sendObj.propic = groupDetail[0].logo;
                        break;
                        case 'condition_next': //for sonarqube fix remove and add new case if needed
                            console.log(ctx.params);
                            break;
                        case 'condition_next2': //for sonarqube fix remove and add new case if needed
                            console.log(ctx.params);
                            break

                        default:
                        break;
                    }
                    if (fromUser[0].id != elem.user_id) {
                        ctx.broker.call("notification.sendAppNotification", sendObj);
                        let _user_notify = await ctx.call("db.users.find", { query: { id: sendObj.to_id }, fields: ["notification"] })
                        if (_user_notify.length > 0 && _user_notify[0].notification) {
                            _this.pushNotify(ctx, sendObj);
                        }
                    }
                })
            })
        },
        pushNotify(ctx, params) {
            ctx.broker.call("userops.getregisterToken", { user_id: params.to_id, is_active: true })
            .then((_devices) => {
                _devices.data.forEach(elem => {
                    params = {
                        ...params,
                        token: elem.device_token,
                        device_type: elem.device_type.toLowerCase(),
                        title: params.message_title,
                        body: this.removeTags(params.message),
                        sub_type: params.sub_type ? params.sub_type : ''
                    }
                    ctx.broker.call("notification.sendPushNotification", params)
                })
            })
            .catch(e => e)
        },
        removeTags(str) {
            if ((str === null) || (str === '')) return '';
            else return str.toString().replace(/(<([^>]+)>)/ig, '');
        },
        membershipPeriodType(ctx) {
            return new Promise((resolve, reject) => {
                let { periodreq } = ctx.params;
                let returnArr = [];
                ctx.call("db.membership_period_type_lookups.find", { query: { period_id: periodreq.membership_period_type_id }, fields: ["id", "type_name", "type_value"] })
                .then(subRres => {
                    returnArr = subRres;
                    resolve(returnArr)
                })
                .catch(err => {
                    console.log(err);
                    reject(returnArr);
                })
            })
        }
    }
}