"use strict";
var async = require('async');

module.exports = {
    name: "globalsearch",
    settings: {},

    actions: {
        globalsearch: {
            // params : {},    
            handler(ctx) {
                return new Promise((resolve, reject) => {
                    //let finaldata = {}
                    let offsetval = ctx.params.offset ? ctx.params.offset : 0;
                    let limitval = ctx.params.limit ? ctx.params.limit : 25;
                    //let phoneArray = ctx.params.phonenumbers ? ctx.params.phonenumbers : [];
                    if(ctx.params.searchtxt && ctx.params.searchtxt != '') {
                        ctx.params.searchtxt = TrimTesxt(ctx.params.searchtxt);
                    }

                    function TrimTesxt(x) {
                        return x.replace(/^\s+|\s+$/gm,'');
                    }

                    // Multiple phone number check start
                    // function multiPhoneCheck(callback) {
                    //     // let phoneArray = ['+919961251985','+917012862013','+911234567890'];
                    //     if (phoneArray.length > 0 && Array.isArray(phoneArray)) {
                    //         let phoneQuery = phoneArray.map(generateMultiNumber);
                    //         function generateMultiNumber(value) {
                    //             let rowphone = value.replace(/[^\d\+]/g, "");
                    //             let realPhone = '%' + rowphone.substr(rowphone.length - 10) + '%';
                    //             return realPhone;
                    //         }
                    //         phoneQuery = phoneQuery.toString();

                    //         ctx.call("db.groups.userPhoneMulticheck", { multiPhone: phoneQuery })
                    //             .then(data => {
                    //                 if (data) {
                    //                     let multiUser = data.map(({ id }) => id);
                    //                     finaldata.multiUser = multiUser;
                    //                     ctx.call("db.groups.getGroupFromPhone", { multiUser: multiUser })
                    //                         .then(groupdata => {
                    //                             let multiGroup = groupdata.map(({ group_id }) => group_id);
                    //                             finaldata.multiGroup = multiGroup;
                    //                             callback(finaldata);
                    //                         })
                    //                         .catch((err) => reject(err))
                    //                 }
                    //             })
                    //             .catch((err) => reject(err))
                    //     } else {
                    //         callback('');
                    //     }

                    // }
                    // Multiple phone number check end

                    function parallelCall(pass_finaldata) {
                        let { multiGroup, multiUser } = pass_finaldata;
                        var searchType = ctx.params.type;
                        if (searchType) {
                            switch (searchType) {
                                case 'family':
                                    var groups = {};
                                    ctx.broker.call('db.groups.searchFamily', { txt: ctx.params.searchtxt, userid: ctx.params.userid, offset: offsetval, limit: limitval, multiGroup: multiGroup })
                                        .then((res) => {
                                            res.sort(function (a, b) {
                                                return b.membercount - a.membercount
                                            })
                                            groups.groups = res;
                                            resolve(groups);
                                        })
                                        .catch((err) => {
                                            console.log("Family Search Error", err);
                                            reject(err);
                                        })
                                    break;
                                case 'users':
                                    var users = {};
                                    ctx.broker.call('db.users.searchUser', { txt: ctx.params.searchtxt, userid: ctx.params.userid, offset: offsetval, limit: limitval, group_id: ctx.params.group_id, multiUser: multiUser })
                                        .then((res) => {
                                            res.sort(function (a, b) {
                                                return b.mutualfamilycount - a.mutualfamilycount
                                            })
                                            users.users = res;
                                            resolve(users);
                                            // users.users = res;
                                            // ctx.call("db.group_map.getFamilyCount", { user_id: ctx.params.userid.toString() })
                                            // .then((fm_count) => {
                                            //     users.count = fm_count[0];
                                            //     resolve(users);
                                            // }).catch(err => console.log("error happened ------ familyCount", err))
                                        })
                                        .catch((err) => {
                                            console.log("User Search Error", err);
                                            reject(err);
                                        })
                                    break;
                                case 'events':
                                    var events = {};
                                    ctx.broker.call('db.events.searchEvent', { txt: ctx.params.searchtxt, userid: ctx.params.userid, offset: offsetval, limit: limitval })
                                        .then((res) => {
                                            // Array sort using location start
                                            if (res && res.length > 0 && res[0].sortlocation) {
                                                var sortvalue = res[0].sortlocation;
                                                if (sortvalue && sortvalue != '') {
                                                    sortvalue = sortvalue.toUpperCase();
                                                    res.sort(function (x, y) {
                                                        if (x.location && y.location)
                                                            return x.location.toUpperCase() == sortvalue ? -1 : y.location.toUpperCase() == sortvalue ? 1 : 0;
                                                    });
                                                }
                                            }
                                            // Array sort using location end 
                                            events.events = res;
                                            resolve(events);
                                        })
                                        .catch((err) => {
                                            console.log("Events Search Error", err);
                                            reject(err);
                                        })
                                    break;
                                case 'posts':
                                    var posts = {};
                                    ctx.broker.call('posts.global_search_feed', { txt: ctx.params.searchtxt, type: 'post', user_id: ctx.params.userid, offset: offsetval, limit: limitval, requestFrom: 'globalSearch' })
                                        .then((res) => {
                                            posts.posts = res;
                                            resolve(posts);
                                        })
                                        .catch((err) => {
                                            console.log("Events Search Error", err);
                                            reject(err);
                                        })
                                    break;
                                case 'announcement':
                                    var announcement = {};
                                    ctx.broker.call('posts.global_search_feed', { txt: ctx.params.searchtxt, type: 'announcement', user_id: ctx.params.userid, offset: offsetval, limit: limitval, requestFrom: 'globalSearch' })
                                        .then((res) => {
                                            announcement.announcement = res;
                                            resolve(announcement);
                                        })
                                        .catch((err) => {
                                            console.log("Events Search Error", err);
                                            reject(err);
                                        })
                                    break;
                                default:
                                // code block
                            }
                        } else {
                            resolve('type is missing');
                        }

                    }

                    // multiPhoneCheck(parallelCall);
                    parallelCall('');

                });

            }
        }
    },
    events: {},
    methods: {},
    created() { },
    started() { },
    stopped() { }
}