"use strict";
const moment = require('moment');
const _async = require('async');

module.exports = {
    name: "user_activities",
    settings: {},
    actions: {
        trackUserActivities: {
            params: {
                user_id: 'number',
                type: 'string'
            },
            handler(ctx) {
                return new Promise((resolve, reject) => {
                    ctx.call('db.user_activities.create',ctx.params)
                    .then(result => {
                        resolve("User activity tracked successfully");
                    })
                    .catch((err) => {
                        console.log("User activity has some error");
                        reject(err);
                    });
                });
            }
        },
        getUserActivities: {
            params: {
                user_id: 'string'
            },
            handler(ctx) {
                return new Promise((resolve, reject) => {
                    ctx.call('db.user_activities.find', {query: { user_id: ctx.params.user_id }})
                    .then(result => {
                        resolve({ message: 'success', data: result });
                    }) 
                    .catch((err) => {
                        reject(err);
                    });
                });
            }
        }
    },
    events: {},
    methods: {},
    created() { },
    started() { },
    stopped() { }
};