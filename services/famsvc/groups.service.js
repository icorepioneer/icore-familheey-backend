"use strict";
const _async = require("async");
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
var rq = require('request');
const CryptoJS = require("crypto-js");
const key = process.env.SECRET_KEY;

const unicKeyGen = (length,id) => {
    let result = id+'-';
    let characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    let charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

module.exports = {
    name: "groups",
    settings: {},
    actions: {
        /**
         * create a new group
         */
        createGroup: {
            params: {
                group_name: "string",
                group_category: "string",
                base_region: "string"
            },
            handler(ctx) {
                let response = {}
                let { f_text, group_category, value } = ctx.params;
                return new Promise(async(resolve, reject) => {

                    if(!f_text) ctx.params = { ...ctx.params, f_text:this.makeFamilyText(6) }

                    /**
                     * group_category check exist
                     * if exist exist id
                     */
                    if(group_category == 'Others' && value){
						let isCatExist = await ctx.broker.call('db.group_categorylookup.isExist',{category:value})
						if(isCatExist && isCatExist.length>0){
							ctx.params.group_category = isCatExist[0].id;
						}else{ //if not exist, create new one
							let newId = await ctx.call('db.group_categorylookup.create',{ category:value })
							ctx.params.group_category = newId.id;
						}
                    }

                    ctx.call('db.groups.create', ctx.params)
                    .then(async (doc) => {
                        doc = { ...doc, type:'family'}

                        //Generate & update firebase link start
                        //this.generatefirebaseLink(ctx,doc);
                        let linkobj = {
                            type : 'family',
                            type_id : doc.id.toString()
                        }
                        ctx.call('common_methods.createFirebaseLink', linkobj)
                        .then((linkRes) => {
                            if(linkRes && linkRes != '') {
                                ctx.call('db.groups.update',{ id:doc.id, firebase_link:linkRes })
                                .then((linkUpdate) => {
                                    console.log("Successfully updated firebase link");
                                })
                                .catch((err) => {
                                    console.log("Firebase link updation to family has some error", err);
                                })
                            }
                        })
                        .catch((err) => {
                            console.log("Firebase link generation has some error", err);
                        })
                        //Generate & update firebase link end

                        response.data = [doc];
                        let groupMap = {
                            user_id: parseInt(ctx.params.created_by),
                            group_id: parseInt(doc.id),
                            type: "admin",
                            following: true,
                            is_blocked: false
                        }
                        // Track user activities start
                        let trackerData = {
                            user_id : parseInt(ctx.params.created_by),
                            type : 'Family',
                            type_id : parseInt(doc.id),
                            activity : 'created a family'
                        };
                        ctx.broker.call('user_activities.trackUserActivities', trackerData);
                        // Track user activities end

                        //Set default membership lookup start
                        let membershipObj = {
                            membership_name : "Unassigned",
                            group_id : doc.id,
                            membership_period : 3650,
                            membership_fees : 0,
                            membership_currency: "USD",
                            membership_period_type : "Life time",
                            created_by : parseInt(ctx.params.created_by),
                            membership_period_type_id:4
                        }
                        await ctx.call('db.membership_lookup.create', membershipObj)
                        .then((result) => {
                            console.log('Successfully created membership lookup');
                            groupMap.membership_id = result.id;

                            // Add default membership id to group table start
                            let updateGroupMemberDefault = {
                                id : doc.id,
                                default_membership : result.id 
                            }
                            ctx.call('db.groups.update', updateGroupMemberDefault)
                            .then((memberResult) => {
                                console.log('Successfully added default membership id to group');   
                            })
                            .catch((err) => {
                                console.log('Add default membership id to group has some error',err);
                            });
                            // Add default membership id to group table end

                        }).catch((err) => {
                            console.log('Membership lookup creation has some error',err);
                        });
                        //Set default membership lookup end

                        await ctx.call("db.group_map.create", groupMap)
                            .then(async res => {
                                console.log(res)
                                await ctx.call("groups.updateGroupSettings", {
                                    "group_id": `${res.group_id}`,
                                    "member_joining": "2",
                                    "member_approval": "4",
                                    "post_create": "6",
                                    "post_visibilty": "8",
                                    "post_approval": "10",
                                    "link_family": "13",
                                    "link_approval": "16"
                                })
                            });

                    })
                    .then((grp_mp) => {
                        console.log(response)
                        resolve(response);
                    })
                    .catch((err) => {
                        console.log(err)
                        reject(err);
                    })
                })
            }
        },
        /**
        * updates a createdGroup
        */
        updateGroup: {
            params: {
                id:"string",
                user_id:"string"
            },
            handler(ctx) {
                let { id, user_id, action, group_type  } = ctx.params;
                let response = {}
                return new Promise((resolve, reject) => {
                    if (ctx.meta && ctx.meta.file) {
                        ctx.meta.file.forEach(element => {
                            ctx.params[element.fieldname] = element.filename;
                            this.calls3(ctx, element)
                        });
                    }
                    if(action && action == 'delete_family'){
                        ctx.params.is_active = false;
                        let noti_data = {
                            notifyType:'delete',
                            user_id:user_id,
                            to_group:id,
                            member_type : ''
                        }
                        this.appNotify(ctx,noti_data);
                        let DeleteFamily = {
                            group_id: ctx.params.id
                        }
                        //Delete Post start
                        ctx.call("db.post.bulk_update_post_byfamily",DeleteFamily)
                        .then((delpost) => {
                            console.log("---------------Post successfully deleted alogn with family delete--------------------");
                        })	
                        .catch((err) => {
                            console.log("---------------Post delete alogn with family delete has some error----------------",err)
                        });	
                        //Delete Post end
                        //Delete events invitations start
                        ctx.call("db.events_invitation.bulkDeleteEventInvitationByFamily",DeleteFamily)
                        .then((delpost) => {
                            console.log("---------------Events invitations successfully deleted alogn with family delete--------------------");
                        })	
                        .catch((err) => {
                            console.log("---------------Events invitations delete alogn with family delete has some error----------------",err)
                        });
                        //Delete events invitations end
                        //Delete events shares start
                        ctx.call("db.events_share.bulkDeleteEventShareByFamily",DeleteFamily)
                        .then((delpost) => {
                            console.log("---------------Events Share successfully deleted alogn with family delete--------------------");
                        })	
                        .catch((err) => {
                            console.log("---------------Events Share delete alogn with family delete has some error----------------",err)
                        });
                        //Delete events shares end
                        //Delete events calender start
                        ctx.call("db.calender.bulkDeleteCalenderByFamily",DeleteFamily)
                        .then((delcaldata) => {
                            console.log("---------------Calender data successfully deleted alogn with family delete--------------------");
                        })	
                        .catch((err) => {
                            console.log("---------------Calender data delete alogn with family delete has some error----------------",err)
                        });
                        //Delete events calender end
                    }

                    if(group_type && group_type == 'public'){//public
                        ctx.params = { ...ctx.params, post_visibilty:9, announcement_visibilty:22 }
                    }else if(group_type && group_type == 'private'){//private
                        ctx.params = { ...ctx.params, post_visibilty:8, announcement_visibilty:21 }                        
                    }

                    ctx.call('db.groups.update', ctx.params)
                    .then((doc) => {
                        if (doc) {
                            doc.f_link = `${process.env.SHARE_BASE_URL}page/groups/${doc.f_text}`;
                        }
                        response.data = [doc];
                        let redisArr = ['groups.viewFamily',
                            'db.group_map',
                            'groups.getFamilyById:group_id|' + ctx.params.id + ''];
                        ctx.broker.call('redis.deleteKeys', redisArr);
                        resolve(response);
                        // Track user activities start
                        let trackerData = {
                            user_id : parseInt(user_id),
                            type : 'Family',
                            type_id : parseInt(doc.id),
                            activity : 'updated a family'
                        };
                        ctx.broker.call('user_activities.trackUserActivities', trackerData);
                        // Track user activities end
                    })
                    .catch((err) => {
                        console.log("== err ==")
                        console.log(err)
                        reject(err);
                    })
                })
            }
        },
        /**
        * is group exist 
        * exist send the list else 
        * error message
        */
        fetchGroup: {
            params: {
                group_name: "string",
                group_category: "string",
                base_region: "string"
            },
            handler(ctx) {
                let { user_id } = ctx.params
                let response = {}
                var gidsQ;
                return new Promise((resolve, reject) => {
                    ctx.broker.call("db.groups.fetchGroup", ctx.params)
                        .then(async (doc) => {
                            let gid = doc.map(elem => elem.id);
                            gidsQ = { user_id: user_id, group_id: gid }
                            response.data = doc;
                            let grp_maps = await ctx.broker.call("db.group_map.find", { query: gidsQ });
                            return grp_maps;
                        })
                        .then(async (grp_maps) => {
                            let indexIds = grp_maps.map(elem => elem.group_id);
                            response.data.map(elem => {
                                elem['is_joined'] = indexIds.indexOf(elem.id) != -1 ? true : false;
                                elem['req_status'] = '';
                            });
                            let grp_req = await ctx.broker.call("db.requests.find", { query: gidsQ });
                            return grp_req;
                        })
                        .then((requests) => {
                            if (requests && requests.length > 0) {
                                response.data.map(elem => {
                                    requests.forEach(reqElem => {
                                        if (reqElem.group_id == elem.id) {
                                            elem['req_status'] = reqElem.status;
                                        }
                                    });
                                });
                            }
                            resolve(response);
                        })
                        .catch((err) => {
                            console.log(err);
                            reject({ data: err })
                        })
                })
            }
        },
        getAllMembers: {
            params: {
                groupId: "number",
            },
            handler: async (ctx) => {
                let { groupId } = ctx.params;
                return await ctx.broker.call("db.group_map.getUsersOfCurrentGroup", { group_id: groupId });
            }
        },
        /**
         * function used to add user to a group
         */
        addMemberTogroup: {
            params: {},
            handler(ctx){
                return new Promise(async(resolve, reject) => {
                    let { userId } = ctx.params;
                    let _this= this
                    let typeIs = typeof userId;
                    if(typeIs == "string"){
                        _this.addMemberTogroupFn(ctx)
                        .then(res => {
                            resolve(res)
                        }).catch(err => {
                            reject(err);
                        })
                        //resolve({ code:200, message:"Data in progress..", data:[] });
                    } else {
                        let new_userId = ctx.params.userId;
                        _async.each(new_userId,(elem, callback)=>{
                            ctx.params.userId = elem.toString();
                            _this.addMemberTogroupFn(ctx);
                            callback();
                        }, function(err) {
                            // if any of the file processing produced an error, err would equal that error
                            if( err ) { console.log("== err === ", err)
                              // One of the iterations produced an error.
                              // All processing will now stop.
                              reject(err)
                            } else {
                              resolve({ code:200, message:"Data in progress..", data:[] });
                            }
                        });
                    }
                });
            }
        },
        /**
         * user send a request to join to a family
         */
        joinFamily: {
            params: {
                user_id: "string",
                group_id: "string"
            },
            async handler(ctx){
                let _this = this;
                let { user_id, group_id } = ctx.params;
                let response = {}
                let groupSettings = await ctx.broker.call("db.groups.getGroupSettings", { group_id: parseInt(group_id) })
                console.log(groupSettings);
                // return groupSettings;
                let groupJoinStatus = groupSettings.filter((el) => el.key === "MEMBER_JOINING");
                if (groupJoinStatus[0].value !== "Invitation only") {
                    // Anyone can join
                    return groupJoinStatus[0].value == "Anyone can join" ? (
                        new Promise((resolve, reject) => {
                            ctx.broker.call("groups.addUserdirectlytoGroup",ctx.params)
                            .then(async (res) => {
                                if(res.error==1) {
                                    return resolve(res);
                                }
                                res = { ...res, status:'joined',user_status:'member' }
                                response.data = res;
                                // Track user activities start
                                    let trackerData = {
                                        user_id : parseInt(user_id),
                                        type : 'Family',
                                        type_id : parseInt(group_id),
                                        activity : 'joined to family'
                                    };
                                    ctx.broker.call('user_activities.trackUserActivities', trackerData);
                                // Track user activities end
                                let allMembers = await ctx.broker.call("groups.getAllMembers", { groupId: parseInt(group_id) });
                                allMembers = allMembers.filter((member) => member.id != user_id);
                                console.log(allMembers);
                                //let emails = [];
                                let _group_name = '';
                                //let req_user = await ctx.broker.call("db.users.find", { query: { id: user_id }, fields: ["id", "full_name", "propic"] })
                                let _group = await ctx.broker.call("db.groups.find", { query: { id: group_id }, fields: ["id", "group_name", "logo"] })
                            
                                // if (req_user && req_user.length > 0) {
                                //     user_name = req_user[0].full_name
                                // }
                                if (_group && _group.length > 0) {
                                    _group_name = _group[0].group_name
                                }

                                /*allMembers.forEach(async each => {
                                    emails.push(each.email);
                                    each.phone && await ctx.broker.call("notification.sendSms", {
                                        message: `${user_name} has joined to ${_group_name}`,
                                        PhoneNumber: each.phone,
                                        subject: "New member joined"
                                    });
                                })*/
                                // emails.length > 0 && await ctx.broker.call("notification.sendEmail", {
                                //     toAddresses: emails,
                                //     message: `${user_name} has joined to ${_group_name}`,
                                //     subject: "New member joined"
                                // }).catch(err => { console.log("errror happened while sending mail --->", err) });
                                resolve(response)
                            }
                            ).catch(err => {
                                console.log(err);
                                reject(err)
                            });
                        })
                    ) : (
                            //Any one can join with approval

                            new Promise(async(resolve, reject) => {
                                //remove accepted/rejected thats already exist requests
                                //else duplicate occurs in discover->families
                                let ExceptList = await ctx.call("db.requests.ExceptList", {  status:'pending', user_id: parseInt(user_id), group_id: parseInt(group_id) });
                                if(ExceptList && ExceptList.length>0){
                                    let idz = ExceptList.map(value=>value.id);
                                    await ctx.call("db.requests.bulkDeleteRequest", { idz:idz.toString()})
                                }
                                ctx.call("db.requests.find", { query: { user_id: parseInt(user_id), group_id: parseInt(group_id), status:'pending' } })
                                .then(async res => {
                                    if (res.length == 0) {
                                        let isUserExist = await ctx.call("db.group_map.find",{ query: { user_id: parseInt(user_id), group_id: parseInt(group_id) }, fields:["is_blocked","is_removed","id"] })

                                        if(isUserExist && isUserExist.length>0 && isUserExist[0].is_blocked){
                                            reject("You are blocked."); return;
                                        }

                                        let requestUpdateObj = {
                                            user_id: parseInt(user_id),
                                            group_id: parseInt(group_id),
                                            type: "request",
                                            status: "pending"   
                                        }
                                        /*if(isUserExist && isUserExist.length>0 && isUserExist[0].id) {
                                            let groupMapUpdateObj = {};
                                            groupMapUpdateObj.id = isUserExist[0].id; 
                                            groupMapUpdateObj.is_removed = false;
                                            ctx.call("db.group_map.update", groupMapUpdateObj)
                                            .then((res) => { 
                                                console.log("Successfully updated groupmap");
                                            }) 
                                            .catch((err) => console.log(err));
                                        }*/
                                        ctx.call("db.requests.create", requestUpdateObj)
                                        .then((res2) => {
                                            response.data = res2;
                                            resolve(response);
                                            // Track user activities start
                                            let trackerData = {
                                                user_id : parseInt(user_id),
                                                type : 'Family',
                                                type_id : parseInt(group_id),
                                                activity : 'sent request to join family'
                                            };
                                            ctx.broker.call('user_activities.trackUserActivities', trackerData);
                                            // Track user activities end
                                            /**admin approval app  notitfivcation comes here */

                                            //db.groups.joinAndLinkAppNotify
                                            _async.parallel({
                                                from_user: (callback) => {
                                                    ctx.call("db.users.find", { query: { id: user_id }, fields: ["email", "full_name", "propic","id"] }).then(res3 => {
                                                        callback(null, res3);
                                                    }).catch(err => {
                                                        console.log("error happened -------- familyMembers", err);
                                                    })
                                                },
                                                group: (callback) => {
                                                    ctx.call("db.groups.find", { query: { id: group_id, is_active:true }, fields: ["id", "group_name"] }).then(res4 => {
                                                        callback(null, res4);
                                                    }).catch(err => {
                                                        console.log("error happened -------- familyMembers", err);
                                                    })
                                                },
                                                groupAdmins: (callback) => {
                                                    ctx.call("db.group_map.fetchGroup_members", { group_id: group_id, type:'admin' }).then(res5 => {
                                                        callback(null, res5)
                                                    }).catch(err => {
                                                        console.log("error happened -------- fetchGroupAdmins", err)
                                                    })
                                                }
                                            }, function (err, result) {
                                                let { from_user, group, groupAdmins } = result;
                                                if (groupAdmins && groupAdmins.length > 0) {
                                                    groupAdmins.forEach(async elem => {
                                                        let sendObj = {
                                                            from_id: from_user.length > 0 ? from_user[0].id : '',
                                                            type: 'family',
                                                            propic: from_user.length > 0 ? from_user[0].propic : '',
                                                            message: `<b>${from_user[0].full_name}</b>  has requested to join  <b> ${group[0].group_name} </b> `,
                                                            category: 'request',
                                                            message_title: 'familheey',
                                                            type_id: group.length > 0 ? parseInt(group[0].id) : '',
                                                            link_to: group.length > 0 ? parseInt(group[0].id) : '',
                                                            to_id: parseInt(elem.id),
                                                            sub_type:'request'
                                                        }
                                                        let _user_notify = await ctx.call("db.users.find",{ query:{id:sendObj.to_id}, fields:["notification"] })
                                                        ctx.broker.call("notification.sendAppNotification", sendObj)
                                                        if(_user_notify.length>0 && _user_notify[0].notification){
                                                            _this.pushNotify(ctx,sendObj);
                                                        }
                                                    });
                                                }
                                            })
                                        })
                                        .catch((err) => reject(err))
                                    }
                                    else {
                                        reject("request already exists");
                                    }
                                })
                            }))

                }
                else {
                    response.data = { status: "PRIVATE_GROUP, Only Admin can add user" }
                    return response
                }
            }
        },
        /**
        * fetch groups for listing
        * in link a family
        */
        fetchLinkFamily: {
            params: {},
            handler(ctx) {
                var response = {}
                let { user_id, group_id } = ctx.params;
                return new Promise((resolve, reject) => {
                    ctx.params = { ...ctx.params, is_linkable:true }
                    ctx.broker.call("db.group_map.findGroupOfaUser", ctx.params)
                        .then(async (doc) => {
                            response.data = [];
                            if (doc.length > 0) {
                                //let linkableArr =  doc.filter(elem => elem.is_linkable);
                                //let link_grp = linkableArr.map(elem => elem.id);
                                let linkableArr =  doc;
                                var search = {
                                    group_id: group_id,
                                    user_id: user_id
                                }
                                let linked_Arr = await ctx.broker.call("db.group_map.fetchlinkedFamily", search)
                                linkableArr.map(elem => {
                                    elem['is_linked'] = '';
                                    linked_Arr.map(elem1 => {
                                        if (elem.id == elem1.to_group && elem1.from_group == group_id && elem1.status!='unlinked')
                                            elem['is_linked'] = elem1.status;
                                    })
                                });
                                response.data = linkableArr;
                            }
                            resolve(response);
                        })
                        .catch((err) => {
                            reject(err);
                        })
                })
            }
        },
        /*
         * @id - request ID
         * @user_id - user who sends the request
         * @group_id - group in which user requested to join
         * @responded_by - Admin/member based on the settings of group
         */
        approveRequest: {
            params: {
                id: "number",
                user_id: "number",
                group_id: "number",
                responded_by: "number"
            },
            handler: async (ctx) => {
                let { user_id, group_id, responded_by } = ctx.params;
                let groupSettings = await ctx.broker.call("db.groups.getGroupSettings", { group_id: parseInt(group_id) })
                console.log(groupSettings);
                let groupApprovalStatus = groupSettings.filter((el) => el.key === "MEMBER_APPROVAL");
                let status = groupApprovalStatus[0].value === "Any member" ? true : false;
                return status ? (new Promise((resolve, reject) => {
                    ctx.call("db.requests.update", {
                        ...ctx.params,
                        status: "accepted"
                    }).then(async (res) => {
                        await ctx.broker.call("groups.addMemberTogroup", { userId: user_id, groupId: group_id, from:'approveRequest' }
                        ).then(async (res2) => {
                            // Track user activities start
                            let trackerData = {
                                user_id : parseInt(user_id),
                                type : 'Family',
                                type_id : parseInt(group_id),
                                activity : 'added you to family'
                            };
                            ctx.broker.call('user_activities.trackUserActivities', trackerData);
                            // Track user activities end
                            // if(res){
                            let allMembers = await ctx.broker.call("groups.getAllMembers", { groupId: group_id });
                            allMembers = allMembers.filter((member) => member.id != user_id);
                            console.log("=========================");
                            console.log(allMembers);
                            //let emails = [];
                            /*allMembers.forEach(async each => {
                                emails.push(each.email);
                                each.phone && await ctx.broker.call("notification.sendSms", {
                                    message: "New member joined our familheey",
                                    PhoneNumber: each.phone,
                                    subject: "test"
                                });
                            })*/
                            // emails.length > 0 && await ctx.broker.call("notification.sendEmail", {
                            //     toAddresses: emails,
                            //     message: `New member joined our familheey`,
                            //     subject: "New member joined"
                            // });
                            // console.log(emails)
                            // console.log("=========================");
                            resolve(res2)
                            // }
                        }
                        ).catch(err => reject(err))

                    })
                        .catch((err) => reject(err));
                })) : (
                        new Promise((resolve, reject) => {
                            ctx.call("db.group_map.find", { query: { user_id: responded_by, group_id: group_id, type: "admin" } }).then(async (res) => {
                                let adminFlag = res.length > 0;
                                !adminFlag ? (reject({ code: "NOT_AUTHORISED" })) : (
                                    await ctx.call("db.requests.update", {
                                        ...ctx.params, status: "accepted"
                                    }).then(async (res2) => {
                                        await ctx.broker.call("groups.addMemberTogroup", { userId: user_id, groupId: group_id, from:'approveRequest' }
                                        ).then(async (res3) => {
                                            // Track user activities start
                                            let trackerData = {
                                                user_id : parseInt(user_id),
                                                type : 'Family',
                                                type_id : parseInt(group_id),
                                                activity : 'added you to family'
                                            };
                                            ctx.broker.call('user_activities.trackUserActivities', trackerData);
                                            // Track user activities end
                                            let allMembers = await ctx.broker.call("groups.getAllMembers", { groupId: group_id });
                                            allMembers = allMembers.filter((member) => member.id != user_id);
                                            console.log("=========================");
                                            console.log(allMembers);
                                            //let emails = [];
                                            /*allMembers.forEach(async each => {
                                                emails.push(each.email);
                                                await ctx.broker.call("notification.sendSms", {
                                                    message: "New member joined our familheey",
                                                    PhoneNumber: each.phone,
                                                    subject: "test"
                                                });
                                            })*/
                                            // emails.length > 0 && await ctx.broker.call("notification.sendEmail", {
                                            //     toAddresses: emails,
                                            //     message: `New member joined our familheey`,
                                            //     subject: "New member joined"
                                            // });
                                            // console.log(emails)
                                            // console.log("=========================");
                                            resolve(res3)
                                        }
                                        ).catch(err => reject(err))
                                    })
                                )
                            })
                        })
                    )

            }
        },
        rejectRequest: {
            params: {
                id: "number",
                user_id: "number",
                group_id: "number",
                responded_by: "number"
            },
            handler: (ctx) => {
                // let{userId,groupId} = ctx.params;
                return new Promise((resolve, reject) => {
                    ctx.call("db.requests.update", {
                        ...ctx.params,
                        status: "rejected"
                    }).then((res) => resolve(res))
                        .catch((err) => reject(err));
                })
            }
        },
        unfollowGroup: {
            params: {
                user_id: "string",
                group_id: "string"
            },
            handler(ctx) {
                let { user_id, group_id } = ctx.params;
                return new Promise((resolve, reject) => {
                    ctx.call("db.group_map.find", { query: ctx.params }).then((res) => {
                        // Track user activities start
                            let trackerData = {
                                user_id : parseInt(user_id),
                                type : 'Family',
                                type_id : parseInt(group_id),
                                activity : 'unfollow family'
                            };
                            ctx.broker.call('user_activities.trackUserActivities', trackerData);
                        // Track user activities end
                        let result = res.filter(r => r.user_id == user_id && r.group_id == group_id);
                        if (!result.length > 0) {
                            reject("NOT_FOUND");
                        }
                        else {
                            let id = result[0].id;
                            ctx.params.notifyType = 'unfollow';
                            ctx.params.toGroup = group_id;
                            ctx.params.member_type = 'admin';
                            this.appNotify(ctx, ctx.params)
                            ctx.call("db.group_map.update", { ...ctx.params, id, following: false }).then((res2) => {
                                resolve(res2);
                            })
                        }
                    }).catch((err) => {
                        reject(err)
                    })
                })
            }
        },
        /**
         * invite a user not in application to groioup
         */
        inviteViaSms: {
            params: {
                name: "string",
                user_id: "string",
                phone: "string",
                from_name: "string"
            },
            handler(ctx) {
                //let response = {};
                return new Promise((resolve, reject) => {
                    let { name, from_name, user_id, group_id } = ctx.params;
                    let from_user_id = ctx.params.user_id;
                    let PhoneNumber = ctx.params.phone ? ctx.params.phone : '';
                    let email = ctx.params.email ? ctx.params.email : '';
                    let urlencoded = `${process.env.SHARE_BASE_URL}page/groups/${group_id}`;

                    let user_data = {
                        from_id: from_user_id,
                        type: 'invite',
                        group_id: group_id,
                        email: email,
                        phone: PhoneNumber
                    }

                    let sendObj = {
                        from_id: parseInt(from_user_id),
                        type: 'family',
                        category: 'family',
                        message_title: 'familheey'
                    }

                    ctx.call('db.users.find', { query: { phone: PhoneNumber } })
                    .then((res) => {
                        if (res && res.length > 0) { //exist in application
                            ctx.call('db.group_map.find', { query: { user_id: res[0].id, group_id: group_id } })
                            .then(async(_doc) => {
                                if (_doc && _doc.length > 0) {
                                    reject("user already exists in group")
                                } else {
                                    user_data = { ...user_data, user_id:res[0].id }
                                    let findQ = { user_id:res[0].id, group_id:group_id, type:'invite' }
                                    let chckExist = await ctx.call("db.requests.find",{query:findQ, fields:["id"]})
                                    if(chckExist && chckExist.length>0){
                                        reject("An invitation already exist for this user.")
                                    } else{
                                        ctx.call('db.requests.create', user_data)
                                        .then(async(result) => {
                                            let fromUser = await ctx.call("db.users.find",{ query:{ id:from_user_id }, fields:["id","full_name","propic"] })
                                            let _Group = await ctx.call("db.groups.find",{ query:{ id:group_id }, fields:["id","group_name","logo"] })
                                            sendObj.propic = fromUser[0].propic 
                                            sendObj.message = fromUser.length > 0 ? `<b>${fromUser[0].full_name}</b> has invited you to join  <b>${_Group[0].group_name}</b>` : "";
                                            sendObj.to_id = parseInt(res[0].id);
                                            sendObj.type_id = parseInt(res[0].id);
                                            sendObj.link_to = parseInt(res[0].id);
                                            sendObj.type ='user';
                                            sendObj.sub_type ='request';
                                            ctx.broker.call("notification.sendAppNotification", sendObj);
                                            var user = await ctx.call("db.users.find", { query: { id: res[0].id }, fields: ["id","full_name","propic","notification"] })
                                            if(user.length>0 && user[0].notification){
                                                ctx.broker.call("userops.getregisterToken", { user_id: user[0].id, is_active:true })
                                                .then((_devices) => {
                                                    if(_devices.length>0) {
                                                        _devices.data.forEach(elem => {
                                                            sendObj.r_id = elem.id;
                                                            sendObj.token = elem.device_token;
                                                            sendObj.device_type = elem.device_type.toLowerCase();
                                                            sendObj.title = sendObj.message_title;
                                                            sendObj.body = sendObj.message.toString().replace( /(<([^>]+)>)/ig, '')
                                                            ctx.broker.call("notification.sendPushNotification", sendObj)
                                                        });   
                                                    }
                                                    // _devices.length>0 && _devices.data.forEach(elem => {
                                                    //     sendObj.r_id = elem.id,
                                                    //     sendObj.token = elem.device_token,
                                                    //     sendObj.device_type = elem.device_type.toLowerCase(),
                                                    //     sendObj.title = sendObj.message_title,
                                                    //     sendObj.body = sendObj.message.toString().replace( /(<([^>]+)>)/ig, '')
                                                    //     ctx.broker.call("notification.sendPushNotification", sendObj)
                                                    // });
                                                })
                                                .catch(e => e)
                                            }
                                            resolve(result);
                                        }).catch((err) => {
                                            reject(err);
                                        })
                                    }
                                }
                            })
                        } else {// not in application
                            ctx.broker.call('db.groups.find', { query: { id: group_id, is_active:true } })
                            .then(async(_group) => {
                                let { group_name, firebase_link } = _group[0];
                                user_data = { ...user_data, full_name:name, from_user: user_id, type:'group', type_value:'invite' }
                                let o_users = await ctx.broker.call("db.other_users.create", user_data)
                                urlencoded  = `${firebase_link}`;
                                if (PhoneNumber != '') {
                                    ctx.call('notification.sendSms', {
                                        message: `You are invited to join the ${group_name} family in Familheey. Download the app and stay connected with your family :) ${urlencoded}`,
                                        PhoneNumber: PhoneNumber,
                                        subject: `Invitation to join ${group_name} in Familheey!`
                                    }).then((res6) => { console.log(res6) }).catch((err) => { console.log(err) })
                                }
                                if (email != '') {
                                     
                                    let _message = `<html>
                                                        <body>
                                                            <table>
                                                                <tr>
                                                                    <td>Dear ${name},</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <b>${from_name}</b> has invited you to join the <b> ${group_name} </b> family in Familheey - The social networking app for families!<br/>
                                                                        <a href='${urlencoded}'>Download</a> the app and stay connected with your family :)<br/>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Best regards,</td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Team Familheey</td>
                                                                </tr>
                                                            </table>
                                                        </body>
                                                    </html>`;
                                    ctx.call('notification.sendEmail', {
                                        toAddresses: [email],
                                        message: _message,
                                        subject: `Invitation to join ${group_name} in Familheey!`
                                    }).then((res7) => { console.log(res7) }).catch((err) => { console.log(err) })    
                                }
                                resolve({code:200, message:"success"});
                            }).catch((err) => {
                                reject(err);
                            })
                        }
                    }).catch((err) => {
                        console.log(err);
                        reject({ data: err });
                    });
                });
            }
        },
        viewFamily: {
            handler(ctx) {
                let response = {};
                return new Promise((resolve, reject) => {
                    let { type } = ctx.params;
                    //let familyDetails = [];
                    let { user_id, query, event_id } = ctx.params;
                    let offsetval = ctx.params.offset ? ctx.params.offset : '';
                    let limitval = ctx.params.limit ? ctx.params.limit : '';
                    ctx.broker.call('db.groups.getFamily', { userId: user_id,event_id:event_id, type:type, txt: query, offset:offsetval,limit:limitval})
                    .then(async(res) => {
                        response.data = res;
                        let _pr = await ctx.broker.call('userops.pendingRequest', { user_id:user_id,type:"request" })
                        response = { ...response, pending_count:_pr.count }
                        resolve(response);
                    })
                    .catch((err) => {
                        console.log(err);
                        reject({ data: err });
                    });

                });
            }
        },
        viewMembers: {
            handler(ctx) {
                return new Promise(async(resolve, reject) => {
                    let memberDetails = [];
                    var memberarray = [];
                    let { group_id, crnt_user_id, query, filter, membership_id } = ctx.params;
                    let response = {}

                    let _group = await ctx.broker.call('db.groups.find', { query:{id: group_id},fields:['group_category'] })

                    ctx.broker.call('db.groups.getMembers', { groupid: group_id, current_id: crnt_user_id, txt: query, filter:filter,membership_id:membership_id })
                        .then((res) => {
                            let Admins = res.filter(r => r.user_type == 'admin');
                            let Adminlength = Admins.length;
                            memberDetails = res;
                            _async.each(memberDetails, (value, callback) => {
                                value.crnt_user_id = ctx.params.crnt_user_id;
                                value.group_category = (_group&&_group.length>0) ? _group[0].group_category : '';
                                ctx.broker.call('db.groups.getMemberDetails', value)
                                    .then((res2) => {
                                        value['relation_ship'] = (res2[0] ? res2[0]['case'] : '');
                                        value['relation_id'] = (res2[0] ? res2[0]['id'] : '');
                                        memberarray.push(value);
                                        callback();
                                    })
                                    .catch((err) => {
                                        console.log(err);
                                    });
                            }, (err) => {
                                if (err) {
                                    console.log('Failed to process ==> ', err);
                                    reject(err);
                                } else {
                                    console.log('Processed successfully ==> ', res);
                                    response.data = memberarray
                                    response.adminsCount = Adminlength;
                                    resolve(response);
                                }
                            });
                        })
                        .catch((err) => {
                            reject(err);
                        });
                });
            }
        },
        getFamilyById: {
            params: {
                user_id: "string",
                group_id: "string"
            },
            handler(ctx) {
                return new Promise((resolve, reject) => {
                    let { user_id, group_id } = ctx.params;
                    let response = {};
                    ctx.broker.call('db.groups.groupDetails', ctx.params)
                    .then((res) => {
                        if(res && res.length>0){
                            let { f_text } = res[0];
                            res[0].f_link = `${process.env.SHARE_BASE_URL}page/groups/${f_text}`;
                        } else {
                          response.data = []; 
                          resolve(response); 
                        }
                        response.data = res;
                        return ctx.broker.call("groups.viewFamilyDetails", { group_id: group_id, user_id: user_id })
                    })
                    .then(res => {
                        response.count = res.data.count;
                        response['data'][0].user_status = res.data.isMemberORAdmin;
                        response['data'][0].following = res.data.isFollowing;
                        return ctx.broker.call("groups.groupEvents", { group_id: group_id })
                    }).then(res => {
                        response.count = { ...response.count, event_count: res.data.event_invitation.length || 0 + res.data.event_share.length || 0 }
                        return ctx.broker.call("db.requests.find", { query: ctx.params, fields: ['status'] })
                    })
                    .then(res => {
                        if (res && res.length > 0) {
                            response['data'][0].joined_status = res[0].status;
                            resolve(response);
                        }
                        else return ctx.broker.call("db.group_map.find", { query: ctx.params, fields: ['id'] })
                    })
                    .then((res) => {
                        if (res && res.length > 0) response['data'][0].joined_status = 'accepted'
                        resolve(response);
                    })
                    .catch(err => {
                        response.err = err;
                        reject(response);
                    })
                })
            }
        },
        updateGroupSettings: {
            params: {
                group_id: "string",
                member_joining: "string",
                member_approval: "string",
                post_create: "string",
                post_approval: "string",
                post_visibilty: "string",
                link_family: "string",
                link_approval: "string"
            },
            handler: (ctx) => {
                // return ctx.params;
                let response = {};
                console.log(ctx.params)
                let { group_id, ...settingObject } = ctx.params;
                return new Promise((resolve, reject) => {
                    ctx.call("db.groups.update", { id: group_id, ...settingObject }).then(
                        (res) => {
                            response.data = [res]
                            resolve(response)
                        }
                    ).catch(
                        (err) => reject(err)
                    )
                });

            }
        },
        /**
         * request to familyLinks db {sendrequest}
         * default : pending
         * for linking the groups
         */
        requestToLinkFamily: {
            params: {},
            handler(ctx) {
                let toGroupArr = ctx.params.to_group;
                var responseArr = {}
                var isexistmsg = []; let count=0;
                return new Promise((resolve, reject) => {
                    let { requested_by } = ctx.params;
                    toGroupArr.forEach(async (item) => {
                        ctx.params.to_group = item;
                        let response = await ctx.call("db.familyLinks.find", { query: ctx.params }); //check already requested group
                        if (response && response.length > 0 && (response[0].status!='unlinked' || response[0].status!='rejected') ) {
                            isexistmsg.push({ "message": "link already exist" })
                        } else {
                            let searchQ = { id: parseInt(item) }
                            let group_is = await ctx.call("db.groups.find", { query: searchQ }); // else find the group details
                            if (group_is && group_is.length > 0 && group_is[0].is_linkable) { //check is_linkable enabled (T/F)
                                let save_data = {
                                    to_group: item,
                                    from_group: ctx.params.from_group,
                                    requested_by: ctx.params.requested_by
                                }
                                if (group_is[0].link_approval == 15) {
                                    save_data.status = "accepted";
                                }
                                if(group_is[0].link_approval == 16){
                                    let _admins = await ctx.broker.call('db.group_map.find',{query:{type:'admin' , group_id:group_is[0].id , is_blocked:false , is_removed:false}, fields:["user_id"]})
                                    ctx.params = { ...ctx.params, _admins:_admins, notifyType:'link_approval',user_id:requested_by}
                                    //if(requested_by != group_is[0].created_by) this.appNotify(ctx,ctx.params);
                                    //this.appNotify(ctx,ctx.params);
                                }
                                let searchAdmin = {
                                    group_id: item,
                                    user_id: ctx.params.requested_by
                                }
                                /** check the user is admin of coming group */
                                let searchIs = await ctx.call("db.group_map.find", { query: searchAdmin });
                                if (searchIs && searchIs.length > 0 && searchIs[0].type == "admin") {
                                    save_data.status = "accepted";
                                }
                                if (response && response.length > 0 && response[0].status=='unlinked') {
                                    save_data = { ...save_data, id:response[0].id }
                                    await ctx.call("db.familyLinks.update", save_data)
                                } else{
                                    await ctx.call("db.familyLinks.create", save_data);
                                }

                                console.log("== =save_data = == ", save_data)
                                

                                if (save_data.status!="accepted") {
                                    console.log("======== app notify =========")
                                    ctx.params = { ...ctx.params, ...save_data }
                                    this.appNotify(ctx,ctx.params);
                                }
                                console.log("======== after app notify =========")
                            } else { //linkable is disabled for that group
                                isexistmsg.push({ "message": "Linking not permitted." })
                            }
                        }
                        count++
                        console.log(count , toGroupArr.length)
                        if(count == toGroupArr.length){
                            responseArr.isexistmsg = isexistmsg;
                            resolve(responseArr)
                        }
                    });
                })
            }
        },

        /**
         * list all the request to admin page 
         * for linking groups request
         */
        adminListLinkRequest: {
            params: {
                group_id: "string"
            },
            handler(ctx) {
                return new Promise((resolve, reject) => {
                    var response = {}
                    ctx.call('db.familyLinks.adminListLinkRequest', ctx.params)
                        .then((requests) => {
                            response.requests = requests;
                            resolve(response)
                        })
                        .catch((err) => {
                            reject(err)
                        })

                })
            }
        },

        /**
         * Accept/Reject a linking request by admin
         */
        adminActionLinking: {
            params: {
                "id": "number"
            },
            handler(ctx) {
                let response = {}
                return new Promise((resolve, reject) => {
                    let {status} = ctx.params;
                    ctx.call('db.familyLinks.update', ctx.params)
                    .then(async(doc) => {
                        response.data = [doc];
                        if(status == "accepted"){
                            ctx.call('db.familyLinks.linkData', ctx.params)
                            .then((doc2)=>{
                                doc2[0]._admins = [{ user_id : doc2[0].user_id }];
                                doc2[0].notifyType = 'link-approved'
                                this.appNotify(ctx,doc2[0])
                            }).catch(e=>e)
                        }
                        resolve(response);
                    })
                    .catch((err) => {
                        console.log("== err ==")
                        console.log(err)
                        reject(err);
                    })
                })
            }
        },
        listAllMembers: {
            params: {
                client_id: "string",
                group_id: "string"
            },
            handler: async (ctx) => {
                let { client_id, group_id } = ctx.params;
                // return clientId
                //let flag = false;
                //let users = [];
                let usersInGroup = [];
                return new Promise(async (resolve, reject) => {
                    //let isUserAdmin = false;
                    //checking whether user is an admin
                    let group_map_fields = ["type"];
                    await ctx.call("db.group_map.find", { query: { user_id: client_id, group_id: group_id },fields:group_map_fields})
                    .then((res) => {
                        //res[0].type === "admin" && (flag = true);
                    })
                    .then(async () => {
                        let user_list_fields = ["id","full_name","propic","location","type","about","work"];
                        await ctx.call("db.users.list", { query: { is_active: true, id: { [Op.ne]: client_id } },fields:user_list_fields, pageSize: 100, page: 1, sort: "-id" }).then(async userSet => {
                            await ctx.call("db.group_map.find", { query: { group_id: group_id, is_blocked:false, is_removed:false } }).then(res => {
                                res.map(r => {
                                    usersInGroup.push(r.user_id)
                                })
                                //flag set as true if the user is already a member
                                userSet.rows.map(user => {
                                    usersInGroup.includes(user.id) ? (user.exists = true) : (user.exists = false)
                                })
                            }).then(async () => {
                                let groupSettings = await ctx.broker.call("db.groups.getGroupSettings", { group_id: parseInt(group_id) })
                                let groupJoinStatus = groupSettings.filter((el) => el.key === "MEMBER_JOINING");
                                userSet.rows.map(user => {
                                    groupJoinStatus[0].value === "Invitation only" ? (user.FLAG = false) : (user.FLAG = true)
                                })
                                let requestTable = await ctx.call("db.requests.find", { query: { status: "pending", group_id: group_id } })
                                let InvitationSet = [];
                                requestTable.map(el => InvitationSet.push(el.user_id))
                                userSet.rows.map((user,i) => {
                                    let { id } = user;
                                    var _index = usersInGroup.indexOf(id);
                                    if(_index != -1)userSet.rows.splice(i, 1);
                                    InvitationSet.includes(user.id) ? (user.invitation_status = true) : (user.invitation_status = false)
                                })
                                resolve(userSet)
                            })
                        })
                        // !flag && reject({ code: "NOT_AUTHORISED" });
                    })
                    .catch(err => reject(err))
                })

            }
        },
        /**
         * user accept a request to join to a family
         */
        invitationStatusbyUser: {
            params: {
                //id: "string",
                user_id: "string",
                group_id: "string",
                status: "string",
                from_id:"string"
            },
            handler(ctx){
                let response = {};
                let { user_id, status, group_id, from_id } = ctx.params;
                return new Promise(async(resolve, reject) => {
                    let isMember = await ctx.call("db.group_map.find",{ query:{ group_id:group_id, is_removed:false, is_blocked:false, user_id:user_id  }, fields:["id"] })
                    if(isMember.length>0){
                        ctx.call("db.requests.update_requests",{ user_id:user_id, group_id:group_id, status:'accepted' })
                        resolve("Already Member in this group!"); return;
                    }
                    if(!ctx.params.id || ctx.params.id == '') {
                        let updateId = await ctx.call("db.requests.find", {query:{ user_id:user_id,group_id:group_id},fields:["id"] });
                        ctx.params.id = updateId[0].id
                    }
                    ctx.call("db.requests.update", { ...ctx.params, responded_by: user_id })
                    .then(async res => {
                        ctx.call("db.requests.update_requests",{ user_id:user_id, group_id:group_id, status:status })
                        let updateData = {
                            user_id: res.user_id,
                            group_id: res.group_id,
                            following: true
                        }
                        // Default membership added start
                        let _Group = await ctx.broker.call("db.groups.find", { query:{ id:group_id  }, fields:["default_membership"] });
                        updateData.membership_period_type = "Life time";
                        let default_membership = parseInt(_Group[0].default_membership);
                        updateData.membership_id = _Group[0].default_membership ? default_membership : null
                        // Default membership added end
                        response.data = res;
                        if (res && status != 'rejected') {
                            var _type = 'create';
                            let isUserExist = await ctx.call("db.group_map.find", { query: { user_id: res.user_id,group_id: res.group_id }, fields:["id"] })
                            if(isUserExist && isUserExist.length>0){
                                _type = 'update';
                                updateData = { ...updateData, id: isUserExist[0].id, is_removed:false, is_blocked:false  }
                            }
                            ctx.call(`db.group_map.${_type}`, { ...updateData })
                            .then(async (result) => {
                                ctx.params = { ...ctx.params, to_group:group_id, notifyType:'invite_accept', requestSendBy:from_id }
                                this.appNotify(ctx, ctx.params);
                                resolve(response)
                            });
                        }else{
                            resolve({code:200, message:"rejected", data:[]})
                        }
                    })
                    .catch(err => reject(err))
                })
            }
        },
        listallInvitation: {
            params: {
                user_id: "string"
            },
            handler: (ctx) => {
                let response = {}
                return new Promise((resolve, reject) => {
                    ctx.broker.call("db.groups.listinvitation", ctx.params)
                        .then((res) => {
                            response.data = res;
                            resolve(response);
                        })
                        .catch((err) => {
                            reject(err);
                        })
                })
            }
        },
        sendInvitation: {
            params: {
                user_id: "string",
                from_id: "string",
                group_id: "string"
            },
            handler(ctx){
                let _this = this;
                let { user_id, from_id, group_id } = ctx.params;
                return new Promise((resolve, reject) => {
                    ctx.broker.call("db.events.invitationUsers", ctx.params)
                        .then(async (doc) => {
                            let sendObj = {
                                from_id: from_id,
                                type: 'user',
                                propic: doc.length > 0 ? doc[0].propic : '',
                                message: doc.length > 0 ? `<b>${doc[0].full_name}</b> has invited you to join  <b>${doc[0].group_name}</b>` : "",
                                category: 'invitation',
                                message_title: 'familheey',
                                type_id: parseInt(user_id),
                                link_to:parseInt(user_id),
                                to_id: parseInt(user_id),
                                sub_type:'request',
                                group_id:group_id
                            }
                            let _user_notify = await ctx.call("db.users.find",{ query:{id:sendObj.to_id}, fields:["notification"] })
                            ctx.broker.call("notification.sendAppNotification", sendObj)
                            if(_user_notify.length>0 && _user_notify[0].notification){
                                _this.pushNotify(ctx,sendObj)
                            }
                            return ctx.call("db.requests.create", {
                                ...ctx.params, type: "invite", status: "pending"
                            })
                        })
                        .then((res) => {
                            resolve(res)
                            // Track user activities start
                            let trackerData = {
                                user_id : parseInt(from_id),
                                type : 'Event',
                                type_id : parseInt(group_id),
                                sub_type : 'user',
	                            sub_type_id : parseInt(user_id),
                                activity : 'sent event invitation'
                            };
                            ctx.broker.call('user_activities.trackUserActivities', trackerData);
                            // Track user activities end
                        }).catch(err => reject(err))
                })
            }
        },
        /**
         * update a group details of a user
         */
        updateGroupmaps: {
            params: {},
            handler(ctx) {
                var response = {}
                return new Promise((resolve, reject) => {
                    ctx.call("db.group_map.update", ctx.params)
                        .then((doc) => {
                            response.data = [doc];
                            resolve(response);
                        })
                        .catch((err) => {
                            console.log("== err ==", err)
                            reject(err);
                        })
                });
            }
        },
        followGroup: {
            params: {
                user_id: "string",
                group_id: "string"
            },
            handler(ctx) {
                let { user_id, group_id } = ctx.params;
                return new Promise((resolve, reject) => {
                    ctx.call("db.group_map.find", { query: ctx.params }).then((res) => {
                        let result = res.filter(r => r.user_id == user_id && r.group_id == group_id);
                        if (!result.length > 0) {
                            reject("NOT_FOUND");
                        }
                        else {
                            let id = result[0].id;
                            ctx.params.notifyType = 'follow';
                            ctx.params.toGroup = group_id;
                            ctx.params.member_type = 'admin';
                            this.appNotify(ctx, ctx.params)
                            ctx.call("db.group_map.update", { ...ctx.params, id, following: true }).then((res2) => {
                                resolve(res2);
                                // Track user activities start
                                let trackerData = {
                                    user_id : parseInt(user_id),
                                    type : 'Family',
                                    type_id : parseInt(group_id),
                                    activity : 'follow family'
                                };
                                ctx.broker.call('user_activities.trackUserActivities', trackerData);
                                // Track user activities end
                            })
                        }
                    }).catch((err) => {
                        reject(err)
                    })
                })
            }
        },
        getAllGroupsBasedOnUserId: {
            params: {
                user_id: "string",
                member_to_add: "string"
            },
            handler: (ctx) => {
                let userGroup = [];
                let memberGroup = [];
                let groupList = {}
                let { user_id, member_to_add, query } = ctx.params;
                return new Promise((resolve, reject) => {
                    ctx.call("groups.viewFamily", { user_id: user_id, query:query})
                    .then(userGroups => {
                        groupList = { ...userGroups }
                        userGroups.data.map((group) => {
                            userGroup.push(group.id);
                        })
                    }).then(async () => {
                        await ctx.call("groups.viewFamily", { user_id: member_to_add })
                        .then(memberGroups => {
                            memberGroups.data.map((group) => {
                                memberGroup.push(group.id)
                            })
                        }).then(() => {
                            memberGroup.map(gid => {
                                userGroup = userGroup.filter(id => id != gid)
                            })
                        })
                        return ctx.call("db.requests.ListRequest", { status:'pending',  user_id:member_to_add })
                    })
                    .then((pend_req)=>{
                        let _g_id = pend_req.map(v=>v.group_id);
                        groupList.data.map((groups) => {
                            !userGroup.includes(groups.id) ? (groups.JOINED = true) : (groups.JOINED = false);
                            _g_id.includes(groups.id) ? (groups.status = 'pending') : (groups.status = '')
                        });
                        resolve({ data: groupList.data })
                    })
                    .catch((err) => reject(err))
                })
            }
        },
        viewFamilyDetails: {
            params: {
                group_id: "string",
                user_id: "string"
            },
            handler: (ctx) => {
                let response = {};
                let { group_id, user_id } = ctx.params;
                // return ctx.params;
                return new Promise((resolve, reject) => {
                    _async.parallel({
                        familyMembers: (callback) => {
                            ctx.call("db.groups.getMembers", {
                                groupid: group_id,
                                current_id: user_id
                            }).then(res => {
                                callback(null, res.length);
                            }).catch(err => {
                                console.log("error happened -------- familyMembers", err);
                            })
                        },
                        knownConnections: (callback) => {
                            ctx.call("db.group_map.getKnownConnections", { user_id: user_id, group_id: group_id }).then(res => {
                                callback(null, res);
                            }).catch(err => {
                                console.log("error happened -------- knownConnections", err);
                            })
                        },
                        isMemberORAdmin: (callback) => {
                            ctx.call("db.group_map.isMemberORAdmin", { user_id: parseInt(user_id), group_id: parseInt(group_id) }).then(res => {
                                callback(null, res)
                            }).catch(err => {
                                console.log("error happened -------- isMemberORAdmin", err)
                            })
                        },
                        isFollowing: (callback) => {
                            ctx.call("db.group_map.find", { query: { user_id: user_id, group_id: group_id }, fields: ['following'] }).then(res => {
                                callback(null, res)
                            }).catch(err => {
                                console.log("error happened -------- isFollowing", err)
                            })
                        },
                        post_count: (callback) => {
                            ctx.call("db.post.countPostByGroup", { to_group_id: group_id, type:'post', is_active:true  })
                            .then(res => {
                                callback(null, res.length>0?res[0].count:0)
                            }).catch(err => {
                                console.log("error happened -------- post_count", err)
                            })
                        },
                        announcement_count: (callback) => {
                            ctx.call("db.post.countPostByGroup", { to_group_id: group_id, type:'announcement', is_active:true  })
                            .then(res => {
                                callback(null, res.length>0?res[0].count:0)
                            }).catch(err => {
                                console.log("error happened -------- post_count", err)
                            })
                        },
                        album_count:(callback)=>{
                            ctx.call("db.group_map.find",{ query:{ user_id:user_id, group_id:group_id, is_blocked:false, is_removed:false}, fields:['id'] })
                            .then(res => {
                                var _params = {  folder_type: 'albums', txt: '', user_id: user_id, group_id: group_id, folder_for: 'groups' }
                                _params.is_member = (res && res.length>0) ? true:false;
                                return ctx.call("db.folders.listFoldersCount", _params)
                            })
                            .then(folders=>{
                                callback(null, folders[0].count);
                            })
                            .catch(e=>e)
                        },
                        document_count:(callback)=>{
                            ctx.call("db.group_map.find",{ query:{ user_id:user_id, group_id:group_id, is_blocked:false, is_removed:false}, fields:['id'] })
                            .then(res => {
                                var _params = {  folder_type: 'documents', txt: '', user_id: user_id, group_id: group_id, folder_for: 'groups' }
                                _params.is_member = (res && res.length>0) ? true:false;
                                return ctx.call("db.folders.listFoldersCount", _params)
                            })
                            .then(folders=>{
                                callback(null, folders[0].count);
                            })
                            .catch(e=>e)
                        }

                    }, function (err, res) {
                        if(err) {
                            console.log("error happened");    
                        } else {
                            response.data = {
                                count: {
                                    familyMembers: parseInt(res.familyMembers),
                                    knownConnections: parseInt(res.knownConnections[0].count),
                                    post_count: parseInt(res.post_count),
                                    announcement_count: parseInt(res.announcement_count),
                                    album_count: parseInt(res.album_count),
                                    document_count: parseInt(res.document_count)
                                },
                                isMemberORAdmin: (res.isMemberORAdmin.length>0 && res.isMemberORAdmin[0].user_type ) ? res.isMemberORAdmin[0].user_type : 'not-member',
                                isFollowing: res.isFollowing[0] ? res.isFollowing[0].following : null
                            };
                            resolve(response);    
                        }
                    })
                })
            }
        },
        /**
         * get the list of all linked family
         */
        listLinkedFamily: {
            params: {
                group_id: "string"
            },
            handler(ctx) {
                return new Promise((resolve, reject) => {
                    var response = {}
                    ctx.call("db.familyLinks.listLinkedFamily", ctx.params)
                        .then((res) => {
                            response.data = res;
                            resolve(response)
                        }).catch((err) => {
                            reject(err)
                        })
                })
            }
        },
        /**
         * update a family link data like 
         * link/unlink a linked family from group
         */
        actionLinkedFamily: {
            params: {
                id: "string"
            },
            handler(ctx) {
                let { id,status } = ctx.params;
                return new Promise((resolve, reject) => {
                    var response = {}
                    ctx.call("db.familyLinks.update", ctx.params)
                    .then((res) => {
                        response.data = res;
                        if(status == 'unlinked') ctx.call("db.familyLinks.remove",{ id: id })
                        resolve(response)
                    }).catch((err) => {
                        reject(err)
                    })
                })
            }
        },
        /**
         * get all members based on a user joined groups
         */
        userGroupMembers: {
            params: {
                user_id: "string"
            },
            handler(ctx) {
                return new Promise((resolve, reject) => {
                    var response = {}
                    ctx.call("db.group_map.userGroupMembers", ctx.params)
                        .then((res) => {
                            response.data = res;
                            resolve(response)
                        }).catch((err) => {
                            reject(err)
                        })
                })
            }
        },

        groupEvents: {
            params: {
                group_id: "string"
            },
            handler(ctx) {
                return new Promise((resolve, reject) => {
                    var response = {}
                    _async.parallel({
                        event_share: (callback) => {
                            ctx.call("db.events_share.sharedToGroup", ctx.params)
                                .then(res => {
                                    callback(null, res);
                                }).catch(err => {
                                    console.log("error happened -------- event_share", err);
                                })
                        },
                        event_invitation: (callback) => {
                            ctx.call("db.events_invitation.invitedToGroup", ctx.params)
                                .then(res => {
                                    callback(null, res);
                                }).catch(err => {
                                    console.log("error happened -------- event_invitation", err);
                                })
                        }
                    }, function (err, results) {
                        // response.data = results
                        let invitation_ = results.event_invitation;
                        let invitation_ids = [];
                        invitation_.map(item=>{
                            invitation_ids.push(item.event_id)
                        })
                        let event_share_ = results.event_share.filter(item=>!invitation_ids.includes(item.event_id));
                        response.data = {
                                        event_share:event_share_,
                                        event_invitation:results.event_invitation
                                    }
                        err ? reject(err) : resolve(response)
                    })
                })
            }
        },
        /** 
         * user leave from a family 
         * */
        leaveFamily: {
            params: {
                group_id: "string",
                user_id: "string"
            },
            handler: (ctx) => {
                let response = {};
                let { group_id, user_id } = ctx.params;
                return new Promise((resolve, reject) => {
                    ctx.call("db.group_map.find", { query: { user_id: user_id, group_id: group_id } }).then(res => {
                        let flag = res.length > 0;
                        if(flag) {
                            ctx.call("db.group_map.update", { id: res[0].id, ...ctx.params, is_removed: true }).then(res2 => {
                                response.data = res2;
                                resolve(response);
                                // Track user activities start
                                let trackerData = {
                                    user_id : parseInt(user_id),
                                    type : 'Family',
                                    type_id : parseInt(group_id),
                                    activity : 'leave from family'
                                };
                                ctx.broker.call('user_activities.trackUserActivities', trackerData);
                                // Track user activities end
                            })    
                        } else {
                            response.err = "NOT_A_MEMBER";
                            reject(response);
                        }

                        // flag ? (
                        //     ctx.call("db.group_map.update", { id: res[0].id, ...ctx.params, is_removed: true }).then(res => {
                        //         response.data = res;
                        //         resolve(response);
                        //         // Track user activities start
                        //         let trackerData = {
                        //             user_id : parseInt(user_id),
                        //             type : 'Family',
                        //             type_id : parseInt(group_id),
                        //             activity : 'leave from family'
                        //         };
                        //         ctx.broker.call('user_activities.trackUserActivities', trackerData);
                        //         // Track user activities end
                        //     })
                        // ) : (
                        //         response.err = "NOT_A_MEMBER",
                        //         reject(response))
                    }).catch(err => {
                        response.err = err;
                        reject(response)
                    })
                })
            }
        },
        getMutualConnections: {
            params: {
                user_one_id: "string",
                user_two_id: "string"
            },
            handler: (ctx) => {
                let response = {};
                return new Promise((resolve, reject) => {
                    ctx.call("db.group_map.getMutualConnections", ctx.params)
                        .then(res => {
                            response.data = res;
                            resolve(response)
                        })
                        .catch((err) => reject(err))
                })
            }
        },

        getMutualConnectionsList: {
            params: {
                user_one_id: "string",
                user_two_id: "string"
            },
            handler: (ctx) => {
                let response = {};
                return new Promise((resolve, reject) => {
                    ctx.call("db.group_map.getMutualConnectionsList", ctx.params)
                        .then(res => {
                            response.data = res;
                            resolve(response)
                        })
                        .catch((err) => reject(err))
                })
            }
        },
        /**
         * based on group , a users mutual connections
         */
        getFamilyMutualList:{
            params: {
                user_id: "string",
                group_id: "string"
            },
            handler: (ctx) => {
                let response = {};
                return new Promise((resolve, reject) => {
                    ctx.call("db.groups.getFamilyMutualList", ctx.params)
                    .then(res => {
                        response.data = res;
                        resolve(response)
                    })
                    .catch((err) => reject(err))
                })
            }
        },
        /**
         * check family link exist/not
         */
        familyLinkExist:{
            params:{
                f_text:"string"
            },
            handler(ctx){
                let response = {};
                //let { f_text } = ctx.params;
                return new Promise((resolve, reject) => {
                    ctx.call("db.groups.count", {query:ctx.params})
                    .then(res => {
                        response = { count:res, code:200 }
                        resolve(response)
                    })
                    .catch((err) => reject(err))
                })
            }
        },

         /**
         * get the details of a group
         * same as  getMutualFamilies but with details
         */
        userMutualGroup:{
            params:{
                user_id_one:'string',
                user_id_two:'string'
            },
            handler(ctx){
                let { user_id_one, user_id_two } = ctx.params
                let response = {};
                return new Promise((resolve, reject) => {
                    ctx.call("db.group_map.userMutualGroup", { user_id_one: user_id_one, user_id_two: user_id_two })
                    .then(res => {
                        response.data  = res;
                        resolve({code:200, ...response})
                    })
                    .catch(err => reject(err))
                })
            }
        },

        checkUserGroups:{
            params: {
                user_id: "string"
            },
            handler: (ctx) => {
                //let response = {};
                return new Promise((resolve, reject) => {
                    ctx.call("db.group_map.count", { query: { user_id: ctx.params.user_id, is_blocked: false, is_removed: false } })
                    .then(res => {
                        resolve({res,code:200})
                    })
                    .catch((err) => reject(err))
                })
            }
        },
        /**
         * remove request from db
         */
        cancelRequest:{
            params:{
                id:"string"
            },
            handler(ctx){
                return new Promise((resolve, reject) => {
                    ctx.call("db.requests.remove", ctx.params)
                    .then(res => {
                        resolve({...res,code:200})
                    })
                    .catch((err) => reject(err))
                })

            }
        },

        /**
         * add a user directly to group maps(into a group)
         */
        addUserdirectlytoGroup:{
            params:{ },
            handler(ctx){
                return new Promise((resolve, reject) => {
                    ctx.call('db.group_map.find',{ query:ctx.params })
                    .then((isExist)=>{
                        if(isExist && isExist.length>0){
                            if(isExist[0] && isExist[0].is_removed == true) {
                                isExist[0].is_removed = false;
                                ctx.call('db.group_map.update', { id:isExist[0].id, is_removed:false }).then((res) => {
                                    resolve(isExist[0]);
                                }); 
                            } else if(isExist[0] && isExist[0].is_blocked == true){
                                resolve({error:1,message:"You are blocked from this family"});
                            } else {
                                resolve({error:1,message:"you are already a member of this family"});
                            }
                        } else { 
                            return ctx.call("db.group_map.create",ctx.params).then(res => {
                                resolve(res)
                            })
                        }
                    }).catch((err) => {
                        reject(err);
                    });
                })
            }
        },

        getSuggestedGroupNewUser: {
            params:{ 
                user_id: "string"
            }, 
            handler(ctx){
                return new Promise((resolve, reject) => {
                    ctx.call("db.groups.getSuggestedGroupNewUser", ctx.params)
                    .then(res => {
                        resolve({data:res,code:200})
                    })
                    .catch((err) => reject(err))
                })
            }  
        },
        getUserAdminGroups: {
            params:{ 
                user_id: "string"
            },
            handler(ctx){
                return new Promise((resolve, reject) => {
                    ctx.call("db.groups.getUserAdminGroups", ctx.params)
                    .then(res => {
                        resolve({data:res,code:200})
                    })
                    .catch((err) => reject(err))
                })
            }     
        },
        groupMapUpdate: {
            params:{ 
                id: "string"
            },
            handler(ctx){
                return new Promise((resolve, reject) => {                    
                    ctx.call("db.group_map.find", { query:{id:ctx.params.id}, fields:['user_id','group_id','membership_total_payed_amount','membership_fees','membership_ref_id'] })
                    .then(getRes => {
                        if(getRes && getRes.length > 0) {
                            var { group_id, membership_total_payed_amount } = getRes[0];
                            
                            //Payment calculations start
                            membership_total_payed_amount = membership_total_payed_amount ? parseInt(membership_total_payed_amount) : 0; 
                            ctx.params.membership_payed_amount = ctx.params.membership_payed_amount ? parseInt(ctx.params.membership_payed_amount) : 0;

                            if(getRes[0].membership_fees == '' || getRes[0].membership_fees == null) {
                                getRes[0].membership_fees = 0;   
                            }
                            let totalPayedAmount = membership_total_payed_amount + ctx.params.membership_payed_amount;
                            let outstandingAmount = getRes[0].membership_fees - totalPayedAmount;
                            ctx.params.membership_total_payed_amount = totalPayedAmount;
                            ctx.params.membership_fees = getRes[0].membership_fees;   
                            //Payment calculations end

                            //Create member ship id if it is null start
                            if(!getRes[0].membership_ref_id || getRes[0].membership_ref_id == null || getRes[0].membership_ref_id == '') {
                                ctx.params.membership_ref_id = unicKeyGen(10,group_id);
                            }
                            //Create member ship id if it is null end

                            let email_data = {
                                payment_type: ctx.params.membership_payment_type,
                                payment_method: ctx.params.membership_payment_method,
                                outstanding_amount:outstandingAmount,
                                payed_amount: ctx.params.membership_payed_amount,
                                total_payed_amount: ctx.params.membership_total_payed_amount
                            }

                            ctx.params = { ...ctx.params, email_data:email_data }
                            
                            if(outstandingAmount <= 0) {
                                //outstandingAmount = 0;
                                //ctx.params.membership_payment_status = 'Completed';   
                            } else {
                                ctx.params.membership_payment_status = 'Partial';
                                ctx.params = { ...ctx.params, Enotify_type:'partial_payment', Enotify_subtype:ctx.params.membership_payment_status } 
                                ctx.call("common_methods.paymentNotifyEmail",ctx.params)
                            }

                            //Update payment start
                            ctx.call("db.group_map.update", ctx.params)
                            .then(res => {
                                resolve(res);

                                //Add payment history start
                                let historyObj = {
                                    user_id : res.user_id ? parseInt(res.user_id) : null,
                                    group_id : res.group_id ? parseInt(res.group_id) : null,
                                    membership_payed_amount : ctx.params.membership_payed_amount,
                                    membership_id : res.membership_id ? parseInt(res.membership_id) : null,
                                    membership_from : res.membership_from ? parseInt(res.membership_from) : null,
                                    membership_to : res.membership_to ? parseInt(res.membership_to) : null,
                                    membership_fees : res.membership_fees ? parseInt(res.membership_fees) : null,
                                    membership_paid_on : res.membership_paid_on ? parseInt(res.membership_paid_on) : null,
                                    membership_duration : res.membership_duration ? parseInt(res.membership_duration) : null,
                                    membership_payment_status : res.membership_payment_status ? res.membership_payment_status : '',
                                    membership_period_type : res.membership_period_type ? res.membership_period_type : '',
                                    membership_payment_notes : res.membership_payment_notes ? res.membership_payment_notes : null,
                                    membership_customer_notes : res.membership_customer_notes ? res.membership_customer_notes : '',
                                    payment_object : res.payment_object ? res.payment_object : '',
                                    membership_payment_type : res.membership_payment_type ? res.membership_payment_type : '',
                                    membership_payment_method : res.membership_payment_method ? res.membership_payment_method : '',
                                    membership_total_payed_amount : res.membership_total_payed_amount ? parseInt(res.membership_total_payed_amount) : 0,
                                    membership_ref_id : res.membership_ref_id ? res.membership_ref_id : '',
                                    membership_updated_by : ctx.params.membership_updated_by ? ctx.params.membership_updated_by : ''
                                }
                                ctx.call("db.membership_payment_history.create", historyObj)
                                .then(res2 => {
                                    console.log('Successfully added to payment history');
                                })
                                .catch((err) => console.log('membership payment history has some error'))
                                //Add payment history end

                                //Notification to admins start
                                ctx.params.notifyType = "membership_payment";
                                ctx.params.user_id = ctx.params.user_id ? ctx.params.user_id : res.user_id;
                                ctx.params.to_group = res.group_id;
                                if(ctx.params.membership_payment_type && ctx.params.membership_payment_type != '') {
                                    ctx.call("db.group_map.find", { query:{group_id:res.group_id, is_blocked:false, is_removed:false,type:'admin'}, fields:['user_id'] })
                                    .then(userData => {
                                        if(userData && userData.length > 0) {
                                            ctx.params._admins = userData;
                                            this.appNotify(ctx, ctx.params)
                                        }
                                    })
                                    .catch((err) => console.log('Notification has some error',err))   
                                }
                                //Notification to admins end
                            })
                            .catch((err) => reject(err))
                            //Update payment end
                        }
                    })
                    .catch((err) => reject(err))
                })
            }
        },
        get_other_invited_users_by_group: {
            params:{ 
                group_id: "string"
            },
            handler(ctx){
                return new Promise((resolve, reject) => {
                    ctx.call("db.groups.get_other_invited_users_by_group", ctx.params)
                    .then(res => {
                        resolve(res);
                    })
                    .catch((err) => reject(err));
                })
            }     
        },
        get_notification_group_data:{
            params:{ 
                group_id: "string"
            },
            handler(ctx){
                return new Promise((resolve, reject) => {
                    ctx.call("db.groups.get_notification_group_data", ctx.params)
                    .then(res => {
                        resolve(res);
                    })
                    .catch((err) => reject(err));
                })
            }     
        },
    },
    events: {},
    methods: {
        /**
        * call s3 service for fiel upload
        */
        calls3(ctx, params) {
            ctx.call('s3upload.fileUpload', params)
                .then((res) => {
                    console.log("== s3uplod success ==", res);
                })
                .catch((err) => {
                    console.log("== s3uplod success ==", err);
                })
        },

        appNotify(ctx, params) {
            let _this = this;
            let { from_group, to_group, user_id, notifyType, _admins, member_type, requestSendBy, membership_payment_type } = params;
            _async.parallel({
                from_user: (callback) => {
                    ctx.call("db.users.find", { query: { id: user_id }, fields: ["email", "full_name", "propic"] }).then(res => {
                        callback(null, res);
                    }).catch(err => {
                        console.log("error happened -------- familyMembers", err);
                    })
                },
                fromGroup: (callback) => {
                    if (from_group)
                        ctx.call("db.groups.find", { query: { id: from_group }, fields: ["id", "group_name", "logo"] }).then(res => {
                            callback(null, res);
                        }).catch(err => {
                            console.log("error happened -------- familyMembers", err);
                        })
                    else callback(null);

                },
                toGroup: (callback) => {
                    if (to_group)
                        ctx.call("db.groups.find", { query: { id: to_group }, fields: ["id", "group_name", "logo"] }).then(res => {
                            callback(null, res);
                        }).catch(err => {
                            console.log("error happened -------- familyMembers", err);
                        })
                    else
                        callback(null, []);

                },
                group_members: (callback) => {
                    if(to_group && !_admins){
                        var searchQ = { group_id: to_group }
                        if(member_type) searchQ = { ...searchQ, type:member_type }
                        ctx.call("db.group_map.fetchGroup_members", searchQ)
                        .then(res => {
                            callback(null, res)
                        }).catch(err => {
                            console.log("error happened -------- fetchGroupAdmins", err)
                        })
                    }
                    else if (_admins) callback(null, _admins)
                    else callback(null, [])
                }
            }, function (err, result) {

                let { fromGroup, toGroup, group_members,from_user } = result;

                if (group_members && group_members.length > 0) {
                    
                    group_members.forEach(async elem => {
                        let sendObj = {
                            from_id: from_user.length > 0 ? from_user[0].id : '',
                            type: 'family',
                            propic: fromGroup && fromGroup.length > 0 ? fromGroup[0].logo : '',
                            type_id: toGroup.length > 0 ? parseInt(toGroup[0].id) : '',
                            link_to: toGroup.length > 0 ? parseInt(toGroup[0].id) : '',
                            to_id: elem.id ? parseInt(elem.id) : ''
                        }
                        switch (notifyType) {
                            case 'follow':
                            sendObj.message = `<b>${from_user[0].full_name}</b> has follow your family  <b>${toGroup[0].group_name}</b>`
                            sendObj.category = 'follow';
                            sendObj.message_title ='familheey';
                            break;
                            case 'unfollow':
                            sendObj.message = `<b>${from_user[0].full_name}</b> has unfollow your family  <b>${toGroup[0].group_name}</b>`
                            sendObj.category = 'unfollow';
                            sendObj.message_title = 'familheey';
                            break;
                            case 'delete':
                                sendObj.propic = from_user && from_user.length > 0 ? from_user[0].propic : ''
                                sendObj.message = `<b>${from_user[0].full_name}</b> has deleted family  <b>${toGroup[0].group_name}</b>`
                                sendObj.category = 'delete';
                                sendObj.message_title = 'familheey';
                            break;
                            case 'invite_accept':
                                sendObj.sub_type = '';
                                sendObj.category = 'family';
                                sendObj.message_title = 'familheey';
                                sendObj.propic = from_user && from_user.length > 0 ? from_user[0].propic : ''
                                sendObj.message = ( elem.id == requestSendBy) ?  `<b>${from_user[0].full_name}</b> has accepted your request to join <b>${toGroup[0].group_name}</b>` : `<b>${from_user[0].full_name}</b> has joined <b>${toGroup[0].group_name}</b>`
                            break;
                            case "link_approval":
                                sendObj.from_id = fromGroup[0].id;
                                sendObj.to_id = parseInt(elem.user_id);
                                sendObj.sub_type = 'family_link';
                                sendObj.category = 'family';
                                sendObj.message_title = 'familheey';
                                sendObj.propic = from_user && from_user.length > 0 ? from_user[0].propic : ''
                                sendObj.message = `<b>${from_user[0].full_name}</b> has requested to link <b>${fromGroup[0].group_name}</b> with <b>${toGroup[0].group_name}</b>`
                            break;

                            case "link-approved":
                                sendObj.to_id = parseInt(elem.user_id);
                                sendObj.sub_type = '';
                                sendObj.category = 'family';
                                sendObj.message_title = 'familheey';
                                sendObj.propic = from_user && from_user.length > 0 ? from_user[0].propic : ''
                                sendObj.message = `Your request to link ${fromGroup[0].group_name} with ${toGroup[0].group_name} was approved`
                            break;
                            case 'membership_payment':
                                sendObj.message  = `${from_user[0].full_name} have paid the membership via ${membership_payment_type}`;
                                sendObj.sub_type = 'member';
                                sendObj.type = 'family';
                                sendObj.to_id = parseInt(elem.user_id);
                            break;
                            default:
                            break;
                        }

                        let _user_notify = await ctx.call("db.users.find",{ query:{id:sendObj.to_id}, fields:["notification"] })
                        console.log(" ###### _user_notify ######3 ", _user_notify)
                        console.log(user_id , elem.id)
                        console.log("#############################")
                        console.log("== sendObj=== ", sendObj)
                        if(_user_notify.length>0 && _user_notify[0].notification){
                            ctx.broker.call("notification.sendAppNotification", sendObj)
                            if(user_id != elem.id) {
                                _this.pushNotify(ctx,sendObj)
                            }
                        }
                    });
                }
            });
        },
        makeFamilyText(length) {
            var result = '';
            var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
            var charactersLength = characters.length;
            for (var i = 0; i < length; i++) {
                result += characters.charAt(Math.floor(Math.random() * charactersLength));
            }
            return result;
        },

        pushNotify(ctx,params){
            ctx.broker.call("userops.getregisterToken", { user_id: params.to_id, is_active:true })
            .then((_devices) => {
                _devices.data.forEach(elem => {
                    params = {
                        ...params,
                        r_id: elem.id,
                        token: elem.device_token,
                        device_type: elem.device_type.toLowerCase(),
                        title: params.message_title,
                        body: this.removeTags(params.message),
                        sub_type:params.sub_type ? params.sub_type : ''
                    }
                    ctx.broker.call("notification.sendPushNotification", params)
                });
            })
            .catch(e => e)
        },
        removeTags(str){ 
            if ((str===null) || (str==='')) return '';
            else return str.toString().replace( /(<([^>]+)>)/ig, '');
        },

        /**
         * generate firebase URl for deeplinking
         * update groups table
         */
        generatefirebaseLink(ctx,doc){
            let { id, type } = doc;
            var gId = CryptoJS.AES.encrypt(JSON.stringify({ id }), key).toString();
                gId = encodeURIComponent(gId);
            let f_link = `${process.env.FRONTEND_BASE_URL}familyview/${gId}?type="${type}"&type_id=${id}`;

            var options = {
                'method': 'POST',
                'url': `https://firebasedynamiclinks.googleapis.com/v1/shortLinks?key=${process.env.FIREBASE_KEY}`,
                'headers': {
                  'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    "dynamicLinkInfo":{
                        "domainUriPrefix":`${process.env.FIREBASE_DOMAIN_URI}`,
                        "link":`${f_link}`,
                        "androidInfo":{
                            "androidPackageName":`${process.env.ANDROID_PACKAGE_NAME}`
                        },
                        "iosInfo":{
                            "iosBundleId":`${process.env.IOS_PACKAGE_NAME}`
                        }
                    },
                    "suffix": {
                        "option": "UNGUESSABLE" 
                    }
                })
            };
            rq(options, function (error, response) {
                if (error) throw new Error(error);
                else{
                    let { shortLink } = JSON.parse(response.body);
                    ctx.call('db.groups.update',{ id:id, firebase_link:shortLink })
                }
            });
        },

        /**
         * function to add memeber to group
         * userId from array & string 
         */
        addMemberTogroupFn(ctx) {
            return new Promise(async(resolve, reject) => {
                if (!ctx.params.from_id) { ctx.params.from_id = ctx.params.userId }
                let { userId, groupId, from_id, from } = ctx.params;
                let _this = this;
                let sendObj = {
                    from_id: from_id,
                    type: 'family',
                    category: 'add_to_group',
                    message_title: 'familheey',
                    type_id: parseInt(groupId),
                    link_to: parseInt(groupId),
                    sub_type: ""
                };
                let addingUser = await ctx.broker.call("db.users.find", { query: { id: from_id }, fields: ["id", "full_name", "propic"] })
                let _Group = await ctx.broker.call("db.groups.find", { query: { id: groupId }, fields: ["id", "group_name", "logo", "default_membership"] })
                let group = await ctx.call("db.group_map.find", { query: { group_id: groupId, user_id: userId } });
                let status = 'pending';
                if (!group.length > 0) {
                    let searchParams = { user_id_one: from_id.toString(), user_id_two: userId.toString() }
                    let createMaps = {
                        user_id: userId,
                        group_id: groupId,
                        following: true,
                        is_blocked: false,
                        membership_period_type: "Life time",
                        membership_id: _Group[0].default_membership ? parseInt(_Group[0].default_membership) : null
                    }
                    ctx.broker.call("db.group_map.getMutualFamilies", searchParams)
                    .then(async (mutual) => {
                        let _group_name = '';
                        // Temporarally added all user to group with out permission
                        // If it change replace the if condition with "if (mutual && mutual.length > 0) {"
                        //if ((mutual && mutual.length > 0)||(mutual.length <= 0)) {//add directly to group
                        //if (mutual && mutual.length > 0) {
                        let CheckMutual = false;
                        if (CheckMutual) { //no need to check mutual connection exist so commented as false
                            let addedUser = await ctx.broker.call("db.users.find", { query: { id: userId }, fields: ["id", "full_name", "propic"] })

                            _group_name = _Group[0].group_name;
                            let searchQ = {
                                user_id: userId,
                                group_id: groupId,
                                from_id: from_id
                            }

                            if (from && from == 'approveRequest') {
                                var _msg = ` Your request to join ${_group_name} was approved`;
                                sendObj = { ...sendObj, message: _msg, sub_type: "" }
                                await ctx.broker.call("db.events.invitationUsers", searchQ)
                                    .then(async (doc) => {
                                        _group_name = doc[0].group_name;
                                        //if(doc.length>0) sendObj.sub_type = 'member';
                                        let _user_notify = await ctx.call("db.users.find", { query: { id: sendObj.to_id }, fields: ["notification"] })
                                        ctx.broker.call("notification.sendAppNotification", sendObj)
                                        if (_user_notify.length > 0 && _user_notify[0].notification) {
                                            _this.pushNotify(ctx, sendObj)
                                        }
                                    })
                            } else {
                                //send message to the user whs nt a member
                                let sendObj_u = {
                                    ...sendObj,
                                    message: `<b>${addingUser[0].full_name}</b> has added you to <b>${_group_name}</b>`,
                                    propic: (addingUser.length > 0) ? addingUser[0].propic : '',
                                    to_id: parseInt(userId)
                                }
                                let _user_notify;
                                ctx.broker.call("notification.sendAppNotification", sendObj_u);
                                _user_notify = await ctx.call("db.users.find", { query: { id: sendObj_u.to_id }, fields: ["notification"] })
                                if (_user_notify.length > 0 && _user_notify[0].notification) _this.pushNotify(ctx, sendObj_u)
                                //send message top others
                                let gp_m = await ctx.broker.call("db.group_map.find", { query: { group_id: groupId, is_blocked: false, is_removed: false }, fields: ['user_id'] })
                                gp_m.forEach(async elm => {
                                    var new_msg = `${addingUser[0].full_name} has added ${addedUser[0].full_name} to ${_group_name}`
                                    let _sendObj_n = {
                                        ...sendObj,
                                        to_id: parseInt(elm.user_id),
                                        message: new_msg,
                                        propic: addingUser[0].propic
                                    }
                                    _user_notify = await ctx.call("db.users.find", { query: { id: _sendObj_n.to_id }, fields: ["notification"] })

                                    if (elm.user_id != parseInt(from_id)) {
                                        ctx.broker.call("notification.sendAppNotification", _sendObj_n);
                                        if (_user_notify.length > 0 && _user_notify[0].notification) _this.pushNotify(ctx, _sendObj_n)
                                    }
                                });

                            }

                            // Track user activities start
                            let trackerData = {
                                user_id: parseInt(from_id),
                                type: 'Family',
                                type_id: parseInt(groupId),
                                sub_type: 'user',
                                sub_type_id: parseInt(userId),
                                activity: 'added user to family'
                            };
                            ctx.broker.call('user_activities.trackUserActivities', trackerData);
                            // Track user activities end


                            status = 'accepted';
                            return ctx.call('db.group_map.create', createMaps)
                        } else {
                            let _sendInvite = { user_id: userId.toString(), from_id: from_id.toString(), group_id: groupId.toString() }
                            // Track user activities start
                            let trackerData = {
                                user_id: parseInt(from_id),
                                type: 'Family',
                                type_id: parseInt(groupId),
                                sub_type: 'user',
                                sub_type_id: parseInt(userId),
                                activity: 'sent family invitation'
                            };
                            ctx.broker.call('user_activities.trackUserActivities', trackerData);
                            // Track user activities end
                            let isExist = await ctx.call("db.requests.find", { query: { user_id: userId, group_id: groupId } })
                            if (isExist && isExist.length > 0) {
                                await ctx.call("db.requests.remove", { id: isExist[0].id })
                            }
                            return ctx.broker.call('groups.sendInvitation', _sendInvite)
                        }
                    })
                    .then((res) => {
                        res = { ...res, status: status }
                        resolve(res)
                    }).catch((err) => { console.log(" %$$%$% err  ^%^%^", err); reject(err) });
                }
                else { //already member notactive/blocked
                    //let { is_removed, is_blocked, id } = group[0];
                    //All type of user will add to family with permission start
                    //Remove existing entry from request table start
                    let isExist = await ctx.call("db.requests.find", { query: { user_id: userId, group_id: groupId } })
                    if (isExist && isExist.length > 0) {
                        await ctx.call("db.requests.remove", { id: isExist[0].id })
                    }
                    //Remove existing entry from request table end
                    //Add new entry to request table start
                    let invitationObj = { user_id: userId.toString(), from_id: from_id.toString(), group_id: groupId.toString() }
                    ctx.broker.call('groups.sendInvitation', invitationObj)
                        .then(async (addres) => {
                            resolve(addres)
                        })
                        .catch((e) => {
                            reject(e)
                        })
                    //Add new entry to request table end
                    //All type of user will add to family with permission end

                    // // Added existing user directly to group without any permission start
                    // ctx.call('db.group_map.update', { id:id, is_removed:false, is_blocked:false })
                    // .then(async(res)=>{
                    //     let sendObj_new = { 
                    //         ...sendObj,
                    //         message: `<b>${addingUser[0].full_name}</b> has added you to <b>${_Group[0].group_name}</b>`,
                    //         propic: (addingUser.length>0)?addingUser[0].propic:'',
                    //         to_id:parseInt(userId)
                    //     }
                    //     ctx.broker.call("notification.sendAppNotification", sendObj_new);
                    //     let _user_notify = await ctx.call("db.users.find",{ query:{id:sendObj_new.to_id}, fields:["notification"] })
                    //     if(_user_notify.length>0 && _user_notify[0].notification) _this.pushNotify(ctx,sendObj_new)
                    //     resolve(res)
                    // }).catch((e)=>{
                    //     reject(e)
                    // })
                    // // Added existing user directly to group without any permission end
                }
            });
        }
    },
    created() { },
    started() { },
    stopped() { }
}
