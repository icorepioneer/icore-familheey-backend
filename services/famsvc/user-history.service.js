module.exports = {
    'name':'user_history',
    actions:{
        addUserHistory:{
            params:{
                user_id:"string"
            },
            handler(ctx){
                return new Promise((resolve,reject)=>{
                    ctx.call('db.user_history.create',ctx.params)
                    .then(res=>{
                        resolve(res);
                    }).catch(err=>{
                        reject(err);
                    });
                });
            }
        }
    },
    events: {

	},
	methods: {

	},

	created() { 
	},
	started() {

	},
	stopped() { 

	}
};