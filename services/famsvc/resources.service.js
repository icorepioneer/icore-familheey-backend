"uses strict";
var _async = require('async');
const moment = require('moment');
module.exports = {
    name: "resources",
    settings: {},
    actions: {
        createFolder: {
            params: {
                folder_name: "string",
                created_by: "string",
                folder_for: "string",
                folder_type: "string"
            },
            handler: (ctx) => {
                let { permissions, users_id, created_by, group_id, event_id } = ctx.params;
                let response = {};
                return new Promise((resolve, reject) => {
                    ctx.call("db.folders.create", ctx.params)
                        .then(res => {
                            response.data = [res];
                            // Track user activities start
                            let trackerData = {
                                user_id : parseInt(created_by),
                                type : 'Folder',
                                type_id : parseInt(res.id),
                                activity : 'Created a folder'
                            };
                            ctx.broker.call('user_activities.trackUserActivities', trackerData);
                            // Track user activities end
                            if (permissions && permissions == 'selected' && users_id) {
                                users_id.forEach(elem => {
                                    ctx.call("db.document_map.create", {
                                        folder_id: res.id,
                                        user_id: elem,
                                        shared_from_user: created_by,
                                        group_id: group_id ? group_id : null,
                                        event_id: event_id ? event_id : null
                                    })
                                })
                            }
                            resolve(response);
                        })
                        .catch(err => reject(err))
                })
            }
        },
        /**
         * delete a folder in album/documents
         */
        deleteFolder: {
            params: {
                user_id: "string"
            },
            handler(ctx){
                let { folder_id, user_id } = ctx.params;
                var response = { code: 200 }
                return new Promise((resolve, reject) => {
                    let uniqFolders = [...new Set(folder_id)];
                    //using folder id get group_id and creator id
                    ctx.call("db.folders.find", { query: { id: uniqFolders }, fields:["id","created_by", "group_id","folder_name","folder_type"] })
                    .then((res) => {
                        let i=1, msg=[];
                        res.forEach(async elem => {
                            
                            let { created_by, group_id, folder_name } = elem;
                            //get admins of the group
                            let _members = await ctx.call("db.group_map.getUsersOfCurrentGroup", { group_id:group_id, type:'admin' });
                            let permitted_users = _members.map(users=>users.id);//map admins id
                            permitted_users.push(created_by);//push creator id to admin ids
                            let isPermitted = permitted_users.includes(parseInt(user_id));//check user_id exist in array
                            if (isPermitted) {//if user_id exist delete
                                    await ctx.call("db.folders.update", { id: elem.id, is_active: false })

                                    //Remove folder post start
                                    ctx.broker.call("resources.removePostByFolder",{publish_type:elem.folder_type,folder_id:folder_id,remove_type:"folder"})
                                    .then((res2) => {
                                        console.log("Successfully removed post");
                                    })
                                    .catch((err) => {
                                        reject(err)
                                    })
                                    //Remove folder post end

                                    // Track user activities start
                                    let trackerData = {
                                        user_id : parseInt(user_id),
                                        type : 'Folder',
                                        type_id : parseInt(elem.id),
                                        activity : 'Deleted a folder'
                                    };
                                    ctx.broker.call('user_activities.trackUserActivities', trackerData);
                                    // Track user activities end
                                    msg.push(`${folder_name} Deleted`)
                                    response = { ...response, message:msg  } 
                            } else {//not in array list , not authorised
                                msg.push(`${folder_name} : NOT_AUTHORISED`)
                                response = { ...response, message:msg  } 
                            }
                            if(i == res.length) resolve(response);
                            i++;
                        });
                    }).catch((err) => {
                        reject(err)
                    })
                })
            }
        },
        updateFolder: {
            /**
             * folder_name:"string"
             * type:"string"
             * is_sharable:"string"
             * is_active:"string"
             */
            params: {
                id: "string"
            },
            handler: (ctx) => {
                let response = {};
                let { id,permissions, users_id } = ctx.params;
                return new Promise(async(resolve, reject) => {
                    let oldP = await ctx.broker.call("db.folders.find",{query:{id:id}, fields:['permissions','folder_type']})
                    let _params = {
                        typeIs:'folder',
                        currentPermission: oldP[0].permissions,
                        folder_id: id,
                        folder_type:oldP[0].folder_type
                    }
                    ctx.params = { ...ctx.params, currentPermission: oldP[0].permissions }
                    ctx.call("db.folders.update", ctx.params)
                    .then(async res => {
                        response.data = res;
                        if(permissions){
                            _params = { ..._params, newPermission: res.permissions,shared_user:users_id,...res  }
                            ctx.broker.call('resources.changeFolderPermission',_params)
                        }
                        resolve(response);
                        ctx.call('db.post.find', { query: { publish_id: res.id }, fields:["id"] })
                        .then((resultArray) => {
                            let postIds = resultArray.map(v=>v.id);
                            let up_data = {
                                where: { id: postIds }
                            };
                            if(res.permissions != 'all'){
                                up_data = { ...up_data, params: { is_active:false } }
                            }else{
                                up_data = { ...up_data, params: { is_active:true } }
                            }
                            ctx.call('db.post.updateByParams', up_data)
                        }).catch((err_post)=>{
                            console.log(" = = = err_post == =", err_post)
                        })
                    }).catch(err => reject(err))
                })
            }
        },
        listFolders: {
            handler: (ctx) => {
                let { group_id, user_id } = ctx.params;
                let paramName, paramValue;
                paramName = group_id ? "group_id" : "created_by";
                paramValue = group_id ? group_id : user_id;
                let response = {};
                return new Promise((resolve, reject) => {
                    ctx.call("db.folders.list", { query: { [paramName]: `${paramValue}`, subfolder_of: null }, pageSize: 100, page: 1, sort: "id" }
                    ).then(res => {
                        let publicId = [];
                        res.rows.map((row) => {
                            row.type === "public" && publicId.push(row.id);
                        })
                        res.rows.map((row) => {
                            publicId.includes(row.id) ? row.SHARABLE = true : (row.is_sharable ? row.SHARABLE = true : row.SHARABLE = false);
                            row.group_id && row.created_by ? row.GROUP = true : row.USER = true;
                        })
                        response.data = res;
                        resolve(res);
                    }
                    ).catch(err => reject(err))
                })
            }
        },
        /**
         * list all the albums(folder) in user view
         */
        listUserFolders: {
            params: {
                profile_id: "string",
                from_user: "string",
                folder_for: "string",
                folder_type: "string"
            },
            handler(ctx) {
                let response = {};
                return new Promise((resolve, reject) => {
                    ctx.call("db.folders.listUserFolders", ctx.params)
                    .then(res => {
                        /*let publicId = [];
                        res.rows.map((row)=>{
                            row.type === "public"&&publicId.push(row.id);
                        })
                        res.rows.map((row)=>{
                            publicId.includes(row.id)?row.SHARABLE = true:(row.is_sharable?row.SHARABLE = true:row.SHARABLE = false);
                            row.group_id&&row.created_by?row.GROUP = true:row.USER = true;
                        })*/
                        response.data = res;
                        resolve(response)
                    }).catch(err => reject(err))
                })
            }
        },
        listGroupFolders: {
            params: {
                group_id: "string",
                user_id: "string",
                folder_for: "string",
                folder_type: "string"
            },
            handler: (ctx) => {
                let response = {}
                let { folder_for, folder_type,txt,folder_id, ...rem } = ctx.params
                return new Promise((resolve, reject) => {
                    ctx.call("db.group_map.find", { query: { ...rem } })
                    .then(async res => {
                        let memberFlag = res.length > 0;
                        ctx.params = { ...ctx.params, is_member: memberFlag }
                        //let query  = memberFlag? {group_id:group_id}:{group_id:group_id,type:"public"}
                        //query = { ...query,folder_for:folder_for, is_active:true  }
                        await ctx.call("db.folders.listFolders", ctx.params)
                        .then(res2 => {
                            response.data = res2;
                            //response = memberFlag ? { ...response, member: memberFlag } : { ...response, member: memberFlag }
                            response = { ...response, member: memberFlag };
                            resolve(response)
                        })
                    })
                    .catch(err => reject(err))
                })
            }
        },
        createDocumentEntry: {
            params: {},
            handler(ctx) {
                let { params, meta } = ctx.params
                let {
                    folder_id, user_id, group_id, is_sharable, document_files,permission_type 
                } = params;
                return new Promise(async(resolve, reject) => {

                    if (meta && meta.file) {
                        console.log("== meta files= = =")
                        //let document_files = [];
                        document_files = document_files ? document_files : [];
                        let postObj = {}
                        let _loop = 0;
                        meta.file.forEach(async (element, i) => {
                            ctx.params[element.fieldname] = element.filename;
                            let fileNameArr = params.file_name ? params.file_name.split(',') : [];
                            
                            let Url = `${process.env.AWS_S3_BASE_URL}Documents/${element.filename}`;
                            let file_name = fileNameArr[i] ? fileNameArr[i] : element.filename;
                            await this.calls3(ctx, element);
                            let doc = await ctx.broker.call("db.documents.findontype", params);
                            if (doc && doc.length > 0 && doc[0].count == 0) {
                                ctx.broker.call("db.folders.update", { id: folder_id, cover_pic: Url })
                            }
                            let _saveData = {
                                folder_id: parseInt(folder_id),
                                user_id: parseInt(user_id),
                                is_sharable: is_sharable,
                                file_type: element.mimetype,
                                url: Url,
                                // file_name: element.originalname ? element.originalname : file_name,
                                file_name: element.filename,
                                // original_name: element.originalname ? element.originalname : file_name ,
                                original_name: element.originalname,
                                video_thumb: element.video_thumb ? element.video_thumb : ''
                            }
                            if (group_id && group_id != '') {
                                _saveData = { ..._saveData, group_id: parseInt(group_id) }
                            }
                            ctx.call("db.documents.create", _saveData)
                            .then(async response => {
                                let { id, file_type } = response;
                                resolve(response);
                                let ext = file_type.split("/")[0];
                                if(ext=='video'){
                                    let thumbnaildata = 'default_video.jpg';
                                    try {
                                        element.original_name = element.originalname
                                        ctx.params.file_type = element.mimetype; //file mimetype
                                        thumbnaildata = await ctx.call("s3upload.generateVideoThumb",element);
                                        element = { ...element, video_thumb:thumbnaildata }
                                    } catch (error_thumb) {
                                        console.log("== try catch error_thumb ===", error_thumb)
                                    }
                                    ctx.call("db.documents.update", { id:id, video_thumb:thumbnaildata })
                                }
                                // Track user activities start
                                    let trackerData = {
                                        user_id : parseInt(user_id),
                                        type : 'Folder',
                                        type_id : parseInt(folder_id),
                                        activity : 'Added document to folder'
                                    };
                                    ctx.broker.call('user_activities.trackUserActivities', trackerData);
                                // Track user activities end

                                //Create post start
                                let publish_type = "";
                                //Get folder type start
                                await ctx.call("db.folders.find", {query:{id:folder_id},fields:["folder_type"]})
                                .then(folderRes => {
                                    if(folderRes && folderRes.length > 0) {
                                        publish_type = folderRes[0].folder_type;
                                    }    
                                })
                                .catch(err => {
                                    console.log(err)
                                })
                                //Get folder type end
                                document_files.push({
                                    filename : element.filename,
                                    height : element.height ? element.height : 400,
                                    width : element.width ? element.width : 400,
                                    is_play : false,
                                    file_type : element.mimetype ? element.mimetype : "image/png", 
                                    // original_name: element.originalname ? element.originalname : file_name ,
                                    original_name: element.originalname ? element.originalname : element.filename ,
                                    video_thumb: element.video_thumb ? element.video_thumb : ''
                                });
                                // let typeis = element.mimetype.split("/")[0];
                                // typeis = (typeis == 'image') ? 'image(s)':'document(s)';
                                postObj.publish_type = publish_type ? publish_type : "albums";
                                postObj.publish_id = folder_id;
                                postObj.document_files = document_files;
                                postObj.user_id = user_id;
                                postObj.snap_description = `new media uploaded to ${postObj.publish_type}`;
                                postObj.sort_date = moment.utc();
                                //Create post end
                                _loop++;
                                if(meta.file.length == _loop){
                                    ctx.params.postObj = postObj;
                                    permission_type == 'all' && this.publishPost(ctx)
                                }
                            })
                            .catch(err => {
                                console.log(err)
                            })
                        });
                    } else {
                        let typeis;
                        if(document_files && document_files.length > 0){
                            document_files.forEach(async (element, i) => {
                                let Url = '';
                                if(element.file_type && element.file_type == "video") {
                                    Url = `${process.env.AWS_S3_BASE_URL}${element.video_thumb}`;
                                } else {
                                    Url = `${process.env.AWS_S3_BASE_URL}${element.url}`;   
                                }
                                ctx.broker.call("db.folders.update", { id: folder_id, cover_pic: Url }) 
                                let _saveData = {
                                    folder_id: parseInt(folder_id),
                                    user_id: parseInt(user_id),
                                    is_sharable: is_sharable ? is_sharable : true,
                                    file_type: element.file_type,
                                    url: Url,
                                    file_name: element.file_name,
                                    original_name: element.original_name ? element.original_name : element.file_name ,
                                    video_thumb: element.video_thumb ? element.video_thumb : ''
                                }  
                                if (group_id && group_id != '') {
                                    _saveData = { ..._saveData, group_id: parseInt(group_id) }
                                } 
                                ctx.call("db.documents.create", _saveData)
                                .then(response => {
                                    resolve(response);
                                    // Track user activities start
                                        let trackerData = {
                                            user_id : parseInt(user_id),
                                            type : 'Folder',
                                            type_id : parseInt(folder_id),
                                            activity : 'Added document to folder'
                                        };
                                        ctx.broker.call('user_activities.trackUserActivities', trackerData);
                                    // Track user activities end
                                })
                                .catch(err => {
                                    console.log(err)
                                });
                                typeis = element.file_type.split("/")[0];
                                typeis = (typeis == 'image') ? 'images(s)':'document(s)';
                            });

                            //Create post start
                            let publish_type = "";
                            //Get folder type start
                            await ctx.call("db.folders.find", {query:{id:folder_id},fields:["folder_type"]})
                            .then(folderRes => {
                                if(folderRes && folderRes.length > 0) {
                                    publish_type = folderRes[0].folder_type;
                                }    
                            })
                            .catch(err => {
                                console.log(err)
                            })
                            //Get folder type end
                            let postObj = {}
                            postObj.publish_type = publish_type ? publish_type : "albums";
                            postObj.publish_id = folder_id;
                            postObj.document_files = document_files;
                            postObj.user_id = user_id;
                            postObj.snap_description = `new media uploaded to ${postObj.publish_type}`
                            postObj.sort_date = moment.utc();
                            permission_type == 'all' &&  ctx.call("common_methods.publish_post", postObj)
                            .then(response => {
                                console.log(response);
                            })
                            .catch(err => {
                                 console.log(err)
                            })
                            //Create post end

                        } else {
                            resolve("ok")
                        }
                    }
                })
            }
        },
        addDocuments: {
            params: {
                folder_id: "string",
                user_id: "string",
                is_sharable: "string"
            },
            handler(ctx) {
                let { folder_id } = ctx.params;
                return new Promise((resolve, reject) => {
                    let permission_type
                    ctx.broker.call("resources.getPermission", { folder_id: folder_id }).then(res => {
                        permission_type = res;
                        return permission_type
                    }).then(res => {
                        ctx.params = { ...ctx.params, permission_type:res }
                        switch (res) {
                            case 'only-me':
                                ctx.broker.call("resources.createDocumentEntry", { params: { ...ctx.params }, meta: { ...ctx.meta } }).then(res2 => {
                                    resolve(res2);
                                })
                                break;
                            case 'selected':
                                ctx.broker.call("resources.createDocumentEntry", { params: { ...ctx.params }, meta: { ...ctx.meta } }).then(res3 => {
                                    return res3;
                                }).then(async docDetails => {
                                    // let { id, user_id } = docDetails;
                                    // var folder_id = docDetails.folder_id;
                                    // var group_id = docDetails.group_id;
                                    // let _array = [];
                                    // let obj = {
                                    //     folder_id: folder_id,
                                    //     file_id: id,
                                    //     shared_from_user: user_id,
                                    //     is_blocked: false,
                                    //     is_removed: false
                                    // }
                                    // if(!selectedIDs){
                                    //     let permittedUsers = await ctx.broker.call("db.document_map.find",{ query:{ folder_id:folder_id }, fields:["user_id"] });
                                    //     selectedIDs = permittedUsers.map(u=>u.user_id);
                                    // }
                                    // selectedIDs.map(each => _array.push({ ...obj, user_id: each }))
                                    // ctx.broker.call("db.document_map.bulkCreate", { data: _array }).then(res2 => {
                                    //     resolve({ document: { ...docDetails }, docMap: { ...res2 } })
                                    // })
                                    resolve({ code:200, document: { ...docDetails } })
                                })
                                break;
                            case 'all':
                                ctx.broker.call("resources.createDocumentEntry", { params: { ...ctx.params }, meta: { ...ctx.meta } }).then(res3 => {
                                    return res3;
                                }).then(docDetails => {
                                    let { id, group_id, user_id } = docDetails;
                                    var folder_Id = docDetails.folder_id;
                                    ctx.call("db.document_map.create", {
                                        folder_id: folder_Id,
                                        file_id: id,
                                        group_id: group_id,
                                        shared_from_user: user_id,
                                        is_blocked: false,
                                        is_removed: false
                                    }).then(res4 => {
                                        resolve({ document: { ...docDetails }, docMap: { ...res4 } })
                                    })
                                })
                                break;
                            case 'private':
                                ctx.broker.call("resources.createDocumentEntry", { params: { ...ctx.params }, meta: { ...ctx.meta } }).then(res5 => {
                                    resolve(res5);
                                })
                                break;
                        }
                    })
                })
            }
        },
        changeFolderPermission: {
            params: {
                folder_id: "string",
                currentPermission: "string",
                newPermission: "string"
            },
            handler: (ctx) => {
                let { selectedIDs, typeIs, folder_type, folder_id, shared_user, ...rem } = ctx.params;
                let response = {};
                return new Promise((resolve, reject) => {

                    /*if(rem.currentPermission != 'all'){
                        let bulkUpdate = {
                            params: { is_active: false },
                            where: { publish_type:folder_type, publish_id:folder_id }
                        };
                        ctx.call('db.post.updateByParams', bulkUpdate) 
                    }*/

                    switch (true) {
                        case (rem.currentPermission == 'selected' || rem.currentPermission == 'all') && rem.newPermission == 'only-me':
                            ctx.call("db.document_map.bulkDelete", { folder_id: folder_id, key: "group_id", value: null }).then(res => {
                                response.updatedCount = res;
                                resolve(response)
                            }).catch(err => {
                                response.err = err;
                                reject(response);
                            })
                            break;
                        case rem.currentPermission == 'only-me' && rem.newPermission == 'selected':
                            ctx.call("db.folders.update", {
                                id: folder_id,
                                permissions: 'selected'
                            }).then(res => {
                                response.data = res;
                                resolve(response);
                            }).catch(err => {
                                response.err = err;
                                reject(response);
                            })
                            break;
                        case rem.currentPermission == 'only-me' && rem.newPermission == 'all':
                            ctx.call("resources.createEntryDocMap", {
                                folder_id: folder_id,
                                group_id: group_id,
                                shared_user: shared_user,
                            }).then(res => {
                                response.data = res;
                                resolve(response);
                            }).catch(err => {
                                response.err = err;
                                reject(response);
                            })
                            break;
                        case rem.currentPermission == 'selected' && rem.newPermission == 'all':
                            typeIs = (typeIs == 'folder') ?  "file_id":"group_id";
                            ctx.call("db.document_map.bulkDelete", { folder_id: folder_id, key: `${typeIs}`, value: null })
                            .then(res => {
                                if( typeIs && typeIs == 'folder') resolve({ ...response, code:200 }) 
                                else return res
                            }).then(count => {
                                ctx.call("resources.createEntryDocMap", {
                                    folder_id: folder_id,
                                    group_id: group_id,
                                    shared_user: shared_user,
                                }).then(res => {
                                    response.data = res;
                                    resolve(response);
                                }).catch(err => {
                                    response.err = err;
                                    reject(response);
                                })
                            })
                            break;
                        case (rem.currentPermission == 'selected'):
                            typeIs = (typeIs == 'folder') ?  "file_id":"group_id"
                            ctx.call("db.document_map.bulkDelete", { folder_id: folder_id, key: `${typeIs}`, value: null })
                            .then(res => {
                                let { shared_user:sh_user, created_by, group_id, event_id } = ctx.params;
                                // shared_user = ctx.params.shared_user;
                                if (sh_user) {
                                    sh_user.forEach(elem => {
                                        ctx.call("db.document_map.create", {
                                            folder_id: folder_id,
                                            user_id: elem,
                                            shared_from_user: created_by,
                                            group_id: group_id ? group_id : null,
                                            event_id: event_id ? event_id : null
                                        })
                                    })
                                }
                            }).catch(err => {
                                response.err = err;
                                reject(response);
                            })
                        break;
                    }
                })
            }
        },
        createEntryDocMap: {
            params: {},
            handler: (ctx) => {
                let { folder_id, group_id, shared_user } = ctx.params;
                return new Promise((resolve, reject) => {
                    ctx.call("db.document_map.create", {
                        folder_id: folder_id,
                        group_id: group_id,
                        shared_from_user: shared_user,
                        is_blocked: false,
                        is_removed: false
                    }).then(res => {
                        resolve(res);
                    }).catch(err => {
                        reject(err);
                    })
                })
            }
        }
        ,
        listFoldersAndDoc: {
            params: {},
            handler: (ctx) => {
                return ctx.params;
            }
        },
        /**
         * list all the contents inside a folder
         */
        viewDocuments: {
            params: {
                user_id: "string",
                folder_id: "string"
            },
            handler: (ctx) => {
                let response = {};
                let { folder_id } = ctx.params;
                return new Promise((resolve, reject) => {
                    //checking whether folder is a user/group folder
                    ctx.call("db.folders.find", { query: { id: folder_id } }).then( async res => {
                        response.folder_details = (res && res.length>0) ? res[0]: {}
                        let { cover_pic } = res[0];
                        //let userFolder = group_id === null && event_id === null ? true : false;
                        let permission_users = await ctx.call("db.folders.permissionusers",{folder_id:folder_id}) 
                        let per_users = ( permission_users[0].pm_users != null) ?  permission_users[0].pm_users : []
                        response.folder_details = { ...response.folder_details, permission_users: per_users }
                        
                        ctx.call("db.documents.filesBasedOnFolder", { folder_id: folder_id }).then((res6) => {
                                for (var i = 0; i < res6.length; i++) {
                                    if(res6[i].file_type == "video") {
                                        res6[i].url = process.env.AWS_S3_BASE_URL + "Documents/" + res6[i].file_name;   
                                    }
                                    //Replace file_name to s3 name if its album start(For fixing android bug)
                                    if(res[0].folder_type == "albums") {
                                        let origUrl = res6[i].url
                                        let urlSplit = origUrl.split("/");
                                        let urlSplitLength = urlSplit.length - 1;
                                        res6[i].original_name = urlSplit[urlSplitLength]; 
                                        res6[i].file_name = urlSplit[urlSplitLength]; 
                                    }
                                    //Replace file_name to s3 name if its album end
                                }
                                response.documents = res6;
                                ctx.call("db.folders.find", { query: { subfolder_of: folder_id } }).then((res7) => {
                                    response = { ...response, folders: res7, cover_pic: cover_pic };
                                    resolve(response);
                                }).catch((err) => {
                                    reject(err);
                                })
                            }).catch(err => reject(err))

                        // userFolder ? (
                        //     //created_by == user_id ? (
                        //     ctx.call("db.documents.filesBasedOnFolder", { folder_id: folder_id }).then((res) => {
                        //         for (var i = 0; i < res.length; i++) {
                        //             if(res[i].file_type == "video") {
                        //                 res[i].url = process.env.AWS_S3_BASE_URL + "Documents/" + res[i].file_name;   
                        //             };
                        //         }
                        //         response.documents = res;
                        //         ctx.call("db.folders.find", { query: { subfolder_of: folder_id } }).then((res) => {
                        //             response = { ...response, folders: res, cover_pic: cover_pic };
                        //             resolve(response);
                        //         }).catch((err) => {
                        //             reject(err);
                        //         })
                        //     }).catch(err => reject(err))
                        // //) : (reject("NOT_AUTHORISED")) 
                        // ) : (
                        //     ctx.call("db.documents.filesBasedOnFolder", { folder_id: folder_id }).then((res) => {
                        //         for (var i = 0; i < res.length; i++) {
                        //             if(res[i].file_type == "video") {
                        //                 res[i].url = process.env.AWS_S3_BASE_URL + "Documents/" + res[i].file_name;   
                        //             };
                        //         }
                        //         response.documents = res;
                        //             ctx.call("db.folders.find", { query: { subfolder_of: folder_id } }).then((res) => {
                        //                 response = { ...response, folders: res, cover_pic: cover_pic };
                        //                 resolve(response);
                        //             }).catch((err) => {
                        //                 reject(err);
                        //             })
                        //         }).catch(err => reject(err))
                        //     )
                        
                    }).catch(err => reject(err))
                })
            }
        },
        /**
         * list albums(folders) for events
         */
        listEventFolders: {
            params: {
                event_id: "string",
                folder_for: "string",
                folder_type: "string",
                user_id: "string"
            },
            handler(ctx) {
                let response = {};
                return new Promise((resolve, reject) => {
                    ctx.call("db.folders.listFolders", ctx.params)
                        .then(res => {
                            response.data = res;
                            resolve(response)
                        })
                        .catch(err => reject(err))
                })
            }
        },
        /**
         * make a pic as album cover
         */
        makePicCover: {
            params: {
                album_id: "string",
                cover_pic: "string"
            },
            handler(ctx) {
                let { album_id, cover_pic } = ctx.params;
                let response = {};
                return new Promise((resolve, reject) => {
                    let updateData = { id: album_id, cover_pic: cover_pic }
                    ctx.call("db.folders.update", updateData)
                        .then(res => {
                            response.data = res;
                            resolve(response);
                            // Track user activities start
                            let trackerData = {
                                user_id : ctx.params.user_id ? parseInt(ctx.params.user_id) : '',
                                type : 'Folder',
                                type_id : parseInt(album_id),
                                activity : 'Changed cover pic to folder'
                            };
                            ctx.broker.call('user_activities.trackUserActivities', trackerData);
                            // Track user activities end
                        })
                        .catch(err => reject(err))
                })
            }
        },
        /**
         * delete  files from a folder
         */
        deleteFile: {
            params: {},
            handler(ctx) {
                return new Promise(async(resolve, reject) => {
                    let { id } = ctx.params
                    let postUpdateObj = [];
                    let fileNmaeArray = [];
                    let originalnameArray = [];
                    // let arraycount = 0;

                    for (var i = 0; i < id.length; i++) {
                        await ctx.call("db.documents.remove", { id: id[i] }).then(async(doc)=>{
                            let { folder_id, original_name, file_name } = doc;
                            let f_data = await ctx.call("db.folders.find",{ query:{ id:folder_id } })
                            let folder_type = '';
                            if(f_data && f_data.length>0){
                                let isUrl = f_data[0].cover_pic.includes(file_name);
                                folder_type = f_data[0].folder_type;
                                if(isUrl) await ctx.call("db.folders.update",{  id:folder_id, cover_pic: ''  })
                            }

                            fileNmaeArray.push(file_name);
                            originalnameArray.push(original_name);

                            let tembPostObj = {
                                publish_type:folder_type,
                                remove_type:"file",
                                folder_id:folder_id,
                                file_name:file_name,
                                original_name:original_name    
                            }
                            postUpdateObj.unshift(tembPostObj);
                        })
                    }
                    resolve({ code: 200, message: "Deleted files" });

                    //Remove folder post start
                    let editObj = {
                        publish_type:postUpdateObj[0].publish_type,
                        remove_type:"file", 
                        folder_id:postUpdateObj[0].folder_id,
                        file_name:fileNmaeArray,
                        original_name:originalnameArray,
                        file_id:ctx.params.id 
                    } 
                    ctx.broker.call("resources.removePostByFolder",editObj)
                    .then((res) => {
                        console.log("Successfully removed file from post");
                    })
                    .catch((err) => {
                        reject(err)
                    })
                    //Remove folder post end

                })
            }

        },
        /**
         * Used to share a folder 
         * or
         * particular file from that folder
        */
        getPermission: {
            params: {
                folder_id: "string"
            },
            handler: (ctx) => {
                //    return ctx.params;
                let { folder_id } = ctx.params;
                return new Promise((resolve, reject) => {
                    ctx.call("db.folders.find", { query: { id: folder_id }, fields: ["permissions"] }).then(res => {
                        let { permissions } = res[0]
                        resolve(permissions);
                    }).catch(err => {
                        reject(err)
                    })
                })
            }
        },
        /**
         * edit files
         * name, url etc
         */
        updateFileName: {
            params: {
                id: "string"
            },
            handler: (ctx) => {
                let response = {};
                let { id,file_name } = ctx.params;
                return new Promise(async(resolve, reject) => {
                    await ctx.call("db.documents.find", { query: { id: id }, fields: ["url"] })
                    .then(urlRes => {
                        let currentUrl = urlRes[0].url;
                        let fileExtension = get_url_extension(currentUrl);
                        if(file_name && file_name != '' && fileExtension && fileExtension != '') {
                            file_name = remove_extension(file_name);
                            ctx.params.original_name = file_name + '.' + fileExtension    
                        }
                    }).catch(err => {
                        console.log(err);
                    })
                    ctx.call("db.documents.update", ctx.params)
                    .then(async res => {
                        response.data = res;
                        resolve(response);
                        var existingFilename = res.url.substring(res.url.lastIndexOf("/") + 1, res.url.length);
                        let postsAre = await ctx.call("db.post.getpostsbyPostAttachment",{ filename:existingFilename  })
                        postsAre.forEach(elem => {
                            let { post_attachment } = elem;
                            // var id = elem.id;
                            let new_post_attachment = [];
                            post_attachment.forEach(val => {
                                if(existingFilename == val.filename) val = { ...val, original_name:res.original_name }
                                new_post_attachment.push(val);
                            });
                            ctx.call("db.post.update",{ id:elem.id, post_attachment:new_post_attachment  });                          
                        });
                    }).catch(err => reject(err))

                    //Get file extension from url start
                    function get_url_extension(url) {
                        return url.split(/[#?]/)[0].split('.').pop().trim();
                    }
                    //Get file extension from url end

                    //Remove file extension from file name start
                    function remove_extension(funFile_name) {
                        return funFile_name.replace(/(.*)\.[^.]+$/, "$1");
                    }
                    //Remove file extension from file name end
                })
            }
        },
        removePostByFolder:{
            params: {},
            handler: (ctx) => {
                let{folder_id,file_id,remove_type} = ctx.params;
                return new Promise((resolve, reject) => {
                    let publish_type = ctx.params.publish_type ? ctx.params.publish_type : "albums"
                    if(remove_type == 'folder') {
                        removePostByFolderId(folder_id);
                        return;
                    }
                    if(remove_type == 'file') {
                        removeFileFromPost(file_id);
                        return;
                    }
                    //Remove post function start
                    function removePostByFolderId(folderId){
                        //Get folder type start
                        ctx.call("db.folders.find",{query:{id:folderId},fields: ["folder_type"]})
                        .then(folderTypeRes => {
                            if(folderTypeRes && folderTypeRes.length > 0) {
                                publish_type = folderTypeRes[0].folder_type;
                                //Update post start 
                               
                                let bulkUpdate = {
                                    params: { is_active: false },
                                    where: { publish_type : publish_type,publish_id : folderId }
                                };
                                ctx.call('db.post.updateByParams', bulkUpdate)
                                .then(res => {
                                    resolve("Successfully removed all posts");
                                    return;
                                }).catch(err => reject(err))  
                                //Update post end             
                            } else {
                                reject("Cant find folder type");
                                return;   
                            }
                        }).catch(err => reject(err)) 
                        //Get folder type end
                    }
                    //Remove post function end

                    //Remove file from post start
                    function removeFileFromPost(funFile_id){
                        let {file_name ,original_name:orgName} = ctx.params;
                        var FolderId = ctx.params.folder_id;
                        var publishType = ctx.params.publish_type;

                        //Update post start 
                        ctx.call("db.post.find",{query:{publish_id:FolderId,publish_type:publishType},fields: ["id","post_attachment","snap_description"]}) 
                        .then(postRes => { 
                            if(postRes && postRes.length > 0) {
                                for (var i = 0; i < postRes.length; i++) {
                                    let post_attachment = postRes[i].post_attachment;
                                    post_attachment = post_attachment.filter(function( obj ) {
                                        if(publishType == 'documents') {
                                            if(file_name.length > 0) {
                                                return !checkArrayExist(obj.original_name,orgName);
                                            } else {
                                                return obj.original_name !== file_name;
                                            }
                                            // return obj.original_name !== file_name;
                                        } else {
                                            if(file_name.length > 0) {
                                                return !checkArrayExist(obj.original_name,orgName);
                                            } else {
                                                return obj.filename !== file_name;
                                            }
                                            //return obj.filename !== file_name;
                                        }
                                    });

                                    //Update snap description with new count start
                                    var getSnapdescription = postRes[i].snap_description;
                                    var snapArray = getSnapdescription.split(" ");
                                    snapArray[0] = post_attachment.length;
                                    var newSnapdescription = snapArray.join(" ");
                                    //Update snap description with new count end

                                    let newUpdateObj = {
                                        id:postRes[i].id,
                                        publish_type : publishType,
                                        publish_id : folder_id,
                                        post_attachment : post_attachment,
                                        snap_description : newSnapdescription  
                                    }
                                    if(post_attachment.length == 0) {
                                        newUpdateObj.is_active = false;   
                                    }
                                    ctx.call("db.post.update", newUpdateObj)
                                    .then(res => {
                                        resolve("Successfully updated all posts");
                                        return;
                                    }).catch(err => reject(err))
                                } 
                            } else {
                                console.log("There is no posts");
                                return;  
                            }

                            function checkArrayExist(val,arrayVal) {
                                let checkVal = arrayVal.includes(val);
                                return checkVal;
                            }     
                        })
                        .catch(err => reject(err)) 
                        //Update post end

                        // //Get folder id start
                        // ctx.call("db.documents.find",{query:{id:file_id},fields: ["folder_id","file_name"]})
                        // .then(documentRes => {
                        //     if(documentRes && documentRes.length > 0) {
                        //         let folder_id = documentRes[0].folder_id;
                        //         let file_name = documentRes[0].file_name;
                        //         //Get folder type start
                        //         ctx.call("db.folders.find",{query:{id:folder_id},fields: ["folder_type"]})
                        //         .then(folderTypeRes => {
                        //             if(folderTypeRes && folderTypeRes.length > 0) {
                        //                 publish_type = folderTypeRes[0].folder_type;

                        //                 //Update post start 
                        //                 ctx.call("db.post.find",{query:{publish_id:folder_id,publish_type:publish_type},fields: ["id","post_attachment"]}) 
                        //                 .then(postRes => { 
                        //                     if(postRes && postRes.length > 0) {
                        //                         let post_attachment = postRes[0].post_attachment;
                        //                         post_attachment = post_attachment.filter(function( obj ) {
                        //                             return obj.filename !== file_name;
                        //                         });
                        //                         newUpdateObj = {
                        //                             id:postRes[0].id,
                        //                             publish_type : publish_type,
                        //                             publish_id : folder_id,
                        //                             post_attachment : post_attachment    
                        //                         }
                        //                         ctx.call("db.post.update", newUpdateObj)
                        //                         .then(res => {
                        //                             resolve("Successfully updated all posts");
                        //                             return;
                        //                         }).catch(err => reject(err)) 
                        //                     } else {
                        //                         console.log("There is no posts");
                        //                         return;  
                        //                     }     
                        //                 })
                        //                 .catch(err => reject(err)) 
                        //                 //Update post end
                                        
                        //             } else {
                        //                 reject("Cant find folder type");
                        //                 return;   
                        //             }
                        //         }).catch(err => reject(err))
                        //         //Get folder type end 
                        //     } else {
                        //         console.log("There is no posts");
                        //         return;
                        //     }
                        // })
                        // .catch(err => reject(err))
                        // //Get folder id end
                    }
                    //Remove file from post end
                })
            }   
        },
        dummyAPI:{
            handler(ctx){
                return new Promise(async(resolve, reject) => {
                    let { file } = ctx.meta;
                    ctx.params = { ...ctx.params, ...file[0] }
                    ctx.call('s3upload.fileUpload', ctx.params)
                    .then((res) => {
                        resolve({ code:200, data:ctx.params });
                    })
                    .catch((err) => {
                        reject(err);
                    })
                })
            }
        }
    },
    events: {},
    methods: {
        /**
         * call s3 service for fiel upload
         */
        calls3(ctx, params) {
            return new Promise((resolve, reject) => {
                ctx.call('s3upload.fileUpload', params)
                    .then((res) => {
                        resolve("SUCCESS");
                    })
                    .catch((err) => {
                        reject("ERROR");
                    })
            })
        },
        /**
         * publish as a post with file attachments
         */
        publishPost(ctx){
            let { postObj } = ctx.params;
            ctx.call("common_methods.publish_post", postObj)
            .then(response => {
                console.log(response);
            })
            .catch(err => {
                console.log(err)
            })
        }
    },
    created() { },
    started() { },
    stopped() { }
}