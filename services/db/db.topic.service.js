/* eslint-disable indent */
"use_strict";
//packages
const DbService = require('moleculer-db');
const SqlAdapter = require('moleculer-db-adapter-sequelize');
const Sequelize = require('sequelize');
//topic table model
module.exports = {
    name: "db.topics",
    mixins: [DbService],
    settings: {},
    dependencies: [],
    cache: false,
    adapter: new SqlAdapter(process.env.SQL_DATABASE,null,null, {
        logger: false,
        pool: {
            max: 1,
            min: 0,
            acquire: 30000,
            idle: 10000,
            handleDisconnects: true
        },
        dialect: 'postgres',
        port: 5432,
        replication: {
            read: [
                { host: process.env.SQL_READ_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
            ],
            write: { host: process.env.SQL_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
        },
    }),
    model: {
        name: "topics",
        define: {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            created_by: {
                field: "created_by",
                type: Sequelize.INTEGER,
                allowNull: false
            },
            title: {
                field: "title",
                type: Sequelize.STRING,
                allowNull: true
            },
            description: {
                field: "description",
                type: Sequelize.TEXT,
                allowNull: false
            },
            topic_attachment: {
                field: "topic_attachment",
                type: Sequelize.JSON,
                allowNull: true
            },
            is_active: {
                field: "is_active",
                type: Sequelize.BOOLEAN,
                defaultValue: true
            },
            type: {
                field: "type",
                type: Sequelize.STRING,
                defaultValue: "topic"
            },
            createdAt: {
                field: "created_at",
                type: "TIMESTAMP",
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            },
            updatedAt: {
                field: "updated_at",
                type: "TIMESTAMP",
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            }
        },
        options: {}
    },
    actions: {
        topic_list: {
            handler(ctx) {
                let { user_id,txt } = ctx.params;
                let searchQuery = '';
                if(txt) {
                    searchQuery = `and (T.title ilike '%${txt}%' or U.full_name ilike '%${txt}%' or 
                    exists (select full_name from users where id in (select distinct(user_id) from topic_maps 
                    where topic_id = T.id and user_id != ${user_id} and is_active is true and full_name ilike '%${txt}%'))
                    )`;
                }
                let offsetval = ctx.params.offset ? ctx.params.offset : 0;
                let offsetQuery =  ctx.params && ctx.params.limit ? ` OFFSET ${offsetval} LIMIT ${ctx.params.limit}` : ``;
                let query = `select TM.id as topic_map_id,TM.is_accept,U.full_name as created_user,T.id as topic_id,
                T.created_by,T.title,T.description,T.topic_attachment,T.created_at,T.is_active,U.propic,
                (select count(*) from post_comments where topic_id = T.id) as conversation_count,
                (select count(*) as conversation_count_new from(select id,
 	                (select last_read_message_id from post_comment_reads where topic_id = T.id and user_id = ${ctx.params.user_id} limit 1) as last_read_message_id 
 	                from post_comments where topic_id = T.id) as results 
 	                where id > (case when last_read_message_id is null then 0 else last_read_message_id end)
                ),
                (select array(select full_name from users where id in 
                    (select distinct(user_id) from topic_maps where topic_id = T.id and user_id != ${user_id} and is_active is true) 
                and is_active is true) as to_users),
                (select sort_date from "sort-datas" where type = 'topic' and type_id = T.id) as sort_data
                from topic_maps TM 
                join topics T on T.id = TM.topic_id
                join users U on U.id = T.created_by
                where TM.user_id = '${user_id}' and TM.is_active is true and T.is_active is true and U.is_active is true ${searchQuery} 
                order by sort_data desc, T.id desc ${offsetQuery}`;
                return this.adapter.db.query(query)
                    .then(([res, metadata]) => res);
            }
        },
        get_topic_withusers: {
            handler(ctx) {
                let { topic_id } = ctx.params;
                let query = `select id,created_by,title,description,topic_attachment,is_active,"type",
                array(
                    select distinct(user_id) from topic_maps where topic_id = ${topic_id} and is_active is true and is_accept is true
                ) as to_user_id
                from topics where id = ${topic_id} and is_active is true`;
                return this.adapter.db.query(query)
                    .then(([res, metadata]) => res);
            }   
        },
        topic_detail: {
            handler(ctx) {
                let { topic_id } = ctx.params;
                let query = `select U.full_name as created_user,T.id as topic_id,T.created_by,T.title,T.description,
                T.topic_attachment,T.created_at,T.is_active,U.propic,
                (select count(*) from post_comments where topic_id = T.id) as conversation_count
                from topics T 
                join users U on U.id = T.created_by
                where T.id = ${topic_id} and T.is_active is true and U.is_active is true`;
                return this.adapter.db.query(query)
                    .then(([res, metadata]) => res);
            }
        },
        commonTopicListByUsers: {
            handler(ctx) {
                let {login_user,second_user} = ctx.params;
                let query = `select TM1.id as topic_map_id,TM1.is_accept,U.full_name as created_user,T.id as topic_id,
                    T.created_by,T.title,T.description,T.topic_attachment,T.created_at,T.is_active,U.propic,
                    (select count(*) from post_comments where topic_id = T.id) as conversation_count,
                    (select array(select full_name from users where id in 
                    (select distinct(user_id) from topic_maps where topic_id = T.id and user_id != ${login_user} and is_active is true) 
                    and is_active is true) as to_users),
                    (select sort_date from "sort-datas" where type = 'topic' and type_id = T.id) as sort_data
                from topic_maps TM1 
                inner join topic_maps TM2 on TM2.topic_id = TM1.topic_id and TM2.user_id = ${second_user} and TM2.is_active is true
                join topics T on T.id = TM1.topic_id 
                join users U on U.id = T.created_by
                where TM1.user_id = ${login_user} and TM1.is_active is true and T.is_active is true and U.is_active is true 
                order by sort_data desc, T.id desc`;
                return this.adapter.db.query(query)
                    .then(([res, metadata]) => res);
            }    
        }
    },
    events: {},
    methods: {},
    created() { },
    started() { },
    stopped() { },
};