/* eslint-disable indent */
"use_strict";
//packages
const DbService = require('moleculer-db');
const SqlAdapter = require('moleculer-db-adapter-sequelize');
const Sequelize = require('sequelize');
const moment = require('moment');
//user table model
module.exports = {
    name: "db.banners",
    mixins: [DbService],
    settings: {},
    dependencies: [],
    cache: false,
    adapter: new SqlAdapter(process.env.SQL_DATABASE,null,null, {
        logger: false,
        pool: {
            max: 1,
            min: 0,
            acquire: 30000,
            idle: 10000,
            handleDisconnects: true
        },
        dialect: 'postgres',
        port: 5432,
        replication: {
            read: [
                { host: process.env.SQL_READ_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
            ],
            write: { host: process.env.SQL_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
        },
    }),
    model: {
        name: "banners",
        define: {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            banner_type: {
                field: "banner_type",
                type: Sequelize.STRING,
            },
            visibility: {
                field: "visibility",
                type:Sequelize.BOOLEAN,
                defaultValue:true
            },
            banner_url: {
                field: "banner_url",
                type: Sequelize.JSON
            },
            link_type: {
                field: "link_type",
                type: Sequelize.STRING
            },
            link_subtype: {
                field: "link_subtype",
                type: Sequelize.STRING
            },
            link_id: {
                field: "link_id",
                type: Sequelize.INTEGER
            },
            createdAt: {
                field: "created_at",
                type: "TIMESTAMP",
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            },
            updatedAt: {
                field: "updated_at",
                type: "TIMESTAMP",
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            }
        },
        options: {}
    },
    actions: {
        
    },
    events: {},
    methods: {},
    created() { },
    started() { },
    stopped() { },
};
