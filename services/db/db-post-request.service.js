/* eslint-disable indent */
"use_strict";
//packages
const DbService = require('moleculer-db');
const SqlAdapter = require('moleculer-db-adapter-sequelize');
const Sequelize = require('sequelize');
const moment = require('moment')
//user table model
module.exports = {
    name: "db.post_requests",
    mixins: [DbService],
    settings: {},
    dependencies: [],
    cache: false,
    adapter: new SqlAdapter(process.env.SQL_DATABASE,null,null, {
        logger: false,
        pool: {
            max: 1,
            min: 0,
            acquire: 30000,
            idle: 10000,
            handleDisconnects: true
        },
        dialect: 'postgres',
        port: 5432,
        replication: {
            read: [
                { host: process.env.SQL_READ_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
            ],
            write: { host: process.env.SQL_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
        },
    }),
    model: {
        name: "post_requests",
        define: {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            user_id: {
                field: "user_id",
                type: Sequelize.INTEGER,
            },
            request_title: {
                field: "request_title",
                type: Sequelize.STRING
            },
            request_location: {
                field: "request_location",
                type: Sequelize.STRING
            },
            group_type: {
                field: "group_type",
                type: Sequelize.STRING
            },
            start_date: {
                field: "start_date",
                type: Sequelize.INTEGER
            },
            end_date: {
                field: "end_date",
                type: Sequelize.INTEGER
            },  
            is_active: {
                field: "is_active",
                type: Sequelize.BOOLEAN,
                defaultValue: true
            },
            createdAt: {
                field: "created_at",
                type: "TIMESTAMP",
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            },
            updatedAt: {
                field: "updated_at",
                type: "TIMESTAMP",
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            },
            request_type: {
                field: "request_type",
                type: Sequelize.STRING,
                defaultValue: "general"
            },
        },
        options: {}
    },
    actions: {
        get_post_need: {
            handler(ctx) {
                let {user_id,group_id,txt,by_user} = ctx.params;
                let offsetval = ctx.params.offset ? ctx.params.offset : 0;
                let offsetQuery =  ctx.params && ctx.params.limit ? ` OFFSET ${offsetval} LIMIT ${ctx.params.limit}` : ``;
                let _where_in = '';
                if(group_id && group_id != null && group_id != '') {
                    _where_in = `and PRG.group_id = ${group_id}`;
                } 
                else if(by_user && by_user != null && by_user != '') {
                    // if by_user=1 fetch user_id created requests and if by_user=2 fetch user_id contributed requests
                    if(by_user == 1) {
                        _where_in = `and PR.user_id = ${user_id}`
                    } else if(by_user == 2) {
                        _where_in = `and PR.id in (select distinct(PRI.post_request_id) from post_requests_item_contributions PRC 
join post_request_items PRI on PRI.id = PRC.post_request_item_id
where contribute_user_id = ${user_id} and PRC.is_active is true)`
                    }
                } else {
                    _where_in = `and PRG.group_id in (select group_id from group_maps GM
                    left join "groups" G on G.id = GM.group_id
                    where user_id = ${user_id} and G.is_active is true and GM.is_blocked is false and is_removed is false)`
                }
                let txt_query = ''
                if(txt && txt != null && txt != '') {
                    txt_query = `and(
U.full_name ilike '%${txt}%' or PR.request_location ilike '%${txt}%'
or (select exists(select id from post_request_items where post_request_id = PR.id and 
(request_item_title ilike '%${txt}%' or request_item_description ilike '%${txt}%') and is_active is true)) is true
)`
                }

                let _query = `select distinct(PR.id) as post_request_id,PR.user_id,U.full_name,U.propic,U.phone,PR.start_date,PR.end_date,PR.request_location,
                PR.request_title,PR.group_type,PR.created_at,PR.request_type
                from post_requests_group_maps PRG 
                left join post_requests PR on PR.id = PRG.post_request_id
                left join  users U on U.id = PR.user_id
                left join groups G on G.id = PRG.group_id
                left join group_maps GM on GM.group_id = G.id and GM.user_id = ${user_id}
                where PRG.is_active is true and PR.is_active is true and 
                (
                    G.request_visibility is null OR G.request_visibility = 27 OR PR.user_id = ${user_id} OR 
                    (G.request_visibility = 26 AND GM.type = 'admin')
                )
                ${_where_in} ${txt_query} 
                order by post_request_id desc,PR.id desc ${offsetQuery}`;
                return this.adapter.db.query(_query)
                .then(([res, metadata]) => res)
                .catch(err => err)
            }
        },
        get_post_need_detail: {
            handler(ctx) {
                let {post_request_id} = ctx.params;
                let detailQuery = `select PR.id as post_request_id,PR.user_id,U.full_name,U.propic,U.phone,PR.request_title,
                PR.request_location,PR.group_type,PR.start_date,PR.end_date,PR.created_at,PR.request_type,
                (select id from posts where publish_id = ${post_request_id} and publish_type = 'request' order by id desc limit 1) as thank_post_id 
                from post_requests PR
                left join users U on U.id = PR.user_id
                where PR.id = ${post_request_id} and PR.is_active is true`;
                return this.adapter.db.query(detailQuery)
                .then(([res, metadata]) => res)
                .catch(err => err)
            }
        },
        countRequestNeed:{
            handler(ctx){
                let { user_id } = ctx.params;
                let _query = `select count(distinct(PR.id))
                from post_requests_group_maps PRG 
                left join post_requests PR on PR.id = PRG.post_request_id
                where PRG.is_active is true and PR.is_active is true 
                and PRG.group_id in (
                select group_id from group_maps GM
                left join "groups" G on G.id = GM.group_id
                where user_id = ${user_id} and G.is_active is true and GM.is_blocked is false and is_removed is false)`;
                return this.adapter.db.query(_query)
                .then(([res, metadata]) => res)
                .catch(err => err)
            }
        },
        contributionlistByUser:{
            handler(ctx){
                let {request_item_id,contribute_use_id,is_anonymous,user_name} = ctx.params;
                let anonymous_filterQuery = '';
                if(is_anonymous) {
                    if(is_anonymous == "true") {
                       anonymous_filterQuery =` and PRIC.is_anonymous is true`;
                    }
                    if(is_anonymous == "false") {
                        anonymous_filterQuery =` and PRIC.is_anonymous is false`;
                    }
                }
                let non_app_user_query = '';
                if(contribute_use_id == 2 && user_name && user_name != '') {
                    non_app_user_query = ` and PRIC.paid_user_name = '${user_name}'`
                }
                let _query = `select PR.id as request_id,PR.user_id as requested_by,PRIC.id,PRIC.contribute_user_id,U.full_name,U.propic,U.phone,U."location",PRI.id as post_request_item_id,
                    PRIC.contribute_item_quantity,PRIC.created_at,PRI.request_item_title,PRIC.is_acknowledge,PRIC.is_thank_post,PRIC.payment_status,PRIC.is_anonymous,PRIC.paid_user_name
                    from post_requests_item_contributions PRIC
                    join post_request_items PRI on PRI.id = PRIC.post_request_item_id
                    join post_requests PR on PR.id = PRI.post_request_id
                    left join users U on U.id = PRIC.contribute_user_id
                    where PRIC.is_active is true ${anonymous_filterQuery} ${non_app_user_query} and PRIC.contribute_user_id = ${contribute_use_id} and PRIC.post_request_item_id = ${request_item_id}`;
                return this.adapter.db.query(_query)
                .then(([res, metadata]) => res)
                .catch(err => err)
            }    
        },
        admin_user_selection:{
            handler(ctx){
                let {group_id,search_text} = ctx.params;
                let _filter = [`GM.group_id = ${group_id}`, `GM.is_blocked is false` , `GM.is_removed is false`];
                if(search_text && search_text!=''){
                    _filter.push(`( U.full_name ilike '%${search_text}%' or U.phone ilike  '%${search_text}%' )`)
                }
                let _where = _filter.join(" and ")
                let query = `select user_id, U.full_name, phone
                from group_maps GM 
                left join users U on U.id = GM.user_id
                where ${_where}`;
                return this.adapter.db.query(query)
                .then(([res, metadata]) => res)
                .catch(err => err)
            }    
        },

        admin_otheruser_selection:{
            handler(ctx){
                let {group_id, search_text} = ctx.params;
                let _filter = [`OS.group_id = ${group_id}`, `OS.is_active is true`];
                if(search_text && search_text!=''){
                    _filter.push(`( OS.full_name ilike '%${search_text}%' or OS.phone ilike  '%${search_text}%' )`)
                }
                let _where = _filter.join(" and ")
                let query = `SELECT full_name, email, phone
                FROM other_users OS
                where ${_where}`;
                return this.adapter.db.query(query)
                .then(([res, metadata]) => res)
                .catch(err => err)
            }    
        },
        checkUserAdminRequest:{
            handler(ctx){
                let {post_request_id, user_id} = ctx.params;
                let query = `select GM."type" from group_maps GM where GM.user_id = ${user_id} and GM.is_blocked is false and 
                GM.is_removed is false and GM.group_id in 
                (
                    select distinct(PRGM.group_id) from post_requests_group_maps PRGM where PRGM.is_active is true and 
                    PRGM.post_request_id = ${post_request_id}
                )`;
                return this.adapter.db.query(query)
                .then(([res, metadata]) => res)
                .catch(err => err)
            }     
        },
        get_notification_needs_data: {
            handler(ctx) {
                let { request_id, item_id } = ctx.params;
                let _query = `select PR.group_type as type,PR.request_location as "location",U.full_name as created_by_user,U.propic as created_by_propic
                from post_requests PR left join users U on U.id = PR.user_id where PR.id = ${request_id}`;
                if (item_id && item_id != '') {
                    _query = `select PR.group_type as type,PR.request_location as "location",U.full_name as created_by_user,U.propic as created_by_propic
                    from post_request_items PRI join post_requests PR on PR.id = PRI.post_request_id
                    left join users U on U.id = PR.user_id where PRI.id = ${item_id}`
                }
                return this.adapter.db.query(_query)
                .then(([res, metadata]) => res);
            }
        }
    },
    events: {},
    methods: {},
    created() { },
    started() { },
    stopped() { },
};
