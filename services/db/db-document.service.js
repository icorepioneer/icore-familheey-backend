"use_strict";
//packages
const DbService = require('moleculer-db');
const SqlAdapter = require('moleculer-db-adapter-sequelize');
const Sequelize = require('sequelize');
//folder table model
module.exports = {
    name: "db.documents",
    mixins: [DbService],
    settings: {},
    dependencies: [],
    cache: false,
    adapter: new SqlAdapter(process.env.SQL_DATABASE,null,null, {
        logger: false,
        pool: {
            max: 1,
            min: 0,
            acquire: 30000,
            idle: 10000,
            handleDisconnects: true
        },
        dialect: 'postgres',
        port: 5432,
        replication: {
            read: [
                { host: process.env.SQL_READ_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
            ],
            write: { host: process.env.SQL_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
        },
    }),
    model: {
        name: "documents",
        define: {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            folder_id:{
                field:"folder_id",
                type:Sequelize.INTEGER,
                // references:{
                //     model:"folders",
                //     key:"id"
                // }
            },
            url:{
                field:"url",
                type:Sequelize.STRING
            },
            user_id:{
                field:"user_id",
                type:Sequelize.INTEGER,
                // references:{
                //     model:"users",
                //     key:"id"
                // }
            },
            group_id:{
                field:"group_id",
                type:Sequelize.INTEGER,
                // references:{
                //     model:"groups",
                //     key:"id"
                // }
            },
            event_id:{
                field:"event_id",
                type:Sequelize.INTEGER,
                // references:{
                //     model:"events",
                //     key:"id"
                // }
            },
            is_active:{
                field:"is_active",
                type:Sequelize.BOOLEAN
            },
            is_sharable:{
                field:"is_sharable",
                type:Sequelize.BOOLEAN
            },
            is_removed:{
                field:"is_removed",
                type:Sequelize.BOOLEAN
            },
            file_type:{
                field:"file_type",
                type:Sequelize.STRING
            },
            file_name:{
                field:"file_name",
                type:Sequelize.STRING
            },
            original_name:{
                field:"original_name",
                type:Sequelize.STRING
            },
            video_thumb:{
                field:"video_thumb",
                type:Sequelize.STRING
            },
            createdAt: {
                field: "created_at",
                type: 'TIMESTAMP',
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            },
            updatedAt: {
                field: "updated_at",
                type: 'TIMESTAMP',
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            }
        },
        options: {}
    },
    actions: {
        findontype:{
            handler(ctx) {
                let { folder_id } = ctx.params;
                let queryString = `SELECT count(*)
                FROM documents
                where file_type ilike '%image/%' and folder_id='${folder_id}'`;
                return this.adapter.db.query(queryString)
                .then(([res, metadata]) => res);
            }
        },
        /** list all files inside a folder */
        filesBasedOnFolder:{
            handler(ctx) {
                let { folder_id } = ctx.params;
                let queryString = `SELECT * FROM documents where folder_id='${folder_id}'`;
                return this.adapter.db.query(queryString)
                .then(([res, metadata]) => res);
            }
        }
    },
    events: {},
    methods: {},
    created() { },
    started() { },
    stopped() { },
};
