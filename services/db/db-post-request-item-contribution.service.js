/* eslint-disable indent */
"use_strict";
//packages
const DbService = require('moleculer-db');
const SqlAdapter = require('moleculer-db-adapter-sequelize');
const Sequelize = require('sequelize');
const moment = require('moment');
//user table model
module.exports = {
    name: "db.post_requests_item_contributions",
    mixins: [DbService],
    settings: {},
    dependencies: [],
    cache: false,
    adapter: new SqlAdapter(process.env.SQL_DATABASE,null,null, {
        logger: false,
        pool: {
            max: 1,
            min: 0,
            acquire: 30000,
            idle: 10000,
            handleDisconnects: true
        },
        dialect: 'postgres',
        port: 5432,
        replication: {
            read: [
                { host: process.env.SQL_READ_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
            ],
            write: { host: process.env.SQL_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
        },
    }),
    model: {
        name: "post_requests_item_contributions",
        define: {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            contribute_user_id: {
                field: "contribute_user_id",
                type: Sequelize.INTEGER,
            },
            post_request_item_id: {
                field: "post_request_item_id",
                type: Sequelize.INTEGER,
            },
            contribute_item_quantity: {
                field: "contribute_item_quantity",
                type: Sequelize.INTEGER
            },
            is_active: {
                field: "is_active",
                type: Sequelize.BOOLEAN,
                defaultValue: true
            },
            is_acknowledge: {
                field: "is_acknowledge",
                type: Sequelize.BOOLEAN,
                defaultValue: false
            },
            is_thank_post: {
                field: "is_thank_post",
                type: Sequelize.BOOLEAN,
                defaultValue: false
            },
            skip_thank_post: {
                field: "skip_thank_post",
                type: Sequelize.BOOLEAN,
                defaultValue: false
            },
            payment_status: {
                field: "payment_status",
                type: Sequelize.STRING,
                defaultValue: 'pending'
            },
            payment_note:{
                field:"payment_note",
                type:Sequelize.TEXT
            },
            payment_type: {
                field: "payment_type",
                type: Sequelize.STRING
            },
            createdAt: {
                field: "created_at",
                type: "TIMESTAMP",
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            },
            updatedAt: {
                field: "updated_at",
                type: "TIMESTAMP",
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            },
            is_anonymous: {
                field: "is_anonymous",
                type: Sequelize.BOOLEAN,
                defaultValue: false
            },
            admin_id: {
                field: "admin_id",
                type: Sequelize.INTEGER,
            },
            phone_no: {
                field: "phone_no",
                type: Sequelize.STRING,
            },
            group_id: {
                field: "group_id",
                type: Sequelize.INTEGER
            },
            paid_user_name: {
                field: "paid_user_name",
                type: Sequelize.STRING
            },
        },
        options: {}
    },
    actions: {
        /**
         * contributors list
         */
        contributionListOrCount: {
            handler(ctx) {
                let { limit,offset, post_request_item_id, typeis } = ctx.params;
                let offsetval = offset ? offset: 0;
                let limitval  = limit  ? limit : 25;

                // total contributions received
                let subWhere = ``;
                let subWhere2 = ``;

                let _filter = [`pr.is_active is true`];
                if(post_request_item_id){
                    _filter.push(`post_request_item_id=${post_request_item_id}`);
                    subWhere = `where is_active is true and post_request_item_id=${post_request_item_id}`;//with item id
                    subWhere2 = `where is_active is true and post_request_item_id=${post_request_item_id} and contribute_user_id=pr.contribute_user_id`;
                }

                if(ctx.params.query && ctx.params.query!=''){
                    _filter.push(`( U.full_name ilike '%${ctx.params.query}%' or U."location" ilike '%${ctx.params.query}%' )`);
                }

                let _where = _filter.join(" and ");
                if(limit) _where = `${_where} OFFSET ${offsetval} LIMIT ${limitval}`;

                let _Params = `pr.id,pri.user_id as requested_by, contribute_user_id,U.full_name, U.propic, U.phone,U."location", 
                post_request_item_id,contribute_item_quantity,pr.created_at,pr.is_acknowledge,pr.is_thank_post,pr.payment_status,
                pr.skip_thank_post,pr.is_anonymous,pr.paid_user_name,
                (select sum(contribute_item_quantity ) as total_received from post_requests_item_contributions ${subWhere})::int,
                (select sum(contribute_item_quantity ) as total_contribution from post_requests_item_contributions ${subWhere2})::int,
                (select count(*) as contribution_count from post_requests_item_contributions ${subWhere2}),
                pri.item_quantity as total_needed,pri.request_item_title,pri.post_request_id`;

                if( typeis && typeis == 'count') _Params = 'count(*)';
    
                let _query = `select ${_Params} FROM post_requests_item_contributions pr
                left join users U on U.id = pr.contribute_user_id
                left join post_request_items pri on pri.id = pr.post_request_item_id
                where ${_where}`;
                return this.adapter.db.query(_query)
                .then(([res, metadata]) => res)
                .catch(err => err);
            }
        },
        contributionAnonimousBasedCounts: {
            handler(ctx) {
                let {post_request_item_id,contribute_user_id,is_anonymous,paid_user_name} = ctx.params;
                let paid_user_name_query = '';
                if(contribute_user_id == 2 && paid_user_name && paid_user_name != '') {
                    paid_user_name_query = ` and PRIC.paid_user_name = '${paid_user_name}'`;   
                }
                let _query = `select sum(PRIC.contribute_item_quantity ) as total_contribution,
                count(*) as contribution_count
                from post_requests_item_contributions PRIC  
                where PRIC.is_active is true ${paid_user_name_query} and PRIC.post_request_item_id=${post_request_item_id} and 
                PRIC.contribute_user_id=${contribute_user_id} and is_anonymous is ${is_anonymous}`;
                return this.adapter.db.query(_query)
                .then(([res, metadata]) => res)
                .catch(err => err);
            }
        },
        /**
         * count of users contributed for all items
         * for a particuler reuest
         */
        supportersCount:{
            handler(ctx) {
                let { post_request_id } = ctx.params;
                let _query = `SELECT count(contribute_user_id)
                FROM public.post_requests_item_contributions
                where is_active is true and post_request_item_id in(select id
                from post_request_items pri 
                where post_request_id =${post_request_id})`;
                return this.adapter.db.query(_query)
                .then(([res, metadata]) => res)
                .catch(err => err);
            }
        },
        cronJob:{
            handler(ctx) {
                let { crnt_time, nxt_time } = ctx.params;
                let _query = `select pr.id,prg.group_id,pr.request_title,pri.request_item_title,pri.item_quantity 
                FROM post_requests pr
                left join post_request_items pri on  pri.post_request_id = pr.id
                left join post_requests_group_maps prg on prg.post_request_id = pr.id
                where pr.end_date >= ${crnt_time} and pr.end_date < ${nxt_time} and pr.is_active is true`;
                return this.adapter.db.query(_query)
                .then(([res, metadata]) => res)
                .catch(err => err);
            }
        },
        /**
         * totaal contribution received for an item
         */
        contributionSumOfaItem:{
            handler(ctx) {
                let { post_request_item_id } = ctx.params;
                let _query = `select sum(contribute_item_quantity)::int as total_contribution 
                from post_requests_item_contributions
                where is_active is true and  post_request_item_id = ${post_request_item_id}`;
                return this.adapter.db.query(_query)
                .then(([res, metadata]) => res)
                .catch(err => err);
            }
        },

        bulkidUpdate:{
            handler(ctx) {
                let { ids, contribute_user_id } = ctx.params;
                let _query = `update post_requests_item_contributions set contribute_user_id=${contribute_user_id} where id in (${ids})`;
                return this.adapter.db.query(_query)
                .then(([res, metadata]) => res)
                .catch(err => err);
            }
        },

        updateByParams: {
            handler(ctx) {
                let { params, where } = ctx.params;
                return this.adapter.db.models.post.update(params, { where: where })
                    .then(([res, metadata]) => res);
            }
        },
        
        get_contribution_list_Byadmin:{
            handler(ctx) {
                let {group_id,post_request_item_id} = ctx.params;
                let whereQuery = '';
                if(post_request_item_id) {
                    whereQuery = ` and PRIC.post_request_item_id = ${post_request_item_id}`;    
                }
                let _query = `select PRIC.id,PRIC.contribute_user_id,PRIC.post_request_item_id,PRIC.contribute_item_quantity,
                PRIC.payment_status,PRIC.payment_note,PRIC.payment_type,PRIC.group_id,PRIC.admin_id,PRIC.phone_no,U.full_name,
                U.email,PRIC.paid_user_name,PRIC.is_anonymous 
                from post_requests_item_contributions PRIC
                left join users U on U.id = PRIC.contribute_user_id 
                where PRIC.group_id = ${group_id} and PRIC.admin_id is not null ${whereQuery} 
                order by PRIC.id desc`;
                return this.adapter.db.query(_query)
                .then(([res, metadata]) => res)
                .catch(err => err);
            }   
        }
    },
    events: {},
    methods: {},
    created() { },
    started() { },
    stopped() { },
};
