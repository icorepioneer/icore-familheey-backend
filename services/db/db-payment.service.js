/* eslint-disable indent */
"use_strict";
//packages
const DbService = require('moleculer-db');
const SqlAdapter = require('moleculer-db-adapter-sequelize');
const Sequelize = require('sequelize');
const moment = require('moment');
//user table model
module.exports = {
    name: "db.payment",
    mixins: [DbService],
    settings: {},
    dependencies: [],
    cache: false,
    adapter: new SqlAdapter(process.env.SQL_DATABASE,null,null, {
        logger: false,
        pool: {
            max: 1,
            min: 0,
            acquire: 30000,
            idle: 10000,
            handleDisconnects: true
        },
        dialect: 'postgres',
        port: 5432,
        replication: {
            read: [
                { host: process.env.SQL_READ_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
            ],
            write: { host: process.env.SQL_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
        },
    }),
    model: {
        name: "payment",
        define: {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            from_user: {
                field: "from_user",
                type: Sequelize.INTEGER,
            },
            to_account_id: {
                field: "to_account_id",
                type: Sequelize.STRING,
            },
            group_id: {
                field: "group_id",
                type: Sequelize.INTEGER
            },
            type: {
                field: "type",
                type: Sequelize.STRING
            },
            type_id: {
                field: "type_id",
                type: Sequelize.INTEGER
            },
            subtype: {
                field: "subtype",
                type: Sequelize.STRING
            },
            subtype_id: {
                field: "subtype_id",
                type: Sequelize.INTEGER
            },
            payment_status: {
                field: "payment_status",
                type: Sequelize.STRING
            },
            payment_object: {
                field: "payment_object",
                type: Sequelize.JSON
            },
            createdAt: {
                field: "created_at",
                type: "TIMESTAMP",
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            },
            updatedAt: {
                field: "updated_at",
                type: "TIMESTAMP",
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            }
        },
        options: {}
    },
    actions: {
        getrequestPaymentHistory: {
            handler(ctx) {
                let { user_id, group_id, limit, offset,filter_startDate, filter_endDate } = ctx.params;
                let _filter = [`pr.request_type='fund'`];
                if(user_id) _filter.push(`pric.contribute_user_id=${user_id} and pric.is_active is true`);
                if(group_id) _filter.push(`G.id=${group_id}`);
                let _where = _filter.join(" and ");
                let filterDate = '';
                if(filter_startDate && filter_startDate != '') {
                   filter_endDate = filter_endDate ? filter_endDate : filter_startDate;
                   // Adding one day to enddate for getting correct result start
                //    filter_endDate = new Date(filter_endDate);
                //    filter_endDate.setDate(filter_endDate.getDate() + 1);
                //    filter_endDate = filter_endDate.toISOString().substring(0, 10);
                   // Adding one day to enddate for getting correct result start
                   filterDate = `and (extract(epoch from pric.created_at)::int between '${filter_startDate}' and '${filter_endDate}')`;
                }
                let _query = `select G.group_name, G.logo ,pri.id as item_id, pri.request_item_title,pric.payment_status, contribute_item_quantity, 
                extract(epoch from pric.created_at)::int as paid_on, payment_status, is_anonymous, pric.is_acknowledge,pric.payment_note
                FROM post_requests_item_contributions pric
                left join post_request_items pri on pri.id = pric.post_request_item_id
                left join post_requests pr on pr.id=pri.post_request_id  
                left join groups G on G.id=pric.group_id 
                where ${_where} and (pric.payment_type is not null or pric.payment_status = 'success' or pric.is_acknowledge is true) ${filterDate} order by  pric.created_at desc`;
                if(limit && offset){ _query = `${_query} offset ${offset} limit ${limit}`; }  
                return this.adapter.db.query(_query)
                .then(([res, metadata]) => res);
            }
        },
        getrequestPaymentHistoryByGroup: {
            handler(ctx) {
                let { group_id, limit, offset, query, filter_startDate, filter_endDate } = ctx.params;
                let _filter = [`pric.group_id is not null`,`pric.is_active is true`];
                if(group_id) _filter.push(`pric.group_id=${group_id}`);
                let _where = _filter.join(" and ");
                let searchQuery = '';
                if(query && query != '') {
                    searchQuery = ` and (G.group_name ilike '%${query}%' or U.full_name ilike '%${query}%' or pri.request_item_title ilike '%${query}%' or payment_status ilike '%${query}%' or pric.paid_user_name ilike '%${query}%')`;   
                }
                let filterDate = '';
                if(filter_startDate && filter_startDate != '') {
                   filter_endDate = filter_endDate ? filter_endDate : filter_startDate;
                   // Adding one day to enddate for getting correct result start
                //    filter_endDate = new Date(filter_endDate);
                //    filter_endDate.setDate(filter_endDate.getDate() + 1);
                //    filter_endDate = filter_endDate.toISOString().substring(0, 10);
                   // Adding one day to enddate for getting correct result start
                   filterDate = `and (extract(epoch from pric.created_at)::int between '${filter_startDate}' and '${filter_endDate}')`;
                }
                let _query = `select G.group_name, G.logo ,pri.id as item_id, pri.request_item_title, contribute_item_quantity, 
                extract(epoch from pric.created_at)::int as paid_on, payment_status, is_anonymous, U.id as user_id,U.full_name,
                pric.payment_status,U.propic,pric.paid_user_name,pric.payment_note
                FROM post_requests_item_contributions pric
                left join post_request_items pri on pri.id = pric.post_request_item_id
                left join post_requests pr on pr.id=pri.post_request_id  
                left join groups G on G.id=pric.group_id
                left join users U on U.id=pric.contribute_user_id 
                where ${_where} and (pric.payment_type is not null or pric.payment_status = 'success' or pric.is_acknowledge is true) ${searchQuery} ${filterDate} order by  pric.created_at desc`;
                if(limit && offset){ _query = `${_query} offset ${offset} limit ${limit}`; }  
                return this.adapter.db.query(_query)
                .then(([res, metadata]) => res);
            }
        }
    },
    events: {},
    methods: {},
    created() { },
    started() { },
    stopped() { },
};
