"use_strict";
//packages
const DbService = require('moleculer-db');
const SqlAdapter = require('moleculer-db-adapter-sequelize');
const Sequelize = require('sequelize');
//folder table model
module.exports = {
    name: "db.events_invitation",
    mixins: [DbService],
    settings: {},
    dependencies: [],
    cache: false,
    adapter: new SqlAdapter(process.env.SQL_DATABASE,null,null, {
        logger: false,
        pool: {
            max: 1,
            min: 0,
            acquire: 30000,
            idle: 10000,
            handleDisconnects: true
        },
        dialect: 'postgres',
        port: 5432,
        replication: {
            read: [
                { host: process.env.SQL_READ_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
            ],
            write: { host: process.env.SQL_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
        },
    }),
    model: {
        name: "events_invitation",
        define: {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            event_id: {
                field: "event_id",
                type: Sequelize.INTEGER,
                // references: {
                //     model: "events",
                //     key: 'id'
                // }
            },
            group_id: {
                field: "group_id",
                type: Sequelize.INTEGER,
                // references: {
                //     model: "groups",
                //     key: 'id'
                // }
            },
            user_id: {
                field: "user_id",
                type: Sequelize.INTEGER,
                // references: {
                //     model: "users",
                //     key: 'id'
                // }
            },
            invited_by: {
                field: "invited_by",
                type: Sequelize.INTEGER,
                // references: {
                //     model: "users",
                //     key: 'id'
                // }
            },
            type: {
                field: "type",
                type: Sequelize.STRING
            },
            duplicate: {
                field: "duplicate",
                type: Sequelize.BOOLEAN,
                defaultValue: false
            },
            createdAt: {
                field: "created_at",
                type: 'TIMESTAMP',
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            },
            updatedAt: {
                field: "updated_at",
                type: 'TIMESTAMP',
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            },
        },
        options: {}
    },
    actions: {
        /**
         * get the list of invitation send
         */
        invitationList: {
            handler(ctx) {
                let { event_id,user_id } = ctx.params;
                let offsetval = ctx.params.offset ? ctx.params.offset : 0;
                let limitval  = ctx.params.limit ? ctx.params.limit : 5;
                let _where = (user_id) ? `where ev_i.user_id = ${user_id}` : `where ev_i.event_id = ${event_id}`;
                let query = `select * from events_invitations ev_i 
                join users usr on ev_i.user_id  = usr.id
                join groups gp on ev_i.group_id = gp.id ${_where} OFFSET ${offsetval} LIMIT ${limitval}`;
                return this.adapter.db.query(query)
                .then(([res, metadata]) => res);
            }
        },
        getInvitedUsersDetails:{
            handler(ctx) {
                let { event_id } = ctx.params;
                let queryString = `select distinct u.id,u.full_name,u."location",u.propic,
                (select count(*) from group_maps where user_id = u.id) as family_count
                from
                events_invitations ei join users u 
                on
                u.id = ei.user_id
                where
                event_id = ${event_id}`;
                return this.adapter.db.query(queryString)
                    .then(([res, metadata]) => res);
            }
        },

        invitedToGroup:{
            params:{
                group_id:"string"
            },
            handler(ctx) {
                let { group_id, type, query } = ctx.params;
                let _filter = [];
                //_filter.push(`ei.duplicate is false`)
                if(group_id) _filter.push(`ei.group_id='${group_id}'`);
                if(query) {
                    _filter.push(`(e.event_name ilike '%${ctx.params.query}%' or e.location ilike '%${ctx.params.query}%')`);   
                }
                if(type && type !='-1') _filter.push(`e.is_public is ${type}`);
                let _where = _filter.join(' and ');

                let queryString = ` select distinct(ei.event_id,ei.group_id) ,ei.event_id,ei.invited_by,
                e.event_name,e.event_page,e.rsvp,e.event_image,e.event_original_image,
                extract(epoch from e.created_at) as created_at_,e.category,e.event_type,e.is_public as public_event,
                e.ticket_type,e.is_active,e.is_cancel,e.from_date,e.to_date,e."location",e.meeting_link,e.meeting_dial_number,
                e.meeting_pin,e.meeting_logo,
                (select count(*) from event_rsvps where attendee_type = 'going' and event_id = e.id) as going_count,
                (select full_name from users where id = e.created_by) as created_by_name,(select propic from users where id = e.created_by) as propic,e.created_by,
                ( select u.full_name from event_rsvps er join users u on er.attendee_id = u.id where attendee_type = 'going' and event_id = e.id limit 1) as first_person_going ,
                               (select count(*) from event_rsvps where event_id = e.id and attendee_type= 'interested' and is_active is true) as interested_count,
                               (
                           select count(*) from 
                               (
                               select user_id from group_maps where user_id
                                   in
                                   (
                                   select attendee_id from event_rsvps where event_id = e.id and attendee_type = 'going'
                                   ) 
                                   and
                                   group_id='${group_id}'
                                  group by 
                                  user_id
                              )
                                  as
                                  list) 
                              as known_count
                from events_invitations ei
                join events e
                on e.id = ei.event_id
                where ${_where} and is_active is true order  by created_at_ desc`;
                return this.adapter.db.query(queryString)
                .then(([res, metadata]) => res);
            }
        },

        bulkCreate:{
            handler(ctx){
                let { data } = ctx.params;
                return this.adapter.db.models.events_invitation.bulkCreate(data)
                .then(([res, metadata]) => res);
            }
        },
        /**
         * event invitations send to  groups
         */
        forGroups:{
            params:{
                user_id:"string"
            },
            handler(ctx) {
                let { user_id, event_id, query } = ctx.params;
                let _filter = [];
                _filter.push(`ei.type='groups'`);
                if(user_id)  _filter.push(`ei.invited_by = '${user_id}'`);
                if(event_id) _filter.push(`ei.event_id='${event_id}'`);
                if(query && query != '') _filter.push(`(g.group_name ilike '%${query}%' or g."base_region" ilike '%${query}%' )`);
                let _where = _filter.join(" and ");
                let queryString = `select distinct ei.event_id, ei.group_id ,e.event_name, e.event_image,e.event_original_image,e.from_date, e.to_date, g.group_name, g.base_region, g.base_region as location, g.logo
                from events_invitations ei 
                join groups g
                on g.id=ei.group_id
                join events e 
                on e.id=ei.event_id
                where  ${_where}  `;
                return this.adapter.db.query(queryString)
                .then(([res, metadata]) => res);
            }
        },
        /**
         * event invitations send to  users
         */
        forUsers:{
            params:{
                user_id:"string"
            },
            handler(ctx){
                let { user_id, event_id, query } = ctx.params;
                let _filter = [];
                _filter.push(`ei.type='users'`);
                if(user_id)  _filter.push(`ei.invited_by = '${user_id}'`);
                if(event_id) _filter.push(`ei.event_id='${event_id}'`);
                if(query && query != '') _filter.push(`(u.full_name ilike '%${query}%' or u."location" ilike '%${query}%' )`);
                let _where = _filter.join(" and ");
                let queryString = `select e.event_name, e.event_image,e.event_original_image,e.from_date, e.to_date, u.full_name, u.propic, u.location as location
                from events_invitations ei 
                join users u
                on u.id=ei.user_id
                join events e 
                on e.id=ei.event_id
                where ${_where}`;
                return this.adapter.db.query(queryString)
                .then(([res, metadata]) => res);
            }
        },

        /**
         * event invitations send to  users
         */
        other_users:{
            params:{
                user_id:"string"
            },
            handler(ctx){
                let { user_id, event_id, query } = ctx.params;
                let _filter = [];
                if(user_id)  _filter.push(`from_user = '${user_id}'`);
                if(event_id) _filter.push(`event_id='${event_id}'`);
                if(query && query != '') _filter.push(`(full_name ilike '%${query}%' or phone ilike '%${query}%' or email ilike '%${query}%')`);
                let _where = _filter.join(" and ");
                let masterQuery = `SELECT *
                FROM other_users
                where  ${_where}`;
                return this.adapter.db.query(masterQuery)
                .then(([res, metadata]) => res);
            }
        },
        bulkDeleteEventInvitationByFamily:{
            handler(ctx){
                let { group_id } = ctx.params;
                return this.adapter.db.models.events_invitation.destroy({
                    where:{
                        group_id:group_id
                    }
                }).then(res=>{
                    return res;
                });
            }
        },
        /* count of invitatons send*/
        invitationsSendCount:{
            handler(ctx){
                let { event_id } = ctx.params;
                let masterQuery = `SELECT count(*)
                FROM public.events_invitations
                where event_id=${event_id} and duplicate is false`;
                return this.adapter.db.query(masterQuery)
                .then(([res, metadata]) => res);
            }
        },


    },
    events: {},
    methods: {},
    created() { },
    started() { },
    stopped() { },
};
