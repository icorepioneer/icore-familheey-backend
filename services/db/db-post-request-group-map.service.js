/* eslint-disable indent */
"use_strict";
//packages
const DbService = require('moleculer-db');
const SqlAdapter = require('moleculer-db-adapter-sequelize');
const Sequelize = require('sequelize');
const moment = require('moment');
//user table model
module.exports = {
    name: "db.post_requests_group_map",
    mixins: [DbService],
    settings: {},
    dependencies: [],
    cache: false,
    adapter: new SqlAdapter(process.env.SQL_DATABASE,null,null, {
        logger: false,
        pool: {
            max: 1,
            min: 0,
            acquire: 30000,
            idle: 10000,
            handleDisconnects: true
        },
        dialect: 'postgres',
        port: 5432,
        replication: {
            read: [
                { host: process.env.SQL_READ_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
            ],
            write: { host: process.env.SQL_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
        },
    }),
    model: {
        name: "post_requests_group_map",
        define: {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            user_id: {
                field: "user_id",
                type: Sequelize.INTEGER,
            },
            group_id: {
                field: "group_id",
                type: Sequelize.INTEGER,
            },
            post_request_id: {
                field: "post_request_id",
                type: Sequelize.INTEGER,
            },
            is_active: {
                field: "is_active",
                type: Sequelize.BOOLEAN,
                defaultValue: true
            },
            createdAt: {
                field: "created_at",
                type: "TIMESTAMP",
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            },
            updatedAt: {
                field: "updated_at",
                type: "TIMESTAMP",
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            }
        },
        options: {}
    },
    actions: {
        bulkDelete:{
            handler(ctx) {
                let { idz } = ctx.params;
                let _query = `DELETE FROM post_requests_group_maps WHERE id IN (${idz})`;
                return this.adapter.db.query(_query)
                .then(([res, metadata]) => res);
            } 
        },
        /**
         * get the details of groups in which
         * request of need is send
         */
        requestToGroup:{
            handler(ctx){
                let { post_request_id,user_id } = ctx.params;
                let _query=`select g2.id,group_name,post_create,logo,base_region,stripe_account_id from "groups" g2
                left join group_maps GM on GM.group_id = g2.id and GM.user_id = ${user_id} 
                left join post_requests PR on PR.user_id = ${user_id} AND PR.id = ${post_request_id}
                where g2.is_active=true and g2.id in(SELECT group_id
                FROM post_requests_group_maps 
                where post_request_id = ${post_request_id} and is_active=true) AND 
                (
                    g2.request_visibility is null OR g2.request_visibility = 27 OR PR.user_id = ${user_id} OR 
                    (g2.request_visibility = 26 AND GM.type = 'admin')
                )`;
                return this.adapter.db.query(_query)
                .then(([res, metadata]) => res);
            }
        }
        
    },
    events: {},
    methods: {},
    created() { },
    started() { },
    stopped() { },
};
