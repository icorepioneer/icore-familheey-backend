/**
 * used to store information about each user request
 * for joining a family 
 */
"use_strict";
//packages
const DbService = require('moleculer-db');
const SqlAdapter = require('moleculer-db-adapter-sequelize');
const Sequelize = require('sequelize');


module.exports = {
    name: 'db.requests',
    mixins: [DbService],
    settings: {},
    dependencies: [],
    cache: false,
    adapter: new SqlAdapter(process.env.SQL_DATABASE,null,null, {
        logger: false,
        pool: {
            max: 1,
            min: 0,
            acquire: 30000,
            idle: 10000,
            handleDisconnects: true
        },
        dialect: 'postgres',
        port: 5432,
        replication: {
            read: [
                { host: process.env.SQL_READ_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
            ],
            write: { host: process.env.SQL_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
        },
    }),
    model: {
        name: "requests",
        define: {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            user_id:{
                field:"user_id",
                type:Sequelize.INTEGER,
                references:{
                    model:"users",
                    key:"id"
                },
            },
            group_id:{
                field:"group_id",
                type:Sequelize.INTEGER,
                references:{
                    model:"groups",
                    key:"id"
                },
                allowNull: false,
            },
            responded_by:{
                field:"responded_by",
                type:Sequelize.INTEGER,
                references:{
                    model:"users",
                    key:"id"
                }
            },
            email:{
                field:"email",
                type:Sequelize.STRING,
            },
            phone:{
                field:"phone",
                type:Sequelize.STRING,

            },
            from_id:{
                field:"from_id",
                type:Sequelize.INTEGER,
                references:{
                    model:"users",
                    key:"id"
                }
            },
            type:{
                field:"type",
                type:Sequelize.ENUM,
                values:["request","invite"]
            },
            status: {
                field: "status",
                type: Sequelize.ENUM,
                values: ["accepted", "rejected", "pending"],
                defaultValue:"pending"
            },
            location: {
                field: "location",
                type: Sequelize.STRING
            },
            lat: {
                field: "lat",
                type: Sequelize.DOUBLE
            },
            long: {
                field: "long",
                type: Sequelize.DOUBLE
            },
            createdAt: {
                field: "created_at",
                type: 'TIMESTAMP',
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            },
            updatedAt: {
                field: "updated_at",
                type: 'TIMESTAMP',
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            }
        }
    },
    actions:{
        findAllRequests:{
            params:{
                group_Id:"string"
            },
            handler(ctx){
                let { group_Id } = ctx.params;
                let masterQuery = `select DISTINCT ON (r.user_id) r.*,u.full_name,u.propic from requests r join users u on r.user_id = u.id  where r.group_id = ${group_Id} and r.status = 'pending' and r.type = 'request'`;
                return this.adapter.db.query(masterQuery)
                .then(([res, metadata]) => res);
            }
        },

        /**
         * list all requests send
         */
        ListRequest:{
            handler(ctx){
                let { group_id, user_id, type, status, txt } = ctx.params;
                let _filter = [];
                if(status) _filter.push(`r.status = '${status}'`)
                if(type) _filter.push(`r.type = '${type}'`)
                if(group_id) _filter.push(`r.group_id='${group_id}'`)
                if(user_id) _filter.push(`r.user_id='${user_id}'`)
                if(txt && user_id ) _filter.push(`( g.group_name ilike '%${txt}%' or g.base_region ilike '%${txt}%')`)
                if(txt && group_id)  _filter.push(`( u.full_name ilike '%${txt}%' )`)
                let _where = _filter.join(" and ");

                let masterQuery = `select r.*,u.full_name,u.propic,u.origin, u.location, g.group_name, g.logo, g.base_region,g.group_category,
                (select _u.full_name as created_by from users _u where _u.id=g.created_by)
                from requests r 
                join users u 
                on r.user_id = u.id  
                join groups g
                on (r.group_id = g.id and g.is_active is true)
                where ${_where}`;
                return this.adapter.db.query(masterQuery)
                .then(([res, metadata]) => res);
            }
        },
        update_requests: {
            handler(ctx) {
                let { user_id, group_id, status } = ctx.params;
                let _query = `UPDATE requests SET status = '${status}' where user_id = ${user_id} and  group_id = ${group_id}`;
                return this.adapter.db.query(_query)
                .then(([res, metadata]) => res);
            }
        },

        ExceptList: {
            handler(ctx) {
                let { user_id, group_id, status } = ctx.params;
                let _query = `SELECT id
                FROM requests
                where user_id=${user_id} and group_id=${group_id} and status!='${status}'`;
                return this.adapter.db.query(_query)
                .then(([res, metadata]) => res);
            }
        },

        bulkDeleteRequest:{
            handler(ctx) {
                let { idz } = ctx.params;
                let _query = `DELETE FROM requests WHERE id IN (${idz})`;
                return this.adapter.db.query(_query)
                .then(([res, metadata]) => res);
            } 
        }
    }   
}