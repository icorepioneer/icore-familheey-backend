"use_strict";
//packages
const DbService = require('moleculer-db');
const SqlAdapter = require('moleculer-db-adapter-sequelize');
const Sequelize = require('sequelize');
//folder table model
module.exports = {
    name: "db.document_map",
    mixins: [DbService],
    settings: {},
    dependencies: [],
    cache: false,
    adapter: new SqlAdapter(process.env.SQL_DATABASE,null,null, {
        logger: false,
        pool: {
            max: 1,
            min: 0,
            acquire: 30000,
            idle: 10000,
            handleDisconnects: true
        },
        dialect: 'postgres',
        port: 5432,
        replication: {
            read: [
                { host: process.env.SQL_READ_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
            ],
            write: { host: process.env.SQL_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
        },
    }),
    model: {
        name: "document_map",
        define: {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            folder_id:{
                field:"folder_id",
                type:Sequelize.INTEGER,
                allowNull:false
            },
            file_id:{
                field:"file_id",
                type:Sequelize.INTEGER
            },
            user_id:{
                field:"user_id",
                type:Sequelize.INTEGER
            },
            group_id:{
                field:"group_id",
                type:Sequelize.INTEGER
            },
            event_id:{
                field:"event_id",
                type:Sequelize.INTEGER,
                references:{
                    model:"events",
                    key:"id"
                }
            },
            shared_from_user:{
                field:"shared_from_user",
                type:Sequelize.INTEGER
            },
            shared_from_group:{
                field:"shared_from_group",
                type:Sequelize.INTEGER
            },
            is_blocked:{
                field:"is_blocked",
                type:Sequelize.BOOLEAN,
                defaultValue:false
            },
            is_removed:{
                field:"is_removed",
                type:Sequelize.BOOLEAN,
                defaultValue:false
            },
            createdAt: {
                field: "created_at",
                type: 'TIMESTAMP',
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            },
            updatedAt: {
                field: "updated_at",
                type: 'TIMESTAMP',
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            },
        },
        options: {}
    },
    actions: {
        bulkCreate:{
            handler(ctx){
                let { data } = ctx.params;
                return this.adapter.db.models.document_map.bulkCreate(data).then(res=>{
                    return res;
                });
            }
        },
        bulkDelete:{
            handler(ctx){
                let { folder_id,key,value } = ctx.params;
                return this.adapter.db.models.document_map.destroy({
                    where:{
                        folder_id:folder_id,
                        [`${key}`]:value 
                    }
                }).then(res=>{
                    return res;
                });
            }
        }
    },
    events: {},
    methods: {},
    created() { },
    started() { },
    stopped() { },
};
// .then(([res, metadata]) => res);
