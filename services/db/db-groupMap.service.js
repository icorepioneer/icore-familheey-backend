"use_strict";
//packages
const DbService = require('moleculer-db');
const SqlAdapter = require('moleculer-db-adapter-sequelize');
const Sequelize = require('sequelize');
module.exports = {
    name: "db.group_map",
    mixins: [DbService],
    settings: {},
    dependencies: [],
    cache: false,
    adapter: new SqlAdapter(process.env.SQL_DATABASE,null,null, {
        logger: false,
        pool: {
            max: 1,
            min: 0,
            acquire: 30000,
            idle: 10000,
            handleDisconnects: true
        },
        dialect: 'postgres',
        port: 5432,
        replication: {
            read: [
                { host: process.env.SQL_READ_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
            ],
            write: { host: process.env.SQL_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
        },
    }),
    model: {
        name: "group_map",
        define: {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            user_id: {
                field: "user_id",
                type: Sequelize.INTEGER,
                // references: {
                //     model: "users",
                //     key: "id"
                // }
            },
            group_id: {
                field: "group_id",
                type: Sequelize.INTEGER,
                // references: {
                //     model: "groups",
                //     key: "id"
                // }
            },
            following: {
                field: "following",
                type: Sequelize.BOOLEAN,
                defaultValue:true
            },
            is_blocked: {
                field: "is_blocked",
                type: Sequelize.BOOLEAN,
                defaultValue: false
            },
            type: {
                field: "type",
                type: Sequelize.STRING,
                defaultValue:"member",
                allowNull:false
            },
            membership_id: {
                field: "membership_id",
                type: Sequelize.INTEGER,
            },
            membership_from: {
                field: "membership_from",
                type: Sequelize.INTEGER,
            },
            membership_to: {
                field: "membership_to",
                type: Sequelize.INTEGER,
            },
            membership_fees: {
                field: "membership_fees",
                type: Sequelize.INTEGER,
            },
            membership_paid_on: {
                field: "membership_paid_on",
                type: Sequelize.INTEGER,
            },
            membership_duration: {
                field: "membership_duration",
                type: Sequelize.INTEGER,
            },
            membership_payment_status: {
                field: "membership_payment_status",
                type: Sequelize.STRING,
            },
            membership_period_type: {
                field: "membership_period_type",
                type: Sequelize.STRING,
            },
            membership_payment_notes:{
                field:"membership_payment_notes",
                type:Sequelize.TEXT
            },
            membership_customer_notes:{
                field:"membership_customer_notes",
                type:Sequelize.TEXT
            },
            payment_object: {
                field: "payment_object",
                type: Sequelize.JSON
            },
            createdAt: {
                field: "created_at",
                type: 'TIMESTAMP',
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            },
            updatedAt: {
                field: "updated_at",
                type: 'TIMESTAMP',
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            },
            is_removed: {
                field: "is_removed",
                type: Sequelize.BOOLEAN,
                defaultValue: false
            },
            membership_payment_type: {
                field: "membership_payment_type",
                type: Sequelize.STRING
            },
            membership_payment_method: {
                field: "membership_payment_method",
                type: Sequelize.STRING
            },
            membership_total_payed_amount: {
                field: "membership_total_payed_amount",
                type: Sequelize.FLOAT,
                defaultValue:0.0
            },
            membership_ref_id: {
                field: "membership_ref_id",
                type: Sequelize.STRING,
            },
        }
    },
    actions: {
        getUsersOfCurrentGroup: {
            handler(ctx) {
                let { group_id,type } = ctx.params;
                let _filter = ["is_blocked=false and is_removed=false"];
                if(group_id) { _filter.push(`m.group_id = ${group_id}`); }
                if(type) { _filter.push(`m.type ='${type}'`); }
                let _where = _filter.join(" and ");
                let masterQuery = `select distinct(u.id),m.type as member_type,u.full_name,u.email,u.phone,u.propic,u.dob,u.gender,u.about
                from group_maps m join users u on u.id = m.user_id where ${_where}`;
                return this.adapter.db.query(masterQuery)
                .then(([res, metadata]) => res);
            }
        },
        /**
         * fetch group details of a user
         * params : userid
         */
        findGroupOfaUser: {
            handler(ctx) {
                let { user_id, group_id,txt, is_linkable } = ctx.params;
                let _filter = [];
                _filter.push(`g.is_active = true and g.searchable = true and gm.is_blocked=false and gm.is_removed=false`);
                if(user_id)  { _filter.push(`gm.user_id = ${user_id}`); }
                if(group_id) { _filter.push(`gm.group_id != ${group_id}`); }
                if(txt){ _filter.push(`g.group_name ilike '%${txt}%'`); }
                if(is_linkable){ _filter.push(`g.is_linkable=${is_linkable}`); }

                let _where = _filter.join(' and ');
                let _query = `SELECT g.id,g.group_name,g.logo, g.is_active,g.searchable,g.is_linkable,g.link_family,
                g.link_approval,g.post_visibilty,g.post_approval,g.post_create,g.member_approval,g.member_joining,g.base_region,
                (select full_name from users where id = g.created_by) as created_by
                FROM groups g
                JOIN group_maps gm
                on gm.group_id = g.id
                where ${_where} group by g.id`;
                return this.adapter.db.query(_query)
                .then(([res, metadata]) => res)
                .catch(err => err);
            }
        },
        fetchlinkedFamily: {
            handler(ctx) {
                let { group_id, user_id } = ctx.params;
                let _query = `SELECT distinct(l.id),l.from_group,l.to_group,l.requested_by,l.status,l.created_at,l.updated_at,l.is_active,
                (select count(*) as mutual_count from ((select user_id from group_maps where group_id = l.from_group ) 
                   intersect
                (select user_id from group_maps where group_id = l.to_group)) as mutual)
                    as mutual_count
                FROM 
                family_links l join group_maps g
                on l.from_group = g.group_id
                where
                from_group= ${group_id} and to_group in (
                SELECT g.id FROM groups g JOIN group_maps gm
                                        on
                                        gm.group_id = g.id
                                        where
                                        g.is_active = true 
                                        and
                                        g.searchable = true 
                                        and
                                        gm.is_blocked = false
                                        and
                                        gm.is_removed = false
                                        and
                                        gm.user_id = ${user_id}
                                        and
                                        gm.group_id != ${group_id}
                                        group by
                                        g.id )`;
                return this.adapter.db.query(_query)
                    .then(([res, metadata]) => res)
                    .catch(err => err);

            }
        },
        eventInviteGroup: {
            params: {},
            handler(ctx) {
                let { group_id,reqFrom } = ctx.params;
                let _group_id = group_id.join().split(',').map(Number);
                let post_request_query = '';
                if(reqFrom && reqFrom == "post_request") {
                    post_request_query = `and ((g.request_visibility = 26 and gm."type" = 'admin') or g.request_visibility = 27)`;
                }
                let _query = `select  gm.user_id,gm.group_id,gm.is_blocked,gm.is_removed,g.group_name, usr.email, usr.phone,usr.full_name,usr.propic
                FROM group_maps gm
                join users usr
                on gm.user_id = usr.id
                join "groups" as g 
                on gm.group_id = g.id
                WHERE group_id IN (${_group_id}) ${post_request_query} and gm.is_removed is false and gm.is_blocked is false`;
                return this.adapter.db.query(_query)
                    .then(([res, metadata]) => res)
                    .catch(err => err);
            }
        },
        getFamilyCount: { 
            params: {
                user_id: "string"
            },
            handler(ctx) {
                let { user_id } = ctx.params;
                let masterQuery = `select count(distinct(g.id)) from group_maps gm join "groups" g on g.id = gm.group_id 
                                where g.is_active is true and gm.user_id = ${user_id} and gm.is_removed is false and gm.is_blocked is false`;
                return this.adapter.db.query(masterQuery)
                    .then(([res, metadata]) => res)
                    .catch(err => err);
            }
        },
        getConnection: {
            params: {
                user_id: "string"
            },
            handler(ctx) {
                let { user_id } = ctx.params;
                let masterQuery = `select u.* 
                                            from
                                            group_maps gm join users u 
                                            on
                                            gm.user_id = u.id
                                            join "groups" g on g.id = gm.group_id
                                            where
                                            gm.is_blocked is false
                                            and gm.is_removed is false
                                            and u.is_active is true
                                            and u.is_verified is true
                                            and g.is_active is true
                                            and g.group_type = 'private'
                                            and u.id != ${user_id}
                                            and
                                            gm.group_id in (select group_id from group_maps where user_id = ${user_id} group by group_id) group by u.id`;
                return this.adapter.db.query(masterQuery)
                    .then(([res, metadata]) => res)
                    .catch(err => err);
            }
        },
        getMutualConnections: {
            params: {
                user_one_id: "string",
                user_two_id: "string"
            },
            handler(ctx) {
                let { user_one_id, user_two_id } = ctx.params;
                let masterQuery = `select distinct(u.id),u.full_name,u.origin,u.gender,u.propic,u.location
                                                from
                                                group_maps gm  join  users u 
                                                on
                                                u.id = gm.user_id 
                                                where 
                                                gm.group_id 
                                                in
                                                (
                                                    (select gm.group_id from group_maps gm join "groups" grp on grp.id = gm.group_id where grp.is_active is true and gm.user_id = ${user_one_id} and gm.is_blocked is false and gm.is_removed is false order by group_id )
                                                        intersect
                                                    (select gm.group_id from group_maps gm join "groups" grp on grp.id = gm.group_id where grp.is_active is true and gm.user_id = ${user_two_id} and gm.is_blocked is false and gm.is_removed is false order by group_id)
                                                ) and gm.user_id not in (${user_one_id},${user_two_id})`;
                return this.adapter.db.query(masterQuery)
                    .then(([res, metadata]) => res)
                    .catch(err => err);

            }
        },
        getMutualFamilies: {
            params: {
                user_id_one: "string",
                user_id_two: "string",
            },
            handler(ctx) {
                let { user_id_one, user_id_two } = ctx.params;
                let masterQuery = `select id as group_id 
                from "groups" g 
                where id in ((select group_id from group_maps where user_id = ${user_id_one} and is_removed is false and is_blocked is false)
                intersect (select group_id from group_maps where user_id = ${user_id_two} and is_removed is false and is_blocked is false)) and g.is_active is true`;
                return this.adapter.db.query(masterQuery)
                .then(([res, metadata]) => res)
                .catch(err => err);
            }
        },
        /**
         * get the details of a group
         * same as  getMutualFamilies but with details
         */
        userMutualGroup: {
            params: {
                user_id_one: "string",
                user_id_two: "string",
            },
            handler(ctx) {
                let { user_id_one, user_id_two } = ctx.params;
                let masterQuery = `select g.*, u.full_name from "groups" g
                join users u on g.created_by = u.id
                where g.id in ((select group_id from group_maps where user_id = ${user_id_one} and is_blocked is false and is_removed is false)
                intersect (select group_id from group_maps where user_id = ${user_id_two} and is_blocked is false and is_removed is false)) and g.is_active is true`;
                return this.adapter.db.query(masterQuery)
                .then(([res, metadata]) => res)
                .catch(err => err);
            }
        },


        getKnownConnections: {
            params: {
                group_id: "string",
                user_id: "string",
            },
            handler(ctx) {
                let { group_id, user_id } = ctx.params;
               
                let masterQuery = `select count(*) from ( 
                    select user_id from group_maps GM1, users U1 where group_id=${group_id} and user_id != ${user_id} 
                    and is_blocked is false and is_removed is false  and U1.id = GM1.user_id and U1.is_active is true
                        intersect
                    select user_id from group_maps GM2, users U2 where group_id in (
                        select group_id from group_maps gmp  
                        join "groups" grp on grp.id = gmp.group_id where user_id = ${user_id}
                        and is_blocked is false and is_removed is false and grp.is_active is true
                    ) 
                    and is_blocked is false and is_removed is false and U2.id = GM2.user_id and U2.is_active is true
                ) as known`;
                return this.adapter.db.query(masterQuery)
                .then(([res, metadata]) => res)
                .catch(err => err);
                // return this.adapter.db.query(`select count(*) from ((select distinct(user_id) from group_maps _gm3 where group_id in(
                //     (select group_id from group_maps GM1, users U1 where user_id = ${user_id} and is_blocked is false and is_removed is false and U1.id = GM1.user_id)) and
                //     _gm3.is_removed is false and _gm3.is_blocked is false order by user_id asc)
                //         intersect
                //     (select user_id from group_maps GM2, users U2 where group_id = ${group_id} and is_removed is false and is_blocked is false and U2.id = GM2.user_id))as known
                // `)
                // .then(([res, metadata]) => res)
                // .catch(err => err)
            }
        },
        isMemberORAdmin: {
            params: {
                group_id: "number",
                user_id: "number"
            },
            handler(ctx) {
                let { group_id, user_id } = ctx.params;
                let masterQuery = `
                select case 
                when (select count (*) from (select * from group_maps where user_id = ${user_id} and group_id = ${group_id} and is_removed is false and is_blocked is false) as member) > 0
                    then
                        case		
                            when gm.type = 'admin' then 'admin'
                                                else 'member'
                        end						
                    else case when type is null then 'not-member' end 	
                    end	
                as user_type from group_maps gm where user_id = ${user_id} and group_id = ${group_id}`;
                return this.adapter.db.query(masterQuery).then(([res, metadata]) => res);
            }
        },
        fetchGroup_members:{
            handler(ctx) {
                let { group_id, type } = ctx.params;

                let _filter = [];
                _filter.push(`gm.group_id='${group_id}'`);
                if(type && type != '') _filter.push(`gm.type = '${type}'`);
                let _where = _filter.join(" and ");

                let _query = `select u.id,u.email, u.full_name, u.phone
                from group_maps as gm
                join users as u
                on gm.user_id = u.id
                where ${_where} and gm.is_blocked is false and gm.is_removed is false`;
                return this.adapter.db.query(_query).then(([res, metadata]) => res);
            }
        },
        /**
         * get all members based on a user joined groups
         */
        userGroupMembers:{
            handler(ctx) {
                let { user_id, limit, offset,query } = ctx.params;
                let offsetval = offset ? offset : 0;
                let limitval  = limit  ? limit : 50;
                let event_id = ctx.params.event_id ? ctx.params.event_id : 0;
                let searchQuery = query && query!=''?`and u.full_name ilike '%${query}%'`:'';
                let defaultQuery = `SELECT distinct(gm1.user_id), u.full_name,u.propic,u.location,u.created_at,
                (case when (select count(*) from events_invitations where user_id = u.id and event_id = ${event_id}) > 0
                then true else false end) as invitation_status
                FROM group_maps gm
                INNER JOIN group_maps gm1 
                ON gm.group_id = gm1.group_id
                AND gm.user_id = '${user_id}'
                join users u
                on u.id = gm1.user_id
                where u.id != '${user_id}' and u.is_active is true
                ${searchQuery} order by gm1.user_id OFFSET ${offsetval} LIMIT ${limitval}`;
                if(searchQuery && searchQuery != '' && searchQuery != null) {
                    defaultQuery =  `SELECT u.id as user_id,u.full_name,u.propic,u.location,u.created_at,
                    (case when (select count(*) from events_invitations where user_id = u.id and event_id = ${event_id}) > 0
                    then true else false end) as invitation_status
                    FROM users u
                    where u.is_active is true
                    ${searchQuery} order by u.id OFFSET ${offsetval} LIMIT ${limitval}`; 
                }
                let _query = defaultQuery;
                return this.adapter.db.query(_query).then(([res, metadata]) => res);
            }
        },
        /**
         * blocked users list of a group
         */
        getBlockedUsersGroup:{
            handler(ctx) {
                let { group_id, query } = ctx.params;
                let _filter = [];
                _filter.push(`is_blocked is true`);
                if(group_id) _filter.push(`group_id=${group_id}`);
                if(query){ _filter.push(` (u.full_name ilike '%${query}%' or u.location ilike '%${query}%' )`); }
                let _where = _filter.join(' and ');

                let _query = `SELECT u.full_name, u.propic,  gp.id as groupmap_id, u.id as user_id,gp.group_id
                FROM group_maps as gp
                join users u
                on u.id=gp.user_id
                where ${_where}`;
                return this.adapter.db.query(_query).then(([res, metadata]) => res);
            }
        },

        getMutualConnectionsList: {
            params: {
                user_one_id: "string",
                user_two_id: "string"
            },
            handler(ctx) {
                let { user_one_id, user_two_id } = ctx.params;
                let masterQuery = `select distinct(gm.user_id),u.full_name,u."location",u.gender,u.propic,to_char(u.created_at, 'DD MON YYYY') as member_since
                from
                group_maps gm join users u 
                on
                u.id = gm.user_id 
                where 
                gm.group_id 
                in
                ( (select gm.group_id from group_maps gm where gm.user_id = ${user_one_id} order by group_id )
                intersect
                (select gm.group_id from group_maps gm where gm.user_id = ${user_two_id} order by group_id) )`;
                return this.adapter.db.query(masterQuery)
                .then(([res, metadata]) => res)
                .catch(err => err);
            }
        },
        family_count:{
            params:{
                user_id:"string"
            },
            handler(ctx){
                let { user_id } = ctx.params;
                let masterQuery = `select count(*) from groups g 
                where g.id in( select group_id from group_maps gm where gm.is_blocked is false and gm.is_removed is false and user_id=${user_id} )
                and g.is_active is true`;
                return this.adapter.db.query(masterQuery)
                .then(([res, metadata]) => res)
                .catch(err => err);
            }
        },

        mygroupMember:{
            params:{
                user_id:"string"
            },
            handler(ctx){
                let { user_id } = ctx.params;
                let masterQuery = `select distinct(user_id) from group_maps _gm where group_id in(
                    select group_id
                    from group_maps gm 
                    where user_id = '${user_id}' and gm.is_blocked is false and gm.is_removed is false) and _gm.user_id !='${user_id}'`;
                return this.adapter.db.query(masterQuery)
                .then(([res, metadata]) => res)
                .catch(err => err);
            }
        },
        checkUsersConnected:{
            params:{
                // user_id:"string",
                // login_user:"string"
            }, 
            handler(ctx){
                let { user_id,login_user } = ctx.params;
                let masterQuery = `with checkGroupmap as (
                    (select gm.group_id from group_maps gm where 
                    gm.user_id = '${login_user}' and gm.is_blocked is false and gm.is_removed is false)
                        intersect
                    (select gm.group_id from group_maps gm where
                    gm.user_id = '${user_id}' and gm.is_blocked is false and gm.is_removed is false order by group_id)
                )                
                SELECT * FROM checkGroupmap WHERE (SELECT COUNT(*) FROM checkGroupmap) > 0 
                    union
                select * from (select topic_id from topic_maps where user_id = '${login_user}' and is_active is true and is_accept is true 
                    intersect
                select topic_id from topic_maps where user_id = '${user_id}' and is_active is true and is_accept is true) as topicData
                where (SELECT COUNT(*) FROM checkGroupmap) = 0`;
                return this.adapter.db.query(masterQuery)
                .then(([res, metadata]) => res)
                .catch(err => err);
            }  
        }
    },
    events: {},
    methods: {},
    created() { },
    started() { },
    stopped() { },
};
