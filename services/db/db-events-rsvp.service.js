"use_strict";
//packages
const DbService = require('moleculer-db');
const SqlAdapter = require('moleculer-db-adapter-sequelize');
const Sequelize = require('sequelize');
//folder table model
module.exports = {
    name: "db.event_rsvp",
    mixins: [DbService],
    settings: {},
    dependencies: [],
    cache: false,
    adapter: new SqlAdapter(process.env.SQL_DATABASE,null,null, {
        logger: false,
        pool: {
            max: 1,
            min: 0,
            acquire: 30000,
            idle: 10000,
            handleDisconnects: true
        },
        dialect: 'postgres',
        port: 5432,
        replication: {
            read: [
                { host: process.env.SQL_READ_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
            ],
            write: { host: process.env.SQL_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
        },
    }),
    model: {
        name: "event_rsvp",
        define: {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            event_id: {
                field:"event_id",
                type: Sequelize.INTEGER,
                // references:{
                //     model:"events",
                //     key:"id"
                // }
            }, 
            attendee_id: {
                field:"attendee_id",
                type: Sequelize.INTEGER,
                // references:{
                //     model:"users",
                //     key:"id"
                // }
            },
            attendee_type: {
                field:"attendee_type",
                type: Sequelize.ENUM,
                values: ["going", "interested", "not-going"]
            },
            is_active:{
                field:"is_active",
                type:Sequelize.BOOLEAN,
                defaultValue:true
            },
            others_count:{
                field:"others_count",
                type:Sequelize.INTEGER,
                defaultValue:0
            },
            createdAt: {
                field: "created_at",
                type: 'TIMESTAMP',
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            },
            updatedAt: {
                field: "updated_at",
                type: 'TIMESTAMP',
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            },
        },
        options: {}
    },
    actions: {
        getRsvpCount:{
            handler(ctx) {
                let { event_id } = ctx.params;
                let queryString = `select event_id,attendee_type,count(attendee_type) from event_rsvps where event_id = ${event_id} group by attendee_type,event_id`;
                return this.adapter.db.query(queryString)
                    .then(([res, metadata]) => res);
            }
        },
        getRsvpDetails:{
            handler(ctx){
                let {event_id} = ctx.params;
                let queryString = `select u.*,e.event_name,er.event_id,er.attendee_type
                from
                event_rsvps er join events e 
                on
                er.event_id = e.id 
                join 
                users u on u.id = er.attendee_id
                where er.event_id = ${event_id}
                group by attendee_type,event_id,e.id,u.id`;
                return this.adapter.db.query(queryString).then(([res, metadata]) => res);
            }
        },
        getDetailsOfUsersGoing:{
            handler(ctx){
                let {user_id,event_id,txt} = ctx.params;
                let searchQuery = txt?`and u.full_name ilike '%${txt}%'`:'';
                let queryString = `select u.id,u.full_name,u.propic,u."location",er.others_count,
                (select count(*) from group_maps _gm join "groups" _g on _g.id=_gm.group_id where user_id = u.id and is_removed is false and is_blocked is false and _g.is_active is true) as total_family,
                (select count(*) from (select distinct gm.user_id,u.full_name,u."location",u.gender,u.created_at
									from
									group_maps gm
									where 
									gm.group_id 
									in
									(
										(select gm.group_id from group_maps gm where gm.user_id = ${user_id} and gm.is_blocked is false and is_removed is false order by group_id )
											intersect
										(select gm.group_id from group_maps gm where gm.user_id = u.id and gm.is_blocked is false and is_removed is false order by group_id)
									)) as mutual) as mutual_contact
                from
                event_rsvps er join users u
                on
                u.id = er.attendee_id
                where 
                u.is_active is true
                and
                u.is_verified is true
                and er.attendee_type = 'going'
                and er.event_id = ${event_id}
                ${searchQuery}`;
                return this.adapter.db.query(queryString).then(([res, metadata]) => res);
            }
        },
        getDetailsOfUsersBasedOnRSVP:{
            handler(ctx){
                let {user_id,event_id,rsvp_status,txt} = ctx.params;
                let searchQuery = txt?`and u.full_name ilike '%${txt}%'`:'';
                let queryString = `select u.id,u.full_name,u.propic,u."location",er.others_count,
                (select count(*) from group_maps _gm join groups _g on _g.id=_gm.group_id where user_id = u.id and is_removed is false and is_blocked is false and _g.is_active is true) as total_family,
                ( select count(*) from (select distinct gm.user_id
									from
                                    group_maps gm
                                    join groups _G
                                    on _G.id=gm.group_id    
									where _G.is_active is true and
									gm.group_id 
									in
									(
                                        (select gm.group_id from group_maps gm where gm.user_id = ${user_id} and gm.is_blocked is false and gm.is_removed is false order by group_id )
                                        intersect
										(select gm.group_id from group_maps gm where gm.user_id = u.id and gm.is_blocked is false and gm.is_removed is false order by group_id)
									) and gm.user_id not in (${user_id},u.id) ) as mutual) as mutual_contact
                from
                event_rsvps er join users u
                on
                u.id = er.attendee_id
                where 
                u.is_active is true
                and
                u.is_verified is true
                and er.attendee_type = '${rsvp_status}'
                and er.event_id = ${event_id}
                ${searchQuery}`;
                return this.adapter.db.query(queryString).then(([res, metadata]) => res);
            }
        },
        rsvpExceptList:{//interested & going list
            handler(ctx){
                let {event_id, except} = ctx.params;
                let queryString = `select u.id as user_id,u.full_name,u.email, u.phone, e.event_name,er.event_id 
                from
                event_rsvps er join events e 
                on
                er.event_id = e.id 
                join 
                users u on u.id = er.attendee_id
                where er.event_id = ${event_id} and er.attendee_type !='${except}'`;
                return this.adapter.db.query(queryString).then(([res, metadata]) => res);
            }
        },
        /**
         * count the event going,not-going-, interested
         */
        countOfEventList:{//interested & going list
            handler(ctx){
                let { event_id, attendee_type } = ctx.params;
                let _filter = [`attendee_type = '${attendee_type}'`,`event_id=${event_id}`,`is_active is true`];
                let varQ = `count(*)`
                if(attendee_type == 'going'){
                    varQ = `count(*)+sum(others_count) as count`
                }
                let where = _filter.join(" and ");
                let _Query = `SELECT ${varQ}
                from event_rsvps
                where ${where}`;
                return this.adapter.db.query(_Query)
                .then(([res, metadata]) => res);
            }
        },
        /*  rsvp list by attendee id*/
        findByUserid:{//byuserId
            handler(ctx){
                let { event_id, attendee_id } = ctx.params;
                let _Query = `SELECT *
                FROM event_rsvps
                where event_id = ${event_id} and attendee_id=${attendee_id}`;
                return this.adapter.db.query(_Query)
                .then(([res, metadata]) => res);
            }
        }

    },
    events: {},
    methods: {},
    created() { },
    started() { },
    stopped() { },
};
