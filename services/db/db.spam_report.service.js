/* eslint-disable indent */
"use_strict";
//packages
const DbService = require('moleculer-db');
const SqlAdapter = require('moleculer-db-adapter-sequelize');
const Sequelize = require('sequelize');

//user table model
module.exports = {
    name: "db.spam_report",
    mixins: [DbService],
    settings: {},
    dependencies: [],
    cache: false,
    adapter: new SqlAdapter(process.env.SQL_DATABASE,null,null, {
        logger: false,
        pool: {
            max: 1,
            min: 0,
            acquire: 30000,
            idle: 10000,
            handleDisconnects: true
        },
        dialect: 'postgres',
        port: 5432,
        replication: {
            read: [
                { host: process.env.SQL_READ_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
            ],
            write: { host: process.env.SQL_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
        },
    }),
    model: {
        name: "spam_report",
        define: {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            type_id: {
                field: "type_id",
                type: Sequelize.INTEGER,
            },
            user_id: {
                field: "user_id",
                type: Sequelize.INTEGER,
            },
            spam_page_type: {
                field: "spam_page_type",
                type: Sequelize.STRING,
            },
            description: {
                field: "description",
                type: Sequelize.TEXT,
            },
            is_active: {
                field: "is_active",
                type: Sequelize.BOOLEAN,
                defaultValue: true
            }
        },
        options: {}
    },
    actions: {
        check: {
            handler(ctx) {
                let { post_id } = ctx.params;
                let query = `select (select count(*) as view_count from post_views where post_id = ${post_id}) ,(select count(*) as spam_count from spam_reports where type_id = ${post_id} and spam_page_type = 'post')  from spam_reports limit 1;`;
                return this.adapter.db.query(query)
                    .then(([res, metadata]) => res);
            }
        },
    },
    events: {},
    methods: {},
    created() { },
    started() { },
    stopped() { },
};