"use_strict";
//packages
const DbService = require('moleculer-db');
const SqlAdapter = require('moleculer-db-adapter-sequelize');
const Sequelize = require('sequelize');

//family-look up table model
module.exports = {
    name: "db.relationshipLookup",
    mixins: [DbService],
    settings: {},
    dependencies: [],
    cache: false,
    adapter: new SqlAdapter(process.env.SQL_DATABASE,null,null, {
        logger: false,
        pool: {
            max: 1,
            min: 0,
            acquire: 30000,
            idle: 10000,
            handleDisconnects: true
        },
        dialect: 'postgres',
        port: 5432,
        replication: {
            read: [
                { host: process.env.SQL_READ_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
            ],
            write: { host: process.env.SQL_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
        },
    }),
    model: {
        name: "relationship_lookups",
        define: {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            relationship: {
                field: "relationship",
                type: Sequelize.STRING,
            },
            opposite_relation_male: {
                field: "opposite_relation_male",
                type: Sequelize.STRING
            },
            opposite_relation_female: {
                field: "opposite_relation_female",
                type: Sequelize.STRING
            },
            type: {
                field: "type",
                type: Sequelize.STRING,
                defaultValue:'org'
            },
            createdAt: {
                field: "created_at",
                type: 'TIMESTAMP',
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            },
            updatedAt: {
                field: "updated_at",
                type: 'TIMESTAMP',
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            }

        },
        options: {}
    },
    actions: {
        getallRelations:{
            handler(ctx){
                let { type, query } = ctx.params;
                var _filter = [];
                let _type = (type == "regular") ? "regular":"org";
                _filter.push(`type = '${_type}'`)
                if(query && query != ''){ _filter.push(`relationship ilike '%${query}%'`) }
                let _where = _filter.join(" and ");
                let masterQuery = `SELECT * FROM relationship_lookups where ${_where} ORDER by relationship ASC`;
                return this.adapter.db.query(masterQuery)
                .then(([res, metadata]) => res);
            }
        },
        /**
         * check relationship string exist
         */
        isExist:{
            handler(ctx){
                let { relationship } = ctx.params;
                let masterQuery = `select id,relationship
                from relationship_lookups rl
                where LOWER( rl.relationship ) iLIKE LOWER('%${relationship}%')`;
                return this.adapter.db.query(masterQuery)
                .then(([res, metadata]) => res);
            }
        }
    },
    events: {},
    methods: {},
    created() { },
    started() { },
    stopped() { },
}