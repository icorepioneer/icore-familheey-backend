"use_strict";
//packages
const DbService = require('moleculer-db');
const SqlAdapter = require('moleculer-db-adapter-sequelize');
const Sequelize = require('sequelize');

module.exports = {
    name: "db.calender",
    mixins: [DbService],
    settings: {},
    dependencies: [],
    cache: false,
    adapter: new SqlAdapter(process.env.SQL_DATABASE,null,null, {
        logger: false,
        pool: {
            max: 1,
            min: 0,
            acquire: 30000,
            idle: 10000,
            handleDisconnects: true
        },
        dialect: 'postgres',
        port: 5432,
        replication: {
            read: [
                { host: process.env.SQL_READ_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
            ],
            write: { host: process.env.SQL_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
        },
    }),
    model: {
        name: "calender",
        define: {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            event_id: {
                field: "event_id",
                type: Sequelize.INTEGER,
                // references:{
                //     model:"events",
                //     key:"id"
                // }
            },
            group_id: {
                field: "group_id",
                type: Sequelize.INTEGER,
                // references:{
                //     model:"groups",
                //     key:"id"
                // }
            },
            user_id: {
                field: "user_id",
                type: Sequelize.INTEGER,
                // references:{
                //     model:"users",
                //     key:"id"
                // }
            },
            type: {
                field: "user_id",
                type: Sequelize.STRING
            },
            is_active:{
                field: "is_active",
                type: Sequelize.BOOLEAN,
                defaultValue:true
            },
            reminder:{
                field: "reminder",
                type: Sequelize.STRING,
            },
            createdAt: {
                field: "created_at",
                type: 'TIMESTAMP',
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            },
            updatedAt: {
                field: "updated_at",
                type: 'TIMESTAMP',
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            }
        },
        options: {}
    },
    actions: {
        /**
         * fetch calender data for a user
         */
        fetchCalender:{
            params:{
                user_id:"string"
            },
            handler(ctx){
                let { user_id, query , from_date,to_date,ticket_type,event_type, type } = ctx.params;

                let _filter = [];
                _filter.push(` el.is_active is true and el.is_cancel is false`);
                if(query){ _filter.push(`(el.event_name ILIKE '%${query}%' or el.location ILIKE '%${query}%' or el.group_name ILIKE '%${query}%')`);}
                if(from_date)  { _filter.push(` el.from_date >= '${from_date}'`);}
                if(to_date)    { _filter.push(` el.to_date <= '${to_date}'`);}
                if(ticket_type){ _filter.push(` el.ticket_type = '${ticket_type}'`); }
                if(event_type) { _filter.push(` el.event_type = '${event_type}'`); }              
                if(type && type!="-1") { _filter.push(` el.is_public = '${type}'`); }             
                let _where = _filter.join(" and ");
                let _query = `select * from (select distinct(e.id) as event_id,e.is_active,e.is_cancel,e.event_name,e.is_public,e.location,e.event_type,e.event_image,e.event_original_image,e.from_date,e.to_date, 
                u.full_name as created_by_name, u.propic as created_by_pic, g.group_name 
                from events e 
                left join events_shares es on e.id = es.event_id
                left join calenders c on c.event_id = e.id
                left join users  u on u.id = e.created_by
                left join groups g on g.id = c.group_id
                where  e.id not in (select event_id from event_rsvps where attendee_id=${user_id} and attendee_type='not-going')  and (es.user_id = ${user_id} and es.duplicate is false) or  (e.created_by = '${user_id}' and e.is_active is true and e.is_cancel is false) or (c.user_id = '${user_id}' and c.is_active is true) or (e."location" = (select "location" from users where id = '${user_id}') and e.is_public is true)) as el where ${_where}`;
                return this.adapter.db.query(_query) 
                .then(([res, metadata]) => res);
            }
        },

        customUpdate: {
            handler(ctx) {
                let { params, where } = ctx.params;
                return this.adapter.db.models.calender.update(params,{ where:where })
                .then(([res, metadata]) => res);
            }
        },

        bulkDeleteCalenderByFamily: {
           handler(ctx) {
                let { group_id } = ctx.params;
                let queryString = `DELETE FROM calenders WHERE event_id in (
                    select event_id from events_invitations where group_id = ${group_id}
                    union
                    select event_id from events_shares where group_id = ${group_id})`;
                return this.adapter.db.query(queryString)
                .then(([res, metadata]) => res);   
           } 
        }
    },
    events: {},
    methods: {},
    created() { },
    started() { },
    stopped() { }
};