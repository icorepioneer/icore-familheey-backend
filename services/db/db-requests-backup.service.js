/**
 * used to store information about each user request
 * for joining a family 
 */
"use_strict";
//packages
const DbService = require('moleculer-db');
const SqlAdapter = require('moleculer-db-adapter-sequelize');
const Sequelize = require('sequelize');


module.exports = {
    name: 'db.requests_backup',
    mixins: [DbService],
    settings: {},
    dependencies: [],
    cache: false,
    adapter: new SqlAdapter(process.env.SQL_DATABASE,null,null, {
        logger: false,
        pool: {
            max: 1,
            min: 0,
            acquire: 30000,
            idle: 10000,
            handleDisconnects: true
        },
        dialect: 'postgres',
        port: 5432,
        replication: {
            read: [
                { host: process.env.SQL_READ_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
            ],
            write: { host: process.env.SQL_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
        },
    }),
    model: {
        name: "requests_backup",
        define: {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            user_id:{
                field:"user_id",
                type:Sequelize.INTEGER,
                references:{
                    model:"users",
                    key:"id"
                },
            },
            group_id:{
                field:"group_id",
                type:Sequelize.INTEGER,
                references:{
                    model:"groups",
                    key:"id"
                },
                allowNull: false,
            },
            responded_by:{
                field:"responded_by",
                type:Sequelize.INTEGER,
                references:{
                    model:"users",
                    key:"id"
                }
            },
            email:{
                field:"email",
                type:Sequelize.STRING,
            },
            phone:{
                field:"phone",
                type:Sequelize.STRING,

            },
            from_id:{
                field:"from_id",
                type:Sequelize.INTEGER,
                references:{
                    model:"users",
                    key:"id"
                }
            },
            type:{
                field:"type",
                type:Sequelize.ENUM,
                values:["request","invite"]
            },
            status: {
                field: "status",
                type: Sequelize.ENUM,
                values: ["accepted", "rejected", "pending"],
                defaultValue:"pending"
            },
            location: {
                field: "location",
                type: Sequelize.STRING
            },
            lat: {
                field: "lat",
                type: Sequelize.DOUBLE
            },
            long: {
                field: "long",
                type: Sequelize.DOUBLE
            },
            createdAt: {
                field: "created_at",
                type: 'TIMESTAMP',
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            },
            updatedAt: {
                field: "updated_at",
                type: 'TIMESTAMP',
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            }
        }
    },
    actions:{
        bulkCreate:{
            handler(ctx){
                let { data } = ctx.params;
                return this.adapter.db.models.requests_backup.bulkCreate(data)
                .then(([res, metadata]) => res);
            }
        }
    }   
}