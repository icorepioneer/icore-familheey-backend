/* eslint-disable indent */
"use_strict";
//packages
const DbService = require('moleculer-db');
const SqlAdapter = require('moleculer-db-adapter-sequelize');
const Sequelize = require('sequelize');
const moment = require('moment');
//user table model
module.exports = {
    name: "db.post_request_items",
    mixins: [DbService],
    settings: {},
    dependencies: [],
    cache: false,
    adapter: new SqlAdapter(process.env.SQL_DATABASE,null,null, {
        logger: false,
        pool: {
            max: 1,
            min: 0,
            acquire: 30000,
            idle: 10000,
            handleDisconnects: true
        },
        dialect: 'postgres',
        port: 5432,
        replication: {
            read: [
                { host: process.env.SQL_READ_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
            ],
            write: { host: process.env.SQL_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
        },
    }),
    model: {
        name: "post_request_items",
        define: {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            user_id: {
                field: "user_id",
                type: Sequelize.INTEGER,
            },
            post_request_id: {
                field: "post_request_id",
                type: Sequelize.INTEGER,
            },
            request_item_title: {
                field: "request_item_title",
                type: Sequelize.STRING
            },
            request_item_description: {
                field: "request_item_description",
                type: Sequelize.TEXT
            },
            item_quantity: {
                field: "item_quantity",
                type: Sequelize.INTEGER
            },
            is_active: {
                field: "is_active",
                type: Sequelize.BOOLEAN,
                defaultValue: true
            },
            createdAt: {
                field: "created_at",
                type: "TIMESTAMP",
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            },
            updatedAt: {
                field: "updated_at",
                type: "TIMESTAMP",
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            }
        },
        options: {}
    },
    actions: {
        get_request_item_list: {
            handler(ctx) {
                let {request_id,limit,offset,user_id} = ctx.params;
                let offsetval = offset ? offset : 0;
                let limitval = limit ? limit : 25;
                let offsetQuery =  ctx.params && ctx.params.limit ? ` OFFSET ${offsetval} LIMIT ${limitval}` : ``;
                let _query = `select PRI.id as item_id,request_item_title,request_item_description,PRI."item_quantity",
                sum(PIC."contribute_item_quantity")::int as total_contribution,
                (select sum(contribute_item_quantity) from post_requests_item_contributions pric2 
                where PRI.id=pric2.post_request_item_id and pric2.contribute_user_id =${user_id} and pric2.is_active is true),
                (select sum(contribute_item_quantity) from post_requests_item_contributions 
                where post_request_item_id = PRI.id and is_active is true and is_acknowledge is false) as pledged_amount,
                (select sum(contribute_item_quantity) from post_requests_item_contributions 
                where post_request_item_id = PRI.id and is_active is true and is_acknowledge is true) as received_amount  
                from post_request_items PRI
                left join post_requests_item_contributions PIC on PIC.post_request_item_id = PRI.id and PIC.is_active is true
                where PRI.post_request_id = ${request_id} and PRI.is_active is true group by PRI.id 
                order by PRI.id desc ${offsetQuery}`;
                return this.adapter.db.query(_query)
                .then(([res, metadata]) => res)
                .catch(err => err);
            }
        },
        /**
         * list/count  the items for a request
         */
        list_items:{
            handler(ctx){
                let{ post_request_id, typeis } = ctx.params;
                let _params = '*';
                if(typeis && typeis=='count' ) _params = 'count(*)::int';
                let _query = `select ${_params} from post_request_items pri where post_request_id =${post_request_id} and is_active=true`;
                return this.adapter.db.query(_query)
                .then(([res, metadata]) => res)
                .catch(err => err);
            }
        },
        contribute_item_amount_split:{
            handler(ctx){
                let{ item_id } = ctx.params;
                let _query = `select (select sum(contribute_item_quantity) from post_requests_item_contributions 
                where post_request_item_id = ${item_id} and is_active is true and is_acknowledge is false) as pledged_amount,
                (select sum(contribute_item_quantity) from post_requests_item_contributions 
                where post_request_item_id = ${item_id} and is_active is true and is_acknowledge is true) as received_amount 
                from post_request_items where id = 23`;
                return this.adapter.db.query(_query)
                .then(([res, metadata]) => res)
                .catch(err => err);
            }    
        },
        isSearchUserAdmin:{
            handler(ctx){
                let{ post_request_item_id } = ctx.params;
                let _query = `select pr.user_id 
                from post_request_items pri
                left join post_requests pr
                on pr.id = pri.post_request_id 
                where pri.id = ${post_request_item_id}`;
                return this.adapter.db.query(_query)
                .then(([res, metadata]) => res)
                .catch(err => err);
            }
        },
    },
    events: {},
    methods: {},
    created() { },
    started() { },
    stopped() { },
};
