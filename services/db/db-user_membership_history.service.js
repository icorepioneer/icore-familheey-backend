"use_strict";
//packages
const DbService = require('moleculer-db');
const SqlAdapter = require('moleculer-db-adapter-sequelize');
const Sequelize = require('sequelize');
//folder table model
module.exports = {
    name: "db.user_membership_histories",
    mixins: [DbService],
    settings: {},
    dependencies: [],
    cache: false,
    adapter: new SqlAdapter(process.env.SQL_DATABASE,null,null, {
        logger: false,
        pool: {
            max: 1,
            min: 0,
            acquire: 30000,
            idle: 10000,
            handleDisconnects: true
        },
        dialect: 'postgres',
        port: 5432,
        replication: {
            read: [
                { host: process.env.SQL_READ_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
            ],
            write: { host: process.env.SQL_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
        },
    }),
    model: {
        name: "user_membership_histories",
        define: {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            group_id: {
                field: "group_id",
                type: Sequelize.INTEGER,
            },
            member_user_id: {
                field: "member_user_id",
                type: Sequelize.INTEGER,
            },
            edited_user_id: {
                field: "edited_user_id",
                type: Sequelize.INTEGER,
            },
            paid_amount: {
                field: "paid_amount",
                type: Sequelize.INTEGER,
            },
            payment_status:{
                field: "payment_status",
                type: Sequelize.STRING
            },
            membership_id:{
                field:"membership_id",
                type:Sequelize.INTEGER,
            },
            current_data: {
                field: "current_data",
                type: Sequelize.JSON
            },
            previous_data: {
                field: "previous_data",
                type: Sequelize.JSON
            },
            changed_data: {
                field: "changed_data",
                type: Sequelize.JSON
            },
            payment_object: {
                field: "payment_object",
                type: Sequelize.JSON
            },
            createdAt: {
                field: "created_at",
                type: 'TIMESTAMP',
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            },
            updatedAt: {
                field: "updated_at",
                type: 'TIMESTAMP',
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            },
        },
        options: {}
    },
    actions: {
        
    },
    events: {},
    methods: {},
    created() { },
    started() { },
    stopped() { },
}