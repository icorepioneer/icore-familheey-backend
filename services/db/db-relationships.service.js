"use_strict";
//packages
const DbService = require('moleculer-db');
const SqlAdapter = require('moleculer-db-adapter-sequelize');
const Sequelize = require('sequelize');
const Cuid = require('cuid');

//user table model
module.exports = {
    name: "db.relationships",
    // version:1,
    mixins: [DbService],
    settings: {},
    dependencies: [],
    cache: false,
    adapter: new SqlAdapter(process.env.SQL_DATABASE,null,null, {
        logger: false,
        pool: {
            max: 1,
            min: 0,
            acquire: 30000,
            idle: 10000,
            handleDisconnects: true
        },
        dialect: 'postgres',
        port: 5432,
        replication: {
            read: [
                { host: process.env.SQL_READ_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
            ],
            write: { host: process.env.SQL_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
        },
    }),
    model: {
        name: "relationships",
        define: {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true,
            },
            primary_user: {
                field: "primary_user",
                type: Sequelize.INTEGER,
                // references:{
                //     model:"users",
                //     key:"id"
                // }
            },
            relation_id: {
                field: "relation_id",
                type: Sequelize.INTEGER,
                // references:{
                //     model:"relationship_lookups",
                //     key:"id"
                // }
            },
            secondary_user: {
                field: "secondary_user",
                type: Sequelize.INTEGER,
                // references:{
                //     model:"users",
                //     key:"id"
                // }
            },
            others: {
                field: "others",
                type: Sequelize.STRING
            },
            createdAt: {
                field: "created_at",
                type: 'TIMESTAMP',
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            },
            updatedAt: {
                field: "updated_at",
                type: 'TIMESTAMP',
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            },
            is_active: {
                field: "is_active",
                type: Sequelize.BOOLEAN,
                defaultValue: true
            },
            group_id: {
                field: "group_id",
                type: Sequelize.INTEGER,
                // references:{
                //     model:"groups",
                //     key:"id"
                // }
            },
            type: {
                field: "type",
                type: Sequelize.STRING,
                defaultValue: 'role'
            }
        },
        options: {}
    },
    actions: {
        isRelationExist:{
            handler(ctx){
                let { group_id, primary_user,secondary_user } = ctx.params;
                let masterQuery = `select * 
                from relationships r
                where group_id= '${group_id}'
                and ( 
                    (primary_user='${primary_user}' and secondary_user='${secondary_user}') 
                    or 
                    (primary_user='${secondary_user}' and secondary_user='${primary_user}') 
                ) and is_active is true`;
                return this.adapter.db.query(masterQuery)
                .then(([res, metadata]) => res);
            }
        }
    },
    events: {},
    methods: {},
    created() { },
    started() { },
    stopped() { },
}