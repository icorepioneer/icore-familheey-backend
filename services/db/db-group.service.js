/* eslint-disable indent */
"use_strict";
//packages
const DbService = require('moleculer-db');
const SqlAdapter = require('moleculer-db-adapter-sequelize');
const Sequelize = require('sequelize');

//user table model
module.exports = {
    name: "db.groups",
    mixins: [DbService],
    settings: {},
    dependencies: [],
    cache: false,
    adapter: new SqlAdapter(process.env.SQL_DATABASE,null,null, {
        logger: false,
        pool: {
            max: 1,
            min: 0,
            acquire: 30000,
            idle: 10000,
            handleDisconnects: true
        },
        dialect: 'postgres',
        port: 5432,
        replication: {
            read: [
                { host: process.env.SQL_READ_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
            ],
            write: { host: process.env.SQL_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
        },
    }),
    model: {
        name: "groups",
        define: {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            group_name: {
                field: "group_name",
                type: Sequelize.STRING,
                allowNull: false
            },
            group_type: {
                field: "group_type",
                type: Sequelize.STRING,
                defaultValue: "public"
            },
            /**
             * ==group category==
             * regular
             * family
             * community
             * organisation
             */
            group_category: {
                field: "group_category",
                type: Sequelize.STRING,
                allowNull: false
            },
            logo: {
                field: "logo",
                type: Sequelize.STRING
            },
            cover_pic: {
                field: "cover_pic",
                type: Sequelize.STRING
            },
            group_original_image: {
                field: 'group_original_image',
                type: Sequelize.STRING
            },
            base_region: {
                field: "base_region",
                type: Sequelize.STRING
            },
            created_by: {
                field: "created_by",//admin
                type: Sequelize.INTEGER,
                // references: {
                //     model: "users",
                //     key: 'id'
                // }
            },
            group_level: {
                field: "group_level",
                type: Sequelize.STRING
            },
            intro: {
                field: "intro",
                type: Sequelize.TEXT
            },
            is_active: {
                field: "is_active",
                type: Sequelize.BOOLEAN,
                defaultValue: false
            },
            is_membership: {
                field: "is_membership",
                type: Sequelize.BOOLEAN,
                defaultValue: false
            },
            searchable: {
                field: "searchable",
                type: Sequelize.BOOLEAN,
                defaultValue: true
            },
            visibility: {
                field: "visibility",
                type: Sequelize.BOOLEAN
            },
            createdAt: {
                field: "created_at",
                type: 'TIMESTAMP',
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            },
            updatedAt: {
                field: "updated_at",
                type: 'TIMESTAMP',
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            },
            member_joining: {
                field: "member_joining",
                type: Sequelize.INTEGER,
                reference: {
                    model: "settings_lookup",
                    key: 'id'
                }
            },
            member_approval: {
                field: "member_approval",
                type: Sequelize.INTEGER,
                reference: {
                    model: "settings_lookup",
                    key: 'id'
                }
            },
            post_create: {
                field: "post_create",
                type: Sequelize.INTEGER,
                reference: {
                    model: "settings_lookup",
                    key: 'id'
                },
                defaultValue:6
            },
            post_approval: {
                field: "post_approval",
                type: Sequelize.INTEGER,
                reference: {
                    model: "settings_lookup",
                    key: 'id'
                }
            },
            post_visibilty: {
                field: "post_visibilty",
                type: Sequelize.INTEGER,
                reference: {
                    model: "settings_lookup",
                    key: 'id'
                }
            },
            link_family: {
                field: "link_family",
                type: Sequelize.INTEGER,
                reference: {
                    model: "settings_lookup",
                    key: 'id'
                }
            },
            link_approval: {
                field: "link_approval",
                type: Sequelize.INTEGER,
                reference: {
                    model: "settings_lookup",
                    key: 'id'
                }
            },
            is_linkable: {
                field: "is_linkable",
                type: Sequelize.BOOLEAN,
                defaultValue: true
            },
            event_approval: {
                field: "event_approval",
                type: Sequelize.STRING
            },
            history_text:{
                field: "history_text",
                type: Sequelize.TEXT
            },
            history_images:{
                field: "history_images",
                type: Sequelize.JSON,
                allowNull: true
            },
            announcement_create: {
                field: "announcement_create",
                type: Sequelize.INTEGER,
                reference: {
                    model: "settings_lookup",
                    key: 'id'
                },
                defaultValue:'18'
            },
            announcement_visibility: {
                field: "announcement_visibility",
                type: Sequelize.INTEGER,
                reference: {
                    model: "settings_lookup",
                    key: 'id'
                },
                defaultValue:'22'
            },
            announcement_approval: {
                field: "announcement_approval",
                type: Sequelize.INTEGER,
                reference: {
                    model: "settings_lookup",
                    key: 'id'
                },
                defaultValue:'24'
            },
            f_text:{
                field: "f_text",
                type: Sequelize.STRING
            },
            lat: {
                field: "lat",
                type: Sequelize.DOUBLE
            },
            long: {
                field: "long",
                type: Sequelize.DOUBLE
            },
            default_membership: {
                field: "default_membership",
                type: Sequelize.INTEGER
            },
            request_visibility: {
                field: "request_visibility",
                type: Sequelize.INTEGER,
                reference: {
                    model: "settings_lookup",
                    key: 'id'
                },
                defaultValue:27
            },
            firebase_link:{
                field: "firebase_link",  
                type: Sequelize.STRING
            },
            stripe_account_id: {
                field: "stripe_account_id",
                type: Sequelize.STRING
            },
            stripe_oauth_ref_key: {
                field: "stripe_oauth_ref_key",
                type: Sequelize.STRING
            },
            sticky_post:{
                field: "sticky_post",
                type: Sequelize.JSON,
                allowNull: true
            }
        },
        options: {}
    },
    actions: {
        getFamily: {
            handler(ctx) {
                let { userId,txt,type} = ctx.params;
                let offsetval = ctx.params.offset ? ctx.params.offset : 0;
                let event_id = ctx.params.event_id ? ctx.params.event_id : 0;
                // let limitval  = ctx.params.limit ? ctx.params.limit : 5;
                let offsetQuery =  ctx.params && ctx.params.limit ? ` OFFSET ${offsetval} LIMIT ${ctx.params.limit}` : ``;
                
                let _filter = [`GM.user_id = ${userId}`,`G.is_active = true`,`GM.is_blocked = false`,`GM.is_removed = false`];
                if(type == 'announcement') {
                    _filter.push(`(G.announcement_create = 18 or G.announcement_create = 19 or (G.announcement_create = 17 and GM."type" = 'admin'))`);
                } else if(type == 'post'){
                    _filter.push(`(G.post_create = 6 or G.post_create = 20 or (G.post_create = 7 and GM."type" = 'admin'))`);
                }
                if(txt){
                    _filter.push(`(G.base_region ilike '%${txt}%' or G.group_name ilike '%${txt}%')`);
                }
                let _where = _filter.join(" and ");     
 
                let queryString = `select distinct(G.id) id,G.group_name,G.group_type,G.group_category,G.logo,G.base_region,G.member_joining,
                (SELECT status as request_status from requests r where r.user_id=${userId} and r.group_id=G.id limit 1),
                (select full_name from users where id = G.created_by ) as created_by,G.created_at,
                (select count(*) from (select distinct(user_id) from group_maps as gm,users as u 
                    where gm.group_id = G.id and u.id = gm.user_id and gm.is_blocked = false and gm.is_removed = false) as member_count) 
                as member_count,
                (select type from group_maps _gm where group_id = G.id and is_blocked = false and is_removed = false and _gm.user_id=${userId} limit 1) as logined_user,
                (SELECT type as request_status from requests r where r.user_id=${userId} and r.group_id=G.id limit 1) as req_type,
                (select count(*) from ((select distinct(user_id) from group_maps where group_id in
                                (select group_id from group_maps GM1, users U1 where user_id = ${userId} and is_blocked is false and is_removed is false and U1.id = GM1.user_id)
                                 order by user_id asc)
                                    intersect
                                (select user_id from group_maps GM2, users U2 where group_id = G.id and is_removed is false and is_blocked is false and U2.id = GM2.user_id))as known) as knownCount,
                (case when (select count(*) from events_invitations where group_id = G.id and event_id = ${event_id}) > 0
                then true else false end) as invitation_status,
                (select count(*) from  events where id 
                  in 
                  (select 
                  event_id 
                  from 
                  events_invitations 
                  where 
                  group_id = G.id
                  union
                  select 
                  event_id
                  from 
                  events_shares 
                  where group_id = G.id) 
                  and
                  id not in 
                  (
                  select
                  viewed_id 
                  from
                  vieweds
                  where 
                  viewed_id 
                  in 
                  (select viewed_id from vieweds where user_id = ${userId} and viewed_type = 'event') group by viewed_id)
                  and is_active = true) as new_events_count,
                  (select count(*) from posts p where p.to_group_id = G.id and p.type='post' and p.is_active is true) as post_count,
                  (case  
                    when
                        (select "createdAt" from post_comments where is_active is true and 
                        post_id in (select id from posts where to_group_id = G.id and is_active is true)
                        order by "createdAt" desc limit 1) > (select "createdAt" from posts p2 where p2.to_group_id = G.id and p2.type='post' 
                        and p2.is_active is true order by "createdAt" desc limit 1)  
                    then
                        (select "createdAt" from post_comments where is_active is true and 
                        post_id in (select id from posts where to_group_id = G.id and is_active is true)
                        order by "createdAt" desc limit 1) 
                    when 
                        (select "createdAt" from posts p2 where p2.to_group_id = G.id and p2.type='post' 
                        and p2.is_active is true order by "createdAt" desc limit 1) > (select "createdAt" from post_comments where is_active is true and 
                        post_id in (select id from posts where to_group_id = G.id and is_active is true)
                        order by "createdAt" desc limit 1) 
                    then 
                        (select "createdAt" from posts p2 where p2.to_group_id = G.id and p2.type='post' 
                        and p2.is_active is true order by "createdAt" desc limit 1) 
                    else 
                        G.created_at 
                 end) as sortkey
                from groups as G join group_maps as GM
                on GM.group_id = G.id
                where ${_where}
                order by sortkey desc ${offsetQuery}`;
                return this.adapter.db.query(queryString)
                    .then(([res, metadata]) => res);
            }
        },
        getMembers: {
            handler(ctx) { 
                let { groupid, current_id, txt,membership_id } = ctx.params;
                let filterQuery = '';
                let typeFilter = '';
                if(ctx.params.filter == "current") {
                    filterQuery = `and gm.membership_to >= date_part('epoch', now()) `;   
                } else if(ctx.params.filter == "overdue") {
                    filterQuery = `and gm.membership_to < date_part('epoch', now()) `; 
                }
                if(ctx.params.membership_id && ctx.params.membership_id != '') {
                    typeFilter = ` and gm.membership_id = ${membership_id}`;    
                }
                let searchQuery = (txt && txt != '') ? `and ((u.full_name ilike '%${txt}%') or 
                ((select RL.relationship from relationships R 
	            join relationship_lookups RL on RL.id = R.relation_id
                where (primary_user = gm.user_id and R."type" = 'role' and R.group_id = ${groupid})
                or (primary_user = ${current_id} and R.secondary_user = gm.user_id and R."type" = 'relation') limit 1
                ) ilike '%${txt}%'))
                ` : '';

                let queryString = `select DISTINCT ON (u.id) gm.id,gm.is_blocked,gm.is_removed,gm.group_id,user_id,
                gm.type as user_type,gm.updated_at as member_since,u.full_name,u.email,u.gender,u.propic,gm.membership_id,gm.membership_payment_status,
                gm.membership_from,gm.membership_to,gm.membership_fees,gm.membership_paid_on,gm.membership_duration,gm.membership_payment_notes,gm.membership_period_type,
                gm.membership_total_payed_amount,gm.membership_customer_notes,
                (select membership_name from membership_lookups where id = gm.membership_id) as membership_type,
                (select membership_period_type_id from membership_lookups where id = gm.membership_id) as membership_period_type_id,
                (case when gm.membership_to < date_part('epoch', now()) then true else false end) as is_expaired,
                age(to_timestamp(membership_from)) membership_since,
                (select type from group_maps where user_id = ${current_id} and group_id = ${groupid} and is_blocked is false and is_removed is false limit 1) as is_primar_admin
                from group_maps as gm,users as u 
                where group_id=${groupid} ${filterQuery}${typeFilter} and u.id = gm.user_id and gm.is_removed is false and gm.is_blocked is false ${searchQuery} order by u.id,u.full_name`;
                return this.adapter.db.query(queryString)
                    .then(([res, metadata]) => res);
            }
        },
        getMemberDetails: {
            handler(ctx) { 
                let { user_id, crnt_user_id, gender,group_category,group_id } = ctx.params;
                let p_user = crnt_user_id;
                let s_user = user_id;
                let role_group = '';
                let type_group = '';
                let qGender = ''; let _typeis = 'relation'; 
                if (gender == "male" || gender == "female") {
                    qGender = `when primary_user=${s_user} and secondary_user=${p_user} then rl.opposite_relation_${gender} `;
                }
                let conArr = [`rl.id = r.relation_id`];
                if(group_category.toLowerCase() != 'regular'){
                    role_group = group_id ? ` and r.group_id=${group_id}` : '';
                    type_group = ` and r.type='role'`;
                    _typeis = 'role';

                } else {
                    type_group = ` and r.type='relation'`;
                }

                let _thirdstmnt = '';
                let _thirdstmntwhere = '';
                if(_typeis == 'role' && group_id){
                    _thirdstmnt = `when group_id=${group_id} and secondary_user=${user_id} then rl.relationship `;
                    _thirdstmntwhere = `or (group_id=${group_id} and secondary_user=${user_id})`;
                }

                let _condition1 = conArr.join(" and ");
                _condition1 = false;//dont know y this "_condition1" used , so for purpose making this as false
                let queryString = `select r.id,(select case when (${_condition1}) then rl.relationship when primary_user=${p_user} and secondary_user=${s_user} then rl.relationship ${qGender} ${_thirdstmnt} end)
                from relationships as r ,relationship_lookups as rl where rl.id=r.relation_id and ((primary_user = ${p_user} and secondary_user = ${s_user}) or (primary_user = ${s_user} and secondary_user =${p_user}) ${_thirdstmntwhere}) ${role_group} ${type_group}`;
                return this.adapter.db.query(queryString)
                .then(([res, metadata]) => res);
            }
        },
        listinvitation: {
            handler(ctx) {
                let { user_id } = ctx.params;
                let queryString = `select r.id as req_id,r.from_id,g.id as group_id,g.base_region,r.created_at,g.group_name,g.logo,u.full_name,u.propic from requests r
                join groups g on g.id = r.group_id join users u on u.id= r.from_id where r.user_id=${user_id} and r.status = 'pending'`;
                return this.adapter.db.query(queryString)
                .then(([res, metadata]) => res);
            }
        },
        getfamilyByid: {
            handler(ctx) {
                let user_ID = ctx.params.user_id;
                let group_ID = ctx.params.group_id;
                let queryString = `select g.*,gm.type,gm.following from groups g join group_maps gm on gm.group_id=g.id or gm.user_id=${user_ID} where g.id=${group_ID}`
                return this.adapter.db.query(queryString)
                    .then(([res, metadata]) => res);
            }
        },
        fetchGroup: {
            handler(ctx) {
                let { group_name,user_id } = ctx.params;
                group_name = group_name.replace("'", "''");

                let queryString = `select u.full_name as created_by_,(select count(*) from group_maps where group_id = g.id ) as member_count,
                (select count(*) from ((select distinct(user_id) from group_maps where group_id in
                                                (select group_id from group_maps where user_id = ${user_id} and is_blocked is false and is_removed is false)
                                                 order by user_id asc)
                                                    intersect
                                                (select user_id from group_maps where group_id = g.id and is_removed is false and is_blocked is false))as known) as known_count,g.*
                from groups g join group_maps gm 
                        on
                        g.id = gm.group_id
                        join 
                        users u on u.id = g.created_by
                                where g.is_active = true and
                                group_name ilike '%${group_name}%'  group by g.id,u.full_name`;
                return this.adapter.db.query(queryString)
                    .then(([res, metadata]) => res);
            }
        },
        searchFamily: {
            handler(ctx) {
                let searchtxt = ctx.params.txt;
                let searchuser = ctx.params.userid;
                let offsetval = ctx.params.offset;
                let limitval = ctx.params.limit ? ctx.params.limit : 20;
                let multiGroup = '';
                let defaultQuery = '';
                if(ctx.params.multiGroup && ctx.params.multiGroup.length>0) {
                   multiGroup = `and G.id = any('{${ctx.params.multiGroup}}')`; 
                } 
                if(!searchtxt) {
                    defaultQuery = `and G.id not in(select distinct(group_id) from group_maps where user_id = ${searchuser} and is_blocked is false and is_removed is false)`;
                }
                let _Query = `select distinct(G.id) as group_id,MJ.is_joined,MJ.is_removed,MJ.is_blocked,G.group_name,G.logo,
                G.base_region,G.group_type,G.group_category,G.visibility,G.member_approval,G.created_by as created_by_id,
                U.full_name as created_by_name,U.propic,G.member_joining,G.group_category,G.is_linkable,G.base_region,G.intro,
                R.type,R.status,R.id as req_id,R.from_id,GM.membercount,
                (select count(*) as post_count from posts P where P.to_group_id = G.id and P.is_active is true)
                from groups as G
                LEFT JOIN requests R on G.id = R.group_id and user_id = ${searchuser}
                LEFT join
                	(
                	select count(*) as membercount,group_id from group_maps as gm1,users as u where gm1.is_removed is false and gm1.is_blocked is false and u.id = gm1.user_id
                	group by group_id
                	) as GM on G.id = GM.group_id
                LEFT join
                	(
                	select count(*) as is_joined,is_blocked,group_id,is_removed from group_maps as mj1 where mj1.user_id = ${searchuser}
                	group by group_id,is_removed,is_blocked
                	) as MJ on MJ.group_id = G.id
                join users U on U.id = G.created_by
                where G.searchable=true and G.is_active=true ${multiGroup} ${defaultQuery} 
                and  (G.group_name ilike '%${searchtxt}%' or G.base_region ilike '%${searchtxt}%') order by GM.membercount desc OFFSET ${offsetval} LIMIT ${limitval}`;

                return this.adapter.db.query(_Query)
                    .then(([res, metadata]) => res);
            }
        },
        getGroupSettings: {
            handler(ctx) {
                let { group_id } = ctx.params;
                let queryString = `select s.id,s."key",s.value from "groups" g join settings_lookups s on g.post_approval = s.id 
                or g.post_create = s.id or g.post_visibilty = s.id or g.member_approval = s.id or g.member_joining = s.id
                or g.link_approval = s.id or g.link_family = s.id or g.announcement_create = s.id where g.id = ${group_id} group by s.id,s."key",s.value`;
                return this.adapter.db.query(queryString).then(([res, metadata]) => res);
            }
        },
        groupDetails: {
            handler(ctx) { 
                let { group_id, user_id } = ctx.params;
                let queryString = `select u.full_name as created_by_name, u.propic, g.*,gm.membership_from,gm.membership_to,
                gm.membership_fees,gm.membership_paid_on,gm.membership_duration,gm.membership_payment_status,gm.membership_period_type,
                gm.id as group_map_id,gm.membership_payment_notes,gm.membership_customer_notes,gm.membership_total_payed_amount,
                (select membership_name from membership_lookups where id = gm.membership_id) as membership_type,
                (select membership_period_type_id from membership_lookups where id = gm.membership_id) as membership_period_type_id,
                (select (case when (select id from requests where status = 'pending' 
                and group_id = ${group_id} and user_id = ${user_id} limit 1) > 0 then true else false end) as invitation_status)
                from "groups" as g
                left join group_maps gm on gm.group_id = g.id and user_id = ${user_id}
                join users as u
                on g.created_by = u.id
                where g.id=${group_id} and g.is_active is true`;
                return this.adapter.db.query(queryString).then(([res, metadata]) => res);
            }
        },
        /**
         * based on group , a users mutual connections
         */
        getFamilyMutualList: {
            params: {
                group_id: "string",
                user_id: "string",
            },
            handler(ctx) {
                let { group_id, user_id } = ctx.params;
                let queryString = `select u1.id, u1.full_name, u1.propic,u1.location, u1.gender, u1.origin
                from (
	            (select user_id from group_maps GM2, users U2 where group_id in
		            (select group_id from group_maps gmp join "groups" grp on grp.id = gmp.group_id where user_id = ${user_id} 
                    and is_blocked is false 
		            and is_removed is false and grp.is_active is true)
 		            order by user_id asc
 	            )
                intersect
	            (select user_id from group_maps GM1, users U1 where group_id=${group_id} and user_id != ${user_id} 
                and is_blocked is false and is_removed is false  
	            and U1.id = GM1.user_id and U1.is_active is true)
                )as known
                join users u1 
                on known.user_id = u1.id`;
                return this.adapter.db.query(queryString)
                .then(([res, metadata]) => res)
                .catch(err => err);
            }
        },
        userPhoneMulticheck: {
            params: {

            },
            handler(ctx) {
                let queryString = ctx.params.multiPhone;
                let queryStringMain = `select id from users where phone ilike any('{${queryString}}')`;
                return this.adapter.db.query(queryStringMain)
                .then(([res, metadata]) => res);
            }
        },
        getGroupFromPhone: {
            params: {

            },
            handler(ctx) {
                let grpqueryString = ctx.params.multiUser;
                let masterQuery = `select distinct(group_id) from group_maps where user_id = any('{${grpqueryString}}')`
                return this.adapter.db.query(masterQuery)
                .then(([res, metadata]) => res);
            }
        },
        /** fetch groups of a user based on userid 
         * who is not blocked and  removed
         */
        userGroups:{
            handler(ctx) { 
                let {  user_id } = ctx.params;
                let _Query = `SELECT distinct (GM.group_id)
                FROM group_maps GM 
                join groups G on G.id = GM.group_id
                where GM.user_id = ${user_id} and GM.is_removed is false 
                and GM.is_blocked is false and G.is_active is true
                order by GM.group_id asc`;
                return this.adapter.db.query(_Query)
                .then(([res, metadata]) => res);
            }
        },
        getSuggestedGroupNewUser:{
            handler(ctx) { 
                let {  user_id } = ctx.params;
                let _Query = `WITH radious_match AS (
                    select G.id,G.group_name,G.group_type,G.group_category,G.logo,G.base_region,G.is_active,
                    U.full_name as created_by,U.propic,R.status,MJ.is_joined,
                    (select count(*) from group_maps GM 
                        left join users U on U.id = GM.user_id
                        where GM.group_id = G.id and GM.is_blocked is false and GM.is_removed is false and U.is_active is true
                    ) as membercount
                    from "groups" G
                    LEFT JOIN (
                        select r1.status,r1.group_id from requests as r1 where user_id = ${user_id}
                    ) as R on G.id = R.group_id
                    LEFT JOIN (
                        select count(*) as is_joined,group_id from group_maps as mj1 where mj1.user_id = ${user_id} and mj1.is_blocked is false and mj1.is_removed is false
                		group by group_id
                    ) as MJ on MJ.group_id = G.id
                    join users U on U.id = G.created_by 
                    join users U2 on U2.id = ${user_id}
                    WHERE 
	                (
			            (
			                (
			                    acos(
			                        sin(( U2.lat * pi() / 180))
			                        *
			                        sin(( G.lat * pi() / 180)) + cos(( U2.lat * pi() /180 ))
			                        *
			                        cos(( G.lat * pi() / 180)) * cos((( U2.long - G.long) * pi()/180)))
			                ) * 180/pi()
			            ) * 60 * 1.1515 * 1.609344
			        ) <= 500 
			        and G.is_active is true and G.group_type = 'public' and U.is_active is true 
	                order by (
			            (
			                (
			                    acos(
			                        sin(( U2.lat * pi() / 180))
			                        *
			                        sin(( G.lat * pi() / 180)) + cos(( U2.lat * pi() /180 ))
			                        *
			                        cos(( G.lat * pi() / 180)) * cos((( U2.long - G.long) * pi()/180)))
			                ) * 180/pi()
			            ) * 60 * 1.1515 * 1.609344
			        ) asc limit 3
                )
                SELECT * FROM radious_match WHERE (SELECT COUNT(*) FROM radious_match) > 0
                	UNION
                select G.id,G.group_name,G.group_type,G.group_category,G.logo,G.base_region,G.is_active,
                U.full_name as created_by,U.propic,R.status,MJ.is_joined,
                (select count(*) from group_maps GM 
                    left join users U on U.id = GM.user_id
                    where GM.group_id = G.id and GM.is_blocked is false and GM.is_removed is false and U.is_active is true
                ) as membercount
                from "groups" G
                LEFT JOIN (
                    select r1.status,r1.group_id from requests as r1 where user_id = ${user_id}
                ) as R on G.id = R.group_id
                LEFT JOIN (
                    select count(*) as is_joined,group_id from group_maps as mj1 where mj1.user_id = ${user_id} and mj1.is_blocked is false and mj1.is_removed is false
                	group by group_id
                ) as MJ on MJ.group_id = G.id
                join users U on U.id = G.created_by 
                WHERE (SELECT COUNT(*) FROM radious_match) <= 0 AND G.is_active is true AND G.group_type = 'public' 
                AND U.is_active is true order by membercount desc limit 3`;
                return this.adapter.db.query(_Query)
                .then(([res, metadata]) => res);
            }   
        },
        getGroupSetting:{
            handler(ctx) { 
                let {group_id,user_id} = ctx.params;
                let masterQuery = `select distinct(G.id),G.post_create,GM.type as user_type 
                from "groups" G
                left join group_maps GM on GM.group_id = G.id and GM.user_id = ${user_id}
                where G.id = ${group_id}`;
                return this.adapter.db.query(masterQuery)
                .then(([res, metadata]) => res);
            }
        },
        getUserAdminGroups:{
            handler(ctx) { 
                let {user_id} = ctx.params;
                let searchtxt = ctx.params.query;
                let offsetval = ctx.params.offset ? ctx.params.offset : 0;
                let limitval = ctx.params.limit ? ctx.params.limit : 100;
                let searchQuery = '';
                if(searchtxt && searchtxt != '') {
                    searchQuery = `and G.group_name ilike '%${searchtxt}%'`;
                }
                let _Query = `select distinct(G.id),G.group_name,G.base_region,G.logo,G.base_region,G.stripe_account_id,U.full_name,U.propic
                from group_maps GM
                join "groups" G on G.id = GM.group_id
                left join users U on U.id = G.created_by
                where GM.user_id = ${user_id} and GM."type" = 'admin' and G.is_active is true and GM.is_blocked is false 
                and GM.is_removed is false ${searchQuery}
                order by G.group_name asc OFFSET ${offsetval} LIMIT ${limitval}`;
                return this.adapter.db.query(_Query)
                .then(([res, metadata]) => res);
            }   
        },
        get_other_invited_users_by_group:{
            handler(ctx) { 
                let {group_id} = ctx.params;
                let masterQuery = `select distinct(OU.phone),OU.full_name,OU.email,OU."type",OU.type_value 
                from other_users OU
                where OU.group_id = ${group_id} and is_active is true`
                return this.adapter.db.query(masterQuery)
                .then(([res, metadata]) => res);
            }
        },
        get_notification_group_data:{
            handler(ctx) { 
                let {group_id} = ctx.params;
                let masterQuery = `select U.full_name as created_by_user,U.propic as created_by_propic,G.logo as cover_image,G.base_region as location,
    (select count(*) from group_maps GM 
	    left join users U on U.id = GM.user_id
        where GM.group_id = G.id and GM.is_blocked is false and GM.is_removed is false and U.is_active is true
    ) as membercount
    from "groups" G
    left join users U on U.id = G.created_by
    where G.id = ${group_id}`;
                return this.adapter.db.query(masterQuery)
                .then(([res, metadata]) => res);
            }   
        },

        notification_settings_familyCount:{
            handler(ctx) { 
                let { user_id } = ctx.params;
                let masterQuery = `select count(distinct(G.id))::int
                from groups as G join group_maps as GM
                on GM.group_id = G.id
                where GM.user_id = ${user_id} and G.is_active = true and GM.is_blocked = false and GM.is_removed = false`;
                return this.adapter.db.query(masterQuery)
                .then(([res, metadata]) => res);
            }   
        }
    },
    events: {},
    methods: {},
    created() { },
    started() { },
    stopped() { },
};