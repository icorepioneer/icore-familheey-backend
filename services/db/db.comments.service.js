/* eslint-disable indent */
"use_strict";
//packages
const DbService = require('moleculer-db');
const SqlAdapter = require('moleculer-db-adapter-sequelize');
const Sequelize = require('sequelize');

//user table model
module.exports = {
    name: "db.post_comments",
    mixins: [DbService],
    settings: {},
    dependencies: [],
    cache: false,
    adapter: new SqlAdapter(process.env.SQL_DATABASE,null,null, {
        logger: false,
        pool: {
            max: 1,
            min: 0,
            acquire: 30000,
            idle: 10000,
            handleDisconnects: true
        },
        dialect: 'postgres',
        port: 5432,
        replication: {
            read: [
                { host: process.env.SQL_READ_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
            ],
            write: { host: process.env.SQL_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
        },
    }),
    model: {
        name: "post_comments",
        define: {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            post_id: {
                field: "post_id",
                type: Sequelize.INTEGER
            },
            topic_id: {
                field: "topic_id",
                type: Sequelize.INTEGER
            },
            comment: {
                field: "comment",
                type: Sequelize.TEXT,
                allowNull: false
            },
            commented_by: {
                field: "commented_by",
                type: Sequelize.INTEGER,
                allowNull: false
            },
            quoted_item: {
                field: "quoted_item",
                type: Sequelize.TEXT,
                allowNull: true
            },
            quoted_id: {
                field: "quoted_id",
                type: Sequelize.INTEGER,
                allowNull: true
            },
            quoted_date: {
                field: "quoted_date",
                type: 'TIMESTAMP',
                allowNull: true
            },
            quoted_user: {
                field: "quoted_user",
                type: Sequelize.STRING
            },
            attachment: {
                field: "attachment",
                type: Sequelize.JSON,
                allowNull: true
            },
            file_name: {
                field: "file_name",
                type: Sequelize.STRING,
                allowNull: true
            },
            type: {
                field: "type",
                type: Sequelize.STRING,
                allowNull: true
            },
            file_type: {
                field: "file_type",
                type: Sequelize.STRING,
                allowNull: true
            },
            is_active: {
                field: "is_active",
                type: Sequelize.BOOLEAN,
                defaultValue: true
            }
        },
        options: {}
    },
    actions: {
        getCommentsByPost: {
            handler(ctx) {
                let { post_id, user_id, topic_id } = ctx.params;
                let topicQuery1 = '';
                let topicQuery2 = '';
                if(topic_id && topic_id != null) {
                    topicQuery1 = `topic_id = ${topic_id}`;
                    topicQuery2 = `p.topic_id = ${topic_id}`;
                } else {
                    topicQuery1 = `post_id = ${post_id}`;
                    topicQuery2 = `p.post_id = ${post_id}`;
                }
                let query = `select u.full_name,u.phone,u.id as user_id ,p.id as comment_id ,u.propic,
                (select last_read_message_id from post_comment_reads where ${topicQuery1} 
                and user_id  = ${user_id} limit 1)< p.id as unread,p."createdAt", p.comment, p.commented_by, p.attachment,
                p.quoted_item,p.quoted_id,p.quoted_date,p.quoted_user
                from post_comments p join users u on u.id = p.commented_by where ${topicQuery2} order by comment_id`;
                return this.adapter.db.query(query)
                    .then(([res, metadata]) => res);
            }
        },
        /**
         * get commented user of a post
         */
        getCommentedUser:{
            handler(ctx) {
                let { post_id,topic_id } = ctx.params;
                let _whereQuery = '';
                if(topic_id) {
                    _whereQuery = `topic_id = ${topic_id}`;
                } else {
                    _whereQuery = `post_id = ${post_id}`;
                }
                let query = `SELECT DISTINCT(PC.commented_by) FROM post_comments PC where ${_whereQuery}`;
                return this.adapter.db.query(query)
                .then(([res, metadata]) => res);
            }
        },
        bulkCommentDelete:{
            handler(ctx){
                let { comment_id } = ctx.params;
                return this.adapter.db.models.post_comments.destroy({
                    where:{
                        id:comment_id
                    }
                }).then(res=>{
                    return res;
                })
            }
        }
    },
    events: {},
    methods: {

    },
    created() { },
    started() { },
    stopped() { },
};