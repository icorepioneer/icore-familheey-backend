/* eslint-disable indent */
"use_strict";
//packages
const DbService = require('moleculer-db');
const SqlAdapter = require('moleculer-db-adapter-sequelize');
const Sequelize = require('sequelize');
//user table model
module.exports = {
    name: "db.post",
    mixins: [DbService],
    settings: {},
    dependencies: [],
    cache: false,
    adapter: new SqlAdapter(process.env.SQL_DATABASE,null,null, {
        logger: false,
        pool: {
            max: 1,
            min: 0,
            acquire: 30000,
            idle: 10000,
            handleDisconnects: true
        },
        dialect: 'postgres',
        port: 5432,
        replication: {
            read: [
                { host: process.env.SQL_READ_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
            ],
            write: { host: process.env.SQL_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
        },
    }),
    model: {
        name: "post",
        define: {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            category_id: {
                field: "category_id",
                type: Sequelize.INTEGER,
                allowNull: false
            },
            orgin_id: {
                field: "orgin_id",
                type: Sequelize.INTEGER,
                allowNull: true
            },
            from_id: {
                field: "from_id",
                type: Sequelize.INTEGER,
                allowNull: true
            },
            post_ref_id: {
                field: "post_ref_id",
                type: Sequelize.STRING,
                allowNull: true
            },
            to_group_id: {
                field: "to_group_id",
                type: Sequelize.INTEGER,
                allowNull: true
            },
            to_user_id: {
                field: "to_user_id",
                type: Sequelize.INTEGER,
                allowNull: true
            },
            shared_user_id: {
                field: "shared_user_id",
                type: Sequelize.INTEGER,
                allowNull: true
            },
            created_by: {
                field: "created_by",
                type: Sequelize.INTEGER,
                allowNull: false
            },
            title: {
                field: "title",
                type: Sequelize.STRING,
                allowNull: true
            },
            snap_description: {
                field: "snap_description",
                type: Sequelize.TEXT,
                allowNull: false
            },
            post_attachment: {
                field: "post_attachment",
                type: Sequelize.JSON,
                allowNull: true
            },
            post_info: {
                field: "post_info",
                type: Sequelize.JSON,
                allowNull: false
            },
            file_type: {
                field: "file_type",
                type: Sequelize.STRING
            },
            is_shareable: {
                field: "is_shareable",
                type: Sequelize.BOOLEAN,
                defaultValue: true
            },
            conversation_enabled: {
                field: "conversation_enabled",
                type: Sequelize.BOOLEAN,
                defaultValue: true
            },
            privacy_type: {
                field: "privacy_type",
                type: Sequelize.STRING,
                defaultValue: true
            },
            type: {
                field: "type",
                type: Sequelize.STRING,
            },
            is_active: {
                field: "is_active",
                type: Sequelize.BOOLEAN,
                defaultValue: true
            },
            is_approved: {
                field: "is_approved",
                type: Sequelize.INTEGER,
                defaultValue: 0
            },
            post_type: {
                field: "post_type",
                type: Sequelize.STRING,
            },
            valid_urls: {
                field: "valid_urls",
                type: Sequelize.ARRAY(Sequelize.TEXT)   
            },
            url_metadata: {
                field: "url_metadata",
                type: Sequelize.JSON    
            },
            publish_type: {
                field: "publish_type",
                type: Sequelize.STRING,
            },
            publish_id: {
                field: "publish_id",
                type: Sequelize.INTEGER
            },
            publish_mention_users: {
                field: "publish_mention_users",
                type: Sequelize.JSON,
                allowNull: true
            },
            publish_mention_items: {
                field: "publish_mention_items",
                type: Sequelize.JSON,
                allowNull: true
            },
            sort_date: {
                field: "sort_date",
                type: 'TIMESTAMP',
                defaultValue:Sequelize.literal('CURRENT_TIMESTAMP')
            },
            firebase_link:{
                field: "firebase_link",  
                type: Sequelize.STRING
            },
            elasticsearch_id:{
                field: "elasticsearch_id",  
                type: Sequelize.STRING
            },
        },
        options: {}
    },
    actions: {
        getByID: {
            handler(ctx) {
                let { id } = ctx.params;
                let inactive_active_array = `(select array_agg(to_group_id) from posts where post_ref_id=p.post_ref_id and created_by=p.created_by) as inactive_active_array`;
                let selected_family_count = `(select count(*) from posts where post_ref_id = p.post_ref_id and created_by = p.created_by and to_group_id  notnull ) as selected_family_count`;
                let selected_user_count = `(select count(*) from posts where post_ref_id = p.post_ref_id and created_by = p.created_by and to_user_id  notnull ) as selected_user_count`;
                let query = `select p.*,p.id as post_id,${selected_family_count},${selected_user_count},${inactive_active_array},u.full_name, u.propic from  posts p join post_category_look_ups pc on p.category_id = pc.id  join users u on u.id = p.created_by where p.id=${id}`;
                return this.adapter.db.query(query)
                    .then(([res, metadata]) => res);
            }
        },
        post_by_user: {
            handler(ctx) {
                let { type, txt } = ctx.params;
                let offsetval = ctx.params.offset ? ctx.params.offset : 0;
                let post_id = ctx.params.post_id ? 'and p.id = ' + ctx.params.post_id : '';
                let limitval = ctx.params.limit ? ctx.params.limit : 20;
                let to_group_id_search = ctx.params.to_group_id ? 'and to_group_id = ' + ctx.params.to_group_id : '';
                let user_id_query = (!ctx.params.post_id && ctx.params.user_id) ? `and (p.to_user_id = ${ctx.params.user_id} or p.created_by = ${ctx.params.user_id})` : ``;
                if(ctx.params.post_ref_id && ctx.params.post_ref_id != '') {
                    user_id_query = `and p.post_ref_id = '${ctx.params.post_ref_id}' and p.created_by = ${ctx.params.user_id}`;   
                }
                let muted_query = ctx.params.user_id ? `(select is_active from post_notification_settings where post_id = p.id and user_id = ${ctx.params.user_id}) as muted,` : ``;
                // let to_user_id_search = ctx.params.to_user_id ? 'and to_user_id = ' + ctx.params.to_group_id : '';
                let searchTxt = txt ? `and (p.snap_description ilike '%${txt}%'
                or g.group_name ilike '%${txt}%'
                or u.full_name ilike '%${txt}%')` : ''
                // let query = `select p.id as post_id ,g.group_name,(select count(distinct(user_id)) from post_views where post_id = p.id) as views_count , (select count(*) from post_comments where post_id = p.id) as conversation_count , * from  
                // posts p join post_category_look_ups pc on p.category_id = pc.id 
                // join "groups" g  on g.id = p.to_group_id 
                // join users u on u.id = p.created_by where p.is_active= true and p.to_group_id in (select distinct(group_id) from group_maps where user_id =${ctx.params.user_id} and is_blocked is false and is_removed is false
                // order  by group_id asc);`
                let query = `select p.id as post_id ,g.id, g.group_name,g.logo as family_logo,u_s1.full_name as to_user_name,u.full_name as created_user_name ,u.propic ,
                (select u2.full_name from posts pu join users u2 on pu.created_by = u2.id where pu.id = p.orgin_id) as parent_post_created_user_name,
                (select u2.propic from posts pu join users u2 on pu.created_by = u2.id where pu.id = p.orgin_id) as parent_post_created_propic,
                (select gm2.group_name from posts pu1 join "groups" gm2 on pu1.to_group_id = gm2.id where pu1.id = p.orgin_id) as parent_post_grp_name, 
                u_s.full_name as shared_user_name,(select count(distinct(user_id)) from post_views where post_id = p.id and user_id != ${ctx.params.user_id}) as views_count , 
                (select count(*) from post_comments where post_id = p.id) as conversation_count ,(select count(*) as conversation_count_new  from (select * , (select last_read_message_id from post_comment_reads where post_id = p.id and user_id = ${ctx.params.user_id} limit 1) as last_read_message_id
                from post_comments where post_id = p.id) as results where  id > (case when last_read_message_id is null then 0 else last_read_message_id end)), 
                p.created_by,p.publish_type,p.publish_id,p.publish_mention_users,p.publish_mention_items,
                (select (count(*)) as shared_user_count from posts where from_id = p.id and is_active = true),(select (count(*)) as origin_shared_user_count from posts where orgin_id = p.id and is_active = true), 
                (select count(*) from posts where post_ref_id = p.post_ref_id and created_by = p.created_by and to_group_id  notnull ) as selected_family_count,
                (select count(*) from posts where post_ref_id = p.post_ref_id and created_by = p.created_by and to_user_id  notnull ) as selected_user_count,
                (select u1.full_name as shared_user_names
                    from posts p1 join users u1 on p1.created_by = u1.id  where orgin_id = p.orgin_id and  to_group_id = p.to_group_id limit 1 ), 
                (select count(*) as common_share_count from posts psc where orgin_id = p.orgin_id and  to_group_id = p.to_group_id) ,
                (select count(*) from posts P2 where P2.post_ref_id = p.post_ref_id and P.created_by = p.created_by) as aggregated_count,
                ${muted_query} p.* from  
                posts p join post_category_look_ups pc on p.category_id = pc.id
                join users u on u.id = p.created_by
                left join users u_s on u_s.id = p.shared_user_id
                left join users u_s1 on u_s1.id = p.to_user_id
                left join "groups" g  on g.id = p.to_group_id
                where p.is_active= true and p.type = '${type}' ${post_id} ${searchTxt} ${to_group_id_search}  ${user_id_query} order by p."updatedAt" desc OFFSET ${offsetval} LIMIT ${limitval};`
                // console.log('main query');
                // console.log(query);
                //   and p.shared_user_id is null 
                // let query = "select p.id as post_id ,* from  posts p join post_category_look_ups pc on p.category_id = pc.id  join users u on u.id = p.created_by where p.is_active= true";
                return this.adapter.db.query(query)
                    .then(([res, metadata]) => res);
            }
        },
        post_feed_count: {
            handler(ctx) {
                let user_removed_post_str = `and p.id not in (select post_id from user_removed_posts where user_id = ${ctx.params.user_id} )`;
                let following_check = `and "following" = true`;
                let viewQuery = `and p.id not in (select post_id from post_views where user_id = ${ctx.params.user_id})`;
                let query = `select count (*) from posts p 
                where p.is_active= true and p.is_approved = 1    and p.type = '${ctx.params.type}' ${viewQuery}  and (p.to_group_id in
                (select distinct(group_id) from group_maps where user_id = ${ctx.params.user_id} and is_blocked is false and is_removed is false ${following_check}
                order  by group_id asc)) ${user_removed_post_str} `;
                return this.adapter.db.query(query)
                    .then(([res, metadata]) => res);
            }
        },
        post_by_family: {
            handler(ctx) {
                let { type, txt, post_visibilty, user_id } = ctx.params;
                let offsetval = ctx.params.offset ? ctx.params.offset : 0;
                let limitval = ctx.params.limit ? ctx.params.limit : 5;

                // let _finalFilter = [];
                // _finalFilter.push(`full_result.post_id != 0`);
                // if (ctx.params.read_status !== undefined && ctx.params.read_status !== false) {
                //     _finalFilter.push('read_status = ' + ctx.params.read_status);
                // } else if (ctx.params.read_status === false) {
                //     _finalFilter.push('(read_status = ' + ctx.params.read_status + ' or read_status is null)');
                // }

                let following_check = !ctx.params.to_group_id ? 'and "following" = true' : '';
                
                let _filter = [];
                
                _filter.push('post.is_active = true')
                _filter.push('post.is_approved = 1')
                if(user_id){
                    _filter.push(`post.id not in (select post_id from user_removed_posts where user_id = ${ctx.params.user_id} )`);
                }
                if(type) _filter.push(`post.type = '${ctx.params.type}'`)
                if(txt) _filter.push(`(post.snap_description ilike '%${txt}%' or posted_family.group_name ilike '%${txt}%' or created_user.full_name ilike '%${txt}%')`)

                if(ctx.params.to_group_id) _filter.push(`post.to_group_id = '${ctx.params.to_group_id}'`)
                if(post_visibilty!=9)_filter.push(`post.to_group_id in (
                    select distinct(group_id)
                    from group_maps
                    where
                        user_id = ${ctx.params.user_id}
                        and is_blocked is false
                        and is_removed is false ${following_check}
                    order by group_id asc)`)
                if(!ctx.params.to_group_id) _filter.push(`post.id not in (select post_id from user_removed_posts where user_id = ${ctx.params.user_id} )`)
                let where = _filter.join(" and ");
                if(post_visibilty!='' && post_visibilty == 9){
                    where = `(${where})`
                }
               
                let querysuper = `select
                        post.id as post_id,
                        posted_family.id,
                        posted_family.logo as family_logo,
                        posted_family.group_name,
                        created_user.full_name as created_user_name,
                        created_user.propic,
                        original_creator.full_name as parent_post_created_user_name,
                        origin_post.created_by as parent_post_created_user_id,
                        original_creator.propic as parent_post_created_user_propic,
                        origin_posted_family.group_name as parent_post_grp_name,
                        origin_posted_family.logo as parent_family_logo,
                        shared_user.full_name as shared_user_name,
                        notification_settings.is_active as muted,post.sort_date as sort_key,
                        shared_user.full_name as shared_user_names,
                        (
                            select
                                count(*) as conversation_count_new
                            from
                                (
                                    select
                                        id,
                                        (
                                            select
                                                last_read_message_id
                                            from
                                                post_comment_reads
                                            where
                                                post_id = post.id
                                                and user_id = ${ctx.params.user_id}
                                            limit
                                                1
                                        ) as last_read_message_id
                                    from
                                        post_comments
                                    where
                                        post_id = post.id
                                ) as results
                            where
                                id > (
                                    case
                                        when last_read_message_id is null then 0
                                        else last_read_message_id
                                    end
                                )
                        ),
                        (
                            select
                                (count(*)) as shared_user_count
                            from
                                posts
                            where
                                from_id = post.id
                                and is_active = true
                        ),
                        (
                            select
                                count(*) as common_share_count
                            from
                                posts psc
                            where
                                orgin_id = post.orgin_id
                                and to_group_id = post.to_group_id
                        ),
                        (
                            select
                                count(distinct(user_id))
                            from
                                post_views
                            where
                                post_id = post.id
                                and user_id != ${ctx.params.user_id}
                        ) as views_count,
                        (
                            select
                                count(*)
                            from
                                post_comments
                            where
                                post_id = post.id
                        ) as conversation_count,
                        post.*
                    from
                        posts post
                        join "groups" posted_family on posted_family.id = post.to_group_id
                        join users created_user on created_user.id = post.created_by
                        left join users shared_user on shared_user.id = post.shared_user_id
                        left join posts origin_post on post.orgin_id = origin_post.id
                        left join "groups" origin_posted_family on origin_post.to_group_id = origin_posted_family.id
                        left join users original_creator on origin_post.created_by = original_creator.id
                        left join post_notification_settings notification_settings on notification_settings.post_id = post.id and notification_settings.user_id = ${ctx.params.user_id}
                    where ${where}
                    order by sort_key desc OFFSET ${offsetval} LIMIT ${limitval}`;
                    // if (type=='announcement' && ctx.params.read_status !== undefined && ctx.params.read_status !== false) {
                    //     querysuper = `select * from (${querysuper}) as fulldata where read_status is true`
                    // } else if (type=='announcement' && ctx.params.read_status === false) {
                    //     querysuper = `select * from (${querysuper}) as fulldata where read_status is not true`
                    // }
                    // querysuper = `${querysuper} OFFSET ${offsetval} LIMIT ${limitval}`
                    return this.adapter.db.query(querysuper)
                    .then(([res, metadata]) => res);
            }
        },
        announcement_by_family: {
            handler(ctx) {
                let { type, txt, post_visibilty, user_id } = ctx.params;
                let offsetval = ctx.params.offset ? ctx.params.offset : 0;
                let limitval = ctx.params.limit ? ctx.params.limit : 5;

                // let _finalFilter = [];
                // _finalFilter.push(`full_result.post_id != 0`);
                // if (ctx.params.read_status !== undefined && ctx.params.read_status !== false) {
                //     _finalFilter.push('read_status = ' + ctx.params.read_status);
                // } else if (ctx.params.read_status === false) {
                //     _finalFilter.push('(read_status = ' + ctx.params.read_status + ' or read_status is null)');
                // }

                let following_check = !ctx.params.to_group_id ? 'and "following" = true' : '';
                
                let _filter = [];
               
                _filter.push('post.is_active = true')
                _filter.push('post.is_approved = 1')
                if(user_id){
                    _filter.push(`post.id not in (select post_id from user_removed_posts where user_id = ${ctx.params.user_id} )`);
                }
                if(type) _filter.push(`post.type = '${ctx.params.type}'`)
                if(txt) _filter.push(`(post.snap_description ilike '%${txt}%' or posted_family.group_name ilike '%${txt}%' or created_user.full_name ilike '%${txt}%')`)

                if(ctx.params.to_group_id) _filter.push(`post.to_group_id = '${ctx.params.to_group_id}'`)
                if(post_visibilty!=9)_filter.push(`post.to_group_id in (
                    select distinct(group_id)
                    from group_maps
                    where
                        user_id = ${ctx.params.user_id}
                        and is_blocked is false
                        and is_removed is false ${following_check}
                    order by group_id asc)`)
                if(!ctx.params.to_group_id) _filter.push(`post.id not in (select post_id from user_removed_posts where user_id = ${ctx.params.user_id} )`)
                let where = _filter.join(" and ");
                if(post_visibilty!='' && post_visibilty == 9){
                    where = `(${where})`
                }

                let read_status_query = '';
                if (type=='announcement' && ctx.params.read_status !== undefined && ctx.params.read_status !== false) {
                    read_status_query = ` and PW.read_status is true`
                } else if (type=='announcement' && ctx.params.read_status === false) {
                    read_status_query = ` and PW.read_status is not true`
                }
                
                let querysuper = `select
                        DISTINCT ON(1)
                        post.id as post_id,
                        posted_family.id,
                        posted_family.logo as family_logo,
                        posted_family.group_name,
                        created_user.full_name as created_user_name,
                        created_user.propic,
                        original_creator.full_name as parent_post_created_user_name,
                        origin_post.created_by as parent_post_created_user_id,
                        original_creator.propic as parent_post_created_user_propic,
                        origin_posted_family.group_name as parent_post_grp_name,
                        origin_posted_family.logo as parent_family_logo,
                        shared_user.full_name as shared_user_name,
                        notification_settings.is_active as muted,post.sort_date as sort_key,
                        shared_user.full_name as shared_user_names,
                        PW.read_status,
                        (
                            select
                                count(*) as conversation_count_new
                            from
                                (
                                    select
                                        id,
                                        (
                                            select
                                                last_read_message_id
                                            from
                                                post_comment_reads
                                            where
                                                post_id = post.id
                                                and user_id = ${ctx.params.user_id}
                                            limit
                                                1
                                        ) as last_read_message_id
                                    from
                                        post_comments
                                    where
                                        post_id = post.id
                                ) as results
                            where
                                id > (
                                    case
                                        when last_read_message_id is null then 0
                                        else last_read_message_id
                                    end
                                )
                        ),
                        (
                            select
                                (count(*)) as shared_user_count
                            from
                                posts
                            where
                                from_id = post.id
                                and is_active = true
                        ),
                        (
                            select
                                count(*) as common_share_count
                            from
                                posts psc
                            where
                                orgin_id = post.orgin_id
                                and to_group_id = post.to_group_id
                        ),
                        (
                            select
                                count(distinct(user_id))
                            from
                                post_views
                            where
                                post_id = post.id
                                and user_id != ${ctx.params.user_id}
                        ) as views_count,
                        (
                            select
                                count(*)
                            from
                                post_comments
                            where
                                post_id = post.id
                        ) as conversation_count,
                        post.to_group_id,post.to_user_id,post.shared_user_id,post.created_by,
                        post.title,post.snap_description,post.post_attachment,post.is_shareable,post.conversation_enabled,
                        post.privacy_type,post.type,post.is_active,post."createdAt",post.post_ref_id
                    from
                        posts post
                        join "groups" posted_family on posted_family.id = post.to_group_id
                        join users created_user on created_user.id = post.created_by
                        left join users shared_user on shared_user.id = post.shared_user_id
                        left join posts origin_post on post.orgin_id = origin_post.id
                        left join "groups" origin_posted_family on origin_post.to_group_id = origin_posted_family.id
                        left join users original_creator on origin_post.created_by = original_creator.id
                        left join post_notification_settings notification_settings on notification_settings.post_id = post.id and notification_settings.user_id = ${ctx.params.user_id}
                        left join post_views PW on PW.post_id = post.id and PW.user_id = ${ctx.params.user_id}
                    where ${where} ${read_status_query}
                    order by 1 desc, sort_key desc OFFSET ${offsetval} LIMIT ${limitval}`;
            
                    return this.adapter.db.query(querysuper)
                    .then(([res, metadata]) => res);
            }
        },
        publicFeed: {
            handler(ctx) {
                let { txt } = ctx.params;
                let offsetval = ctx.params.offset ? ctx.params.offset : 0;
                let limitval = ctx.params.limit ? ctx.params.limit : 5;
                let searchTxt = txt ? `and (ps.snap_description ilike '%${txt}%'
                or g1.group_name ilike '%${txt}%'
                or u1.full_name ilike '%${txt}%')` : '';
                let select_fields = `ps.id as post_id ,g1.id, g1.group_name,g1.logo as family_logo,
                u1.full_name as created_user_name ,u1.propic ,ps.sort_date as sort_key,
                (select u2.full_name from posts pu join users u2 on pu.created_by = u2.id where pu.id = ps.orgin_id) as parent_post_created_user_name,
                (select gm2.group_name from posts pu1 join "groups" gm2 on pu1.to_group_id = gm2.id where pu1.id = ps.orgin_id) as parent_post_grp_name, 
                (select count(distinct(user_id)) from post_views where post_id = ps.id and user_id != ${ctx.params.user_id}) as views_count , 
                (select count(*) from post_comments where post_id = ps.id) as conversation_count ,
                (select count(*) as conversation_count_new  from (select * , (select last_read_message_id from post_comment_reads where post_id = ps.id and user_id = ${ctx.params.user_id} limit 1) as last_read_message_id
                from post_comments where post_id = ps.id) as results where  id > (case when last_read_message_id is null then 0 else last_read_message_id end)), 
                ps.created_by,(select (count(*)) as shared_user_count from posts where from_id = ps.id and is_active = true), (select (count(*)) as origin_shared_user_count from posts where orgin_id = ps.id and is_active = true),
                (select u1.full_name as shared_user_names
                    from posts p1 join users u1 on p1.created_by = u1.id  where orgin_id = ps.orgin_id and  to_group_id = ps.to_group_id limit 1 ), 
                (select count(*) as common_share_count from posts psc where orgin_id = ps.orgin_id and  to_group_id = ps.to_group_id) ,
                (select is_active from post_notification_settings where post_id = ps.id and user_id = ${ctx.params.user_id}) as muted,ps.*`;


                let query = `
                    select ${select_fields} 
                    from posts ps 
                    join users u1 on u1.id = ps.created_by 
                    left join "groups" g1 on g1.id = ps.to_group_id  
                    where ps.is_active= true and ps.is_approved = 1 and ps.id not in 
                    (
                        select post_id from user_removed_posts where user_id = ${ctx.params.user_id} 
                    ) 
                    and privacy_type = 'public' and to_group_id is null and to_user_id is null and 
                    ps.type = '${ctx.params.type}'  ${searchTxt} and 
                    ps.id not in 
                    (
                        select p.id from  
                        posts p 
                        join post_category_look_ups pc on p.category_id = pc.id
                        join "groups" g  on g.id = p.to_group_id
                        join users u on u.id = p.created_by
                        left join users u_s on u_s.id = p.shared_user_id
                        where p.is_active= true and p.is_approved = 1 and p.type = '${ctx.params.type}' and 
                        to_group_id is null and to_user_id is null  and 
                        p.id not in 
                        (
                            select post_id 
                            from user_removed_posts 
                            where user_id = ${ctx.params.user_id} 
                        ) 
                    ) order by sort_key DESC OFFSET ${offsetval} LIMIT ${limitval};`
                //   and p.shared_user_id is null 
                // let query = "select p.id as post_id ,* from  posts p join post_category_look_ups pc on p.category_id = pc.id  join users u on u.id = p.created_by where p.is_active= true";
                console.log('PUBLIC FEED qeury============================');
                console.log(query);
                return this.adapter.db.query(query)
                    .then(([res, metadata]) => res);
            }
        },
        pendingApprovals: {
            handler(ctx) {
                let { group_id, type } = ctx.params;

                let query = `select p.*,p.id as post_id,u.full_name as created_user_name, u.propic from  posts p join users u on p.created_by = u.id where p.is_approved = 0 and p.to_group_id = ${group_id} and p.type = '${type}' `;
                return this.adapter.db.query(query)
                    .then(([res, metadata]) => res);
            }
        },
        getFamilyList: {
            handler(ctx) {
                let { created_by, post_ref_id, type } = ctx.params;
                var permission_var = '';
                if (type == 'post') {
                    permission_var = 'post_create';
                } else {
                    permission_var = 'announcement_create';
                }
                let query = `(select to_group_id as id, (select ${permission_var} from groups where id = to_group_id) from posts p where post_ref_id = '${post_ref_id}' and created_by = ${created_by} and to_group_id  notnull and is_active=true) `;
                return this.adapter.db.query(query)
                    .then(([res, metadata]) => res);
            }
        },
        getFamilyListForPost: {
            handler(ctx) {
                let { user_id } = ctx.params;
                let ref_id = ctx.params.ref_id ? ctx.params.ref_id : '';
                let postAnnounce_setting = '';
                if(ctx.params.type == 'announcement') {
                    postAnnounce_setting = `and (g.announcement_create = 18 or g.announcement_create = 19 or (g.announcement_create = 17 and gm."type" = 'admin'))`;
                } else {
                    postAnnounce_setting = `and (g.post_create = 6 or g.post_create = 20 or (g.post_create = 7 and gm."type" = 'admin'))`;
                }
                let searchTxt = ctx.params.query ? `and (g.group_name ilike '%${ctx.params.query}%' or g.base_region ilike '%${ctx.params.query}%')` : '';
                let query = `SELECT u.full_name as created_by, g.id,g.group_name,g.post_create,g.announcement_create,g.post_approval,g.group_type,g.base_region,g.logo ,to_char(g.created_at, 'DD MON YYYY') as member_since,
                gm.type as user_type,
                (case when (select group_id from posts where to_group_id = g.id and post_ref_id = '${ref_id}' limit 1) is null 
                then 'false' else 'true' end) as is_shared,gm.type as user_type
                from group_maps gm join "groups" g on g.id = gm.group_id 
                join "users" u on u.id = g.created_by 
                where g.is_active = true and gm.user_id = ${user_id} ${postAnnounce_setting} and gm.is_blocked = false and gm.is_removed = false ${searchTxt}`;
                return this.adapter.db.query(query)
                    .then(([res, metadata]) => res);
            }
        },
        list_post_views: {
            handler(ctx) {
                let { post_id, user_id } = ctx.params;
                let query = `select pv.user_id,u.full_name,u.location,u.propic from post_views pv join users u on pv.user_id = u.id 
                where post_id = ${post_id} and pv.user_id != ${user_id} group by u.full_name,pv.user_id,u.location,u.propic`;
                return this.adapter.db.query(query)
                    .then(([res, metadata]) => res);
            }
        },
        listAll: {
            handler(ctx) {
                // let query = `select p.id as post_id ,(select count(distinct(user_id)) from post_views where post_id = p.id) as views_count , (select count(*) from post_comments where post_id = p.id) as conversation_count , * from  
                // posts p join post_category_look_ups pc on p.category_id = pc.id  
                // join users u on u.id = p.created_by where p.is_active= true and p.to_group_id in (954,955);`
                let query = "select p.id as post_id ,* from  posts p join post_category_look_ups pc on p.category_id = pc.id  join users u on u.id = p.created_by where p.is_active= true";
                return this.adapter.db.query(query)
                    .then(([res, metadata]) => res);
            }
        },
        getSharedUserList: {
            handler(ctx) {
                let { post_id, group_id } = ctx.params;
                let query = `select u.id as user_id,u.full_name as shared_user_name,u.location,u.propic,* from posts p 
                join users u on p.shared_user_id = u.id 
                where (orgin_id = ${post_id}) and to_group_id = ${group_id};`;
                return this.adapter.db.query(query)
                    .then(([res, metadata]) => res);
            }
        },
        getCommonSharedUserList: {
            handler(ctx) {
                let { origin_post_id } = ctx.params;
                let query = `select u.id as user_id,u.full_name as shared_user_name,u.location,u.propic,p.to_group_id,p.to_user_id,
                p.created_by,p.from_id,p."createdAt" as created_date,
                    (select group_name from groups where id=p.to_group_id) as to_group_name,
                    (select full_name from users where id=p.to_user_id) as to_user_name
                from posts p 
                join users u on p.shared_user_id = u.id 
                where orgin_id = ${origin_post_id} and p.is_active = true;`;
                return this.adapter.db.query(query)
                    .then(([res, metadata]) => res);
            }
        },
        getSharedUserListByUserid: {
            handler(ctx) {
                let { post_id, user_id } = ctx.params;
                let query = `select * from (select distinct(P.shared_user_id),P.orgin_id,U.propic,U.full_name,U."location" 
                from posts P
left join users U on U.id = P.shared_user_id
where P.to_user_id = ${user_id} or to_group_id in (select group_id from group_maps where user_id = ${user_id}) 
and P.shared_user_id is not null
and P.is_active is true) as shared where orgin_id = ${post_id};`;
                return this.adapter.db.query(query)
                    .then(([res, metadata]) => res);
            }
        },
        bulkCreate: {
            handler(ctx) {
                return this.adapter.db.models.post.bulkCreate(ctx.params,{returning: true})
                .then((res) => {
                    return res
                })
                    //.then(([res, metadata]) => res);

            }
        },
        check_post_permission: {
            handler(ctx) {
                let { user_id } = ctx.params;
                let query = `select distinct(group_id),G.group_name from group_maps GM 
join "groups" G on G.id = GM.group_id and G.is_active is true and 
((GM."type" = 'admin' and G.announcement_create = 17) or (G.announcement_create = 18))
where GM.user_id = ${user_id} and GM.is_blocked is false and is_removed is false and G.group_name != '' order by G.group_name`
                return this.adapter.db.query(query)
                    .then(([res, metadata]) => res);
            }
        },
        checkPostActiveStatus: {
            handler(ctx) {
                let { post_id,topic_id } = ctx.params;
                let _whereQuery = '';
                if(topic_id) {
                    _whereQuery = `topic_id = ${topic_id}`;
                } else {
                    _whereQuery = `post_id = ${post_id}`;
                }

                // let query = `select p.to_group_id,p.to_user_id,pc.* from post_comments pc join posts p on pc.post_id = p.id  where pc.post_id = 850 and pc."createdAt" > current_timestamp - (10 ||' minutes')::interval`;
                let query = `select * from post_comments where ${_whereQuery} and "createdAt" > current_timestamp - (10 ||' minutes')::interval`;
                return this.adapter.db.query(query)
                    .then(([res, metadata]) => res);
            }
        },
        getAllUserFromGrp: {
            handler(ctx) {
                let { group_id, post_id } = ctx.params;
                let query = `select * 
                from group_maps _gm
                where group_id = ${group_id} and _gm.is_blocked is false and _gm.is_removed is false and 
                _gm.user_id not in(
                select user_id 
                from post_notification_settings pn 
                where pn.post_id=${post_id} and pn.is_active is true and pn.user_id = _gm.user_id )`;
                return this.adapter.db.query(query)
                    .then(([res, metadata]) => res);
            }
        },
        // getActiveNotificationCount: {
        //     handler(ctx) {
        //         let query = `select count(*) from post_comments where post_id = 850 and id > (select last_read_message_id from post_comment_reads where post_id = 850)`;
        //     }
        // },
        deletePost: {
            handler(ctx) {
                let { id } = ctx.params;
                let query = `UPDATE posts SET is_active = false where id = ${id};`;
                return this.adapter.db.query(query)
                .then(([res, metadata]) => res);
            }
        },
        getNewAnnouncementCount: {
            handler(ctx) {
                let { user_id } = ctx.params;
                let query = `select count(*) from (select distinct(P.created_by) from posts P where (P.to_user_id = ${user_id} 
                or to_group_id in (select group_id from group_maps gm where gm.user_id = ${user_id} and gm.is_blocked = false and gm.is_removed = false)) and P.is_active is true 
                and P."type" = 'announcement'
                and P.id not in(select post_id from post_views where user_id = ${user_id}) and p.id not in (select post_id from user_removed_posts where user_id = ${user_id} )) as new_announcement`;
                return this.adapter.db.query(query)
                    .then(([res, metadata]) => res);
            }
        },
        updateByParams: {
            handler(ctx) {
                let { params, where } = ctx.params;
                return this.adapter.db.models.post.update(params, { where: where,returning: true })
                    //.then(([res, metadata]) => res);
                .then((res) => {
                    return res
                })
            }
        },
        updatePostByFamily: {
            handler(ctx) {
                let keyArr = ['category_id', 'orgin_id', 'from_id', 'post_ref_id', 'to_group_id', 'to_user_id', 'shared_user_id', 'created_by', 'title', 'snap_description', 'post_attachment', 'post_info', 'file_type', 'is_shareable', 'conversation_enabled', 'privacy_type', 'type', 'is_active', 'is_approved']
                // let key_params = `to_group_id, post_ref_id,category_id,created_by,snap_description`;
                let keyValueArr = [];
                var key_params = '';
                if (ctx.params.to_group_id && ctx.params.to_group_id.length != 0) {
                    for (var i = 0; i < ctx.params.to_group_id.length; i++) {
                        let inserObj = {};
                        for (var key in ctx.params) {
                            // console.log(key);
                            if (keyArr.indexOf(key) != -1) {
                                if (i == 0) {
                                    // if (key_params) {
                                    //     key_params = key_params + ',' + key
                                    // } else {
                                    //     key_params = key;
                                    // }
                                }
                                // console.log(key_params);

                                if (key == 'to_group_id') {
                                    inserObj[key] = ctx.params.to_group_id[i].id;
                                    inserObj.is_approved = (ctx.params.to_group_id[i].post_create == 20 ? 0 : 1);
                                } else {
                                    inserObj[key] = ctx.params[key];
                                }
                                console.log('inserObj ===================');
                                console.log(inserObj);
                                console.log('inserObj ===================');

                            }
                        }
                        keyValueArr.push(inserObj);
                    }
                }
                var query = '';
                keyValueArr.map((values, k) => {
                    let tempValueString = '(';

                    for (var key2 in values) {
                        if (k == 0) {
                            if (key_params) {
                                key_params = key_params + ',' + key2
                            } else {
                                key_params = key2;
                            }
                            query = `with data(${key_params}) as (
                                values `;
                        }


                        if (tempValueString != '(') {
                            if (values[key2] !== undefined) {
                                if (key2 == 'to_group_id' || key2 == 'is_shareable' || key2 == 'post_attachment') {
                                    if (key2 == 'post_attachment') {
                                        if (values[key2].length == 0) {
                                            tempValueString = `${tempValueString},${values[key2]}`;
                                        } else {
                                            tempValueString = tempValueString + "," + values[key2] + "";
                                        }
                                        console.log('@############ =', key2);
                                        console.log('@############ =', values[key2]);
                                        console.log('@############ =', values[key2] ? true : false);
                                    } else {
                                        tempValueString = tempValueString + "," + values[key2] + "";

                                    }

                                } else {
                                    tempValueString = tempValueString + ",'" + values[key2] + "'";
                                }
                            }
                        } else if (tempValueString == '(') {
                            tempValueString = "('" + values[key2] + "'";
                        } else {
                            tempValueString = tempValueString + ",'" + values[key2] + "'";
                        }
                    }
                    tempValueString = tempValueString + '),';
                    // console.log(tempValueString);
                    return query = query + tempValueString;

                });
                query = query.substring(0, query.length - 1);
                query = query + ')';
                // query = query +
                // (684, 'QqIb0D5Ecf1577437826642', 3, 940, 'ttttttttt'),
                //     (6666, 'adslkejkdsjfds', 3, 940, 'ttttttttt')
                //     )
                query = query + ` insert into posts (${key_params}) 
                    select ${key_params}
                    from data d
                    where not exists (select 1
                    from posts p2
                    where p2.post_ref_id = d.post_ref_id and p2.to_group_id = d.to_group_id);`;
                console.log('query ======================');
                console.log(query);
                return this.adapter.db.query(query)
                    .then(([res, metadata]) => res);
            }
        },
        update_post_views: {
            handler(ctx) {
                let { params, where } = ctx.params;
                return this.adapter.db.models.post_view.update(params, { where: where })
                    .then(([res, metadata]) => res);
            }
        },
        bulk_update_post: {
            handler(ctx) {
                let { post_ref_id, delete_post } = ctx.params
                let query = `update posts set is_active = false where post_ref_id = '${post_ref_id}' and to_group_id in(${delete_post})`;
                return this.adapter.db.query(query)
                    .then(([res, metadata]) => res);
            }
        },
        bulk_update_post_byfamily: {
            handler(ctx) {
                let { group_id } = ctx.params
                let _query = `update posts set is_active = false where to_group_id = ${group_id}`;
                return this.adapter.db.query(_query)
                    .then(([res, metadata]) => res);
            }
        },
        /*
         count of post/announcement which is active based on group
        */
        countPostByGroup:{
            handler(ctx) {
                let { to_group_id, type, is_active } = ctx.params;

                let _filter = [];
                if(to_group_id) _filter.push(`to_group_id=${to_group_id}`);
                if(type) _filter.push(`type='${type}'`);
                if(is_active) _filter.push(`is_active is ${is_active}`);

                let _where = _filter.join(" and ");
                let _query = `select count(*) FROM posts where ${_where}`;
                return this.adapter.db.query(_query)
                .then(([res, metadata]) => res);
            }
        },
        announceBannerCount: {
            handler(ctx) {
                let { user_id,last_seen_date } = ctx.params
                let last_seen_query = '';
                if(last_seen_date) {
                    last_seen_query = `and post."createdAt" > '${last_seen_date}'`;
                } 
                let _query = `select * from (
                    select
                        count(*) as new_announcement_count,post.id as post_id,
                        (select count(*) as removed_count from user_removed_posts urp where urp.post_id=post.id and urp.user_id=${user_id}),
                        (
                            select
                            distinct(read_status) as read_status
                            from
                                post_views
                            where
                                user_id = ${user_id}
                                and post_id = post.id
                            limit
                                1
                        )
                    from
                        posts post
                    where post.is_active = true and post.is_approved = 1 and post.type = 'announcement' ${last_seen_query} and post.to_group_id in (
                    select distinct(group_id)
                    from group_maps
                    where
                        user_id = ${user_id}
                        and is_blocked is false
                        and is_removed is false and "following" = true
                    ) and post.id not in (select post_id from user_removed_posts where user_id = ${user_id}) group by post_id  
                ) as full_result where full_result.removed_count = 0 and (read_status = false or read_status is null) limit 1`;
                return this.adapter.db.query(_query)
                .then(([res, metadata]) => res);  
            }
        },
        checkUserPostPermission: {
            handler(ctx) {
                let { user_id,post_id } = ctx.params;
                let query = `select P.id from posts P where (
                    (P.to_user_id is null and P.to_group_id is null) or 
                    (P.to_user_id =  ${user_id}) or 
                    (P.created_by =  ${user_id}) or
                    (
                        select id from "groups" G where (
                            (G.group_type = 'public') or 
                            (G.group_type = 'private' and (select id from group_maps GM where GM.group_id = G.id and GM.user_id = ${user_id}) is not null)
                        )and id = P.to_group_id and G.is_active is true
                    ) is not null
                ) 
                and P.id = ${post_id} and P.is_active is true` ;
                return this.adapter.db.query(query)
                .then(([res, metadata]) => res);  
            }
        },
        /**
         * fetch post of posttype :: folder
         * check already exist or not and return value
         */
        postForFolder: {
            handler(ctx) {
                let { publish_type,publish_id, StartTime, endTime } = ctx.params;
                let _query = `select id,type,post_attachment,"createdAt" 
                FROM posts 
                where publish_type ='${publish_type}' and  publish_id ='${publish_id}' 
                and ( "createdAt" >= '${StartTime}' and "createdAt" <= '${endTime}')`;
                return this.adapter.db.query(_query)
                .then(([res, metadata]) => res);  
            }
        },
        post_sticky_by_family: {
            handler(ctx) {
                let {group_id,sticky_post,post_visibilty,user_id} = ctx.params;
                let _filter = [];
                _filter.push(`P.is_active is true and P.is_approved = 1 and P.id in (${sticky_post}) and P.to_group_id = '${group_id}'`)
                if(post_visibilty!=9)_filter.push(` P.to_group_id in (
                    select distinct(group_id)
                    from group_maps
                    where
                        user_id = ${user_id}
                        and is_blocked is false
                        and is_removed is false
                    order by group_id asc)`)
                if(!ctx.params.to_group_id) _filter.push(`P.id not in (select post_id from user_removed_posts where user_id = ${user_id} )`)
                let where = _filter.join(" and ");
                if(post_visibilty!='' && post_visibilty == 9){
                    where = `(${where})`
                }
                let _query = `SELECT P.id as post_id,P.post_attachment,P.snap_description,U.full_name as created_user_name
                from posts P 
                left join users U on U.id = P.created_by
                where ${where}`;
                return this.adapter.db.query(_query)
                .then(([res, metadata]) => res);
            }    
        },
        get_user_commented_post: {
            handler(ctx) {
                let { user_id,query } = ctx.params;
                let filter = [`PC.commented_by = ${user_id}` , `PC.is_active is true` , `PC.post_id is not null`]
                if(query && query != '' && query.trim()!='') {
                    query = query.trim();
                    filter.push(`(G.group_name ilike '%${query}%' or P.snap_description ilike '%${query}%')`);
                }
                var where  = filter.join(" and ")
                let _query = `select distinct on(PC.post_id) PC.post_id,G.group_name,
                PC.attachment as last_comment_attachment,P.publish_type,P.post_attachment,P.snap_description,
                (select count(*) as unread_conversation_count from (select * , 
                    (select last_read_message_id from post_comment_reads where post_id = PC.post_id and user_id = ${user_id} limit 1) as last_read_message_id
                    from post_comments where post_id = PC.post_id) as results where  id > (case when last_read_message_id is null then 0 else last_read_message_id end)
                ),
                (select U.full_name as last_commented_user 
                from post_comments PC1
                left join users U on U.id = PC1.commented_by
                where PC1.post_id = PC.post_id and PC1.is_active is true order by PC1.id desc limit 1),
                (select PC2."comment" as last_comment 
                from post_comments PC2
                where PC2.post_id = PC.post_id and PC2.is_active is true order by PC2.id desc limit 1),
                (select "createdAt" as last_commented_date
                from post_comments PC3
                where PC3.post_id = PC.post_id and PC3.is_active is true order by PC3.id desc limit 1),
                (select PC5.id as sort_id
                from post_comments PC5
                where PC5.post_id = PC.post_id and PC5.is_active is true order by PC5.id desc limit 1),
                (select count(id) as total_comments from post_comments PC4 where PC4.post_id = PC.post_id and PC.is_active is true),
                (select count(distinct(commented_by)) as commented_user_count from post_comments PC6
                where PC6.post_id = PC.post_id and PC6.is_active is true)
                from post_comments PC
                join posts P on P.id = PC.post_id
                left join groups G on G.id = P.to_group_id
                where ${where}
                order by PC.post_id desc,PC.id desc`;
                return this.adapter.db.query(_query)
                .then(([res, metadata]) => res);
            }    
        },
        get_null_elasticSearch_public_post: {
            handler(ctx) {
                let _query = `select *
                from posts where elasticsearch_id is null and post_type = 'public' order by id desc limit 50`;
                return this.adapter.db.query(_query)
                .then(([res, metadata]) => res);
            }    
        },
        forElastic:{
            handler(ctx) {
                let { limit, offset } = ctx.params;
                let _query = `SELECT * FROM posts
                where is_active is false and elasticsearch_id is not null
                order by id asc
                offset ${offset} limit ${limit}`;
                return this.adapter.db.query(_query)
                .then(([res, metadata]) => res);
            } 
        },
        get_notification_post_data:{
            handler(ctx) { 
                let {post_id} = ctx.params;
                let masterQuery = `select U.full_name as created_by_user,U.propic as created_by_propic,P.snap_description as description,P.post_attachment,P.privacy_type,P.post_type,G.group_name as to_group_name 
        from posts P 
        left join users U on U.id = P.created_by
        left join "groups" G on G.id = P.to_group_id
        where P.id = ${post_id}`;
                return this.adapter.db.query(masterQuery)
                .then(([res, metadata]) => res);
            }   
        },
        getpostsbyPostAttachment:{
            handler(ctx) { 
                let { filename } = ctx.params;
                let masterQuery = `select id, post_attachment
                from posts p2 
                where post_attachment ::text like '%${filename}%'`;
                return this.adapter.db.query(masterQuery)
                .then(([res, metadata]) => res);
            } 
        }
    },
    events: {},
    methods: {},
    created() { },
    started() { },
    stopped() { },
}