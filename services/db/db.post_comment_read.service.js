/* eslint-disable indent */
"use_strict";
//packages
const DbService = require('moleculer-db');
const SqlAdapter = require('moleculer-db-adapter-sequelize');
const Sequelize = require('sequelize');

//user table model
module.exports = {
    name: "db.post_comment_read",
    mixins: [DbService],
    settings: {},
    dependencies: [],
    cache: false,
    adapter: new SqlAdapter(process.env.SQL_DATABASE,null,null, {
        logger: false,
        pool: {
            max: 1,
            min: 0,
            acquire: 30000,
            idle: 10000,
            handleDisconnects: true
        },
        dialect: 'postgres',
        port: 5432,
        replication: {
            read: [
                { host: process.env.SQL_READ_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
            ],
            write: { host: process.env.SQL_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
        },
    }),
    model: {
        name: "post_comment_read",
        define: {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            post_id: {
                field: "post_id",
                type: Sequelize.INTEGER,
            },
            topic_id: {
                field: "topic_id",
                type: Sequelize.INTEGER
            },
            user_id: {
                field: "user_id",
                type: Sequelize.INTEGER,
            },
            last_read_message_id: {
                field: "last_read_message_id",
                type: Sequelize.INTEGER,
            }
        },
        options: {}
    },
    actions: {
        
    },
    events: {},
    methods: {},
    created() { },
    started() { },
    stopped() { },
};