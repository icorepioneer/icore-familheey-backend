"use_strict";
//packages
const DbService = require('moleculer-db');
const SqlAdapter = require('moleculer-db-adapter-sequelize');
const Sequelize = require('sequelize');
const Cuid = require('cuid');

//user table model
module.exports = {
    name: "db.users",
    // version:1,
    mixins: [DbService],
    settings: {},
    dependencies: [],
    cache: false,
    adapter: new SqlAdapter(process.env.SQL_DATABASE,null,null, {
        logger: false,
        pool: {
            max: 1,
            min: 0,
            acquire: 30000,
            idle: 10000,
            handleDisconnects: true
        },
        dialect: 'postgres',
        port: 5432,
        replication: {
            read: [
                { host: process.env.SQL_READ_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
            ],
            write: { host: process.env.SQL_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
        },
    }),
    model: {
        name: "users",
        define: {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true,
            },
            full_name: {
                field: "full_name",
                type: Sequelize.STRING
            },
            email: {
                field: "email",
                type: Sequelize.STRING
            },
            password: {
                field: "password",
                type: Sequelize.STRING
            },
            phone: {
                field: "phone",
                type: Sequelize.STRING
            },
            propic: {
                field: "propic",
                type: Sequelize.STRING
            },
            cover_pic: {
                field: "cover_pic",
                type: Sequelize.STRING
            },
            user_original_image: {
                field: 'user_original_image',
                type: Sequelize.STRING
            },
            location: {
                field: "location",
                type: Sequelize.STRING
            },
            origin: {
                field: "origin",
                type: Sequelize.STRING
            },
            dob: {
                field: "dob",
                type: Sequelize.DATE
            },
            gender: {
                field: "gender",
                type: Sequelize.STRING
            },
            type: {
                field: "type",
                type: Sequelize.STRING
            },
            about: {
                field: "about",
                type: Sequelize.TEXT
            },
            work: {
                field: "work",
                type: Sequelize.TEXT
            },
            otp: {
                field: "otp",
                type: Sequelize.STRING
            },
            otp_time: {
                field: "otp_time",
                type: Sequelize.DATE
            },
            createdAt: {
                field: "created_at",
                type: 'TIMESTAMP',
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            },
            updatedAt: {
                field: "updated_at",
                type: 'TIMESTAMP',
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            },
            is_active: {
                field: "is_active",
                type: Sequelize.BOOLEAN,
                defaultValue: false
            },
            user_is_blocked: {
                field: "user_is_blocked",
                type: Sequelize.BOOLEAN,
                defaultValue: false
            },
            is_verified: {
                field: "is_verified",
                type: Sequelize.BOOLEAN,
                defaultValue: false
            },
            token: {
                field: "token",
                type: Sequelize.STRING
            },
            auto_password: {
                field: "auto_password",
                type: Sequelize.INTEGER,
                defaultValue: 1
            },
            searchable: {
                field: "searchable",
                type: Sequelize.BOOLEAN,
                defaultValue: true
            },
            notification: {
                field: 'notification',
                type: Sequelize.BOOLEAN,
                defaultValue: true
            },
            lat: {
                field: "lat",
                type: Sequelize.DOUBLE
            },
            long: {
                field: "long",
                type: Sequelize.DOUBLE
            },
            origin_lat: {
                field: "origin_lat",
                type: Sequelize.DOUBLE
            },
            origin_long: {
                field: "origin_long",
                type: Sequelize.DOUBLE
            },
            user_privilege:{
                field:"user_privilege",
                type:Sequelize.STRING
            },
            payment_customer_id:{
                field:"payment_customer_id",
                type:Sequelize.STRING
            },
            payment_customer_object: {
                field: "payment_customer_object",
                type: Sequelize.JSON    
            },
            payment_card_ids: {
                field: "payment_card_ids",
                type: Sequelize.JSON    
            },
            public_notification: {
                field: 'public_notification',
                type: Sequelize.BOOLEAN,
                defaultValue: false
            },
            conversation_notification: {
                field: 'conversation_notification',
                type: Sequelize.BOOLEAN,
                defaultValue: true
            },
            announcement_notification: {
                field: 'announcement_notification',
                type: Sequelize.BOOLEAN,
                defaultValue: true
            },
            family_notification_off: {
                field: "family_notification_off",
                type: Sequelize.JSON    
            },
            event_notification: {
                field: 'event_notification',
                type: Sequelize.BOOLEAN,
                defaultValue: true
            },
            notification_auto_delete: {
                field: 'notification_auto_delete',
                type: Sequelize.BOOLEAN,
                defaultValue: false
            },
        },
        options: {}
    },
    actions: {
        searchUser: {
            handler(ctx) {
                let group_id = ctx.params.group_id ? ctx.params.group_id : '';
                let searchtxt = ctx.params.txt ? ctx.params.txt : '';
                let searchuser = ctx.params.userid ? ctx.params.userid : '';
                let offsetval = ctx.params.offset;
                let limitval = ctx.params.limit;
                let defaultQuery = '';
                if(!group_id){ group_id = 0 }
                let multiUser = ''
                if(ctx.params.multiUser && ctx.params.multiUser.length>0) {
                    multiUser = `and U.id = any('{${ctx.params.multiUser}}')`; 
                } 
                if(!searchtxt) {
                // defaultQuery = `and U.id not in (select distinct(user_id) from group_maps where group_id in (select distinct(group_id) from group_maps where user_id in (
                // select distinct(user_id) from group_maps where group_id in (select group_id from group_maps where user_id = ${searchuser} and is_blocked is false and is_removed is false) 
                // and user_id != ${searchuser} and is_blocked is false and is_removed is false
                // ) and group_id not in (select group_id from group_maps where user_id = ${searchuser} and is_blocked is false and is_removed is false)) and is_blocked is false and is_removed is false)`
                defaultQuery = `and U.id not in (select distinct(user_id) from group_maps where group_id in 
                (select group_id from group_maps where user_id = ${searchuser} and is_blocked is false and is_removed is false) 
                and is_blocked is false and is_removed is false)` 
                }
                
                let masterQuery = `SELECT DISTINCT ON (U.id) U.id,U.full_name,U.location,U.origin,U.gender,U.propic,GM.familycount,
                (select location from users where id = ${searchuser}) as sortLocation,
                (select count(*) from group_maps GMU where GMU.user_id=${searchuser} and GMU.is_blocked is false and GMU.is_removed is false) as logined_user_family_count,
                (select type from group_maps _GM1 where _GM1.user_id=U.id and _GM1.group_id=${group_id} and _GM1.is_blocked is false limit 1),
                (select (case when (select id from group_maps where group_id = ${group_id} and user_id = U.id and is_blocked is false 
                and is_removed is false) > 0 then true else false end) as exists),
                (select (case when (select id from requests where status = 'pending' and group_id = ${group_id} and user_id = U.id limit 1) > 0 then true else false end) as invitation_status)
                from users as U
                LEFT JOIN (select count(distinct(g.id)) as familycount,gm1.user_id from group_maps as gm1 join "groups" g on g.id = gm1.group_id where g.is_active is true and gm1.is_removed is false and gm1.is_blocked is false group by gm1.user_id) as GM 
                on GM.user_id = U.id
                where U.is_active=true and U.searchable=true ${multiUser} ${defaultQuery} and U.id != ${searchuser} and (U.full_name ilike '%${searchtxt}%' or U.location ilike '%${searchtxt}%') 
                order by U.id asc OFFSET ${offsetval} LIMIT ${limitval}`;
                return this.adapter.db.query(masterQuery)
                .then(([res, metadata]) => res)
                .catch((err)=>console.log(err))
            }
        },
        birthday_wish: {
            handler(ctx) {
                let { day, month } = ctx.params;
                let query = `select * from (select dob,EXTRACT(MONTH FROM dob) as "month",EXTRACT(day FROM dob) "day" from users) as user_dobs  where "day" = '${day}' and "month"= '${month}';`
                return this.adapter.db.query(query)
                .then(([res, metadata]) => res)
            }
        },
        Custom:{
            handler(ctx){
                let { user_id } = ctx.params;
                let masterQuery = `DELETE FROM agendas WHERE user_id = ${user_id}`;
                return this.adapter.db.query(masterQuery)
                .then(doc=>{ console.log( "agendas == ",doc)
                    let masterQuery2 = `DELETE FROM calenders WHERE user_id = ${user_id}`;
                    return this.adapter.db.query(masterQuery2) 
                }).then(doc=>{ console.log( "calenders == ",doc)
                    let masterQuery3 = `DELETE FROM documents WHERE user_id = ${user_id}`;
                    return this.adapter.db.query(masterQuery3) 
                }).then(doc=>{ console.log( "documents == ",doc)
                    let masterQuery4 = `DELETE FROM document_maps WHERE user_id = ${user_id}`;
                    return this.adapter.db.query(masterQuery4) 
                }).then(doc=>{ console.log( "document_maps == ",doc)
                    let masterQuery5 = `DELETE FROM events_follows WHERE user_id = ${user_id}`;
                    return this.adapter.db.query(masterQuery5) 
                }).then(doc=>{ console.log( "events_follows == ",doc)
                    let masterQuery6 = `DELETE FROM events_invitations WHERE user_id = ${user_id}`;
                    return this.adapter.db.query(masterQuery6) 
                }).then(doc=>{ console.log( "events_invitations == ",doc)
                    let masterQuery7 = `DELETE FROM events_shares WHERE user_id = ${user_id}`;
                    return this.adapter.db.query(masterQuery7) 
                }).then(doc=>{ console.log( "events_shares == ",doc)
                    let masterQuery8 = `DELETE FROM events_signup_items WHERE user_id = ${user_id}`;
                    return this.adapter.db.query(masterQuery8) 
                }).then(doc=>{ console.log( "events_signup_items == ",doc)
                    let masterQuery9 = `DELETE FROM group_maps WHERE user_id = ${user_id}`;
                    return this.adapter.db.query(masterQuery9) 
                }).then(doc=>{ console.log( "group_maps == ",doc)
                    let masterQuery10 = `DELETE FROM other_users WHERE user_id = ${user_id}`;
                    return this.adapter.db.query(masterQuery10) 
                }).then(doc=>{ console.log( "other_users == ",doc)
                    let masterQuery11 = `DELETE FROM reminders WHERE user_id = ${user_id}`;
                    return this.adapter.db.query(masterQuery11) 
                }).then(doc=>{ console.log( "reminders == ",doc)
                    let masterQuery12 = `DELETE FROM requests WHERE user_id = ${user_id}`;
                    return this.adapter.db.query(masterQuery12) 
                }).then(doc=>{ console.log( "requests == ",doc)
                    let masterQuery13 = `DELETE FROM user_activities WHERE user_id = ${user_id}`;
                    return this.adapter.db.query(masterQuery13) 
                }).then(doc=>{ console.log( "user_activities == ",doc)
                    let masterQuery14 = `DELETE FROM vieweds WHERE user_id = ${user_id}`;
                    return this.adapter.db.query(masterQuery14) 
                }).then(doc=>{ console.log( "vieweds == ",doc)
                    let masterQuery15 = `DELETE FROM device_tokens WHERE user_id = ${user_id}`;
                    return this.adapter.db.query(masterQuery15) 
                }).then(doc=>{ console.log( "device_tokens == ",doc)
                    let masterQuery16 = `DELETE FROM post_comment_reads WHERE user_id = ${user_id}`;
                    return this.adapter.db.query(masterQuery16) 
                }).then(doc=>{ console.log( "agendas == ",doc)
                    let masterQuery17 = `DELETE FROM post_likes WHERE user_id = ${user_id}`;
                    return this.adapter.db.query(masterQuery17) 
                }).then(doc=>{ console.log( "post_likes == ",doc)
                    let masterQuery18 = `DELETE FROM post_notification_settings WHERE user_id = ${user_id}`;
                    return this.adapter.db.query(masterQuery18) 
                }).then(doc=>{ console.log( "post_notification_settings == ",doc)
                    let masterQuery19 = `DELETE FROM post_views WHERE user_id = ${user_id}`;
                    return this.adapter.db.query(masterQuery19) 
                }).then(doc=>{ console.log( "post_views == ",doc)
                    let masterQuery20 = `DELETE FROM spam_reports WHERE user_id = ${user_id}`;
                    return this.adapter.db.query(masterQuery20) 
                }).then(doc=>{ console.log( "spam_reports == ",doc)
                    let masterQuery21 = `DELETE FROM user_removed_posts WHERE user_id = ${user_id}`;
                    return this.adapter.db.query(masterQuery21) 
                }).then(doc=>{ console.log( "user_removed_posts == ",doc)
                    let masterQuery22 = `DELETE FROM event_rsvps WHERE attendee_id = ${user_id}`;
                    return this.adapter.db.query(masterQuery22) 
                }).then(doc=>{ console.log( "agendas == ",doc)
                    let masterQuery23 = `DELETE FROM events_signup_contributions WHERE contribute_user_id = ${user_id}`;
                    return this.adapter.db.query(masterQuery23) 
                }).then(doc=>{ console.log( "events_signup_contributions == ",doc)
                    let masterQuery24 = `DELETE FROM post_comments WHERE commented_by = ${user_id}`;
                    return this.adapter.db.query(masterQuery24) 
                }).then(doc=>{ console.log( "post_comments == ",doc)
                    let masterQuery25 = `DELETE FROM relationships WHERE primary_user = ${user_id} or secondary_user = ${user_id}`;
                    return this.adapter.db.query(masterQuery25) 
                }).then(doc=>{ console.log( "relationships == ",doc)
                    let masterQuery26 = `DELETE FROM posts WHERE to_user_id = ${user_id} or created_by = ${user_id} or shared_user_id = ${user_id}`;
                    return this.adapter.db.query(masterQuery26) 
                }).then(doc=>{ console.log( "posts == ",doc)
                    let masterQuery27 = `DELETE FROM events WHERE created_by = ${user_id}`;
                    return this.adapter.db.query(masterQuery27) 
                }).then(doc=>{ console.log( "events == ",doc)
                    let masterQuery28 = `DELETE FROM groups WHERE created_by = ${user_id}`;
                    return this.adapter.db.query(masterQuery28) 
                }).then(doc=>{ console.log( "groups == ",doc)
                    let masterQuery29 = `DELETE FROM folders WHERE created_by = ${user_id}`;
                    return this.adapter.db.query(masterQuery29) 
                }).then(doc=>{ console.log( "folders == ",doc)
                    let masterQuery30 = `DELETE FROM users WHERE id = ${user_id}`;
                    return this.adapter.db.query(masterQuery30) 
                }).then(([res, metadata]) => res)
            }
        },
        isUserExist: {
            handler(ctx) {
                let { phone } = ctx.params;
                let _query = `SELECT id,auto_password,full_name,phone,is_active,is_verified
                from users
                where phone='${phone}'`
                return this.adapter.db.query(_query)
                .then(([res, metadata]) => res)
            }
        },

        getByid: {
            handler(ctx) {
                let { id } = ctx.params;
                let _query = `SELECT id, full_name, email, phone, propic, cover_pic, "location", origin, dob, gender,  about, "work", created_at, is_active, is_verified, user_original_image, auto_password, notification, searchable
                FROM public.users where id='${id}'`
                return this.adapter.db.query(_query)
                .then(([res, metadata]) => res)
            }
        },
        getConnectionUsers: {
            handler(ctx) {
                let { user_id,txt } = ctx.params;
                let searchQuery = ``;
                if(txt) {
                    searchQuery = `and U.full_name ilike '%${txt}%'`;
                }
                let offsetval = ctx.params.offset ? ctx.params.offset : 0;
                let offsetQuery =  ctx.params && ctx.params.limit ? ` OFFSET ${offsetval} LIMIT ${ctx.params.limit}` : ``;
                let _query = `select distinct(GM.user_id),U.full_name,U."location",U.propic 
                from group_maps GM 
                join users U on U.id = GM.user_id
                where GM.id in (select group_id from group_maps where user_id = ${user_id} and is_blocked is false and is_removed is false) 
                and GM.is_blocked is false and GM.is_removed is false and U.is_active is true and U.id != ${user_id} ${searchQuery} order by U.full_name asc ${offsetQuery}`;
                return this.adapter.db.query(_query)
                .then(([res, metadata]) => res)
            }   
        },
        getUsersList: {
            handler(ctx) {
                let { user_id,txt } = ctx.params;
                let searchQuery = ``;
                if(txt) {
                    searchQuery = `and U.full_name ilike '%${txt}%'`;
                }
                let offsetval = ctx.params.offset ? ctx.params.offset : 0;
                let offsetQuery =  ctx.params && ctx.params.limit ? ` OFFSET ${offsetval} LIMIT ${ctx.params.limit}` : ``;
                let _query = `select U.id as user_id,U.full_name,U."location",U.propic from users U where U.is_active is true 
                and U.searchable is true and U.id != ${user_id} ${searchQuery}
order by U.full_name asc ${offsetQuery}`;
                return this.adapter.db.query(_query)
                .then(([res, metadata]) => res)
            }   
        },
        getOverviewDB: {
            handler(ctx) {
                let _query = `select count(*) as Total_users,
                (select count(*) as User_Verified_false from users where is_verified is false and is_active is false),
                (select count(*) as User_Active_false from users where is_verified is true and is_active is false),
                (select count(*) as Total_events from events),
                (select count(*) as Total_family from "groups"),
                (select count(*) as Total_posts from posts where type ilike 'post'),
                (select count(*) as Total_announcements from posts where type ilike 'announcement')
                from users`;
                return this.adapter.db.query(_query)
                .then(([res, metadata]) => res)
            }   
        },
        getBulkusers: {
            handler(ctx) {
                let { user_ids } = ctx.params;
                let _query = `select id,full_name,propic,phone,"location",is_active from users where id in (${user_ids})`;
                return this.adapter.db.query(_query)
                .then(([res, metadata]) => res)
            }    
        }
    },
    events: {},
    methods: {},
    created() { },
    started() { },
    stopped() { },
}