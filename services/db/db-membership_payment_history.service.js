"use_strict";
//packages
const DbService = require('moleculer-db');
const SqlAdapter = require('moleculer-db-adapter-sequelize');
const Sequelize = require('sequelize');
module.exports = {
    name: "db.membership_payment_history",
    mixins: [DbService],
    settings: {},
    dependencies: [],
    cache: false,
    adapter: new SqlAdapter(process.env.SQL_DATABASE,null,null, {
        logger: false,
        pool: {
            max: 1,
            min: 0,
            acquire: 30000,
            idle: 10000,
            handleDisconnects: true
        },
        dialect: 'postgres',
        port: 5432,
        replication: {
            read: [
                { host: process.env.SQL_READ_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
            ],
            write: { host: process.env.SQL_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
        },
    }),
    model: {
        name: "membership_payment_history",
        define: {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            user_id: {
                field: "user_id",
                type: Sequelize.INTEGER,
            },
            group_id: {
                field: "group_id",
                type: Sequelize.INTEGER,
            },
            membership_payed_amount: {
                field: "membership_payed_amount",
                type: Sequelize.FLOAT,
                defaultValue:0.0
            },
            membership_id: {
                field: "membership_id",
                type: Sequelize.INTEGER,
            },
            membership_from: {
                field: "membership_from",
                type: Sequelize.INTEGER,
            },
            membership_to: {
                field: "membership_to",
                type: Sequelize.INTEGER,
            },
            membership_fees: {
                field: "membership_fees",
                type: Sequelize.INTEGER,
            },
            membership_paid_on: {
                field: "membership_paid_on",
                type: Sequelize.INTEGER,
            },
            membership_duration: {
                field: "membership_duration",
                type: Sequelize.INTEGER,
            },
            membership_payment_status: {
                field: "membership_payment_status",
                type: Sequelize.STRING,
            },
            membership_period_type: {
                field: "membership_period_type",
                type: Sequelize.STRING,
            },
            membership_payment_notes:{
                field:"membership_payment_notes",
                type:Sequelize.TEXT
            },
            membership_customer_notes:{
                field:"membership_customer_notes",
                type:Sequelize.TEXT
            },
            payment_object: {
                field: "payment_object",
                type: Sequelize.JSON
            },
            createdAt: {
                field: "created_at",
                type: 'TIMESTAMP',
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            },
            updatedAt: {
                field: "updated_at",
                type: 'TIMESTAMP',
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            },
            membership_payment_type: {
                field: "membership_payment_type",
                type: Sequelize.STRING
            },
            membership_payment_method: {
                field: "membership_payment_method",
                type: Sequelize.STRING
            },
            membership_total_payed_amount: {
                field: "membership_total_payed_amount",
                type: Sequelize.FLOAT,
                defaultValue:0.0
            },
            membership_ref_id: {
                field: "membership_ref_id",
                type: Sequelize.STRING,
            },
            membership_updated_by: {
                field: "membership_updated_by",
                type: Sequelize.STRING,
            },
        }
    },
    actions: {
        getMembershipPaymentHistory: {
            handler(ctx) {
                let { user_id, group_id, limit, offset,filter_startDate, filter_endDate } = ctx.params;
                let _filter = ["mph.membership_payed_amount is not null"];
                if(user_id) _filter.push(`mph.user_id=${user_id}`);
                if(group_id) _filter.push(`G.id=${group_id}`);
                let _where = _filter.join(" and ");
                let filterDate = '';
                if(filter_startDate && filter_startDate != '') {
                   filter_endDate = filter_endDate ? filter_endDate : filter_startDate;
                   // Adding one day to enddate for getting correct result start
                //    filter_endDate = new Date(filter_endDate);
                //    filter_endDate.setDate(filter_endDate.getDate() + 1);
                //    filter_endDate = filter_endDate.toISOString().substring(0, 10);
                   // Adding one day to enddate for getting correct result start
                   // filterDate = `and (mph.created_at::date between date '${filter_startDate}' and date '${filter_endDate}')`;
                   filterDate = `and (mph.membership_paid_on between '${filter_startDate}' and '${filter_endDate}')`;
                }
                let _query = `SELECT mph.id, ml.membership_name ,mph.group_id , G.group_name, G.logo ,mph.membership_id,
                mph.membership_paid_on as paid_on,mph.membership_fees ,mph.membership_from , 
                mph.membership_paid_on , mph.membership_payed_amount , mph.membership_to , mph.membership_total_payed_amount,
                GM.membership_payment_status as payment_status,mph.membership_payment_notes,mph.membership_customer_notes
                FROM membership_payment_histories mph
                join "groups" G on G.id = mph.group_id
                join membership_lookups ml on ml.id = mph.membership_id
                left join group_maps GM on GM.group_id = G.id and GM.user_id = ${user_id} 
                WHERE ${_where} ${filterDate} order by mph.created_at desc`;
                if(limit && offset){ _query = `${_query} offset ${offset} limit ${limit}`; }  
                return this.adapter.db.query(_query)
                .then(([res, metadata]) => res);
            }
        }, 
        getMembershipPaymentHistoryByGroup: {
            handler(ctx) {
                let { group_id, limit, offset, query, filter_startDate, filter_endDate} = ctx.params;
                let _filter = ["mph.membership_payed_amount is not null"];
                if(group_id) _filter.push(`mph.group_id=${group_id}`);
                let _where = _filter.join(" and ");
                let searchQuery = '';
                if(query && query != '') {
                    searchQuery = ` and (G.group_name ilike '%${query}%' or U.full_name ilike '%${query}%' or ml.membership_name ilike '%${query}%')`;   
                } 
                let filterDate = '';
                if(filter_startDate && filter_startDate != '') {
                   filter_endDate = filter_endDate ? filter_endDate : filter_startDate;
                   // Adding one day to enddate for getting correct result start
                //    filter_endDate = new Date(filter_endDate);
                //    filter_endDate.setDate(filter_endDate.getDate() + 1);
                //    filter_endDate = filter_endDate.toISOString().substring(0, 10);
                   // Adding one day to enddate for getting correct result start
                   //filterDate = `and (mph.created_at::date between date '${filter_startDate}' and date '${filter_endDate}')`;
                   filterDate = `and (mph.membership_paid_on between '${filter_startDate}' and '${filter_endDate}')`;
                }
                let _query = `SELECT mph.id, ml.membership_name ,mph.group_id , G.group_name, G.logo ,mph.membership_id,
                mph.membership_paid_on as paid_on,mph.membership_fees ,mph.membership_from , 
                mph.membership_paid_on , mph.membership_payed_amount , mph.membership_to , mph.membership_total_payed_amount,
                U.id as user_id,U.full_name,U.propic,GM.membership_payment_status as payment_status,mph.membership_payment_notes,
                mph.membership_customer_notes,mph.membership_ref_id as mphRefId,GM.membership_ref_id as GMRefId
                FROM membership_payment_histories mph
                join "groups" G on G.id = mph.group_id
                join membership_lookups ml on ml.id = mph.membership_id
                left join users U on U.id=mph.user_id
                left join group_maps GM on GM.group_id = G.id and GM.user_id = mph.user_id 
                WHERE ${_where} ${searchQuery} ${filterDate} order by mph.created_at desc`;
                if(limit && offset){ _query = `${_query} offset ${offset} limit ${limit}`; }  
                return this.adapter.db.query(_query)
                .then(([res, metadata]) => res);
            }
        },          
    },
    events: {},
    methods: {},
    created() { },
    started() { },
    stopped() { },
};
