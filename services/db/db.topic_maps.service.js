/* eslint-disable indent */
"use_strict";
//packages
const DbService = require('moleculer-db');
const SqlAdapter = require('moleculer-db-adapter-sequelize');
const Sequelize = require('sequelize');
//topic map table model
module.exports = {
    name: "db.topic_map",
    mixins: [DbService],
    settings: {},
    dependencies: [],
    cache: false,
    adapter: new SqlAdapter(process.env.SQL_DATABASE,null,null, {
        logger: false,
        pool: {
            max: 1,
            min: 0,
            acquire: 30000,
            idle: 10000,
            handleDisconnects: true
        },
        dialect: 'postgres',
        port: 5432,
        replication: {
            read: [
                { host: process.env.SQL_READ_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
            ],
            write: { host: process.env.SQL_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
        },
    }),
    model: {
        name: "topic_map",
        define: {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            user_id: {
                field: "user_id",
                type: Sequelize.INTEGER,
            },
            topic_id: {
                field: "topic_id",
                type: Sequelize.INTEGER,
            },
            created_by: {
                field: "created_by",
                type: Sequelize.INTEGER,
            },
            is_active: {
                field: "is_active",
                type: Sequelize.BOOLEAN,
                defaultValue: true
            },
            is_accept: {
                field: "is_accept",
                type: Sequelize.BOOLEAN,
                defaultValue: true
            },
            createdAt: {
                field: "created_at",
                type: "TIMESTAMP",
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            },
            updatedAt: {
                field: "updated_at",
                type: "TIMESTAMP",
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            }
        },
        options: {}
    },
    actions: {
        bulkCreate: {
            handler(ctx) {
                return this.adapter.db.models.topic_map.bulkCreate(ctx.params)
                .then(([res, metadata]) => res);
            }
        },
        remove_user_topic: {
            handler(ctx) {
                let { topic_id,delete_user } = ctx.params;
                let query = `delete from topic_maps where topic_id = ${topic_id} and user_id in (${delete_user})`;
                return this.adapter.db.query(query)
                    .then(([res, metadata]) => res);
            }   
        },
        get_topic_users: {
            handler(ctx) {
                let { not_in_users,topic_id } = ctx.params;
                var not_in_query = '';
                if(not_in_users) {
                    not_in_query = `and user_id not in (${not_in_users})`
                }
                let query = `select user_id from topic_maps where topic_id = ${topic_id} and is_active is true ${not_in_query}`;
                return this.adapter.db.query(query)
                    .then(([res, metadata]) => res);
            }    
        },
        get_topic_users_list: {
            handler(ctx) {
                let { topic_id,txt,user_id } = ctx.params;
                let searchQuery = ``;
                if(txt) {
                    searchQuery = `and U.full_name ilike '%${txt}%'`;
                }
                let offsetval = ctx.params.offset ? ctx.params.offset : 0;
                let offsetQuery =  ctx.params && ctx.params.limit ? ` OFFSET ${offsetval} LIMIT ${ctx.params.limit}` : ``;
                let query = `select TM.user_id,TM.is_accept,U.full_name,U."location",U.propic 
                from topic_maps TM
                join users U on U.id = TM.user_id
                where topic_id = ${topic_id} and TM.is_active is true and U.is_active is true and U.id != ${user_id} ${searchQuery} 
                order by U.full_name asc ${offsetQuery}`;
                return this.adapter.db.query(query)
                    .then(([res, metadata]) => res);
            }   
        }
    },
    events: {},
    methods: {},
    created() { },
    started() { },
    stopped() { },
};