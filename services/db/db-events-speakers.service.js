"use_strict";
//packages
const DbService = require('moleculer-db');
const SqlAdapter = require('moleculer-db-adapter-sequelize');
const Sequelize = require('sequelize');


//user table model
module.exports = {
    name: "db.event_speakers",
    // version:1,
    mixins: [DbService],
    settings: {},
    dependencies: [],
    cache: false,
    adapter: new SqlAdapter(process.env.SQL_DATABASE,null,null, {
        logger: false,
        pool: {
            max: 1,
            min: 0,
            acquire: 30000,
            idle: 10000,
            handleDisconnects: true
        },
        dialect: 'postgres',
        port: 5432,
        replication: {
            read: [
                { host: process.env.SQL_READ_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
            ],
            write: { host: process.env.SQL_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
        },
    }),
    model: {
        name: "event_speakers",
        define: {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            is_active:{
                field:"is_active",
                type:Sequelize.BOOLEAN,
                defaultValue:true
            },
            event_id: {
                field: "event_id",
                type: Sequelize.INTEGER,
                // references:{
                //     model:"events",
                //     key:"id"
                // }
            },
            name: {
                field: "name",
                type: Sequelize.STRING
            },
            description: {
                field: "description",
                type: Sequelize.TEXT
            },
            speaker_image:{
                field: "speaker_image",
                type: Sequelize.STRING
            },
            topic:{
                field: "topic",
                type: Sequelize.STRING
            },
            createdAt: {
                field: "created_at",
                type: 'TIMESTAMP',
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            },
            updatedAt: {
                field: "updated_at",
                type: 'TIMESTAMP',
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            },
        },
        options: {}
    },
    actions: {},
    events: {},
    methods: {},
    created() { },
    started() { },
    stopped() { },
};
