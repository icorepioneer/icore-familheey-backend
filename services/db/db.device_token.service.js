/* eslint-disable indent */
"use_strict";
//packages
const DbService = require('moleculer-db');
const SqlAdapter = require('moleculer-db-adapter-sequelize');
const Sequelize = require('sequelize');

//user table model
module.exports = {
    name: "db.device_token",
    mixins: [DbService],
    settings: {},
    dependencies: [],
    cache: false,
    adapter: new SqlAdapter(process.env.SQL_DATABASE,null,null, {
        logger: false,
        pool: {
            max: 1,
            min: 0,
            acquire: 30000,
            idle: 10000,
            handleDisconnects: true
        },
        dialect: 'postgres',
        port: 5432,
        replication: {
            read: [
                { host: process.env.SQL_READ_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
            ],
            write: { host: process.env.SQL_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
        },
    }),
    model: {
        name: "device_token",
        define: {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            user_id: {
                field: "user_id",
                type: Sequelize.INTEGER,
            },
            device_token: {
                field: "device_token",
                type: Sequelize.TEXT,
            },
            device_type: {
                field: "device_type",
                type: Sequelize.STRING,
            },
            device_id: {
                field: "device_id",
                type: Sequelize.STRING,
            },
            is_active: {
                field: "is_active",
                type: Sequelize.BOOLEAN,
                defaultValue: true
            }
        },
        options: {}
    },
    actions: {
        getTokens: {
            handler(ctx){
                let {user_id,is_active} = ctx.params;
                //user_id is added to the query by Sarath on 02/03/2021
                var _query = `SELECT DISTINCT ON (device_id) device_id, device_token,device_type, id,user_id
                FROM device_tokens 
                WHERE user_id = ${user_id} AND is_active = ${is_active}`
                return this.adapter.db.query(_query).then(([res, metadata]) => res);
            }
        },
    },
    events: {},
    methods: {},
    created() { },
    started() { },
    stopped() { },
};