"use_strict";
//packages
const DbService = require('moleculer-db');
const SqlAdapter = require('moleculer-db-adapter-sequelize');
const Sequelize = require('sequelize');
//folder table model
module.exports = {
    name: "db.user_activities",
    mixins: [DbService],
    settings: {},
    dependencies: [],
    cache: false,
    adapter: new SqlAdapter(process.env.SQL_DATABASE,null,null, {
        logger: false,
        pool: {
            max: 1,
            min: 0,
            acquire: 30000,
            idle: 10000,
            handleDisconnects: true
        },
        dialect: 'postgres',
        port: 5432,
        replication: {
            read: [
                { host: process.env.SQL_READ_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
            ],
            write: { host: process.env.SQL_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
        },
    }),
    model: {
        name: "user_activities",
        define: {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            user_id: {
                field: "user_id",
                type: Sequelize.INTEGER,
            },
            type:{
                field: "type",
                type: Sequelize.STRING
            },
            type_id:{
                field:"type_id",
                type:Sequelize.INTEGER,
            },
            sub_type:{
                field: "sub_type",
                type: Sequelize.STRING
            },
            sub_type_id:{
                field:"sub_type_id",
                type:Sequelize.INTEGER,
            },
            activity:{
                field: "activity",
                type: Sequelize.STRING
            },
            createdAt: {
                field: "created_at",
                type: 'TIMESTAMP',
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            },
            updatedAt: {
                field: "updated_at",
                type: 'TIMESTAMP',
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            },
        },
        options: {}
    },
    actions: {
        bulkCreate:{
            handler(ctx){
                let data = ctx.params;
                return this.adapter.db.models.user_activities.bulkCreate(data).then(res=>{
                    return res;
                })
            }
        }
    },
    events: {},
    methods: {},
    created() { },
    started() { },
    stopped() { },
}