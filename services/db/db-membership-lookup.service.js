"use_strict";
//packages
const DbService = require('moleculer-db');
const SqlAdapter = require('moleculer-db-adapter-sequelize');
const Sequelize = require('sequelize');

//folder table model
module.exports = {
    name: "db.membership_lookup",
    mixins: [DbService],
    settings: {},
    dependencies: [],
    cache: false,
    adapter: new SqlAdapter(process.env.SQL_DATABASE,null,null, {
        logger: false,
        pool: {
            max: 1,
            min: 0,
            acquire: 30000,
            idle: 10000,
            handleDisconnects: true
        },
        dialect: 'postgres',
        port: 5432,
        replication: {
            read: [
                { host: process.env.SQL_READ_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
            ],
            write: { host: process.env.SQL_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
        },
    }),
    model: {
        name: "membership_lookup",
        define: {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            membership_name: {
                field: "membership_name",
                type: Sequelize.STRING
            },
            group_id: {
                field: "group_id",
                type: Sequelize.INTEGER,
            },
            created_by: {
                field: "created_by",
                type: Sequelize.INTEGER,
            },
            membership_period: {
                field: "membership_period",
                type: Sequelize.INTEGER,
            },
            membership_period_type_id: {
                field: "membership_period_type_id",
                type: Sequelize.INTEGER,
            },
            membership_fees: {
                field: "membership_fees",
                type: Sequelize.INTEGER,
            },
            membership_currency: {
                field: "membership_currency",
                type: Sequelize.STRING
            },
            is_active: {
                field: "is_active",
                type: Sequelize.BOOLEAN,
                defaultValue:true
            },
            createdAt: {
                field: "created_at",
                type: 'TIMESTAMP',
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            },
            updatedAt: {
                field: "updated_at",
                type: 'TIMESTAMP',
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            },
        },
        options: {}
    },
    actions: {
        group_by_membership: {
            handler(ctx) {
                let {group_id} = ctx.params;
                let filterQuery = '';
                if(ctx.params.filter == "current") {
                    filterQuery = `and GM.membership_to >= date_part('epoch', now()) and GM.is_blocked is false 
                and GM.is_removed is false`;   
                } else if(ctx.params.filter == "overdue") {
                    filterQuery = `and GM.membership_to < date_part('epoch', now()) and GM.is_blocked is false 
                and GM.is_removed is false`; 
                }
                let _query = `select distinct(ML.id) as membershipid,ML.membership_name,ML.is_active,
                (select count(*) from group_maps GM1 where GM1.membership_id = ML.id and GM1.group_id = ${group_id}
                and is_blocked is false and is_removed is false) as member_count
                from membership_lookups ML
                left join group_maps GM on GM.membership_id = ML.id
                where ML.group_id = ${group_id} ${filterQuery} order by ML.membership_name desc`;
                return this.adapter.db.query(_query)
                .then(([res, metadata]) => res);
            }
        },
        group_membership_counts: {
            handler(ctx) {
                let {group_id} = ctx.params;
                let _query = `select count(*) as all_count,
                (select count(*) from group_maps where membership_to < date_part('epoch', now()) and group_id = ${group_id} and 
                is_blocked is false and is_removed is false) as expaired_count,
                (select count(*) from group_maps where membership_to >= date_part('epoch', now()) and group_id = ${group_id} 
                and is_blocked is false and is_removed is false) as active_count,
                (select sum(membership_fees - membership_total_payed_amount) from group_maps where membership_to < date_part('epoch', now()) 
                and group_id = ${group_id} and membership_payment_status = 'Pending' and is_blocked is false and is_removed is false
                ) as total_due_amount,
                (select default_membership from groups where id = ${group_id}) as default_membership_id
                from group_maps GM 
                where GM.group_id = ${group_id} and GM.is_blocked is false and GM.is_removed is false`;
                return this.adapter.db.query(_query)
                .then(([res, metadata]) => res);
            }   
        }
    },
    events: {},
    methods: {},
    created() { },
    started() { },
    stopped() { },
};