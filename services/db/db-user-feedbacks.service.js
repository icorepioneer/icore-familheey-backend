"use_strict";
//packages
const DbService = require('moleculer-db');
const SqlAdapter = require('moleculer-db-adapter-sequelize');
const Sequelize = require('sequelize');


//user table model
module.exports = {
    name: "db.user_feedbacks",
    // version:1,
    mixins: [DbService],
    settings: {},
    dependencies: [],
    cache: false,
    adapter: new SqlAdapter(process.env.SQL_DATABASE,null,null, {
        logger: false,
        pool: {
            max: 1,
            min: 0,
            acquire: 30000,
            idle: 10000,
            handleDisconnects: true
        },
        dialect: 'postgres',
        port: 5432,
        replication: {
            read: [
                { host: process.env.SQL_READ_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
            ],
            write: { host: process.env.SQL_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
        },
    }),
    model: {
        name: "user_feedbacks",
        define: {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            is_active:{
                field:"is_active",
                type:Sequelize.BOOLEAN,
                defaultValue:true
            },
            description: {
                field: "description",
                type: Sequelize.TEXT
            },
            image:{
                field: "image",
                type: Sequelize.STRING
            },
            type:{
                field: "type",
                type: Sequelize.STRING
            },
            user: {
                field: "user",
                type: Sequelize.INTEGER,
                references: {
                    model: "users",
                    key: 'id'
                }
            },
            os:{
                field: "os",
                type: Sequelize.STRING   
            },
            device:{
                field: "device",
                type: Sequelize.STRING   
            },
            version:{
                field: "version",
                type: Sequelize.STRING   
            },
            createdAt: {
                field: "created_at",
                type: 'TIMESTAMP',
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            },
            updatedAt: {
                field: "updated_at",
                type: 'TIMESTAMP',
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            },
        },
        options: {}
    },
    actions: {},
    events: {},
    methods: {},
    created() { },
    started() { },
    stopped() { },
}
