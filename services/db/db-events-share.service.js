"use_strict";
//packages
const DbService = require('moleculer-db');
const SqlAdapter = require('moleculer-db-adapter-sequelize');
const Sequelize = require('sequelize');
//folder table model
module.exports = {
    name: "db.events_share",
    mixins: [DbService],
    settings: {},
    dependencies: [],
    cache: false,
    adapter: new SqlAdapter(process.env.SQL_DATABASE,null,null, {
        logger: false,
        pool: {
            max: 1,
            min: 0,
            acquire: 30000,
            idle: 10000,
            handleDisconnects: true
        },
        dialect: 'postgres',
        port: 5432,
        replication: {
            read: [
                { host: process.env.SQL_READ_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
            ],
            write: { host: process.env.SQL_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
        },
    }),
    model: {
        name: "events_share",
        define: {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            event_id: {
                field: "event_id",
                type: Sequelize.INTEGER,
                // references: {
                //     model: "events",
                //     key: 'id'
                // }
            },
            group_id: {
                field: "group_id",
                type: Sequelize.INTEGER,
                // references: {
                //     model: "groups",
                //     key: 'id'
                // }
            },
            user_id: {
                field: "user_id",
                type: Sequelize.INTEGER,
                // references: {
                //     model: "users",
                //     key: 'id'
                // }
            },
            shared_by: {
                field: "shared_by",
                type: Sequelize.INTEGER,
                // references: {
                //     model: "users",
                //     key: 'id'
                // }
            },
            type: {
                field: "type",
                type: Sequelize.STRING
            },
            duplicate: {
                field: "duplicate",
                type: Sequelize.BOOLEAN,
                defaultValue: false
            },
            createdAt: {
                field: "created_at",
                type: 'TIMESTAMP',
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            },
            updatedAt: {
                field: "updated_at",
                type: 'TIMESTAMP',
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            },
        },
        options: {}
    },
    actions: {
        /**
         * get events shared to a group by groupId
         */
        sharedToGroup:{
            params:{
                group_id:"string"
            },
            handler(ctx){
                let { group_id, type, query } = ctx.params;
                let _filter = [];
                //_filter.push(`es.duplicate is false`)
                _filter.push(`e.is_active is true`);
                if(group_id) _filter.push(`es.group_id='${group_id}'`);
                if(query) {
                    _filter.push(`(e.event_name ilike '%${ctx.params.query}%' or e.location ilike '%${ctx.params.query}%')`);   
                }
                if(type && type !='-1') _filter.push(`e.is_public is ${type}`);
                let _where = _filter.join(' and ');
                let _query = ` select distinct(es.event_id, es.group_id) ,es.event_id,es.shared_by,e.event_name,e.event_page,
                e.rsvp,e.event_image,e.event_original_image,extract(epoch from e.created_at) as created_at_,e.category,e.event_type,
                e.is_public as public_event,e.ticket_type,e.is_active,e.is_cancel,e.from_date,e.to_date,e."location",
                e.is_active as event_active_status,e.meeting_link,e.meeting_dial_number,e.meeting_pin,e.meeting_logo,
                (select count(*) from event_rsvps where attendee_type = 'going' and event_id = e.id) as going_count,
                (select full_name from users where id = e.created_by) as created_by_name,(select propic from users where id = e.created_by) as propic,e.created_by,
                ( select u.full_name from event_rsvps er join users u on er.attendee_id = u.id where attendee_type = 'going' and event_id = e.id limit 1) as first_person_going ,
                               (select count(*) from event_rsvps where event_id = e.id and attendee_type= 'interested' and is_active is true) as interested_count,
                               (
                           select count(*) from 
                               (
                               select user_id from group_maps where user_id
                                   in
                                   (
                                   select attendee_id from event_rsvps where event_id = e.id and attendee_type = 'going'
                                   ) 
                                   and
                                   group_id='${group_id}'
                                  group by 
                                  user_id
                              )
                                  as
                                  list) 
                              as known_count
                from events_shares es
                join events e
                on e.id = es.event_id
                where ${_where} and is_active is true order  by created_at_ desc`;
                return this.adapter.db.query(_query)
                .then(([res, metadata]) => res);
            }
        },
        event_share_list:{
            params:{
                event_id:"string",
                user_id:"string"
            },
            handler(ctx){
                let { event_id,user_id,txt } = ctx.params;
                let searchQuery = txt && txt!= ''?`and u.full_name ilike '%${txt}%' or u."location" ilike '%${txt}%'`:'';
                let query = `select distinct(u.id),u.full_name,u.propic,u."location",extract(epoch from es.created_at) as created_at_
                from events_shares es join users u on u.id = es.shared_by where es.event_id = ${event_id} and es.user_id = ${user_id} ${searchQuery} and es.duplicate is false`;
                return this.adapter.db.query(query)
                .then(([res, metadata]) => res);
            }
        },
        bulkDeleteEventShareByFamily:{
            handler(ctx){
                let { group_id } = ctx.params;
                return this.adapter.db.models.events_share.destroy({
                    where:{
                        group_id:group_id
                    }
                }).then(res=>{
                    return res;
                });
            }
        }

    },
    events: {},
    methods: {},
    created() { },
    started() { },
    stopped() { },
};
