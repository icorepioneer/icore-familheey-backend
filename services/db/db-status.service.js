/* eslint-disable indent */
"use_strict";
//packages
const DbService = require('moleculer-db');
const SqlAdapter = require('moleculer-db-adapter-sequelize');
const Sequelize = require('sequelize');
const moment = require('moment')
//user table model
module.exports = {
    name: "db.status",
    mixins: [DbService],
    settings: {},
    dependencies: [],
    cache: false,
    adapter: new SqlAdapter(process.env.SQL_DATABASE,null,null, {
        logger: false,
        pool: {
            max: 1,
            min: 0,
            acquire: 30000,
            idle: 10000,
            handleDisconnects: true
        },
        dialect: 'postgres',
        port: 5432,
        replication: {
            read: [
                { host: process.env.SQL_READ_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
            ],
            write: { host: process.env.SQL_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
        },
    }),
    model: {
        name: "status",
        define: {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            user_id: {
                field: "user_id",
                type: Sequelize.INTEGER,
                references: {
                    model: "users",
                    key: "id"
                }
            },
            group_id: {
                field: "group_id",
                type: Sequelize.ARRAY(Sequelize.INTEGER)
            },
            viewed_by: {
                field: "viewed_by",
                type: Sequelize.ARRAY(Sequelize.INTEGER)
            },
            expire_time: {
                field: "expire_time",
                type: Sequelize.INTEGER
            },
            status_file: {
                field: "status_file",
                type: Sequelize.STRING
            },
            is_remove: {
                field: "is_remove",
                type: Sequelize.BOOLEAN,
                defaultValue: false
            },
            file_type: {
                field: "file_type",
                type: Sequelize.STRING
            },
            createdAt: {
                field: "created_at",
                type: "TIMESTAMP",
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            },
            updatedAt: {
                field: "updated_at",
                type: "TIMESTAMP",
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            }
        },
        options: {}
    },
    actions: {
        fetchStatus: {
            params: {},
            handler(ctx) {
                let _where;
                let { group_id, user_id } = ctx.params;
                let currentTime = moment().unix();
                if (group_id) {
                    _where = `where '${group_id}' = ANY(st.group_id) and st.is_remove=false and st.expire_time >= ${currentTime}`;
                } else {
                    _where = `where us.id=${user_id} and st.is_remove=false and st.expire_time >= ${currentTime}`;
                }
                let _query = `select * from statuses st join users us on us.id = st.user_id ${_where}`;
                return this.adapter.db.query(_query).then(([res, metadata]) => res);
            }
        },
        /**
         * updates status related to a user
         */
        updateStatus: {
            params: {},
            handler(ctx) {
                let { id, viewed_by } = ctx.params;
                let _query = `UPDATE statuses SET viewed_by = array_append( viewed_by,${viewed_by} ) where id=${id} RETURNING *`;
                return this.adapter.db.query(_query).then(([res, metadata]) => res);
            }
        }
    },
    events: {},
    methods: {},
    created() { },
    started() { },
    stopped() { },
};
