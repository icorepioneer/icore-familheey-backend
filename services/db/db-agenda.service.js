"uses strict";
//packages
const DbService = require('moleculer-db');
const SqlAdapter = require('moleculer-db-adapter-sequelize');
const Sequelize = require('sequelize');
module.exports = {
    name:"db.agenda",
    mixins: [DbService],
    settings:{},
    dependencies: [],
    cache: false,
    adapter: new SqlAdapter(process.env.SQL_DATABASE,null,null, {
        logger: false,
        pool: {
            max: 1,
            min: 0,
            acquire: 30000,
            idle: 10000,
            handleDisconnects: true
        },
        dialect: 'postgres',
        port: 5432,
        replication: {
            read: [
                { host: process.env.SQL_READ_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
            ],
            write: { host: process.env.SQL_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
        },
    }),
    model:{
        name:"agenda",
        define:{
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            event_id:{
                field:"event_id",
                type:Sequelize.INTEGER,
                // references:{
                //     model:"events",
                //     key:"id"
                // }
            },
            user_id:{
                field:"user_id",
                type:Sequelize.INTEGER,
                // references:{
                //     model:"users",
                //     key:"id"
                // }
            },
            title:{
                field:"title",
                type:Sequelize.STRING
            },
            agenda_pic: {
                field: "agenda_pic",
                type: Sequelize.STRING
            },
            description:{
                field:"description",
                type:Sequelize.TEXT
            },
            start_date:{
                field:"start_date",
                type:Sequelize.DATE
            },
            end_date:{
                field:"end_date",
                type:Sequelize.DATE
            },
            is_active:{
                field:"is_active",
                type:Sequelize.BOOLEAN,
                defaultValue:true
            },
            createdAt: {
                field: "created_at",
                type: 'TIMESTAMP',
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            },
            updatedAt: {
                field: "updated_at",
                type: 'TIMESTAMP',
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            }
        },
        options: {}
    },
    actions:{
        listAgenda:{
            handler(ctx){
                let {event_id} = ctx.params;
                let query = `select id,title,description,agenda_pic,event_id,user_id,start_date::timestamp::date as event_date,
                            start_date::time as event_start_time,
                            end_date::time as event_end_time,
                            created_at::time as event_created_time,
                            start_date,end_date
                            from agendas where event_id = ${event_id} and is_active=true order by start_date`;
                return this.adapter.db.query(query).then(([res, metadata]) => res);
            }
        }
    },
    events:{},
    methods:{},
    created(){},
    started(){},
    stopped(){}
};
