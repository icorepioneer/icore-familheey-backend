"use_strict";
//packages
const DbService = require('moleculer-db');
const SqlAdapter = require('moleculer-db-adapter-sequelize');
const Sequelize = require('sequelize');


//user table model
module.exports = {
    name: "db.familyLinks",
    // version:1,
    mixins: [DbService],
    settings: {},
    dependencies: [],
    cache: false,
    adapter: new SqlAdapter(process.env.SQL_DATABASE,null,null, {
        logger: false,
        pool: {
            max: 1,
            min: 0,
            acquire: 30000,
            idle: 10000,
            handleDisconnects: true
        },
        dialect: 'postgres',
        port: 5432,
        replication: {
            read: [
                { host: process.env.SQL_READ_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
            ],
            write: { host: process.env.SQL_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
        },
    }),
    model: {
        name: "family_links",
        define: {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            from_group: {
                field: "from_group",
                type: Sequelize.INTEGER
            },
            to_group: {
                field: "to_group",
                type: Sequelize.INTEGER
            },
            requested_by: {
                field: "requested_by",
                type: Sequelize.INTEGER
            },
            status: {
                field: 'status',
                type: Sequelize.ENUM,
                values: ["pending", "accepted", "rejected", "unlinked"],
                defaultValue: "pending"
            },
            createdAt: {
                field: "created_at",
                type: 'TIMESTAMP',
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            },
            updatedAt: {
                field: "updated_at",
                type: 'TIMESTAMP',
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            },
        },
        options: {}
    },
    actions: {
        /**
         * list all the linked family for a group
         */
        listLinkedFamily:{
            params:{
            group_id:"string"
            },
            handler(ctx){
            let { group_id, offset, limit, query } = ctx.params;
            let offsetval = offset ? offset : 0;
            let limitval = limit ? limit : 50;
            
            let _filter = [];
            _filter.push(`(fl.from_group = ${group_id} or fl.to_group = ${group_id})`);
            _filter.push(`gp.id != ${group_id}`);
            _filter.push(`(status='accepted')`);
            if(query){ _filter.push(`(gp.group_name ilike '%${query}%' or gp.base_region ilike '%${query}%' )`); }
            let _where = _filter.join(' and ');

            let _query = `select u.full_name as requested_by_name, fl.id, gp.group_name, gp.logo,gp.base_region,gp.group_category,fl.status,
            fl.to_group,fl.from_group,
            (SELECT count(*) FROM group_maps gm where gm.group_id = gp.id and gm.is_blocked = false and gm.is_removed = false ) as member_count
            from family_links fl
            join groups gp
            on (fl.from_group = gp.id or fl.to_group = gp.id)
            join users u 
            on fl.requested_by = u.id
            where ${_where}
            OFFSET ${offsetval} LIMIT ${limitval}`;
            return this.adapter.db.query(_query)
            .then(([res, metadata]) => res);
            }
        },
        /**
         * admin side list all request for 
         * link family
         */
        adminListLinkRequest:{
            params:{},
            handler(ctx){
                let { group_id } = ctx.params;
                let query = `select fl.id,fl.requested_by,u.full_name,u.propic,fl.status, g.id as group_id, g.group_name, g.base_region, g.logo
                from family_links fl
                join "groups" g
                on fl.from_group=g.id
                join users u 
                on u.id= fl.requested_by
                where fl.to_group=${group_id} and fl.status='pending'`;
                return this.adapter.db.query(query)
                .then(([res, metadata]) => res);
            }
        },
        linkData:{
            params:{},
            handler(ctx){
                let { id } = ctx.params;
                let query = `select from_group,to_group,requested_by as user_id,
                (select group_name as from_group_name from "groups" g1 where g1.id=fl1.from_group),
                (select group_name as to_group_name from "groups" g2 where g2.id=fl1.to_group)
                from 
                family_links fl1 
                where id=${id}`;
                return this.adapter.db.query(query)
                .then(([res, metadata]) => res);
            }
        },
    },
    events: {},
    methods: {},
    created() { },
    started() { },
    stopped() { },
};
