"use_strict";
//packages
const DbService = require('moleculer-db');
const SqlAdapter = require('moleculer-db-adapter-sequelize');
const Sequelize = require('sequelize');
const moment = require('moment');
//folder table model
module.exports = {
    name: "db.events",
    mixins: [DbService],
    settings: {},
    dependencies: [],
    cache: false,
    adapter: new SqlAdapter(process.env.SQL_DATABASE,null,null, {
        logger: false,
        pool: {
            max: 1,
            min: 0,
            acquire: 30000,
            idle: 10000,
            handleDisconnects: true
        },
        dialect: 'postgres',
        port: 5432,
        replication: {
            read: [
                { host: process.env.SQL_READ_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
            ],
            write: { host: process.env.SQL_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
        },
    }),
    model: {
        name: "events",
        define: {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            event_type: {
                field: "event_type",
                type: Sequelize.STRING
            },
            lat: {
                field: "lat",
                type: Sequelize.DOUBLE
            },
            long: {
                field: "long",
                type: Sequelize.DOUBLE
            },
            is_active: { //deleted
                field: "is_active",
                type: Sequelize.BOOLEAN,
                defaultValue: true
            },
            is_cancel: { //cancelled
                field: 'is_cancel',
                type: Sequelize.BOOLEAN,
                defaultValue: false
            },
            created_by: {
                field: "created_by",
                type: Sequelize.INTEGER,
                // references:{
                //     model:"users",
                //     key:"id"
                // }
            },
            event_page: {
                field: "event_page",
                type: Sequelize.STRING
            },
            is_public: {
                field: "is_public",
                type: Sequelize.BOOLEAN,
                defaultValue: false
            },
            event_name: {
                field: "event_name",
                type: Sequelize.STRING
            },
            event_host: {
                field: "event_host",
                type: Sequelize.STRING
            },
            location: {
                field: "location",
                type: Sequelize.STRING
            },
            from_date: {
                field: "from_date",
                type: Sequelize.INTEGER
            },
            to_date: {
                field: "to_date",
                type: Sequelize.INTEGER
            },
            description: {
                field: "description",
                type: Sequelize.TEXT
            },
            is_agenda: {
                field: 'is_agenda',
                type: Sequelize.BOOLEAN,
                defaultValue: true
            },
            agenda: {
                field: 'agenda',
                type: Sequelize.STRING
            },
            rsvp: {
                field: 'rsvp',
                type: Sequelize.BOOLEAN,
                defaultValue: true
            },
            event_image: {
                field: 'event_image',
                type: Sequelize.STRING
            },
            event_original_image: {
                field: 'event_original_image',
                type: Sequelize.STRING
            },
            is_item: {
                field: 'is_item',
                type: Sequelize.BOOLEAN,
                defaultValue: true
            },
            is_document: {
                field: 'is_document',
                type: Sequelize.BOOLEAN,
                defaultValue: true
            },
            is_album: {
                field: 'is_album',
                type: Sequelize.BOOLEAN,
                defaultValue: true
            },
            ticket_type: {
                field: 'ticket_type',
                type: Sequelize.STRING,
                defaultValue: "free"
            },
            is_sharable: {
                field: 'is_sharable',
                type: Sequelize.BOOLEAN,
                defaultValue: true
            },
            category: {
                field: 'category',
                type: Sequelize.STRING
            },
            meeting_link: {
                field: 'meeting_link',
                type: Sequelize.STRING
            },
            meeting_dial_number: {
                field: 'meeting_dial_number',
                type: Sequelize.STRING
            },
            meeting_pin: {
                field: 'meeting_pin',
                type: Sequelize.STRING
            },
            meeting_logo: {
                field: 'meeting_logo',
                type: Sequelize.STRING
            },
            allow_others: {
                field: 'allow_others',
                type: Sequelize.BOOLEAN,
                defaultValue: false
            },
            createdAt: {
                field: "created_at",
                type: 'TIMESTAMP',
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            },
            updatedAt: {
                field: "updated_at",
                type: 'TIMESTAMP',
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            },
            firebase_link:{
                field: "firebase_link",  
                type: Sequelize.STRING
            },
        },
        options: {}
    },
    actions: {
        searchEvent: {
            handler(ctx) {
                let searchtxt = ctx.params.txt ? ctx.params.txt : '';
                let searchuser = ctx.params.userid;
                let offsetval = ctx.params.offset ? ctx.params.offset : 0;
                let limitval = ctx.params.limit ? ctx.params.limit : 20;
                let defaultquery = '';
                if(ctx.params.txt) {
                    defaultquery = `or (E.id in (select event_id from events_shares 
                where user_id = ${searchuser} group by event_id)) or 
                (E.id in (select event_id from events_invitations where user_id = ${searchuser} group by event_id))`;
                } 
                
                let _Query = `select DISTINCT ON (E.id) E.id,event_type,event_name,event_host,location,E.meeting_link,E.meeting_dial_number,E.meeting_pin,E.meeting_logo,
                is_sharable,EU.created_user,EU.id as user_id,from_date,to_date,event_image,EU.created_user as full_name,extract(epoch from E.created_at)::integer as created_at_,
                ( select u.full_name from event_rsvps er join users u on er.attendee_id = u.id where attendee_type = 'going' and event_id = E.id limit 1) as first_person_going ,
                (select count(*) from event_rsvps where attendee_type = 'going' and event_id = E.id) as going_count,
                (select count(*) from event_rsvps where event_id = E.id and attendee_type= 'interested' and is_active is true) as interested_count,
                EU.propic,(select location from users where id = ${searchuser}) as sortLocation
                from events as E 
                left join events_shares es on E.id = es.event_id
                LEFT JOIN (select full_name as created_user,id,propic from users) as EU on EU.id = E.created_by
                where ((E.is_public = true) ${defaultquery})
                and es.duplicate is false and  is_active = true and (E.event_name ilike '%${searchtxt}%' or E.location ilike '%${searchtxt}%')
                order by E.id asc OFFSET ${offsetval} LIMIT ${limitval}`;
                return this.adapter.db.query(_Query)
                    .then(([res, metadata]) => res);
            }
        },
        shared_events: {
            async handler(ctx) {
                let { user_id, from_date, to_date, filter, txt } = ctx.params;
                let dateQuery = '';
                let todayTimestamp = moment().unix();
                if (from_date && to_date) {
                    dateQuery = from_date ? `and e.from_date between ${from_date} and ${to_date}` : '';
                }
                else if (filter) {
                    let { start_date, end_date } = await ctx.broker.call("common_methods.date_filter", { filter: filter });
                    dateQuery = `and e.from_date between ${start_date} and ${end_date}`;
                }
                // let dateQuery = from_date?`and ${date} between e.from_date and e.to_date`:'';
                let searchQuery = txt && txt != '' ? `and (e.event_name ilike '%${txt}%' or e."location" ilike '%${txt}%')` : '';
                let queryString = `select distinct(u.id) as user_id,u.full_name,u.propic,e.id as event_id,e.rsvp,e.event_name,e.event_image,e.event_original_image,extract(epoch from e.created_at) as created_at_,e.category,e.event_type,e.is_public as 
                public_event,e.event_page,e.ticket_type,e.from_date,e.to_date,e."location",es.created_at,e.is_sharable,e.meeting_link,e.meeting_dial_number,e.meeting_pin,e.meeting_logo,
                (select count(*) from event_rsvps where attendee_type = 'going' and event_id = e.id) as going_count ,
                ( select u.full_name from event_rsvps er join users u on er.attendee_id = u.id where attendee_type = 'going' and event_id = e.id limit 1) as first_person_going ,
                (select count(*) from event_rsvps where attendee_type = 'interested' and event_id = e.id) as interested_count,
                (
                select count(*) from 
                    (
                    select user_id from group_maps where user_id
                        in
                        (
                        select attendee_id from event_rsvps where event_id = e.id and attendee_type = 'going'
                        ) 
                        and
                        group_id 
                       in 
                       (
                       select group_id from group_maps where user_id = ${user_id} and is_removed is false
                       )
                       group by 
                       user_id
                   )
                       as
                       list) 
                   as known_going_count,
               (select array_to_string(array_agg(distinct "full_name"),' , ') as shared_by from events_shares es join users u on u.id = es.shared_by where es.user_id = ${user_id} and es.event_id = e.id) as shared_by
                from
                events_shares es join events e on e.id = es.event_id join users u on u.id = e.created_by where es.user_id = ${user_id} and es.duplicate is false and e.is_active is true and e.is_cancel=false  and u.is_active is true and u.is_verified is true
                and e.to_date >= ${todayTimestamp}
                ${dateQuery} ${searchQuery} order by e.id desc`;
                return this.adapter.db.query(queryString)
                    .then(([res, metadata]) => res);
            }
        },
        logined_user: {
            async handler(ctx) {
                let { user_id, from_date, to_date, txt, filter } = ctx.params;
                let dateQuery = '';
                let todayTimestamp = moment().unix();
                if (from_date && to_date) {
                    dateQuery = from_date ? `and e.from_date between ${from_date} and ${to_date}` : '';
                }
                else if (filter) {
                    let { start_date, end_date } = await ctx.broker.call("common_methods.date_filter", { filter: filter });
                    dateQuery = `and e.from_date between ${start_date} and ${end_date}`;
                }
                // let dateQuery = date?`and ${date} between e.from_date and e.to_date`:'';
                let searchQuery = txt && txt != '' ? `and (e.event_name ilike '%${txt}%' or e."location" ilike '%${txt}%')` : '';
                let queryString = ` 
                select distinct(select id from users where id = e.created_by) as user_id,
                (select full_name from users where id = e.created_by),(select propic from users where id = e.created_by) as propic,e.id as event_id, e.event_name,e.event_page,e.rsvp,e.event_image,e.event_original_image,extract(epoch from e.created_at) as created_at_,e.category,e.event_type,e.is_public
                as
                public_event
                ,e.ticket_type,e.from_date,e.to_date,e."location",e.is_sharable,e.meeting_link,e.meeting_dial_number,e.meeting_pin,e.meeting_logo,
                (select count(*) from event_rsvps where attendee_type = 'going' and event_id = e.id) as going_count ,
                ( select u.full_name from event_rsvps er join users u on er.attendee_id = u.id where attendee_type = 'going' and event_id = e.id limit 1) as first_person_going ,
                (
                select count(*) from 
                    (
                    select user_id from group_maps where user_id
                        in
                        (
                        select attendee_id from event_rsvps where event_id = e.id and attendee_type = 'going'
                        ) 
                        and
                        group_id 
                       in 
                       (
                       select group_id from group_maps where user_id = ${user_id} and is_removed is false
                       )
                       group by 
                       user_id
                   )
                       as
                       list) 
                   as known_count,
                   (select count(*) from event_rsvps where event_id = e.id and attendee_type= 'interested' and is_active is true) as interested_count,
               (select array_to_string(array_agg(distinct "full_name"),' , ') as shared_by from events_shares es join users u on u.id = es.shared_by where es.user_id = ${user_id} and es.event_id = e.id) as shared_by
                from
                events e 
                join
                users u 
                on
                u."location" = e."location" 
                where
                e.is_public is true and e.is_active is true and e.is_cancel=false and
                u.id = ${user_id}  and e.id not in (
                    select es.event_id
                    from
                    events_shares es join users u on u.id = es.user_id where es.user_id = ${user_id} group by es.event_id)
                    and e.to_date >= ${todayTimestamp} and e.created_by != ${user_id}
               ${dateQuery} ${searchQuery} group by e.id,u.full_name,u.propic,u.id order by e.id desc`;
                return this.adapter.db.query(queryString)
                    .then(([res, metadata]) => res);
            }
        },
        public_events: {
            async handler(ctx) {
                let { user_id, from_date, to_date, txt, filter } = ctx.params;
                let dateQuery = '';
                let todayTimestamp = moment().unix();
                if (from_date && to_date) {
                    dateQuery = from_date ? `and e.from_date between ${from_date} and ${to_date}` : '';
                }
                else if (filter) {
                    let { start_date, end_date } = await ctx.broker.call("common_methods.date_filter", { filter: filter });
                    dateQuery = `and e.from_date between ${start_date} and ${end_date}`;
                }
                //let dateQuery = date?`and ${date} between e.from_date and e.to_date`:'';
                let searchQuery = txt && txt != '' ? `and (e.event_name ilike '%${txt}%' or e."location" ilike '%${txt}%')` : '';
                let queryString = `
                select distinct(u.id) as user_id,
                u.full_name,u.propic,e.id as event_id,e.event_name,e.event_page,e.rsvp,e.event_image,e.event_original_image,extract(epoch from e.created_at) as created_at_,e.category,e.event_type,e.is_public
                as public_event
                ,e.ticket_type,e.from_date,e.to_date,e."location",e.is_sharable,e.meeting_link,e.meeting_dial_number,e.meeting_pin,e.meeting_logo,
            (select count(*) from event_rsvps where attendee_type = 'going' and event_id = e.id) as going_count ,
            ( select u.full_name from event_rsvps er join users u on er.attendee_id = u.id where attendee_type = 'going' and event_id = e.id limit 1) as first_person_going ,
            (
            select count(*) from 
                (
                select user_id from group_maps where user_id
                    in
                    (
                    select attendee_id from event_rsvps where event_id = e.id and attendee_type = 'going'
                    ) 
                    and
                    group_id 
                   in 
                   (
                   select group_id from group_maps where user_id = ${user_id} and is_removed is false
                   )
                   group by 
                   user_id
               )
                   as
                   list) 
               as known_count,
               (select count(*) from event_rsvps where event_id = e.id and attendee_type= 'interested' and is_active is true) as interested_count,
           (select array_to_string(array_agg(distinct "full_name"),' , ') as shared_by from events_shares es join users u on u.id = es.shared_by where es.user_id = ${user_id} and es.event_id = e.id) as shared_by
                from events e
                join users u on u.id =  e.created_by
                where e.is_public is true and e.is_active is true and e.is_cancel=false and e.id not in ( 
                    (select e.id from events e join users u on u."location" = e."location" where u.id = ${user_id}) union ( select es.event_id
                        from
                        events_shares es join users u on u.id = es.user_id where es.user_id = ${user_id} group by es.event_id)
            ) and e.to_date >= ${todayTimestamp} and e.created_by != ${user_id}  ${dateQuery} ${searchQuery} order by e.id desc`;
                return this.adapter.db.query(queryString)
                    .then(([res, metadata]) => res);
            }
        },
        event_invitations: {
            async handler(ctx) {
                let { user_id, txt, filter, from_date, to_date } = ctx.params;
                let todayTimestamp = moment().unix();
                let dateQuery = '';
                if (from_date && to_date) {
                    dateQuery = from_date ? `and e.from_date between ${from_date} and ${to_date}` : '';
                }
                else if (filter) {
                    let { start_date, end_date } = await ctx.broker.call("common_methods.date_filter", { filter: filter });
                    dateQuery = `and e.from_date between ${start_date} and ${end_date}`;
                }
                let searchQuery = txt ? `and e.event_name ilike '%${txt}%'` : '';
                let queryString = `select u.id as user_id, u.full_name, u.propic,  e.id as event_id,e.event_name,
                e.event_image,e.event_original_image,e.event_page,extract(epoch from e.created_at) as created_at_,e.rsvp,e.category,e.event_type,
                e.is_public as public_event,e.ticket_type,e.from_date,e.to_date,e."location",e.meeting_link,e.meeting_dial_number,e.meeting_pin,e.meeting_logo,
                (select count(*) from event_rsvps where event_id = e.id and attendee_type= 'interested' and is_active is true) as interested_count,
                (select count(*) from event_rsvps where attendee_type = 'going' and event_id = e.id and is_active is true) as going_count,
                (select u.full_name from event_rsvps er join users u on er.attendee_id = u.id where attendee_type = 'going' and event_id = e.id limit 1) as first_person_going,
                ei.invited_by
                from events_invitations ei 
                join events e 
                on e.id = ei.event_id
                join users u
                on u.id=e.created_by
                where e.is_active=true and e.is_cancel=false and user_id = ${user_id} and e.created_by != ${user_id}
                and e.to_date >= ${todayTimestamp}
                and ei.duplicate is false
                ${searchQuery} ${dateQuery} group by e.id,ei.invited_by, u.id order by e.created_at desc, e."location" asc`;
                return this.adapter.db.query(queryString)
                    .then(([res, metadata]) => res);
            }
        },
        based_on_rsvp: {
            async handler(ctx) {
                let { status, user_id, from_date, to_date, txt, filter } = ctx.params;
                let dateQuery = '';
                let searchQuery = txt && txt != '' ? `and (e.event_name ilike '%${txt}%' or e."location" ilike '%${txt}%')` : '';
                let todayTimestamp = moment().unix();
                let expire = ctx.params.expire ? ctx.params.expire : '';
                let expireQuery = '';
                if (expire == 0) {
                    expireQuery = `and e.to_date >= ${todayTimestamp}`;
                } else if (expire == 1) {
                    expireQuery = `and e.to_date < ${todayTimestamp}`;
                }
                //let searchQuery = txt&&txt!=''?`and u.full_name ilike '%${txt}%'`:'';
                if (from_date && to_date) {
                    dateQuery = from_date ? `and e.from_date between ${from_date} and ${to_date}` : '';
                }
                else if (filter) {
                    let { start_date, end_date } = await ctx.broker.call("common_methods.date_filter", { filter: filter });
                    dateQuery = `and e.from_date between ${start_date} and ${end_date}`;
                }
                let queryString = `select u.id as user_id,u.full_name,u.propic,e.id as event_id,
                extract(epoch from e.created_at) as created_at_,e.*
                from event_rsvps er 
                join events e 
                on e.id = er.event_id
                join users as u
                on u.id=e.created_by
                where er.attendee_type = '${status}' and er.attendee_id='${user_id}' ${dateQuery} ${searchQuery} ${expireQuery} order by e.from_date asc`;
                return this.adapter.db.query(queryString)
                    .then(([res, metadata]) => res);
            }
        },
        getEventbyID: {
            handler(ctx) {
                let { event_id } = ctx.params;
                let queryString = `select e.id as event_id,e.event_host,e.event_name,u.id,e.ticket_type,e.category,e.is_public,e.event_type,e.event_page,e.rsvp,e.allow_others,e.created_by,e.created_at,u.full_name as created_by_name,u.propic,
                e.from_date,e.to_date,e.event_image,e.event_original_image,e."location",e.description,e.is_sharable,e.allow_others,e.lat,e.long,e.is_active,e.is_cancel,e.meeting_link,e.meeting_dial_number,e.meeting_pin,e.meeting_logo,
                (select remind_on from reminders where event_id = e.id and is_active is true order by id desc limit 1) as remind_on,
                (select id from reminders where event_id = e.id and is_active is true order by id desc limit 1) as reminder_id
                from
                events e left join users u 
                on
                u.id = e.created_by 
                where e.id = ${event_id} and e.is_active is true and e.is_cancel=false`;
                return this.adapter.db.query(queryString)
                    .then(([res, metadata]) => res);
            }
        },
        getAllcounts: {
            handler(ctx) {
                let { event_id } = ctx.params;
                let queryString = `select case when (count+sum) is null then 0 else (count+sum) end as total_count from (select count(*) 
                from
                event_rsvps er join users u 
                on
                u.id = er.attendee_id
                where
                u.is_verified is true 
                and 
                u.is_active is true 
                and
                er.event_id = ${event_id}
                and
                er.attendee_type = 'going'
                )as count_interested,(select sum(others_count) 
                from
                event_rsvps er join users u 
                on
                u.id = er.attendee_id
                where
                u.is_verified is true 
                and
                u.is_active is true 
                and
                er.event_id = ${event_id}
                and
                er.attendee_type = 'going'
                ) as others_count`;
                return this.adapter.db.query(queryString)
                    .then(([res, metadata]) => res);
            }
        },
        /**
         * get from user  detail & group details for send
         * notification
         */
        invitationUsers: {
            handler(ctx) {
                let { from_id, group_id } = ctx.params;
                let queryString = `select usr.id as user_id, usr.full_name, usr.propic, gp.id as group_id, gp.group_name
                from users usr
                join groups gp
                on usr.id='${from_id}' and gp.id='${group_id}'`;
                return this.adapter.db.query(queryString)
                    .then(([res, metadata]) => res);
            }
        },
        eventListbyCreator: {
            params: {
                created_by: "string"
            },
            async handler(ctx) {
                let { created_by, txt, from_date, to_date, filter } = ctx.params;
                let dateQuery = '';
                if (from_date && to_date) {
                    dateQuery = from_date ? `and e.from_date between ${from_date} and ${to_date}` : '';
                }
                else if (filter) {
                    let { start_date, end_date } = await ctx.broker.call("common_methods.date_filter", { filter: filter });
                    dateQuery = `and e.from_date between ${start_date} and ${end_date}`;
                }

                let todayTimestamp = moment().unix();
                let expire = ctx.params.expire ? ctx.params.expire : '';
                let expireQuery = '';
                let orderQuery = `order by e.created_at desc`;
                if (expire == 0) {
                    expireQuery = `and e.to_date >= ${todayTimestamp}`;
                } else if (expire == 1) {
                    expireQuery = `and e.to_date < ${todayTimestamp}`;
                    orderQuery = `order by e.from_date desc`;
                }
                //let searchQuery = txt&&txt !=''?`and event_name ilike '%${txt}%'`:''
                let searchQuery = txt && txt != '' ? `and (e.event_name ilike '%${txt}%' or e."location" ilike '%${txt}%')` : '';

                let queryString = `select u.id as user_id,u.full_name,u.propic, e.id as event_id,e.meeting_link,e.meeting_dial_number,e.meeting_pin,e.meeting_logo,
                (select count(*) from event_rsvps where event_id = e.id and attendee_type= 'interested' and is_active is true) as interested_count,
                (select count(*) from event_rsvps where attendee_type = 'going' and event_id = e.id and is_active is true) as going_count,
                ( select u.full_name from event_rsvps er join users u on er.attendee_id = u.id where attendee_type = 'going' and event_id = e.id limit 1) as first_person_going ,
                e.*,extract(epoch from e.created_at) as created_at_,u.full_name as created_by_name
                from events as e
                join users as u
                on u.id = e.created_by
                where e.is_active=true and e.is_cancel=false and e.created_by = '${created_by}' ${searchQuery} ${dateQuery} ${expireQuery} ${orderQuery} 
                `;
                return this.adapter.db.query(queryString)
                    .then(([res, metadata]) => res);
            }
        },

        /**
         * fetch datat to send reminder notifivccations
         */
        reminderSend: {
            params: {},
            handler(ctx) {
                let { start, end } = ctx.params;
                let queryString = `SELECT e.id as event_id, e.event_name,e.location as event_location, e.event_image,e.from_date, e.to_date,e.created_by, 
                u.id as user_id, u.full_name, u.propic,u.email,u.phone,e.event_type,e.event_host,e.event_page,
                (select full_name as createdby from users where id=e.created_by )
                FROM reminders r
                join events e on e.id = r.event_id
                join users  u on u.id = r.user_id
                where r.remind_on > '${start}' and r.remind_on <= '${end}' and r.is_active is true`; 
                return this.adapter.db.query(queryString)
                    .then(([res, metadata]) => res);
            }
        },
        event_by_date: {
            handler(ctx) {
                let { from_date, to_date } = ctx.params;
                let dateQuery = ` e.from_date between ${from_date} and ${to_date}`;
                let query = `select * from events e where ${dateQuery}`;
                return this.adapter.db.query(query)
                    .then(([res, metadata]) => res);
            }
        },
        eventInterestedList: {
            handler(ctx) {
                let { event_id } = ctx.params;
                let query = `select e.attendee_id ,e.event_id,u.id as user_id ,u.full_name,u.propic from event_rsvps e join users u on u.id = e.attendee_id where  (e.attendee_type = 'going' or e.attendee_type = 'interested') and e.event_id = ${event_id} ;`;
                return this.adapter.db.query(query)
                    .then(([res, metadata]) => res);
            }
        },
        get_notification_event_data:{
            handler(ctx) {
                let { event_id } = ctx.params;
                let queryString = `select U.full_name as created_by_user,U.propic as created_by_propic,E.event_type,E.event_name,E."location",E.rsvp,E.event_image as cover_image,E.from_date 
                from events E 
                left join users U on U.id = E.created_by
                where E.id = ${event_id}`;
                return this.adapter.db.query(queryString)
                .then(([res, metadata]) => res);
            }   
        }
    },
    events: {},
    methods: {},
    created() { },
    started() { },
    stopped() { },
};