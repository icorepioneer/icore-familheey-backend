"use_strict";
//packages
const DbService = require('moleculer-db');
const SqlAdapter = require('moleculer-db-adapter-sequelize');
const Sequelize = require('sequelize');
//folder table model
module.exports = {
    name: "db.folders",
    mixins: [DbService],
    settings: {},
    dependencies: [],
    cache: false,
    adapter: new SqlAdapter(process.env.SQL_DATABASE,null,null, {
        logger: false,
        pool: {
            max: 1,
            min: 0,
            acquire: 30000,
            idle: 10000,
            handleDisconnects: true
        },
        dialect: 'postgres',
        port: 5432,
        replication: {
            read: [
                { host: process.env.SQL_READ_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
            ],
            write: { host: process.env.SQL_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
        },
    }),
    model: {
        name: "folders",
        define: {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            folder_name:{
                field:"folder_name",
                type:Sequelize.STRING
            },
            created_by:{
                field:"created_by",
                type:Sequelize.INTEGER,
                references:{
                    model:"users",
                    key:"id"
                }
            },
            group_id:{
                field:"group_id",
                type:Sequelize.INTEGER,
                references:{
                    model:"groups",
                    key:"id"
                }
            },
            type:{
                field:"type",
                type:Sequelize.ENUM,
                values:["public","private"],
                defaultValue:'public'
            },
            is_sharable:{
                field:"is_sharable",
                type:Sequelize.BOOLEAN,
                defaultValue:false
            },
            is_active:{
                field:"is_active",
                type:Sequelize.BOOLEAN,
                defaultValue:true
            },
            subfolder_of:{
                field:"subfolder_of",
                type:Sequelize.INTEGER,
                references:{
                    model:"folders",
                    key:"id"
                }
            },
            event_id:{
                field:"event_id",
                type:Sequelize.INTEGER,
                references:{
                    model:"events",
                    key:"id"
                }
            },
            folder_type:{
                field:"folder_type",
                type:Sequelize.STRING,
                defaultValue:"documents"
            },
            permissions:{
                field:"permissions",
                type:Sequelize.ENUM,
                values: ["only-me","selected","all","public","private"],
                defaultValue:'all'
            },
            folder_for:{
                field:"folder_for",
                type:Sequelize.ENUM,
                values: ["users", "groups", "events"]
            },
            description:{
                field:"description",
                type:Sequelize.TEXT
            },
            cover_pic:{
                field:"cover_pic",
                type:Sequelize.STRING
            },
            createdAt: {
                field: "created_at",
                type: 'TIMESTAMP',
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            },
            updatedAt: {
                field: "updated_at",
                type: 'TIMESTAMP',
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            },
        },
        options: {}
    },
    actions: {
        /**
         * list all the albums(folder) in user view
         */
        listUserFolders:{
            handler(ctx) {
                let { txt, profile_id, from_user, folder_for,folder_type } = ctx.params;
                let _filter = [];
                _filter.push('F1.is_active=true');
                if(folder_for){ _filter.push(`F1.folder_for = '${folder_for}'`); }
                if(profile_id){ _filter.push(`F1.created_by='${profile_id}'`); }
                if(txt)    { _filter.push(`F1.folder_name ilike '%${txt}%'`); }
                if(folder_type){ _filter.push(`F1.folder_type = '${folder_type}'`); }
                let _where = _filter.join(' and ');
                /*let _query = `SELECT f.*, u.full_name as created_by_name
                FROM folders as f
                JOIN users u
                ON u.id=f.created_by
                WHERE ${_where}`
                let _query = `SELECT * FROM (SELECT  extract(epoch from f.created_at) as created_at_,f.*, u.full_name as created_by_name,
                (select array_to_string(array_agg(distinct "user_id"),' , ') as permission_users 
                from document_maps dm  where dm.folder_id = f.id ) 
                FROM folders as f
                JOIN users u
                ON u.id=f.created_by
                WHERE ${_where})as finaldata 
                where finaldata.permissions='public' 
                OR ( finaldata.permissions='all'  )
                OR ( finaldata.permissions='selected' and finaldata.id in ( select folder_id from document_maps _dms where _dms.shared_from_user = ${user_id}  and _dms.group_id is null and _dms.event_id is null ))`*/
                let _query = `SELECT * FROM (SELECT  *, (select count(*) as doc_count from documents _DM where _Dm.folder_id=F1.id ), (SELECT count(*) FROM ((SELECT gm.group_id FROM group_maps gm WHERE gm.user_id = ${profile_id} and gm.is_blocked is false and gm.is_removed is false ORDER BY group_id )
                INTERSECT
                (SELECT gm.group_id FROM group_maps gm WHERE gm.user_id = ${from_user} and gm.is_blocked is false and gm.is_removed is false ORDER BY group_id)) AS count)
                FROM folders F1
                WHERE ${_where}) AS FD
                WHERE FD.permissions = 'all' OR (FD.permissions='only-me' and FD.created_by= ${from_user} ) OR (FD.permissions='private' AND FD.count > 0 )`;
                return this.adapter.db.query(_query)
                .then(([res, metadata]) => res)
                .catch(err => err);
            }
        },

        /** for events & groups */
        listFolders:{
            handler(ctx) {
                let { txt, group_id, folder_for,is_member, event_id,folder_type, user_id,folder_id } = ctx.params;
                let _filter = [];
                _filter.push(`f.is_active=true`);
                if(group_id){
                    _filter.push(`f.group_id='${group_id}'`); 
                }else if(event_id){
                    _filter.push(`f.event_id='${event_id}'`); 
                }
                if(folder_for){ _filter.push(`f.folder_for = '${folder_for}'`); }
                if(txt)    { _filter.push(`f.folder_name ilike '%${txt}%'`); }
                //if(!is_member){ _filter.push(`f.type='public'`) }
                if(folder_type){ _filter.push(`f.folder_type = '${folder_type}'`); }
                if(folder_id){ _filter.push(`f.id = '${folder_id}'`); }
                let _where = _filter.join(' and ');

                if(!is_member && folder_for=="events") is_member = true;

                let _query = `select * from (SELECT ${is_member} as is_member, extract(epoch from f.created_at) as created_at_,f.*, u.full_name as created_by_name,
                (select array_agg(distinct "user_id") as permission_users 
                from document_maps dm  where dm.folder_id = f.id ),
                (select count (*) from documents dm where dm.folder_id = f.id) as doc_count 
                FROM folders as f
                JOIN users u
                ON u.id=f.created_by
                WHERE ${_where})as finaldata 
                where finaldata.permissions='public' 
                OR ( finaldata.permissions='only-me' and finaldata.created_by= ${user_id})
                OR ( finaldata.permissions='all' and finaldata.is_member is true )
                OR ( finaldata.permissions='selected' and  finaldata.is_member is true and (finaldata.id in ( select folder_id from document_maps _dms where _dms.user_id = ${user_id} ) or finaldata.created_by = ${user_id})) order by finaldata.created_at desc`;
                return this.adapter.db.query(_query)
                .then(([res, metadata]) => res)
                .catch(err => err);
            }
        },
        /**
         * count of folders
         */
        listFoldersCount:{
            handler(ctx) {
                let { txt, group_id, folder_for,is_member, event_id,folder_type, user_id } = ctx.params;
                let _filter = [];
                _filter.push(`f.is_active=true`);
                if(group_id){
                    _filter.push(`f.group_id='${group_id}'`); 
                }else if(event_id){
                    _filter.push(`f.event_id='${event_id}'`); 
                }
                if(folder_for){ _filter.push(`f.folder_for = '${folder_for}'`); }
                if(txt)    { _filter.push(`f.folder_name ilike '%${txt}%'`); }
                //if(!is_member){ _filter.push(`f.type='public'`) }
                if(folder_type){ _filter.push(`f.folder_type = '${folder_type}'`); }
                let _where = _filter.join(' and ');

                if(!is_member && folder_for=="events") is_member = true;

                let _query = `select count(*) from (SELECT ${is_member} as is_member, extract(epoch from f.created_at) as created_at_,f.*, u.full_name as created_by_name,
                (select array_agg(distinct "user_id") as permission_users 
                from document_maps dm  where dm.folder_id = f.id ),
                (select count (*) from documents dm where dm.folder_id = f.id) as doc_count 
                FROM folders as f
                JOIN users u
                ON u.id=f.created_by
                WHERE ${_where})as finaldata 
                where finaldata.permissions='public' 
                OR ( finaldata.permissions='only-me' and finaldata.created_by= ${user_id})
                OR ( finaldata.permissions='all' and finaldata.is_member is true )
                OR ( finaldata.permissions='selected' and  finaldata.is_member is true and (finaldata.id in ( select folder_id from document_maps _dms where _dms.user_id = ${user_id} ) or finaldata.created_by = ${user_id}))`;
                return this.adapter.db.query(_query)
                .then(([res, metadata]) => res)
                .catch(err => err);
            }
        },

        permissionusers:{
            handler(ctx){
                let { folder_id } = ctx.params;
                let _query = `select array_agg(distinct "id") as _ids, array_agg(distinct "user_id") as pm_users from document_maps dm  where dm.folder_id = '${folder_id}' and dm.file_id is null`;
                return this.adapter.db.query(_query)
                .then(([res, metadata]) => res)
                .catch(err => err);

            }
        }


        
    },
    events: {},
    methods: {},
    created() { },
    started() { },
    stopped() { },
};
