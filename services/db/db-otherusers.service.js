"use_strict";
//packages
const DbService  = require('moleculer-db');
const SqlAdapter = require('moleculer-db-adapter-sequelize');
const Sequelize  = require('sequelize');

module.exports = {
    name: "db.other_users",
    mixins: [DbService],
    settings: {},
    dependencies: [],
    cache: false,
    adapter: new SqlAdapter(process.env.SQL_DATABASE,null,null, {
        logger: false,
        pool: {
            max: 1,
            min: 0,
            acquire: 30000,
            idle: 10000,
            handleDisconnects: true
        },
        dialect: 'postgres',
        port: 5432,
        replication: {
            read: [
                { host: process.env.SQL_READ_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
            ],
            write: { host: process.env.SQL_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
        },
    }),
    model: {
        name: "other_users",
        define: {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            full_name: {
                field: "full_name",
                type: Sequelize.STRING
            },
            email: {
                field: "email",
                type: Sequelize.STRING
            },
            phone: {
                field: "phone",
                type: Sequelize.STRING
            },
            event_id: {
                field: "event_id",
                type: Sequelize.INTEGER,
                /*references: {
                    model: "events",
                    key: 'id'
                },*/
                allowNull:true
            },
            group_id: {
                field: "group_id",
                type: Sequelize.INTEGER,
                /*references: {
                    model: "groups",
                    key: 'id'
                },*/
                allowNull:true
            },
            user_id: {
                field: "user_id",
                type: Sequelize.INTEGER,
                /*references: {
                    model: "users",
                    key: 'id'
                },*/
                allowNull:true
            },
            from_user: {
                field: "from_user",
                type: Sequelize.INTEGER,
                /*references: {
                    model: "users",
                    key: 'id'
                }*/
            },
            type: {
                field: "type",
                type: Sequelize.STRING
            },
            type_value: {
                field: "type_value",
                type: Sequelize.STRING
            },
            rsvp: {
                field: "rsvp",
                type: Sequelize.STRING
            },
            is_active: {
                field: "is_active",
                type: Sequelize.BOOLEAN,
                defaultValue:true
            },
            createdAt: {
                field: "created_at",
                type: 'TIMESTAMP',
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            },
            updatedAt: {
                field: "updated_at",
                type: 'TIMESTAMP',
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            },
        },
        options: {}
    },
    actions: {
        /**
         * get the list of invitation send
         */
        Update: {
            handler(ctx) {
                let { params, where } = ctx.params;
                return this.adapter.db.models.other_users.update(params,{ where:where })
                .then(([res, metadata]) => res);
            }
        },
        /**
         * other users invitation request status
         */
        otherReqStatus: {
            handler(ctx) {
                let { queryString,group_id } = ctx.params;
                let masterQuery = `select count(*) 
                from other_users os 
                where os.phone ilike any('{${queryString}}') and group_id=${group_id} and is_active=true`;
                return this.adapter.db.query(masterQuery)
                .then(([res, metadata]) => res);
            }
        }
    },
    events: {},
    methods: {},
    created() { },
    started() { },
    stopped() { },
};
