"use_strict";
//packages
const DbService = require('moleculer-db');
const SqlAdapter = require('moleculer-db-adapter-sequelize');
const Sequelize = require('sequelize');
//folder table model
module.exports = {
    name: "db.subscriptions",
    mixins: [DbService],
    settings: {},
    dependencies: [],
    cache: false,
    adapter: new SqlAdapter(process.env.SQL_DATABASE,null,null, {
        logger: false,
        pool: {
            max: 1,
            min: 0,
            acquire: 30000,
            idle: 10000,
            handleDisconnects: true
        },
        dialect: 'postgres',
        port: 5432,
        replication: {
            read: [
                { host: process.env.SQL_READ_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
            ],
            write: { host: process.env.SQL_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
        },
    }),
    model: {
        name: "subscriptions",
        define: {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            user_id:{
                field:"user_id",
                type:Sequelize.INTEGER
            },
            subscribed_date:{
                field:"subscribed_date",
                type: 'TIMESTAMP',
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            },
            subscribe_expiry:{
                field:"subscribe_expiry",
                type: 'TIMESTAMP',
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            },
            grace_period:{
                field:"grace_period",
                type:Sequelize.INTEGER,
                defaultValue:0
            },
            is_active:{
                field:"is_active",
                type:Sequelize.BOOLEAN,
                defaultValue:true
            },
            createdAt: {
                field: "created_at",
                type: 'TIMESTAMP',
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            },
            updatedAt: {
                field: "updated_at",
                type: 'TIMESTAMP',
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            }
        },
        options: {}
    },
    actions: {
        /**
         * subscription details of user 
         */
        findByUser:{
            handler(ctx) {
                let { user_id } = ctx.params;
                let masterQuery = `SELECT *,(sub.subscribe_expiry::date -NOW()::date)as difference, 
                (case when (sub.subscribe_expiry::date -NOW()::date <0) then true else false end )as isExpired
                from subscriptions sub
                where user_id=${user_id} order by sub.id desc limit 1`;
                return this.adapter.db.query(masterQuery)
                .then(([res, metadata]) => res);
            }
        },

    },
    events: {},
    methods: {},
    created() { },
    started() { },
    stopped() { },
}
