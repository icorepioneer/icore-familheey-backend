/* eslint-disable indent */
"use_strict";
//packages
const DbService = require('moleculer-db');
const SqlAdapter = require('moleculer-db-adapter-sequelize');
const Sequelize = require('sequelize');
const moment = require('moment');
//user table model
module.exports = {
    name: "db.events_signup_items",
    mixins: [DbService],
    settings: {},
    dependencies: [],
    cache: false,
    adapter: new SqlAdapter(process.env.SQL_DATABASE,null,null, {
        logger: false,
        pool: {
            max: 1,
            min: 0,
            acquire: 30000,
            idle: 10000,
            handleDisconnects: true
        },
        dialect: 'postgres',
        port: 5432,
        replication: {
            read: [
                { host: process.env.SQL_READ_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
            ],
            write: { host: process.env.SQL_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
        },
    }),
    model: {
        name: "events_signup_items",
        define: {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            event_id: {
                field:"event_id",
                type: Sequelize.INTEGER,
                // references:{
                //     model:"events",
                //     key:"id"
                // }
            },
            user_id: {
                field:"user_id",
                type: Sequelize.INTEGER,
                // references:{
                //     model:"users",
                //     key:"id"
                // }
            },
            slot_title: {
                field: "slot_title",
                type: Sequelize.STRING
            },
            item_quantity: {
                field: "item_quantity",
                type: Sequelize.INTEGER
            },
            slot_description: {
                field: "slot_description",
                type: Sequelize.TEXT
            },
            start_date: {
                field: "start_date",
                type: Sequelize.DATE
            },
            end_date: {
                field: "end_date",
                type: Sequelize.DATE
            },
            is_active:{
                field:"is_active",
                type:Sequelize.BOOLEAN,
                defaultValue: true
            }, 
            is_date_only:{
                field:"is_date_only",
                type:Sequelize.BOOLEAN,
                defaultValue: false
            }, 
            createdAt: {
                field: "created_at",
                type: "TIMESTAMP",
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            },
            updatedAt: {
                field: "updated_at",
                type: "TIMESTAMP",
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            }
        },
        options: {}
    },
    
    actions: {
        getItemsContribution:{
            handler(ctx){
                let { event_id, item_id } = ctx.params;
                let _filter = [];
                if(event_id) _filter.push(`s.event_id =${event_id}`);
                if(item_id) _filter.push(`c.contribute_items_id =${item_id}`);
                let _where = _filter.join(" and ");
                let queryString = `select u.full_name,u.propic,c.id as contr_id,c.contribute_user_id,c.updated_at as updated_on,s.*,case when c.contribute_item_quantity is null then 0 else c.contribute_item_quantity end as quantity_collected,
                COALESCE((select s.item_quantity-sum(es2.contribute_item_quantity) 
                from events_signup_contributions es2
                where es2.contribute_items_id = c.contribute_items_id), s.item_quantity) as needed
                from events_signup_items s 
                left join 
                events_signup_contributions c
                on
                c.event_id = s.event_id 
                and 
                c.contribute_items_id = s.id 
                left join users u
                on u.id = c.contribute_user_id
                where ${_where}`;
                return this.adapter.db.query(queryString)
                .then(([res, metadata]) => res); 
            }
        },
        getEventItems:{
            handler(ctx){
                let { event_id } = ctx.params;
                let queryString = `select distinct(es.id),es.id,es.event_id,es.user_id,es.slot_title,es.item_quantity,
                es.slot_description,es.start_date,es.end_date,es.is_active,es.created_at,
                (select sum(case when contribute_item_quantity is null then 0 else contribute_item_quantity end) 
                as quantity_collected from events_signup_contributions where contribute_items_id = es.id) as quantity_collected,
                COALESCE((select es.item_quantity-sum(es2.contribute_item_quantity) 
                    from events_signup_contributions es2
                    where es2.contribute_items_id = esc.contribute_items_id), es.item_quantity) as needed
                from events_signup_items es 
                left join events_signup_contributions esc on esc.contribute_items_id = es.id 
                where es.event_id = ${event_id} and es.is_active is true`;
                return this.adapter.db.query(queryString);
            }   
        }
    },
    events: {},
    methods: {},
    created() { },
    started() { },
    stopped() { },
};
