"use_strict";
//packages
const DbService = require('moleculer-db');
const SqlAdapter = require('moleculer-db-adapter-sequelize');
const Sequelize = require('sequelize');
//folder table model
module.exports = {
    name: "db.events_signup_contributions",
    mixins: [DbService],
    settings: {},
    dependencies: [],
    cache: false,
    adapter: new SqlAdapter(process.env.SQL_DATABASE,null,null, {
        logger: false,
        pool: {
            max: 1,
            min: 0,
            acquire: 30000,
            idle: 10000,
            handleDisconnects: true
        },
        dialect: 'postgres',
        port: 5432,
        replication: {
            read: [
                { host: process.env.SQL_READ_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
            ],
            write: { host: process.env.SQL_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
        },
    }),
    model: {
        name: "events_signup_contributions",
        define: {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            event_id: {
                field: "event_id",
                type: Sequelize.INTEGER,
                reference: {
                    model: "events",
                    key: 'id'
                }
            },
            contribute_user_id: {
                field: "contribute_user_id",
                type: Sequelize.INTEGER,
                reference: {
                    model: "users",
                    key: 'id'
                }
            },
            contribute_items_id: {
                field: "contribute_items_id",
                type: Sequelize.INTEGER,
                reference: {
                    model: "events_signup_items",
                    key: 'id'
                }
            },
            contribute_item_quantity: {
                field: "contribute_item_quantity",
                type: Sequelize.INTEGER
            },
            is_active:{
                field:"is_active",
                type:Sequelize.BOOLEAN,
                defaultValue:true
            },
            createdAt: {
                field: "created_at",
                type: 'TIMESTAMP',
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            },
            updatedAt: {
                field: "updated_at",
                type: 'TIMESTAMP',
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            },
        },
        options: {}
    },
    actions: {},
    events: {},
    methods: {},
    created() { },
    started() { },
    stopped() { },
};
