"use strict"

const DbService = require('moleculer-db');
const SqlAdapter = require('moleculer-db-adapter-sequelize');
const Sequelize = require('sequelize');
const Cuid = require('cuid');

module.exports = {
    name: "db.user_history",
    mixins: [DbService],
    settings: {},
    dependencies: [],
    cache: false,
    adapter: new SqlAdapter(process.env.SQL_DATABASE,null,null, {
        logger: false,
        pool: {
            max: 1,
            min: 0,
            acquire: 30000,
            idle: 10000,
            handleDisconnects: true
        },
        dialect: 'postgres',
        port: 5432,
        replication: {
            read: [
                { host: process.env.SQL_READ_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
            ],
            write: { host: process.env.SQL_HOST, username: process.env.SQL_USERNAME, password: process.env.SQL_PASSWORD }
        },
    }),
    model: {
        name: "user_history",
        define: {
            id: {
                primaryKey: true,
                type: Sequelize.INTEGER,
                autoIncrement: true
            },
            user_id: {
                field: "user_id",
                type: Sequelize.INTEGER,
                references: {
                    model: "users",
                    key: 'id'
                }
            },
            login_time: {
                field: "login_time",
                type: "TIMESTAMP",
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            },

            login_ip: {
                type: Sequelize.STRING,
                field: "login_ip"
            },
            login_location: {
                type: Sequelize.STRING,
                field: "login_location"
            },
            login_browser: {
                type: Sequelize.STRING,
                field: "login_browser"
            },
            login_country: {
                type: Sequelize.STRING,
                field: "login_country"
            },
            login_type: {
                type: Sequelize.STRING,
                field: "login_type"
            },
            login_device: {
                type: Sequelize.STRING,
                field: "login_device"
            },
            app_version: {
                type: Sequelize.STRING,
                field: "app_version"
            },
            createdAt: {
                field: "created_at",
                type: 'TIMESTAMP',
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            },
            updatedAt: {
                field: "updated_at",
                type: 'TIMESTAMP',
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
            }
        }
    },
    actions: {},
    events: {},
    methods: {},
    created() { },
    started() { },
    stopped() { }
}